\version "2.19.80"
\include "common.ily"

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Betulia Liberata"
  }
  \markup \null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #8
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \include "livret/livret.ily"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-0
  \pieceTocNb "1-0" "Overtura"
  \includeScore "AAovertura"
}
\bookpart {
  \act "Parte Prima"
  %% 1-1
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia: \italic { Popoli di Betulia, ah qual v’ingombra }
  }
  \includeScore "ABrecitativo"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-2
  \pieceToc\markup\wordwrap {
    Aria. Ozia: \italic { D’ogni colpa la colpa maggiore }
  }
  \includeScore "ACaria"
}
\bookpart {
  %% 1-3
  \pieceToc\markup\wordwrap {
    Recitativo. Cabri, Amital: \italic { E in che sperar? }
  }
  \includeScore "ADrecitativo"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 1-4
  \pieceToc\markup\wordwrap {
    Aria. Cabri: \italic { Ma qual virtù non cede }
  }
  \includeScore "AEaria"
}
\bookpart {
  %% 1-5
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia, Cabri, Amital: \italic { Già le memorie antiche }
  }
  \includeScore "AFrecitativo"
}
\bookpart {
  %% 1-6
  \pieceToc\markup\wordwrap {
    Aria. Amital: \italic { Non hai cor, se in mezzo a questi }
  }
  \includeScore "AGaria"
}
\bookpart {
  %% 1-7
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia, Amital, Coro: \italic { E qual pace sperate }
  }
  \includeScore "AHrecitativo"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-8
  \pieceToc\markup\wordwrap {
    Aria con Coro. Ozia, Coro: \italic { Pietà, se irato sei }
  }
  \includeScore "AIcoro"
}
\bookpart {
  %% 1-9
  \pieceToc\markup\wordwrap {
    Recitativo. Cabri, Amital, Ozia, Giuditta:
    \italic { Chi è costei, che qual sorgente aurora }
  }
  \includeScore "AJArecitativo"
}
\bookpart {
  \paper { systems-per-page = 3 }
  \includeScore "AJBrecitativo"
}
\bookpart {
  %% 1-10
  \pieceToc\markup\wordwrap {
    Aria. Giuditta: \italic { Del pari infeconda }
  }
  \includeScore "AKaria"
}
\bookpart {
  %% 1-11
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia, Cabri, Giuditta:
    \italic { Oh saggia, oh santa, oh eccelsa donna! }
  }
  \includeScore "ALrecitativo"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-12
  \pieceToc\markup\wordwrap {
    Aria con Coro. Ozia, Coro: \italic { Pietà, se irato sei }
  }
  \reIncludeScore "AIcoro" "AMcoro"
}
\bookpart {
  %% 1-13
  \pieceToc\markup\wordwrap {
    Recitativo. \italic { Oh saggia, oh santa, oh eccelsa donna! }
  }
  \includeScore "ANrecitativo"
}
\bookpart {
  %% 1-14
  \pieceToc\markup\wordwrap {
    Aria. Achior: \italic { Terribile d’aspetto }
  }
  \includeScore "AOaria"
}
\bookpart {
  %% 1-15
  \pieceToc\markup\wordwrap {
    Recitativo. \italic { Ti consola, Achior }
  }
  \includeScore "APrecitativo"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-16
  \pieceToc\markup\wordwrap {
    Aria. Giuditta: \italic { Parto inerme, e non pavento }
  }
  \includeScore "AQaria"
}
\bookpart {
  \paper { systems-per-page = 2 }
  %% 1-17
  \pieceToc\markup\wordwrap {
    Coro: \italic { Oh prodigio! Oh stupor! }
  }
  \includeScore "ARcoro"
}

\bookpart {
  \act "Parte Seconda"
  %% 2-1
  \pieceToc\markup\wordwrap {
    Recitativo. Achior, Ozia: \italic { Troppo mal corrisponde }
  }
  \includeScore "BArecitativo"
}
\bookpart {
  %% 2-2
  \pieceToc\markup\wordwrap {
    Aria. Ozia: \italic { Se Dio veder tu vuoi }
  }
  \includeScore "BBaria"
}
\bookpart {
  %% 2-3
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia, Achior, Amital: \italic { Confuso io son }
  }
  \includeScore "BCrecitativo"
}
\bookpart {
  %% 2-4
  \pieceToc\markup\wordwrap {
    Aria. Amital: \italic { Quel nocchier che in gran procella }
  }
  \includeScore "BDaria"
}
\bookpart {
  %% 2-5
  \pieceToc\markup\wordwrap {
    Recitativo. \italic { Lungamente non dura }
  }
  \includeScore "BEArecitativo"
  \includeScore "BEBrecitativo"
}
\bookpart {
  %% 2-6
  \pieceToc\markup\wordwrap {
    Aria. Giuditta: \italic { Prigionier che fa ritorno }
  }
  \includeScore "BFaria"
}
\bookpart {
  %% 2-7
  \pieceToc\markup\wordwrap {
    Recitativo. Achior: \italic { Giuditta, Ozìa, popoli, amici: io cedo }
  }
  \includeScore "BGrecitativo"
}
\bookpart {
  \paper { systems-per-page = 3 }
  %% 2-8
  \pieceToc\markup\wordwrap {
    Aria. Achior: \italic { Te solo adoro }
  }
  \includeScore "BHaria"
}
\bookpart {
  %% 2-9
  \pieceToc\markup\wordwrap {
    Recitativo. Ozia, Amital: \italic { Di tua vittoria un glorioso effetto }
  }
  \includeScore "BIrecitativo"
  %% 2-10
  \pieceToc\markup\wordwrap {
    Aria. Amital: \italic { Con troppa rea viltà }
  }
  \includeScore "BJaria"
}
\bookpart {
  %% 2-11
  \pieceToc\markup\wordwrap {
    Recitativo. \italic { Quanta cura hai di noi, Bontà Divina! }
  }
  \includeScore "BKrecitativo"
}
\bookpart {
  %% 2-12
  \pieceToc\markup\wordwrap {
    Aria. Carmi: \italic { Quei moti che senti }
  }
  \includeScore "BLaria"
}
\bookpart {
  %% 2-13
  \pieceToc\markup\wordwrap {
    Recitativo. \italic { Seguansi, o Carmi, i fuggitivi }
  }
  \includeScore "BMrecitativo"
}
\bookpart {
  %% 2-14
  \pieceToc\markup\wordwrap {
    Coro. \italic { Lodi al gran Dio che oppresse }
  }
  \includeScore "BNcoro"
}
