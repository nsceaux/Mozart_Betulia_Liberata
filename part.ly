\version "2.19.80"
\include "common.ily"

\paper {
  %% plus d'espace entre titre et musique
  markup-system-spacing.padding = #2.5 % default: 0.5
  system-system-spacing.padding = #2.5 % default: 1
  %% de la place entre dernier system et copyright
  last-bottom-spacing.padding = #3
}

%% pour avoir la place de noter les coups d'archet :
\layout {
  %% plus d'espace entre les paroles et la ligne de l'instrument
  \context {
    \Lyrics
    \override VerticalAxisGroup.nonstaff-unrelatedstaff-spacing.minimum-distance = #8
  }
  %% Plus d'espace entre les indications de tempo et la portée
  \context {
    \Score
    \override MetronomeMark.padding = #2.5 % default 0.8
  }
  %% Plus d'espace entre les notes et les nuances
  \context {
    \Voice
    \override DynamicText.Y-offset = #(scale-by-font-size -3) % default -0.6
    \override Hairpin.Y-offset = #(scale-by-font-size -3)
    \override DynamicTextSpanner.Y-offset = #(scale-by-font-size -2.5)
  }
}

%% Title page
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \header {
    title = "Betulia Liberata"
  }
  \markup\null
}
%% Table of contents
\bookpart {
  \paper { #(define page-breaking ly:minimal-breaking) }
  \markuplist
  \abs-fontsize-lines #7
  \override-lines #'(use-rehearsal-numbers . #t)
  \override-lines #'(column-number . 2)
  \table-of-contents
}

%% 1-0
\pieceTocNb "1-0" "Overtura"
\includeScore "AAovertura"

\act "Parte Prima"
%% 1-1
\pieceToc\markup\wordwrap {
  Recitativo. Ozia: \italic { Popoli di Betulia, ah qual v’ingombra }
}
\includeScore "ABrecitativo"
%% 1-2
\pieceToc\markup\wordwrap {
  Aria. Ozia: \italic { D’ogni colpa la colpa maggiore }
}
\includeScore "ACaria"
%% 1-3
\pieceToc\markup\wordwrap {
  Recitativo. Cabri, Amital: \italic { E in che sperar? }
}
\includeScore "ADrecitativo"
%% 1-4
\pieceToc\markup\wordwrap {
  Aria. Cabri: \italic { Ma qual virtù non cede }
}
\includeScore "AEaria"
%% 1-5
\pieceToc\markup\wordwrap {
  Recitativo. Ozia, Cabri, Amital: \italic { Già le memorie antiche }
}
\includeScore "AFrecitativo"
%% 1-6
\pieceToc\markup\wordwrap {
  Aria. Amital: \italic { Non hai cor, se in mezzo a questi }
}
\includeScore "AGaria"
%% 1-7
\pieceToc\markup\wordwrap {
  Recitativo. Ozia, Amital, Coro: \italic { E qual pace sperate }
}
\includeScore "AHrecitativo"
%% 1-8
\pieceToc\markup\wordwrap {
  Aria con Coro. Ozia, Coro: \italic { Pietà, se irato sei }
}
\includeScore "AIcoro"
%% 1-9
\pieceToc\markup\wordwrap {
  Recitativo. Cabri, Amital, Ozia, Giuditta:
  \italic { Chi è costei, che qual sorgente aurora }
}
\includeScore "AJArecitativo"
\includeScore "AJBrecitativo"
\newBookPart#'(flauti)
%% 1-10
\pieceToc\markup\wordwrap {
  Aria. Giuditta: \italic { Del pari infeconda }
}
\includeScore "AKaria"
%% 1-11
\pieceToc\markup\wordwrap {
  Recitativo. Ozia, Cabri, Giuditta:
  \italic { Oh saggia, oh santa, oh eccelsa donna! }
}
\includeScore "ALrecitativo"
%% 1-12
\pieceToc\markup\wordwrap {
  Aria con Coro. Ozia, Coro: \italic { Pietà, se irato sei }
}
\reIncludeScore "AIcoro" "AMcoro"
%% 1-13
\pieceToc\markup\wordwrap {
  Recitativo. \italic { Oh saggia, oh santa, oh eccelsa donna! }
}
\includeScore "ANrecitativo"
%% 1-14
\pieceToc\markup\wordwrap {
  Aria. Achior: \italic { Terribile d’aspetto }
}
\includeScore "AOaria"
%% 1-15
\pieceToc\markup\wordwrap {
  Recitativo. \italic { Ti consola, Achior }
}
\includeScore "APrecitativo"
%% 1-16
\pieceToc\markup\wordwrap {
  Aria. Giuditta: \italic { Parto inerme, e non pavento }
}
\includeScore "AQaria"
%% 1-17
\pieceToc\markup\wordwrap {
  Coro: \italic { Oh prodigio! Oh stupor! }
}
\includeScore "ARcoro"

\newBookPart #'()
\act "Parte Seconda"
%% 2-1
\pieceToc\markup\wordwrap {
  Recitativo. Achior, Ozia: \italic { Troppo mal corrisponde }
}
\includeScore "BArecitativo"
%% 2-2
\pieceToc\markup\wordwrap {
  Aria. Ozia: \italic { Se Dio veder tu vuoi }
}
\includeScore "BBaria"
%% 2-3
\pieceToc\markup\wordwrap {
  Recitativo. Ozia, Achior, Amital: \italic { Confuso io son }
}
\includeScore "BCrecitativo"
%% 2-4
\pieceToc\markup\wordwrap {
  Aria. Amital: \italic { Quel nocchier che in gran procella }
}
\includeScore "BDaria"
%% 2-5
\pieceToc\markup\wordwrap {
  Recitativo. \italic { Lungamente non dura }
}
\includeScore "BEArecitativo" \noPageTurn
\includeScore "BEBrecitativo"
%% 2-6
\pieceToc\markup\wordwrap {
  Aria. Giuditta: \italic { Prigionier che fa ritorno }
}
\includeScore "BFaria"
%% 2-7
\pieceToc\markup\wordwrap {
  Recitativo. Achior: \italic { Giuditta, Ozìa, popoli, amici: io cedo }
}
\includeScore "BGrecitativo"
%% 2-8
\pieceToc\markup\wordwrap {
  Aria. Achior: \italic { Te solo adoro }
}
\includeScore "BHaria"
%% 2-9
\pieceToc\markup\wordwrap {
  Recitativo. Ozia, Amital: \italic { Di tua vittoria un glorioso effetto }
}
\includeScore "BIrecitativo"
%% 2-10
\pieceToc\markup\wordwrap {
  Aria. Amital: \italic { Con troppa rea viltà }
}
\includeScore "BJaria"
%% 2-11
\pieceToc\markup\wordwrap {
  Recitativo. \italic { Quanta cura hai di noi, Bontà Divina! }
}
\includeScore "BKrecitativo"
%% 2-12
\pieceToc\markup\wordwrap {
  Aria. Carmi: \italic { Quei moti che senti }
}
\includeScore "BLaria"
%% 2-13
\pieceToc\markup\wordwrap {
  Recitativo. \italic { Seguansi, o Carmi, i fuggitivi }
}
\includeScore "BMrecitativo"
%% 2-14
\pieceToc\markup\wordwrap {
  Coro. \italic { Lodi al gran Dio che oppresse }
}
\includeScore "BNcoro"
