\clef "soprano/treble" R2. |
r4 r r8 do'' |
fa'4 fa'4. lab'8 |
sol'4 sol' r8 sol' |
sol'4. sol'8 sib'4 |
la' la' r8 la' |
sib'[ la'] sib'[ do''] reb''[ mi''] |
fa''4 fa' r |
r r r8 do' |
lab'4. lab'8 solb'4 |
fa' fa' r8 fa' |
fa'4 fa''4. mib''8 |
reb''4 reb'' r8 reb'' |
reb''4 sib' sol' |
dob'' lab' mib' |
dob'' sib' lab' |
sol' r r8 mib' |
reb''![ sib'] reb''[ sib'] reb''[ sib'] |
mib''[ do''!] lab'4 r8 lab' |
fab'4 mib' re' |
mib' mib' r8 mib' |
reb''[ sib'] reb''[ sib'] reb''[ sib'] |
fab''4 sol' r8 sol' |
sib'[ sol'] sib'[ sol'] sib'[ sol'] |
reb''4 fab' r8 fab' |
mib'[ fa'!] sol'[ lab'] sib'[ do''] |
reb''4 fa'' r8 mib'' |
reb''4 do'' sib' |
mib''2 do''8[ lab'] |
fa''2 reb''8 sib' |
mib'2. |
sib'2\startTrillSpan~ sib'8.\stopTrillSpan lab'16 |
lab'4 r r |
R2.*7 |
r4 r r8 sol' |
do''4. do''8 do''4 |
do'' si' r8 re'' |
re''[ si'] sol'4. fa''8 |
mib''4 do'' r8 do'' |
lab'[ fa''] mib''[ re''] do''[ si'] |
do''4 do'' r8 do'' |
reb''!4. do''8 sib'4 |
\grace lab'8 sol'4 sol' r8 sol' |
reb''4 do'' sib' |
la' r la' |
sib' reb'' fa'' |
solb' do'' mib'' |
reb''8[ solb''] fa''[ mib''] reb''[ do''] |
fa''4 r reb'' |
do''8[ solb''] fa''[ mib''] reb''[ do''] |
sib'4 r r |
R2.*4 |
r4 r r8 do'' |
fa'4 fa' r8 lab' |
sol'4 sol' r8 sol' |
sol'4. sol'8 sib'4 |
la'4 la' r8 la' |
sib'[ la'] sib'[ do''] reb''[ mi''] |
fa''4 fa' r |
r r r8 do' |
solb'4. solb'8 solb'4 |
fa' fa' r8 fa' |
fa'4 reb'' fa' |
mi' mi' r8 reb'' |
do''4. fa''8 lab'4 |
sib' reb'' r8 sol' |
lab'4 sol' fa' |
do''4 r r8 do'' |
reb''[ do''] reb''[ do''] reb''[ do''] |
si'4 do'' r8 do'' |
lab'4 fa' reb' |
do'4 do' r8 do'' |
reb''[ do''] si'[ do''] sib'[ do''] |
la'4 sib' r8 reb'' |
mib''[ reb''] do''[ reb''] do''[ reb''] |
si'4 do'' r8 do'' |
sib'![ lab'] sol'[ fa'] mi'[ reb'] |
do'4 reb'' r8 do'' |
sib'4 lab' sol' |
solb' r4\fermata r8 solb' |
fa'8[ fa''] mi''[ fa''] mi''[ fa''] |
fa''4 do'' r8 do'' |
sib'4 lab' sol' |
fa''2 do''4 |
reb'' mi'' fa'' |
do'2. |
sol'2\startTrillSpan~ sol'8.\stopTrillSpan fa'16 |
fa'4 r r |
R2.*8 |
