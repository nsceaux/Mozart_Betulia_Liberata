\clef "alto" fa16\p fa fa fa fa2:16 |
sol2.:16 |
fa:16 |
sol:16 |
sol:16 |
la:16 |
sib4 fa'2:16\f |
fa'4 r8 fa' mib'! reb' |
do' do' do' do' do' do' |
do'\p do' do' do' do' do' |
reb' reb' reb' reb' reb' reb' |
la la la la la la |
sib sib sib sib sib sib |
mib' mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' |
dob'2.:16 |
sib:16\f |
mib'8\p mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' |
fab'4 mib' re' |
mib'8\f mib' mib' mib' mib' mib' |
mib'\p mib' mib' mib' mib' mib' |
reb' reb' sol' sib' sol' mib' |
reb' reb' reb' reb' reb' reb' |
sol2.:16 |
lab4 r r |
sol'8 sol' sol' sol' lab' lab' |
reb' reb' mib' mib' mib' mib' |
do' do' do' do' do' do' |
reb' reb' reb' reb' reb' reb' |
mib' mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' |
lab'2.:16\f |
sib':16 |
lab'2.:16 |
sib':16 |
lab'8 lab' lab' lab' lab' lab' |
reb' reb' mib' mib' mib' mib' |
lab'4-. do''-. fa'-. |
lab'-. re'-. fa'-. |
si8-. lab'-. sol'-. fa'-. mib'-. re'-. |
do'16 sol\p sol sol sol2:16 |
sol2.:16 |
sol:16 |
sol2:16 mib4:16 |
fa4 sol sol |
sol2.:16 |
sol2.:16 |
sol2.:16 |
sol2:16 sib4:16 |
la2.:16 |
sib4 reb!8 reb reb reb |
mib mib mib mib mib mib |
fa fa fa fa fa fa |
reb reb reb reb reb reb |
mib mib fa fa fa fa |
sib4\f reb'' r8 sib' |
sol'4 do'' r8 lab' |
fa'4 reb' si |
do' reb'' r8 mi' |
lab'4 si si |
do'8 do'' sib'! sol' reb'' mi' |
fa'16\p fa fa fa fa2:16 |
sol2.:16 |
sol:16 |
la:16 |
fa4 fa'2:16\f |
fa'4 r8 fa' mib'! reb' |
do' do' do' do' do' do' |
solb2.:16\p |
fa:16 |
fa:16 |
mi:16 |
fa16 lab lab lab lab2:16 |
reb'2:16 do'4:16 |
do'8 fa mib4 reb |
do r r |
do2.:16\p |
do:16 |
lab4 fa reb |
do2.:16\f |
do:16\p |
fa4-. fa-. r |
R2. |
sol4-. sol-. r |
R2. |
r4 sol' do'8 fa' |
r8 reb' do'4 do' |
la\f r\fermata r |
R2. |
lab8\p lab lab lab lab lab |
sib sib do' do' do' do' |
lab lab lab lab lab lab |
sib sib sib sib sib sib |
do' do' do' do' do' do' |
do' do' do' do' do' do' |
fa'2.:16\f |
sol':16 |
fa':16 |
sol':16 |
fa'8 fa' sol' sol' lab' lab' |
sib'4 do'' do' |
fa' fa''-. do''-. |
reb''-. sib'-. do''-. |
fa'-. r r |
