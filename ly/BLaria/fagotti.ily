\clef "bass" R2.*2 |
<>\p <<
  \tag #'(fagotto1 fagotti) { fa2.~ | fa~ | fa~ | fa~ | fa~ | fa4 }
  \tag #'(fagotto2 fagotti) { fa,2.~ | fa,~ | fa,~ | fa,~ | fa,~ | fa,4 }
  { s2.*4 | s2.\f }
>> r4 r |
\twoVoices #'(fagotto1 fagotto2 fagotti) << do2. do >> |
<>\p <<
  \tag #'(fagotto1 fagotti) { lab2.~ | lab | fa~ | fa4 }
  \tag #'(fagotto2 fagotti) { do2. | reb | \tag #'fagotti \once\slurDown la,( | sib,4) }
>> r4 r |
R2.*19 |
<>\f <<
  \tag #'(fagotto1 fagotti) {
    lab2. |
    sol |
    lab |
    sol |
    lab4 sib do' |
  }
  \tag #'(fagotto2 fagotti) {
    lab,2. |
    sol, |
    lab, |
    sol, |
    lab,4 sib, do |
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { reb'4 mib' mib | }
  { reb4 mib2 | }
>>
\tag #'fagotti <>^"a 2." lab4-. do'-. fa-. |
lab-. re-. fa-. |
si,8 lab-. sol-. fa-. mib-. re-. |
do4 r r |
R2.*5 |
<>\p <<
  \tag #'(fagotto1 fagotti) { sol2. | sib | reb'! | do'4 }
  \tag #'(fagotto2 fagotti) { mi2. | sol | sib | la4 }
>> r4 r |
R2.*8 |
<>\f <<
  \tag #'(fagotto1 fagotti) { mi'2. | fa' | mi'8 }
  \tag #'(fagotto2 fagotti) { sol2. | lab | sol8 }
  { s2. s\f }
>> r8 r4 r |
R2. |
<>\p <<
  \tag #'(fagotto1 fagotti) { fa2.~ | fa~ | fa~ | fa | fa4 }
  \tag #'(fagotto2 fagotti) { fa,2.~ | fa,~ | fa,~ | fa, | fa,4 }
  { s2.*3 s2.\f }
>> r4 r |
\twoVoices #'(fagotto1 fagotto2 fagotti) << do2. do >>
R2.*7 |
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) << do2. do >>
<>\p <<
  \tag #'(fagotto1 fagotti) { sol2.~ | sol }
  \tag #'(fagotto2 fagotti) { mi~ | mi }
>>
R2. |
<>\f \twoVoices #'(fagotto1 fagotto2 fagotti) << do2. do >> |
R2.*7 |
R2.^\fermataMarkup |
R2.*7 |
<>\f <<
  \tag #'(fagotto1 fagotti) {
    fa2. |
    mi |
    fa |
    mi |
    fa4 sol lab |
  }
  \tag #'(fagotto2 fagotti) {
    fa,2. |
    mi, |
    fa, |
    mi, |
    fa,4 sol, lab, |
  }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib4 do' do | }
  { sib,4 do2 | }
>>
\tag#'fagotti <>^"a 2." fa4 fa'-. do'-. |
reb'-. sib-. do'-. |
fa-. r r |
