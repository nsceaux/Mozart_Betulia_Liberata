Quei mo -- ti che sen -- ti
per l’or -- ri -- da not -- te,
per l’or -- ri -- da not -- te,
son que -- ru -- li ac -- cen -- ti,
son gri -- da in -- ter -- rot -- te,
che de -- sta lon -- ta -- no
l’in -- sa -- no ter -- ror,
quei mo -- ti che sen -- ti
per l’or -- ri -- da not -- te,
son que -- ru -- li ac -- cen -- ti,
son gri -- da in -- ter -- rot -- te,
che de -- sta lon -- ta -- no
l’in -- sa -- no ter -- ror,
l’in -- sa -- no,
l’in -- sa -- no __ ter -- ror.

Per vin -- ce -- re, a no -- i
non re -- stan ne -- mi -- ci,
non re -- stan ne -- mi -- ci,
del fer -- ro gli uf -- fi -- zi
com -- pi -- sce il ti -- mor,
del fer -- ro gli uf -- fi -- zi
com -- pi -- sce il ti -- mor,
com -- pi -- sce il ti -- mor.

Quei mo -- ti che sen -- ti
per l’or -- ri -- da not -- te,
per l’or -- ri -- da not -- te,
son que -- ru -- li ac -- cen -- ti,
son gri -- da in -- ter -- rot -- te,
che de -- sta lon -- ta -- no
l’in -- sa -- no ter -- ror,
quei mo -- ti che sen -- ti
per l’or -- ri -- da not -- te,
son que -- ru -- li ac -- cen -- ti,
son gri -- da in -- ter -- rot -- te,
che de -- sta lon -- ta -- no
l’in -- sa -- no ter -- ror,
che de -- sta lon -- ta -- no
l’in -- sa -- no ter -- ror,
l’in -- sa -- no,
l’in -- sa -- no __ ter -- ror,
