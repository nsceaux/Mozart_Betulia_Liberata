\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (corni #:score-template "score-corni"
          #:tag-global () #:instrument "Corni in F.")
   (fagotti #:music , #{
\quoteBasso "BLbasso"
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2.*2 }
s2.*11
\cue "BLbasso" {
  \mmRestDown <>_\markup\tiny "Basso" s2.*10
  \mmRestUp s2.*2
  \mmRestDown s2.*7
}
s2.*10
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2.*5 }
s2.*4
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2.*8 }
s2.*3
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2. }
s2.*6
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2.*7 }
s2.*3
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2. \mmRestCenter }
s2.*2
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2. \mmRestCenter }
s2.
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2. \mmRestCenter }
s2.
\cue "BLbasso" { \mmRestDown <>_\markup\tiny "Basso" s2.*10 }
            #}))
