\clef "treble" \transposition fa
<>\p \twoVoices #'(corno1 corno2 corni) << do'2. do' >> |
<<
  \tag #'(corno1 corni) { sol'2. }
  \tag #'(corno2 corni) { sol }
>>
\twoVoices #'(corno1 corno2 corni) << do'8 do' >> r r4 r |
R2.*3 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { do'2.~ | do'8 }
  { do'2.~ | do'8 }
>> r8 r4 r |
<<
  \tag #'(corno1 corni) { sol'2. }
  \tag #'(corno2 corni) { sol }
>>
R2.*7 |
<>\f <<
  \tag #'(corno1 corni) { fa''2. }
  \tag #'(corno2 corni) { re'' }
>>
R2.*3 |
<>\f <<
  \tag #'(corno1 corni) { fa''2. }
  \tag #'(corno2 corni) { re'' }
>>
R2.*19 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' | re'' }
  { re'' re'' | sol' }
>> r4 r |
R2.*3 |
r4 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' | re'' }
  { re'' re'' | sol' }
>> r4 r |
R2.*12 |
<<
  \tag #'(corno1 corni) { sol'2. | do'' | sol'4 }
  \tag #'(corno2 corni) { sol2. | do' | sol4 }
  { s2.\f | s\f }
>> r4 r |
R2.*4 |
<>\f <<
  \tag #'(corno1 corni) { do''2. | do''8 }
  \tag #'(corno2 corni) { do'2. | do'8 }
>> r8 r4 r |
<<
  \tag #'(corno1 corni) { sol'2. | }
  \tag #'(corno2 corni) { sol }
>>
R2.*7 |
<>\f <<
  \tag #'(corno1 corni) { sol'2. }
  \tag #'(corno2 corni) { sol }
>>
R2.*3 |
<>\f <<
  \tag #'(corno1 corni) { re''2. }
  \tag #'(corno2 corni) { sol }
>>
R2.*7 |
R2.^\fermataMarkup |
R2.*7 |
<>\f <<
  \tag #'(corno1 corni) {
    do''2. |
    re'' |
    do'' |
    sol' |
    do''4
  }
  \tag #'(corno2 corni) {
    do'2. |
    sol |
    do' |
    sol |
    do'4
  }
>> r4 r |
r <<
  \tag #'(corno1 corni) { re''4 re'' | do'' }
  \tag #'(corno2 corni) { sol'4 sol' | do' }
>> r4 r |
r r <<
  \tag #'(corno1 corni) { re''4 | do'' }
  \tag #'(corno2 corni) { sol4 | do' }
>> r4 r |
