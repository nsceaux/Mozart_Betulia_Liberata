\clef "treble"
<>\p <<
  \tag #'(oboe1 oboi) {
    lab'2.( |
    sib') |
    lab'8
  }
  \tag #'(oboe2 oboi) {
    fa'2.( |
    sol') |
    fa'8
  }
>> r8 r4 r |
R2.*3 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { reb''2 mi''4 | }
  { sib'2. | }
>>
<<
  \tag #'(oboe1 oboi) { fa''8 }
  \tag #'(oboe2 oboi) { lab' }
>> r8 r4 r |
\twoVoices #'(oboe1 oboe2 oboi) << do''2. do'' >> |
R2.*7 |
<>\f <<
  \tag #'(oboe1 oboi) { sol'2. }
  \tag #'(oboe2 oboi) { mib' }
>>
R2.*3 |
<>\f <<
  \tag #'(oboe1 oboi) { sol'2. }
  \tag #'(oboe2 oboi) { mib' }
>>
R2.*11 |
<>\f <<
  \tag #'(oboe1 oboi) { do''2. | reb'' | do'' | reb'' | }
  \tag #'(oboe2 oboi) { lab'2. | sib' | lab' | sib' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 reb'' mib'' | }
  { lab'2. | }
>>
<<
  \tag #'(oboe1 oboi) { reb''4 do'' sib' | }
  \tag #'(oboe2 oboi) { sib' lab' sol' | }
>>
\tag#'oboi <>^"a 2." lab'4-. do''-. fa''-. |
lab''-. re''-. fa''-. |
si' <<
  \tag #'(oboe1 oboi) { re''2( | mib''8) }
  \tag #'(oboe2 oboi) { si'2( | do''8) }
>> r8 r4 r |
R2.*2 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''2( sol''4) |
    lab'' sol''8( fa'' mib'' re''!) |
    do'' }
  { do''2.~ |
    do''8( fa'') mib''([ re'' do'' si']) |
    do''8 }
>> r r4 r |
R2.*4 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''2. | solb''2 }
  \tag #'(oboe2 oboi) { \tag#'oboi \once\tieDown sib'2.~ | sib'2 }
>> r4 |
R2.*6 |
<<
  \tag #'(oboe1 oboi) { sol''2. | lab'' | sol''8 }
  \tag #'(oboe2 oboi) { mi''2. | fa'' | mi''8 }
  { s2.\f s\f }
>> r8 r4 r |
R2.*4 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { reb''2 mi''4 | fa''8 }
  { sib'2. | lab'8 }
>> r8 r4 r |
\twoVoices #'(oboe1 oboe2 oboi) << do''2. do'' >> |
R2.*7 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) << do''2. do'' >> |
R2.*3 |
<>\f <<
  \tag #'(oboe1 oboi) { do''2. | }
  \tag #'(oboe2 oboi) { mi' | }
>>
R2.*7 |
R2.^\fermataMarkup |
R2.*3 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''2.~ | fa''2 }
  \tag #'(oboe2 oboi) { do''2. | reb''2 }
>> r4 |
R2.*2 |
<>\f <<
  \tag #'(oboe1 oboi) {
    lab''2. |
    sib'' |
    lab'' |
    sib'' |
    lab''4 mi'' fa'' |
  }
  \tag #'(oboe2 oboi) {
    fa''2. |
    sol'' |
    fa'' |
    sol'' |
    fa''4 sib' do'' |
  }
>>
\tag#'oboi <>^"a 2." sib'4 do'' do' |
fa' fa'' do'' |
reb'' sib' <<
  \tag #'(oboe1 oboi) { mi''4 | fa'' }
  \tag #'(oboe2 oboi) { sol' | lab' }
>> r4 r |
