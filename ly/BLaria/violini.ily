\clef "treble"
<<
  \tag #'violino1 {
    do'16\p do' do' do' do'2:16 |
    do'2.:16 |
    do':16 |
    mi':16 |
    mi':16 |
    mib'!:16 |
    reb'4 sib''2:16\f |
    lab''4
  }
  \tag #'violino2 {
    lab16\p lab lab lab lab2:16 |
    sib2.:16 |
    lab:16 |
    sib:16 |
    sib:16 |
    do':16 |
    sib4 reb'':16\f mi'':16 |
    fa''4
  }
>> r8 fa' mib'! reb' |
do'2.:16 |
<<
  \tag #'violino1 {
    do'16\p mib' mib' mib' mib'2:16 |
    fa'2.:16 |
    fa'2.:16 |
    fa'2.:16 |
    sol':16 |
    lab':16 |
    lab':16 |
    <sol' sib>2.:16\f |
    sol'2.:16\p |
    lab':16 |
  }
  \tag #'violino2 {
    lab2.:16\p |
    lab:16 |
    do':16 |
    reb':16 |
    sib:16 |
    dob':16 |
    fa':16 |
    <mib' sol>:16\f |
    sib:16\p |
    do':16 |
  }
>>
fab'8 fab' mib' mib' re' re' |
<mib' sol>2.:16\f |
<<
  \tag #'violino1 {
    sol'2.:16\p |
    sol':16 |
    sol':16 |
    fab':16 |
    mib'4 r r |
    reb''8 reb'' fa'' fa'' mib'' mib'' |
    reb'' reb'' do'' do'' sib' sib' |
    mib'' mib'' mib'' mib'' mib'' mib'' |
    fa'' fa'' fa'' fa'' fa'' fa'' |
    do''2.:16 |
    sib':16 |
    lab'4 mib''2:16\f |
    mib''2.:16 |
    mib'':16 |
    mib'':16 |
    \grace fa''16 mib''8 reb''16 do''
  }
  \tag #'violino2 {
    sib2.:16\p |
    sib:16 |
    sib:16 |
    reb':16 |
    do'4 r r |
    sib'8 sib' reb'' reb'' do'' do'' |
    sib' sib' lab' lab' sol' sol' |
    lab' lab' lab' lab' lab' lab' |
    lab' lab' lab' lab' lab' lab' |
    lab'2.:16 |
    sol':16 |
    lab'4 mib''2:16\f |
    reb''2.:16 |
    do'':16 |
    reb'':16 |
    do''4
  }
>> reb''16 mib'' fa'' sol'' lab''8[ r16 do''] |
\grace mib''16 reb''8 do''16 sib' mib''4-. mib'-. |
lab'-. do''-. fa'-. |
lab'-. re'-. fa'-. |
si8 lab'-. sol'-. fa'-. mib'-. re'-. |
<<
  \tag #'violino1 {
    do'16 mib'\p mib' mib' mib'2:16 |
    fa'2.:16 |
    re':16 |
    mib'2:16 do'4:16 |
    do'8( lab' sol' fa' mib' re') |
    mib'2.:16 |
    reb'!:16 |
    reb':16 |
    reb'2:16 sol'4:16 |
    solb'2.:16 |
    fa':16 |
    solb':16 |
    fa'8 solb' fa' mib' reb' do' |
    fa'2.:16 |
    solb'4 fa'8 mib' reb' do' |
  }
  \tag #'violino2 {
    do'2.:16\p |
    re':16 |
    si:16 |
    do'2:16 sol4:16 |
    lab8( fa' mib' re' do' si) |
    do'2.:16 |
    sib!:16 |
    sib:16 |
    sib2:16 reb'!4:16 |
    do'2.:16 |
    sib2.:16 |
    sib:16 |
    reb'8 mib' reb' do' sib la |
    sib2.:16 |
    sib8 mib' reb' do' sib la |
  }
>>
sib4\f reb'' r8 sib' |
sol'!4 do'' r8 lab' |
fa'4 reb' si |
do'4 reb'' r8 mi' |
lab'4 si si |
do'8 do'' sib'! sol' reb'' mi' |
fa'16\p <<
  \tag #'violino1 {
    do'16 do' do' do'2:16 |
    mi'2.:16 |
    mi'2.:16 |
    mib'!:16 |
    reb'4 sib''2:16\f |
    lab''4
  }
  \tag #'violino2 {
    lab16 lab lab lab2:16 |
    sib2.:16 |
    sib:16 |
    do':16 |
    sib4 reb'':16\f mi'':16 |
    fa''4
  }
>> r8 fa' mib'! reb' |
do'2.:16 |
<<
  \tag #'violino1 {
    mib'2.:16\p |
    mib':16 |
    reb':16 |
    reb':16 |
    do'16 fa' fa' fa' fa'2:16 |
    fa':16 mi'4:16 |
    fa'8 lab' sol' sol' fa' fa' |
    mi'2.:16\f |
    mi':16\p |
    mi':16 |
    lab'8
  }
  \tag #'violino2 {
    do'2.:16\p |
    do':16 |
    sib:16 |
    sib:16 |
    lab16 do' do' do' do'2:16 |
    sib2.:16 |
    lab8 do'4 do' do'8 |
    do'2.:16\f |
    sol:16\p |
    sol:16 |
    lab8
  }
>> lab'8 fa' fa' reb' reb' |
do'2.:16\f |
<<
  \tag #'violino1 {
    mi'2.:16\p |
    mib'!4-. reb'-. r |
    fa'2.:16 |
    fa'4-. mi'-. r8 do'' |
    sib' lab' sol' fa' mi' reb' |
    do' reb'' reb'' reb'' do'' do'' |
    sib' sib' lab' lab' sol' sol' |
    <solb' la>4 r\fermata r8 solb'\p |
    fa'-. fa''-. mi''( fa'') mi''( fa'') |
    fa'2:16 do''4:16 |
    sib'8 sib' lab' lab' sol' sol' |
    do'' do'' do'' do'' do'' do'' |
    reb'' reb'' reb'' reb'' reb'' reb'' |
    lab'2.:16 |
    sol':16 |
    lab'16 do''' do''' do''' do'''2:16\f |
    do'''2.:16 |
    do'':16 |
    do'':16 |
    do''16( lab'') sol''-. fa''-. mi''( sib'') lab''-. sol''-. fa''8[
  }
  \tag #'violino2 {
    sol2.:16\p |
    la4-. sib-. r |
    lab!2.:16 |
    reb'4-. do'-. r |
    R2. |
    r8 sib' sib' sib' lab' lab' |
    sol' sol' fa' fa' mi' mi' |
    <mib'! do'>4 r\fermata r |
    R2. |
    do'2:16\p fa'4:16 |
    sol'8 sol' fa' fa' mi' mi' |
    fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' fa' fa' |
    fa'2.:16 |
    mi':16 |
    fa'4 lab''2:16\f |
    sib''2.:16 |
    lab''16 lab' lab' lab' lab'2:16 |
    sib'2.:16 |
    lab'8 lab' sib' sib' do''[
  }
>> r16 lab'] |
\grace do''16 sib'8 lab'16 sol' do''4 do' |
fa' fa''-. do''-. |
reb''-. sib'-. <sol' do'' mi''> |
<fa' do'' fa''> r r |
