\clef "bass" fa8\p fa fa fa fa fa |
mi mi mi mi mi mi |
fa fa fa fa fa fa |
fa fa fa fa fa fa |
fa fa fa fa fa fa |
fa fa fa fa fa fa |
fa\f fa fa fa fa fa |
fa4 r8 fa mib! reb |
do8 do do do do do |
do\p do do do do do |
reb reb reb reb reb reb |
la, la, la, la, la, la, |
sib, sib, sib, sib, sib, sib, |
mib mib mib mib mib mib |
mib mib mib mib mib mib |
re re re re re re |
mib\f sol sib sol sib sol |
mib\p mib mib mib mib mib |
mib mib mib mib mib mib |
fab4 mib re |
mib8\f mib mib mib mib mib |
mib\p mib mib mib mib mib |
reb reb sol sib sol mib |
reb reb reb reb reb reb |
sib, sib, reb sol reb sib, |
do4 r r |
sol8 sol sol sol lab lab |
reb reb mib mib mib mib |
do do do do do do |
reb reb reb reb reb reb |
mib mib mib mib mib mib |
mib mib mib mib mib mib |
lab\f lab do' mib' do' lab |
sol sol sib mib' sib sol |
lab lab do' mib' do' lab |
sol sol sib mib' sib sol |
lab lab sib sib do' do |
reb reb mib mib mib mib |
lab4-. do'-. fa-. |
lab-. re-. fa-. |
si,8-. lab-. sol-. fa-. mib-. re-. |
do8\p do do do do do |
sol sol sol sol sol sol |
sol sol sol sol sol sol |
do do do do mib mib |
fa fa sol sol sol, sol, |
do do do do do do |
mi mi mi mi mi mi |
mi mi mi mi mi mi |
mi mi mi mi mi mi |
mib! mib mib mib mib mib |
reb! reb reb reb reb reb |
mib mib mib mib mib mib |
fa fa fa fa fa fa |
reb reb reb reb reb reb |
mib mib fa fa fa fa |
sib,4\f reb' r8 sib |
sol4 do' r8 lab |
fa4 reb si, |
do reb' r8 mi |
lab4 si, si, |
do8 do' sib! sol reb' mi |
fa8\p fa fa fa fa fa |
fa fa fa fa fa fa |
fa fa fa fa fa fa |
fa fa fa fa fa fa |
fa\f fa fa fa fa fa |
fa4 r8 fa mib! reb |
do do do do do do |
do\p do do do sib, sib, |
la, la, la, la, la, la, |
sib, sib, sib, sib, sib, sib, |
sol sol sol sol sol sol |
lab lab lab lab fa fa |
sol sol sol sol do do |
fa fa mib mib reb reb |
do\f do do do do4 |
R2.*2 |
lab4\p fa reb |
do8\f do do do do4 |
R2. |
fa4-.\p sib,-. r |
R2. |
sol4-. do-. r |
R2. |
mi2 fa8 fa |
sib, sib, do do do do |
do4\f r\fermata r |
reb4\p r r |
lab,8 lab, lab, lab, lab, lab, |
sib, sib, do do do do |
lab, lab, lab, lab, lab, lab, |
sib, sib, sib, sib, sib, sib, |
do do do do do do |
do do do do do do |
fa\f fa lab do' lab fa |
mi mi sol do' sol mi |
fa fa lab do' lab fa |
mi mi sol do' sol mi |
fa fa sol sol lab lab |
sib4 do' do |
fa fa'-. do'-. |
reb'-. sib-. do'-. |
fa-. r r |
