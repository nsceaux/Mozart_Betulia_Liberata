\clef "treble" r4 |
<<
  \tag #'(oboe1 oboi) { mi''4 s mi'' s | mi'' }
  \tag #'(oboe2 oboi) { sol' s sol' s | sol' }
  { s4 r s r | }
>> r4 r2 |
R1 |
\twoVoices #'(oboe1 oboe2 oboi) << do'2 do' >> r |
R1 |
<<
  \tag #'(oboe1 oboi) {
    la''2 s |
    la'' do''' |
    do''' si'' |
    do'''4 s sol''2~ |
    sol'' s4. si''8 |
    do'''4 mi'' mi''
  }
  \tag #'(oboe2 oboi) {
    do''2 s |
    do'' fa'' |
    mi'' re'' |
    mi''4 s sol'2~ |
    sol' s4. re''8 |
    mi''4 sol' sol'
  }
  { s2 r | s1*2 | s4 r s2 | s r4 r8 }
>> r4 |
R1 |
<<
  \tag #'(oboe1 oboi) { mi''2 s | s1 | do''2 s s1 | la''1~ | la''4 }
  \tag #'(oboe2 oboi) { sol'2 s | s1 | mi'2 s | s1 | do''1~ | do''4 }
  { s2\p r | R1 | s2\p r | R1 | s1\p | }
>> r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'2( | do''4) }
  { fa''2( | mi''4) }
>> r4 <>\f <<
  \tag #'(oboe1 oboi) {
    sol''2 |
    sol'' s4 s8 si'' |
    do'''4 mi'' mi''
  }
  \tag #'(oboe2 oboi) {
    sol'2~ |
    sol' s4 s8 re'' |
    mi''4 sol' sol'
  }
  { s2 | s r4 r8 }
>> r4 |
\tag #'oboi <>^"a 2." sol'4.\f la'16 si' do''8 re'' mi'' fad'' |
sol''4 r r2 |
sol'4.\f la'16 si' do''8 re'' mi'' fad'' |
sol''4 r r2 |
R1*2 |
<>\f <<
  \tag #'(oboe1 oboi) { re'''2. do'''4 | }
  \tag #'(oboe2 oboi) { re''2. la''4 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si''4 fad'' sol'' dod'' | re'' }
  { sol'' re''2 sol''4 | fad'' }
>> r4 r2 |
<>\f <<
  \tag #'(oboe1 oboi) { re'''2. do'''4 | }
  \tag #'(oboe2 oboi) { re''2. la''4 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si''4 fad'' sol'' dod'' | re'' }
  { sol'' re''2 sol''4 | fad'' }
>> r4 r2 |
fad''8\p\cresc la'' sol'' fad'' sol'' la'' si'' dod''' |
re'''4\f <<
  \tag #'(oboe1 oboi) { fad''4 fad'' }
  \tag #'(oboe2 oboi) { la' la' }
>> r4 |
\tag #'oboi <>^"a 2." sol'4\f sol' sol' r |
R1 |
sol'4\f sol' sol' r |
R1*3 | \allowPageTurn
<<
  \tag #'(oboe1 oboi) { si''2 la'' | }
  \tag #'(oboe2 oboi) { sol''2 fad'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 re''2 mi''4~ |
    mi'' fad''2 sol''4~ |
    sol'' la'' fad'' la'' | }
  { sol''4 la' si'2 |
    do'' re'' |
    mi'' la'4 fad''! | }
>>
<<
  \tag #'(oboe1 oboi) {
    si''4 s mi'' s |
    mi'' s s fad'' |
    sol''1~ |
    sol''~ |
    sol'' |
    fa''2 mi'' |
    re''4 s2. |
    si'4( re''2) si'4-. |
    do'' mi''2( do''4) |
    re''2 do'' |
    si'4 sold'' sold'' s |
    la''2
  }
  \tag #'(oboe2 oboi) {
    sol''4 s sol' s |
    do'' s s la' |
    sol'1~ |
    sol'~ |
    sol' |
    si'2 do'' |
    si'4 s s2 |
    sold'4( si'2) sold'4-. |
    la' do''2( la'4) |
    si'2 la' |
    sold'4 si' si' s |
    la'2
  }
  { s4 r s r |
    s r r s |
    s1\p | s1*2 |
    s1\p |
    s4 r r2 |
    s1\p |
    s4 s2.\f |
    s1\p |
    s4 s2\f r4 | }
>> r2 |
R1 |
r2 r8 \tag #'oboi <>^"a 2." fa''8 mi'' re'' |
do''4. re''16 mi'' fa''8 sol''! la'' si'' |
do'''4 r r2 |
R1*2 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) << sol''2. sol'' >> <<
  \tag #'(oboe1 oboi) { si''4 | do''' fa'' mi'' }
  \tag #'(oboe2 oboi) { fa''4 | mi'' re'' do'' }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 | si' }
  { fad'' | sol'' }
>> r4 r2 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) << sol''2. sol'' >> <<
  \tag #'(oboe1 oboi) { si''4 | do''' fa'' mi'' }
  \tag #'(oboe2 oboi) { fa''!4 | mi'' re'' do'' }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 | si' }
  { fad'' | sol'' }
>> r4 r2 |
r8 \tag #'oboi <>^"a 2." sol''-\sug\p si'' sol'' do'''-\sug\cresc sol'' mi'' do'' |
si'4\f <<
  \tag #'(oboe1 oboi) {
    si'' sol'' s |
    mi'' sol'' mi''
  }
  \tag #'(oboe2 oboi) {
    re''4 si' s |
    do'' mi'' do''
  }
  { s2 r4 | }
>> r4 |
R1 |
r2 r4 <>\p <<
  \tag #'(oboe1 oboi) {
    sol''4 |
    la''2 fa'' |
    mi'' re'' |
  }
  \tag #'(oboe2 oboi) {
    do''4 |
    do''2 re'' |
    do'' si' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) << do''4 do'' >> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) { mi''2 re'' | }
  \tag #'(oboe2 oboi) { do''2 si' | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 r8 sol'' mi'' do'' r la'' |
    fa'' re'' r si'' do'''2~ |
    do'''1~ |
    do'''2 si'' | }
  { do''4 r4 mi''2 |
    fa'' sol'' |
    la'' fa'' |
    mi'' re'' | }
>>
<<
  \tag #'(oboe1 oboi) {
    do'''4 s sol''2~ |
    sol'' s4. si''8 |
    do'''4 mi'' mi''
  }
  \tag #'(oboe2 oboi) {
    do''4 s sol'2~ |
    sol' s4. re''8 |
    mi''4 sol' sol'
  }
  { s4 r s2 | s r4 r8 }
>> r4
<>\p <<
  \tag #'(oboe1 oboi) { do''1~ | do'' | la'2 }
  \tag #'(oboe2 oboi) { \tag #'oboi \once\slurDown la'1( | sol') | fa'2 }
>> r2 |
r <<
  \tag #'(oboe1 oboi) { mi''2 | fa''4 }
  \tag #'(oboe2 oboi) { sol'2 | la'4 }
>> r4 r2 |
R1 |
r8 <>\f <<
  \tag #'(oboe1 oboi) { la''8 fa'' la'' fa''4 }
  \tag #'(oboe2 oboi) { do''8 la' do'' la'4 }
>> r4 |
R1*7 |
<>\f <<
  \tag #'(oboe1 oboi) { sol''2 fad'' | }
  \tag #'(oboe2 oboi) { mi'' red'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) << mi''2 mi'' >> <<
  \tag #'(oboe1 oboi) { sol''2 }
  \tag #'(oboe2 oboi) { dod'' }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''!1 | mi''2 la'' | }
  { re''!2. si'8 re'' | do''!1 | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 s sol'' sol'' | }
  \tag #'(oboe2 oboi) { si'4 s sol' sol' | }
  { s4 r }
>>
