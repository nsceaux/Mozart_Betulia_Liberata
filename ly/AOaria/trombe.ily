\clef "treble" <> r4 |
<<
  \tag #'(tromba1 trombe) { do''4 s do'' s | do'' }
  \tag #'(tromba2 trombe) { mi'4 s mi' s | mi' }
  { s4 r s r | }
>> r4 r2 |
R1 |
\tag #'trombe <>^"a 2." do'2 r |
R1 |
do'2 r |
do'4 r do' r |
sol' r sol' r |
<<
  \tag #'(tromba1 trombe) {
    do''4 s do'' s |
    do'' s do'' s8 re'' |
    do''4 do'' do''
  }
  \tag #'(tromba2 trombe) {
    mi'4 s mi' s |
    mi' s mi' s8 sol' |
    mi'4 mi' mi'
  }
  { s4 r s r | s r s r8 }
>> r4 |
R1 |
\tag #'trombe <>^"a 2." do'2\p r |
R1 |
do'2\p r |
R1*3 |
r2 <>\f <<
  \tag #'(tromba1 trombe) {
    do''4 s |
    do'' s do'' s8 re'' |
    do''4 do'' do''
  }
  \tag #'(tromba2 trombe) {
    mi'4 s |
    mi' s mi' s8 sol' |
    mi'4 mi' mi'
  }
  { s4 r | s r s r8 }
>> r4 |
\tag #'trombe <>^"a 2." sol'4\f r r2 |
R1 |
sol'4\f r r2 |
R1*3 |
re''4\f r r2 |
re''4 r r2 |
re''4 r r2 |
re''4 r r2 |
re''4 r r2 |
re''4 r r2 |
re''4 r r2 |
re''4 re'' re'' r |
sol'\f sol' sol' r |
R1 |
sol'4\f sol' sol' r |
R1*3 |
re''4 r r2 |
<<
  \tag #'(tromba1 trombe) { re''4 }
  \tag #'(tromba2 trombe) { sol' }
>> r4 r2 |
R1 |
r2 r4 \twoVoices #'(tromba1 tromba2 trombe) << re''4 re'' >> |
<<
  \tag #'(tromba1 trombe) { re''4 }
  \tag #'(tromba2 trombe) { sol' }
>> r4 \tag #'trombe <>^"a 2." sol' r |
r2 r4 re'' |
sol' r r2 |
R1*2 |
sol'4\p r r2 |
sol'4 r r2 |
R1*2 |
mi'4\p r r2 |
mi'4 r r2 |
R1*3 |
<<
  \tag #'(tromba1 trombe) { mi'4 }
  \tag #'(tromba2 trombe) { do' }
>> r4 r2 |
R1*3 |
\tag #'trombe <>^"a 2." sol'4 r r2 |
sol'4 r r2 |
sol'4 r r2 |
sol'4 r r2 |
sol'4 r r2 |
sol'4 r r2 |
sol'4-\sug\p r r2 |
sol'4\f sol' sol' r |
<<
  \tag #'(tromba1 trombe) { do''4 do'' do'' }
  \tag #'(tromba2 trombe) { mi' mi' mi' }
>> r4 |
R1*7 |
<>\f <<
  \tag #'(tromba1 trombe) { do''4 s2. | s2 do''4 }
  \tag #'(tromba2 trombe) { mi'4 s2. | s2 do'4 }
  { s4 r r2 | r2 }
>> r4 |
\tag #'trombe <>^"a 2." do'4 r do' r |
sol' r sol' r |
<<
  \tag #'(tromba1 trombe) {
    do''4 s do'' s |
    do'' s do'' s8 re'' |
    do''4 do'' do''
  }
  \tag #'(tromba2 trombe) {
    mi'4 s mi' s |
    mi' s mi'4 s8 sol' |
    mi'4 mi' mi'
  }
  { s4 r s r | s r s r8 }
>> r4 |
R1*16 |
r2 \tag #'trombe <>^"a 2." sol'4\f r |
do'4 r r2 |
sol'4 r sol' sol' |
