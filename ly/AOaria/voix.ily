\clef "bass" r4 |
R1*10 |
r2 r4 sol |
do'4. do'8 do'4 do' |
do' do r2 |
si8[ do'] si la sol[ fa] mi[ re] |
do4 la, r do' |
si8[ la] sol[ fa] mi[ re] do[ si,] |
la,4 la do'2~ |
do'4 la8. fa16 re4 sol |
mi do r2 |
R1*2 |
r2 r4 r8 re |
sol4 la si do' |
si sol r r8 re |
sol4 la si do' |
si2 sol |
mi4 do' la sol |
fad r r2 |
r2 r4 sol |
fad8[ la] do'[ la] si[ la] si[ sol] |
fad4 re r2 |
R1 |
fad8[ la] do' la si[ la] si[ sol] |
fad[ la] sol[ fad] sol[ la] si[ dod'] |
re'4 re r2 |
r r4 r8 si |
do'4 la fad re |
sol si r4 r8 si |
do'4 la fad red |
mi2 sol |
do do' |
re2. re4 |
<< { \voiceOne sol4 \oneVoice } \new Voice { \voiceTwo sol, } >> r4 r2 |
R1*4 |
R1*2 |
r2 r4 do' |
si4. sol8 do'4 do |
sol si, r2 |
R1*2 |
sold8[ si] sold mi la[ si] do'[ la] |
sold4 mi r2 |
r r4 r8 mi |
la4 do' si sold |
la mi r2 |
r r4 r8 sol |
do'4 sol8[ do'] si[ la] sol[ fa] |
mi2 do' |
la8[ sol] fa[ mi] fa[ mi] re[ do] |
sol4 r r2 |
r r4 r8 do' |
si8[ la] si[ sol] do'[ sol] mi[ do] |
sol4 sol r2 |
R1 |
si8[ la] si sol do'[ sol] mi[ do] |
sol4 si8[ sol] do'[ sol] mi[ do] |
sol4 sol, r2 |
r r4 sol |
do' sol do sib |
sold la r do' |
fa2 la |
sol2. sol4 |
la2 do' |
fa re |
sol2. sol4 |
do4 r r2 |
R1*6 |
fa4 fa r r8 fa |
do'4 r r r8 do' |
la4 la fa2~ |
fa4 mi8. re16 do4 sib |
la do' fa2~ |
fa4 sib?8 re' do'[ sib!] la[ sol] |
fa4 fa r4 r8 la |
la4. la8 la4. do'8 |
si!4 si r r8 si |
si4. fad8 fad4. la8 |
sol4 si do'2 |
si4 la sol fad |
mi2 si |
do'2 la |
si2. si4 |
mi4 r r2 |
R1*3 |
