Ter -- ri -- bi -- le d’a -- spet -- to,
bar -- ba -- ro di co -- stu -- mi,
ter -- ri -- bi -- le d’a -- spet -- to,
bar -- ba -- ro di co -- stu -- mi,
o con -- ta sé fra’ nu -- mi,
o nu -- me al -- cun non ha,
o nu -- me al -- cun non ha,
ter -- ri -- bi -- le d’a -- spet -- to,
bar -- ba -- ro di co -- stu -- mi, di co -- stu -- mi,
o con -- ta sé fra nu -- mi,
o nu -- me al -- cun non ha,
o nu -- me al -- cun non ha.

Ter -- ri -- bi -- le d’a -- spet -- to,
bar -- ba -- ro di co -- stu -- mi,
o con -- ta sé fra’ nu -- mi,
o nu -- me al -- cun non ha,
o nu -- me al -- cun non ha,
ter -- ri -- bi -- le d’a -- spet -- to,
bar -- ba -- ro di co -- stu -- mi, di co -- stu -- mi,
o con -- ta sé fra nu -- mi,
o nu -- me al -- cun non ha,
o nu -- me al -- cun non ha.

Fa -- sto, fu -- ror, di -- spet -- to
sem -- pre da -- gli oc -- chi spi -- ra,
sem -- pre da -- gli oc -- chi spi -- ra,
e quan -- to è pron -- to all’ i -- ra,
e quan -- to è pron -- to all’ i -- ra,
è tar -- do al -- la pie -- tà,
è tar -- do al -- la pie -- tà.
