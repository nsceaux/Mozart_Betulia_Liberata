\clef "treble" <> r4 |
<<
  \tag #'(corno1 corni) {
    mi''4 s sol'' s |
    mi'' s2. |
    s1 |
    do''2 s |
    s1 |
    do''2
  }
  \tag #'(corno2 corni) {
    do''4 s do'' s |
    do'' s2. |
    s1 |
    do'2 s |
    s1 |
    do'2
  }
  { s4 r s r | s r r2 | R1 | s2 r | R1 | }
>> r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 fa'' | }
  { do'1 | }
>>
<<
  \tag #'(corno1 corni) {
    mi''2 re'' |
    do''4 s4. mi''8 re''4 |
    s8 mi'' re''4 s8 mi'' re'' re'' |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    do''2 sol' |
    mi'4 s4. do''8 sol'4 |
    s8 do'' sol'4 s8 do'' sol' sol' |
    mi'4 mi' mi'
  }
  { s1 | s4 r r8 s4. | r8 s4. r8 s4. | }
>> r4 |
R1 |
<>\p <<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1 |
<>\p <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*2 |
r2 <>\p <<
  \tag #'(corno1 corni) {
    re''2 |
    do''4 s4. mi''8 re''4 |
    s8 mi'' re''4 s8 mi'' re'' re'' |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    sol'2 |
    mi'4 s4. do''8 sol'4 |
    s8 do'' sol'4 s8 do'' sol' sol' |
    mi'4 mi' mi'
  }
  { s2 | s4 r r8 s4.\f | r8 s4. r8 s4. | }
>> r4 |
\tag #'corni <>^"a 2." sol'2\f r |
R1 |
sol'2\f r |
R1*3 |
re''1\f~ |
re''2 re''4 mi'' |
re'' r r2 |
re''1~ |
re''2 re''4 mi'' |
re'' r r2 |
re''1 |
re''4 re'' re'' r |
<>\f <<
  \tag #'(corno1 corni) {
    re''4 re'' re'' s |
    s1 |
    re''4 re'' re''
  }
  \tag #'(corno2 corni) {
    sol'4 sol' sol' s |
    s1 |
    sol'4 sol' sol'
  }
  { s2. r4 | R1 | s2.\f }
>> r4 |
R1*3 |
<<
  \tag #'(corno1 corni) { re''1 | re''4 }
  \tag #'(corno2 corni) { re''1 | sol'4 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 s8 re'' mi''4 |
    s8 mi''4 mi''8 re''4 sol'' |
    mi''2 re''4 re'' | }
  { re''4 s8 re'' mi''4 |
    s8 do'' do''4 re''2 |
    do''2 re''4 re'' | }
  { s4 r8 s4. | r8 }
>>
<<
  \tag #'(corno1 corni) { re''4 s mi'' s | mi'' }
  \tag #'(corno2 corni) { sol'4 s do'' s | do'' }
  { s4 r s r | }
>> r4 r \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> |
<<
  \tag #'(corno1 corni) {
    re''4 s2. |
    re''4 fa''2 re''4 |
    mi'' sol''2 mi''4 |
    re''2 mi'' |
    re''4 s mi''2~ |
    mi''1~ |
    mi''~ |
    mi''~ |
    mi''4 mi'' mi'' s |
    mi''2
  }
  \tag #'(corno2 corni) {
    sol'4 s2. |
    sol'4 re''2 sol'4 |
    do'' mi''2 do''4 |
    sol'2 do'' |
    sol'4 s mi'2~ |
    mi'1~ |
    mi'~ |
    mi'~ |
    mi'4 mi' mi' s |
    do''2
  }
  { s4 r r2 | s1\p | s4 s2.\f | s1\p | s4 r s2\p | s1*3 | s4 s2\f r4 | }
>> r2 |
R1*2 |
<<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*3 |
<>-\sug\f <<
  \tag #'(corno1 corni) { sol''1~ | sol''2. }
  \tag #'(corno2 corni) { sol'1~ | sol'2. }
>> \twoVoices #'(corno1 corno2 corni) << do''4 do'' >> |
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
<>-\sug\f <<
  \tag #'(corno1 corni) { sol''1~ | sol''2. }
  \tag #'(corno2 corni) { sol'1~ | sol'2. }
>> \twoVoices #'(corno1 corno2 corni) << do''4 do'' >> |
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
<<
  \tag #'(corno1 corni) {
    re''2 mi'' |
    re''4 re'' re'' s |
    mi'' mi'' mi''
  }
  \tag #'(corno2 corni) {
    sol'2 do'' |
    sol'4 sol' sol' s |
    do'' do'' do''
  }
  { s2-\sug\p s2-\sug\cresc | s2.\f r4 | }
>> r4 |
R1*3 |
<>\p <<
  \tag #'(corno1 corni) { sol'1 | do''4 }
  \tag #'(corno2 corni) { sol'1 | mi'4 }
>> r4 r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''4 s8 re'' mi''2 |
    fa'' sol'' |
  }
  \tag #'(corno2 corni) {
    sol'1 |
    mi'4 s8 sol' do''2 |
    re'' mi'' |
  }
  { s1 | s4\f r8 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 do''2 }
  { do''4 do''2 }
>> <<
  \tag #'(corno1 corni) {
    fa''4 |
    mi''2 re'' |
    do''8 mi'' re'' s s mi'' re''4 |
    s8 mi'' re''4 s8 mi'' re'' re'' |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    do''4 |
    do''2 sol' |
    mi'8 do'' sol' s s do'' sol'4 |
    s8 do'' sol'4 s8 do'' sol' sol' |
    mi'4 mi' mi'
  }
  { s4 | s1 | s4. r8 r s4. | r8 s4. r8 }
>> r4 |
R1*16 |
r2 <>\f <<
  \tag #'(corno1 corni) { re''2 | mi'' }
  \tag #'(corno2 corni) { sol' | do'' }
>> \twoVoices #'(corno1 corno2 corni) << re''2 re'' >>
<<
  \tag #'(corno1 corni) { re''4 s re'' re'' | }
  \tag #'(corno2 corni) { sol'4 s sol' sol' | }
  { s4 r }
>>
