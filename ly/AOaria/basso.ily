\clef "bass" r4 |
do8 mi sol mi do mi sol mi |
do re mi fa sol la si do' |
si la sol fa mi re do si, |
la, si, do re mi fa sol la |
sol fa mi re do si, la, sol, |
fa,2 r |
fa8 fa fa fa la la la la |
sol sol sol sol sol, sol, sol, sol, |
do mi sol si, do mi sol si, |
do mi sol si, do mi sol sol, |
do4 do do r |
do8\p mi sol mi do mi sol mi |
do re mi fa sol la si do' |
si la sol fa mi re do si, |
la, si, do re mi fa sol la |
sol fa mi re do si, la, sol, |
fa, fa, fa, fa, fa fa fa fa |
fa fa fa fa sol sol sol sol |
do8\f mi sol si, do mi sol si, |
do mi sol si, do mi sol sol, |
do4 do do r |
sol,4.\f la,16 si, do8 re mi fad |
sol4 r r2 |
sol,4.\f la,16 si, do8 re mi fad |
sol4 r r2 |
sol8\p sol sol sol si, si, si, si, |
do do do do do do dod dod |
re\f re fad re fad re fad re |
sol re la re si re sol mi |
fad\mf la do' la si la si sol |
fad re\f fad re fad re fad re |
sol re la re si re sol mi |
fad\mf la do' la si la si sol |
fad\cresc la sol fad sol la si dod' |
re'4\f re re r |
sol\f sol sol sol\p |
do' la fad re |
sol\f sol sol, si\p |
do' la fad red |
mi8 mi mi mi mi mi mi mi |
do do do do do do do do |
re re re re re re re re |
sol\f la si fad sol la si sol |
la si do' la si do' re' si |
do' re' mi' do' re'4 re |
sol r do' r |
la r r re |
sol r r2 |
R1 |
r2 r4 do'\p |
si8 sol si sol do' sol mi do |
sol4 sol, r2 |
R1 |
r2 r4 la\f |
sold8\p si sold mi la si do' la |
mi'4 mi\f mi r |
la,4. si,16 do re8 mi fa sold |
la4 r r2 |
r8 do'\f si la sol! fa mi re |
do4. re16 mi fa8 sol la si |
do'4 r sol\p r |
do'8 do' do' do' mi mi mi mi |
fa fa fa fa fa fa fad fad |
sol sol\f si sol si sol re' sol |
do' sol si sol do' sol la sol |
si\p la si sol do' sol mi do
sol sol\f si sol si sol re' sol |
do' sol si sol do' sol la sol |
si\p la si sol do' sol mi do |
sol sol si-\sug\cresc sol do' sol mi do |
sol4\f sol, sol, r |
do do do sol\p |
do' sol mi do |
fa fa fa\f r |
fa8\p fa fa fa fa fa fa fa |
sol sol sol sol sol sol sol sol |
la la la la mi mi mi mi |
fa fa fa fa re re re re |
sol\cresc sol sol sol sol, sol, sol, sol, |
do\f re mi si, do re mi do |
re mi fa re mi4 r |
fa8 fa fa fa la la la la |
sol sol sol sol sol, sol, sol, sol, |
do mi sol si, do mi sol si, |
do mi sol si, do mi sol sol, |
do4 do do r |
fa8\p fa fa fa fa fa fa fa |
mi mi mi mi mi mi mi mi |
re re re re la la la la |
sol sol sol sol do do do do |
re re' do' sib la la la la |
sib sib sib sib do' do' do do |
fa\f fa fa fa fa fa fa fa |
fa\p fa fa fa mi mi mi mi |
red red red red red red red red |
red red red red red red red red |
mi mi mi mi la la la la |
si! si si si si, si, si, si, |
mi mi mi mi sol sol sol sol |
la la la la do'\cresc do' do' do' |
si\f si si si si, si, si, si, |
mi\f fad sol mi la si dod' la |
re' re fa! re si re' sol si |
do'! sol la mi fa re fa fad |
sol4 r r8 sol, si, re |

