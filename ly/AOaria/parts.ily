\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (corni #:score-template "score-corni"
          #:tag-global () #:instrument "Corni in C.")
   (trombe #:score-template "score-trombe"
           #:tag-global () #:instrument "Clarini in C."))
