\tag #'all \key do \major
\tempo "Allegro" \midiTempo#120
\time 4/4 \partial 4 s4 s1*47 \bar "|." \segnoMark
s1*39 \bar "|." \mark\markup\larger\musicglyph#"scripts.ufermata"
s1*19 \dalSegnoMark \bar "|."
