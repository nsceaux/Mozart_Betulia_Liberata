\clef "treble" <> sol'4 |
<sol mi' do''>4 r q r |
q r r mi''8.\trill re''32 mi'' |
re''8 do'' si' la' sol' fa' mi' re' |
do'2 r4 do''8.\trill si'32 do'' |
si'8 la' sol' fa' mi' re' do' si |
la2 r |
<<
  \tag #'violino1 {
    \ru#8 la''16 do'''2:16 |
    \ru#4 { mi''16 do''' } \ru#4 { re'' si'' } |
    do'''4
  }
  \tag #'violino2 {
    \ru#8 do''16 <do'' fa''>2:16 |
    \ru#4 { do''16 mi'' } \ru#4 { si' re'' } |
    do''4
  }
>> r8 sol'' mi'' do'' r sol'' |
mi'' do'' r sol'' mi'' do'' sol'
<<
  \tag #'violino1 { si''8 | <do''' mi''>4 }
  \tag #'violino2 { re''8 | << mi''4 \\ do'' >> }
>> <do'' mi' sol>4 q sol'\p |
q r q r |
q r r mi''8.\trill re''32 mi'' |
re''8 do'' si' la' sol' fa' mi' re' |
do'2 r4 do''8.\trill si'32 do'' |
si'8 la' sol' fa' mi' re' do' si |
la la la la <<
  \tag #'violino1 {
    la''8 la'' la'' la'' |
    la''4 fa''8. re''16 si'4
  }
  \tag #'violino2 {
    do''8 do'' do'' do'' |
    do''4 la'8. fa'16 re'4
  }
>> <re' si' sol''>4 |
<mi'' do'' sol'> r8 sol''\f mi'' do'' r sol'' |
mi'' do'' r sol'' mi'' do'' sol' <<
  \tag #'violino1 { si''8 | do'''4 }
  \tag #'violino2 { re''8 | mi''4 }
>> <do'' mi' sol>4 q r |
<>\f <<
  \tag #'violino1 {
    <sol sol'>4. la'16 si' do''8 re'' mi'' fad'' |
    sol''32(\p la'' si''8.) la''32( si'' do'''8.) si''32( do''' re'''8.) do'''32( si'' la''8.) |
    <sol' sol>4.\f la'16 si' do''8 re'' mi'' fad'' |
    sol''32(\p la'' si''8.) la''32( si'' do'''8.) si''32( do''' re'''8.) do'''32( si'' la''8.) |
    si''16-> si'' si'' si'' si'' si'' si'' si'' sol''-> sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    mi''8 mi'' do''' do''' la'' la'' sol'' sol'' |
    <fad'' la' re'>4\f r r la''8.\trill sol''32 la'' |
    si''4 fad''8.\trill mi''32 fad'' sol''4 dod''8.\trill si'32 dod'' |
    re''4\mf <re' la' fad''> <re' si' sol''> <re' si' si''> |
    <re' la' la''>4\f r r la''8.\trill sol''32 la'' |
    si''4 fad''8.\trill mi''32 fad'' sol''4 dod''8.\trill si'32 dod'' |
    re''4\mf <re' la' fad''> <re' si' sol''> <re' si' si''> |

  }
  \tag #'violino2 {
    sol4. la16 si do'8 re' mi' fad' |
    sol'8\p re' fad' re' sol' re' la' re' |
    sol4.\f la16 si do'8 re' mi' fad' |
    sol'8\p re' fad' re' sol' re' la' re' |
    sol'16-> sol' sol' sol' sol' sol' sol' sol' re''-> re'' re'' re'' re'' re'' re'' re'' |
    mi''8 mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    <re'' re'>16\f re' re' re' re'4:16 re'2:16 |
    re':16 re'4:16 sol':16 |
    fad'16\mf re' re' re' re'4:16 re'2:16 |
    <re' la' fad''>16\f re' re' re' re'4:16 re'2:16 |
    re':16 re'4:16 sol':16 |
    fad'16\mf re' re' re' re'4:16 re'2:16 |
  }
>>
fad''8\cresc la'' sol'' fad'' sol'' la'' si'' dod''' |
re'''4\f <fad'' la' re'> q r |
<sol sol'>4\f <re' si' sol''> <sol sol'> sol':16\p |
do''4:16 la':16 fad':16 re':16 |
<sol sol'>4\f <re' si' sol''> <sol sol'> si':16\p |
do''4:16 la':16 fad':16 red':16 |
mi'2:16 <<
  \tag #'violino1 {
    sol''2:16 |
    sol'':16 do''':16 |
    si'':16 la'':16 |
  }
  \tag #'violino2 {
    si'2:16 |
    mi'':16 mi'':16 |
    sol'':16 fad'':16 |
  }
>>
sol''4\f r8 re'' si'-. sol'-. r mi'' |
do''-. la'-. r fad'' re''-. si'-. r sol'' |
mi''-. do''-. r la'' fad''4 <re' la' re'''>4 |
si'' si''16 la'' sol'' fad'' mi''4 sol''16 fad'' mi'' re'' |
do''4 mi''16 re'' do'' si' la'4 <re' la' fad''>4 |
<re' si' sol''>4 r r2 |
r r4 si''8.\trill\p la''32 si'' |
do'''4 sol''\f mi'' do'' |
si'4\p \grace sol''16 fa''8 mi''16 re'' mi''4 <<
  \tag #'violino1 { do'''4 si'' }
  \tag #'violino2 { mi'' re'' }
>> r4 r2 |
r r4 sold''8.\trill\p fad''32 sold'' |
la''4 mi''\f do'' la' |
mi' \grace mi''16 re''8\p do''16 si' <<
  \tag #'violino1 { la'4 la'' | sold'' }
  \tag #'violino2 { do''4 do'' | si' }
>> <mi' si' mi''>4\f q r |
<<
  \tag #'violino1 {
    la'4. si'16 do'' re''8 mi'' fa'' sold'' |
    la''4 do''32(\p re'' mi''8.) sold''32( la'' si''8.) si'32( do'' re''8.) |
    do''8
  }
  \tag #'violino2 {
    la4. si16 do' re'8 mi' fa' sold' |
    la'8\p mi' la' mi' sold' mi' sold' mi' |
    la'
  }
>> do''8 si' la' sol'!\f <<
  \tag #'violino1 {
    fa'' mi'' re'' |
    do''4. re''16 mi'' fa''8 sol'' la'' si'' |
    do'''4 sol''8\p do''' si'' la'' sol'' fa'' |
    mi''16-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' sol''-> sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    la''8 sol''8
  }
  \tag #'violino2 {
    fa'8 mi' re' |
    do'4. re'16 mi' fa'8 sol' la' si' |
    do''8 mi''\p mi'' mi'' re'' fa'' mi'' re'' |
    do''16-> do'' do'' do'' do'' do'' do'' do'' do''-> do'' do'' do'' do'' do'' do'' do'' |
    do''4
  }
>> fa''8 mi'' fa'' mi'' re'' do'' |
<<
  \tag #'violino1 {
    si'4 r r si''8.\trill\f la''32 si'' |
    do'''4 re''8.\trill do''32 re'' mi''4 fad''8.\trill mi''32 fad'' |
    sol''4 <si' re' sol>4\p <sol mi' do''> <sol' do'' mi''> |
    <re' si' sol''>4 r r si''8.\f\trill la''32 si'' |
    do'''4 re''8.\trill do''32 re'' mi''4 fad''8.\trill mi''32 fad'' |
    sol''4 <si' re' sol>4\p <sol mi' do''> <sol' do'' mi''> |
    re''8
  }
  \tag #'violino2 {
    si'16 sol'\f sol' sol' sol'4:16 sol'2:16 |
    sol':16 sol'4:16 do'':16 |
    si'16 sol'\p sol' sol' <sol' sol>4:16 q2:16 |
    <sol re' si'>16 sol'\f sol' sol' sol'4:16 sol'2:16 |
    sol':16 sol'4:16 do'':16 |
    si'16\p sol' sol' sol' <sol' sol>4:16 q2:16 |
    si'8
  }
>> sol''8 si''-\sug\cresc sol'' do''' sol'' mi'' do'' |
si'4\f <re' si' sol''> <sol re' si' sol''> r |
<sol mi' do''>4 mi'' q sol'4:16\p |
do'':16 sol':16 do':16 sib':16 |
sold'( la') <fa' do'' fa''>4\f r8 do''\p |
<<
  \tag #'violino1 {
    la''8 la'' la'' la'' la'' la'' la'' la'' |
    mi''2:16 re'':16 |
    do''8 do'' do'' do'' mi'' mi'' mi'' mi'' |
    la'' la'' la'' la'' la'' la'' la'' la'' |
    mi''2:16\cresc re'':16 |
  }
  \tag #'violino2 {
    do''8 do'' do'' do'' do'' do'' do'' do'' |
    do''2:16 si':16 |
    do''8 do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' fa'' fa'' fa'' fa'' |
    do''2:16\cresc si':16 |
  }
>>
do''4\f r8 sol'' mi'' do'' r la'' |
fa'' re'' r si'' do'''16 si'' la'' sol'' fa'' mi'' re'' do'' |
la''2:16 do''':16 |
\ru#4 { mi''16 do''' } \ru#4 { re'' si'' } |
do'''4 r8 sol'' mi'' do'' r sol'' |
mi'' do'' r sol'' mi'' do'' sol' si'' |
do'''4 <do'' mi' sol> q r |
<<
  \tag #'violino1 {
    fa'8[\p r16 fa''32( sol''] la''8)[ r16 la''32( sib''] do'''4) r |
    do'8[ r16 do''32( re''] mi''8)[ r16 mi''32( fa''] sol''4) r |
    fa'8[ r16 fa''32( sol''] la''8)[ r16 la''32( sib''] do'''8) fa''[ fa'' fa''] |
    fa'' fa'' mi'' re'' do''4. sib''8 |
    la''2:16 fa'':16 |
    fa''4:16 sib''16 sib'' re''' re''' do''' do''' sib'' sib'' la'' la'' sol'' sol'' |
  }
  \tag #'violino2 {
    <>\p \ru#8 { do'16 la' } |
    \ru#8 { sol sol' } |
    \ru#4 { la fa' } \ru#4 { fa' do'' } |
    sib'2:16 \ru#4 { sol'16 sib' } |
    fa'2:16 do'':16 |
    re''4:16 sol''16 sol'' sib'' sib'' la'' la'' sol'' sol'' fa'' fa'' mi'' mi'' |
  }
>>
fa''4 r <fa'' do'' fa'>8\f <<
  \tag #'violino1 {
    do''8[ la' do''] |
    la''16->\p la'' la'' la'' la'' la'' la'' la'' la''-> la'' la'' la'' la'' la'' la'' la'' |
    \ru#4 { la''-> la'' la'' la'' la'' la'' la'' la'' } |
    sol''-> sol'' sol'' sol'' sol'' sol'' sol'' sol'' do'''-> do''' do''' do''' do''' do''' do''' do''' |
    si''!8 si'' la'' la'' sol'' sol'' fad'' fad'' |
    sol''16-> sol'' sol'' sol'' sol'' sol'' sol'' sol'' si''-> si'' si'' si'' si'' si'' si'' si'' |
    do'''-> do''' do''' do''' do''' do''' do''' do''' la''->\cresc la'' la'' la'' la'' la'' la'' la'' |
    sol''->\f sol'' sol'' sol'' sol'' sol'' sol'' sol'' fad''-> fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
  }
  \tag #'violino2 {
    la'8[ fa' la'] |
    <>\p \ru#2 { do''16-> do'' do'' do'' do'' do'' do'' do'' } |
    si'!-> si' si' si' si' si' si' si'
    \ru#3 { fad''-> fad'' fad'' fad'' fad'' fad'' fad'' fad'' } |
    \ru#2 { mi''-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' } |
    sol''8 sol'' fad'' fad'' mi'' mi'' red'' red'' |
    \ru#2 { mi''16-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' } |
    mi''16-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi''16->\cresc mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi''16->\f mi'' mi'' mi'' mi'' mi'' mi'' mi'' red''-> red'' red'' red'' red'' red'' red'' red'' |
  }
>>
mi''4\f r8 sol'' mi'' la' r la'' |
fa''! re''! r fa'' re'' sol' r sol'' |
mi'' do'' r do''' la'' fa'' re'' do'' |
si' sol' si' re'' sol''4 <si'' re'' sol'> |
