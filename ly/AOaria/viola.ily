\clef "alto" r4 |
do'8 mi' sol' mi' do' mi' sol' mi' |
do' re' mi' fa' sol' la' si' do'' |
si' la' sol' fa' mi' re' do' si |
la si do' re' mi' fa' sol' la' |
sol' fa' mi' re' do' si la sol |
fa2 r |
fa'8 fa' fa' fa' la' la' la' la' |
sol' sol' sol' sol' sol sol sol sol |
do' do'' si' sol' r do'' si' sol' |
r do'' si' sol' sol'4 r8 <sol' sol> |
q4 sol' sol r |
do'8\p mi' sol' mi' do' mi' sol' mi' |
do' re' mi' fa' sol' la' si' do'' |
si' la' sol' fa' mi' re' do' si |
la si do' re' mi' fa' sol' la' |
sol' fa' mi' re' do' si la sol |
fa fa fa fa fa' fa' fa' fa' |
fa' fa' fa' fa' sol' sol' sol' sol' |
do' do''\f si' sol' r do'' si' sol' |
r do'' si' sol' sol'4 r8 sol' |
<sol' sol>4 sol' sol r |
sol4.\f la16 si do'8 re' mi' fad' |
sol'4 r r2 |
sol4. la16 si do'8 re' mi' fad' |
sol'4 r r2 |
sol'8\p sol' sol' sol' si si si si |
do' do' do' do' do' do' dod' dod' |
re'8\f re' fad' re' fad' re' fad' re' |
sol' re' la' re' si' re' sol' mi' |
fad'\mf la' do'' la' si' la' si' sol' fad' re'\f fad' re' fad' re' fad' re' |
sol' re' la' re' si' re' sol' mi' |
fad'\mf la' do'' la' si' la' si' sol' |
fad'\cresc la' sol' fad' sol' la' si' dod'' |
re''4\f re' re' r |
sol'\f sol' sol' sol'\p |
do'' la' fad' re' |
sol'\f sol' fa' si'\p |
do'' la' fad' red' |
mi'8 mi' mi' mi' mi' mi' mi' mi' |
do' do' do' do' do' do' do' do' |
re' re' re' re' re' re' re' re' |
sol'8\f la' si' fad' sol' la' si' sol' |
la' si' do'' la' si' do'' re'' si' |
do'' re'' mi'' do'' re''4 re' |
sol' r do'' r |
la' r r re' |
<sol' sol>8\p q q q q q q q |
\ru#28 q mi' mi' mi' mi' |
\ru#16 mi' |
sold'\p si' sold' mi' la' si' do'' la' |
mi''4 mi'\f mi' r |
la4. si16 do' re'8 mi' fa' sold' |
la'4 r r2 |
r8 do''\f si' la' sol'! fa' mi' re' |
do'4. re'16 mi' fa'8 sol' la' si' |
do''4 r sol'\p r |
do''8 do'' do'' do'' mi' mi' mi' mi' |
fa' fa' fa' fa' fa' fa' fad' fad' |
sol' sol'\f si' sol' si' sol' re'' sol' |
do'' sol' si' sol' do'' sol' la' sol' |
si'\p la' si' sol' do'' sol' mi' do'
sol' sol'\f si' sol' si' sol' re'' sol' |
do'' sol' si' sol' do'' sol' la' sol' |
si'\p la' si' sol' do'' sol' mi' do' |
sol' sol' si'-\sug\cresc sol' do'' sol' mi' do' |
sol'4\f sol sol r |
do' do' do' sol'\p |
do'' sol' mi' do' |
fa' fa' fa'\f r |
fa'8\p fa' fa' fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
la' la' la' la' mi' mi' mi' mi' |
fa' fa' fa' fa' re' re' re' re' |
sol'\cresc sol' sol' sol' sol sol sol sol |
do'\f re' mi' si do' re' mi' do' |
re' mi' fa' re' mi'4 r |
fa'8 fa' fa' fa' la' la' la' la' |
sol' sol' sol' sol' sol sol sol sol |
do' do'' si' sol' r do'' si' sol' |
r do'' si' sol' r do'' si' sol' |
sol'4 sol' sol' r |
<>\p \ru#8 { la16 fa' } |
\ru#8 { do' mi' } |
\ru#4 { re' la' } \ru#4 { do' la' } |
sol'2:16 \ru#4 { mi'16 sol' } |
fa'8 re' do' sib la la la la |
sib sib sib sib do' do' do' do' |
fa'\f fa' fa' fa' fa' fa' fa' fa' |
fa'\p fa' fa' fa' mi' mi' mi' mi' |
fad'16-> fad' fad' fad' fad' fad' fad' fad' si!-> si si si si si si si |
si-> si si si si si si si si'-> si' si' si' si' si' si' si' |
si'-> si' si' si' si' si' si' si' la'-> la' la' la' la' la' la' la' |
si'8 si' si' si' si si si si |
mi' mi' mi' mi' sol' sol' sol' sol' |
la' la' la' la' do''\cresc do'' do'' do'' |
si'\f si' si' si' si si si si |
mi'\f fad' sol' mi' la' si' dod'' la' |
re'' re' fa'! re' si' re'' sol' si' |
do''! sol' la' mi' fa' re' fa' fad' |
sol'4 r r8 sol si re' |
