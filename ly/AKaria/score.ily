\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \flautiInstr } <<
        \new Staff << \global \keepWithTag #'flauto1 \includeNotes "flauti" >>
        \new Staff << \global \keepWithTag #'flauto2 \includeNotes "flauti" >>
      >>
      \new Staff \with {
        instrumentName = "Corni in F."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Giuditta
      shortInstrumentName = \markup\character Giu.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s2.*6\pageBreak
        s2.*6\break s2.*7\break s2.*7\pageBreak
        s2.*7\break s2.*6\break s2.*7\pageBreak
        s2.*7\break s2.*8\break s2.*8\pageBreak
        s2.*7\break s2.*6\break s2.*8\pageBreak
        s2.*6\break s2.*8\break s2.*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
