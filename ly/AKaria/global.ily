\tag #'all \key fa \major
\midiTempo#120
\time 3/4 \partial 8 s8 s2.*57 \bar "||" \segnoMark
s2.*43 \bar "||" \mark\markup\larger\musicglyph#"scripts.ufermata"
s2.*18 \bar "|." \dalSegnoMark
