\clef "treble"
<<
  \tag #'violino1 {
    do'8 |
    do'( fa') la'4. sib'16( sol') |
    fa'8( la') do''4. do''8 |
    dod''( re'') sol'4. la'16( sib') |
    la'8-. fa'16( la') do''!8-. do''-. do''4 |
    re''8( sib') la'( sol') do''( mi') |
    fa'16( sol') la'-. la'-. la'16( sib') do''-. do''-. do''( re'') mib''-. mib''-. |
    re''16( mi'' fa'' mi'')
  }
  \tag #'violino2 {
    r8 |
    r4 r r8 do' |
    do'( fa') la'4. la'8 |
    fa'4. mi'16( re') mi'8-. fa'16( sol') |
    fa'8-. do'16( fa') la'8-. la'-. la'4 |
    fa'8( re') do'( sib) la( sol) |
    la8 fa'4 fa' fa'8 |
    fa'4
  }
>> re''16-. do''-. sib'-. la'-. sib'-. la'-. sol'-. fa'-. |
mi' sol' si' do'' do'4 r |
<<
  \tag #'violino1 {
    la'2\p do''8( mi') |
    fa'2 la'8( do') |
    re'2 fa'8( la) |
    sib4
  }
  \tag #'violino2 {
    r16 do'(\p si do') re'( do' si do') do'( re' do' sib) |
    r la( sold la) sib( la sold la) la( do' mib' do') |
    sib4 r r |
    r
  }
>> sib''4\f sib'' |
sib''16 la'' sol'' la'' sol'' fa'' mi'' fa'' mi'' re'' do'' sib' |
la'( do'') fa''( sol'') sol''2\trill |
fa''8 do'' <fa' la>4 <<
  \tag #'violino1 {
    r8 do'\p |
    do'( fa') la'4. sib'16( sol') |
    fa'8( la') do''16( fa'') la''( fa'') do''8-. r |
    r sol' sol' sol' sol' sol' |
    la'( sib') do''-. fa''16( mi'') fa''8-. fa''-. |
    fa''( sib'') la''( sol'') do'''( mi'') |
    fa'' la'4\f sib'16 do'' re'' mi'' fa'' sol'' |
    la''8 do''\p do'' do'' do'' do'' |
    r do'' do'' do'' do'' do'' |
    r si' si' re''4 fa''8 |
    r \grace fa''32 mi''16( re'' mi''8) \grace sol''32 fa''16( mi'' fa''8) \grace la''32 sol''16( fa'' |
    sol''8) \grace sib''32 la''16( sold'' la''8) la'( fa'' mi'') |
    re''4
  }
  \tag #'violino2 {
    r4 |
    r r r8 do'8\p |
    do'( fa') la'16( do'') fa''( do'') fa''8-. r |
    r8 fa'( mi' re') mi'-. mi'-. |
    fa'( sol') la'-. la'16( sol') fa'( mib') re'( dod') |
    re'8( re'') do''( sib') la'( sol') |
    fa' fa'4\f sol'16 la' sib' do'' re'' mi'' |
    fa''8 la'\p la' la' la' la' |
    r sol' sol' sol' sol' sol' |
    r8 fa'4 si' re''8 |
    do'' do'4 do' do'8~ |
    do' do'4 dod'8( re' do') |
    si4
  }
>> r8 \grace la'32 sol'16.\f fad'32 sol'8 sol |
<<
  \tag #'violino1 {
    sol''8-.\p sol''-. fa''-. fa''-. mi''-. mi''-. |
    mi''([ fa'']) fa''8-.
  }
  \tag #'violino2 {
    mi'8-.\p mi'-. re'-. re'-. do'-. do'-. |
    do'([ re']) re'8-.
  }
>> \grace la'32 sol'16.[\f fad'32] sol'8 sol |
<<
  \tag #'violino1 {
    fa''8\p fa'' mi'' mi'' re'' re'' |
    re''( mi'') mi''4 r |
    mi''2\p sol''8( si') |
    do''2 mi''8( sol') |
    la'16( do'') do''( fa'') fa''( la'') la''( do''') do'''8 la'' |
    sol'' sol'' sol'' sol'' sol'' sol'' |
    fa'' fa'' fa'' fa'' fa'' fa'' |
    mi'' r fa'' r sol'' r |
    fa'' r re''' r fa''16( mi'' fa'' re'') |
    mi''8 r fa'' r sol'' r |
    fa'' r re''' r fa''16( mi'' fa'' re'') |
    mi''8 sol'' sol'' sol'' sol'' sol'' |
    la'' la'' si'' si'' do''' do''' |
    la'' fa'' mi'' mi'' re'' re'' |
    sol''-> sol'' sol'' sol'' sol'' sol'' |
    la''-> la'' la'' la'' la'' la'' |
    mi''16-> mi'' mi'' mi'' mi''2:16 |
    re''16-> re'' re'' re'' re'' re'' re'' re'' re'' re'' re'' re'' |
  }
  \tag #'violino2 {
    re'8\p re' do' do' si si |
    si( do') do'4 r |
    r16 sol'( fad' sol') la'( sol' fad' sol') sol'( fa' mi' fa') |
    r16 mi'( re' mi') fa'( mi' re' do') si( do' re' do') |
    do'4 r r8 do'' |
    do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' si' si' |
    do'' r do'' r do'' r |
    do'' r si' r re''16( do'' re'' si') |
    do''8 r do'' r do'' r |
    do'' r si' r re''16( do'' re'' si') |
    do''8 do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' |
    do'' re'' do'' do'' si' si' |
    do''-> do'' do'' do'' do'' do'' |
    do''-> do'' do'' do'' do'' do'' |
    do''16-> do'' do'' do'' do''2:16 |
    do''16-> do'' do'' do'' do'' do'' do'' do'' si' si' si' si' |
  }
>>
do''8\f sol''~ sol''16 do'''( si'' la'') sol''( la'') mi''( fad'') |
sol''8 sol''~ sol''16 la'' re''( mi'') fa''!( re'') sol'' fa'' |
mi''8 sol''~ sol''16 do'''( si'' la'') sol''( la'') mi''( fad'') |
sol''8 sol''~ sol''16 la'' re''( mi'') fa''!( re'') sol'' fa'' |
mi'' sol'' do''' do'' re''2\trill |
do''8 sol' do'4 r |
<<
  \tag #'violino1 {
    r8 do''\p do'' do'' do'' do'' |
    r sib' sib' sib' sib' sib' |
    r mib'' mib''( re'') re''( do'') |
    r sib' sib'( sol') mi'!( do') |
    do'( fa') la'4. sib'16( sol') |
    fa'8( la') do''16( la') fa''( mi'') fa''8-. r |
    r sol' sol' sol' sol' sol' |
    la'( sib') do''-. fa''16( mi'') fa''8 fa'' |
    fa''( sib'') la''( sol'') do'''( mi'') |
    fa''8 la'4\f sib'16 do'' re'' mi'' fa'' sol'' |
    la''8\p la'' la'' la'' la'' la'' |
    sib'' sib'' sib'' sib'' sib'' sib'' |
    do''' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re''( fad'') |
    sol'' sol'' sol'' sol'' sol'' fa''! |
    mi''!4
  }
  \tag #'violino2 {
    r8 fad'\p fad' fad' fad' fad' |
    r sol' sol' sol' sol' sol' |
    la' la' la' la' la' la' |
    r sol' do'4( sib) |
    la r r8 do' |
    do'( fa') la'-. do''-. do'-. r |
    r fa'( mi' re') mi'-. mi'-. |
    fa'( sol') la'-. la'16( sol') fa'( mib') re'( dod') |
    re'8( re'') do''( sib') la'( sol') |
    fa' fa'4\f sol'16 la' sib' do'' re'' mi'' |
    fa''8\p fa'' fa'' fa'' fa'' fa'' |
    fa'' fa'' fa'' fa'' fa'' fa'' |
    mib'' do'' do'' do'' do'' do'' |
    fa' fa' fa' fa' re'' re'' |
    re'' re'' re'' re'' re'' re'' |
    sol'4
  }
>> r8 \grace re''32\f do''16. si'32 do''8 do' |
<<
  \tag #'violino1 {
    do''8\p do''' sib''! sib'' la'' la'' |
    la''([ sib'']) sib''-.
  }
  \tag #'violino2 {
    la'8\p la' sol' sol' fa' fa' |
    fa'([ sol']) sol'8-.
  }
>> \grace re''32\f do''16.[ si'32] do''8 do' |
<<
  \tag #'violino1 {
    sib''!8\p sib'' la'' la'' sol'' sol'' |
    sol''( la'') la''4 r |
    la''2\p do'''8( mi'') |
    fa''2 la''8( do'') |
    re''2 fa''8( la') |
    sib'8 mi'' sol'' sol'' sib'' sib'' |
    la'' la'' la'' la'' fa'' fa'' |
    mi'' r sol'' r mi'' r |
    fa'' r fa'' r do'''16( la'' sol'' fa'') |
    mi''8 r sol'' r mi'' r |
    fa'' r fa'' r fa'' r |
    r fad''( sol'' la'' sib'' re''') |
    fa''!4( mi'' fa'') |
    re''8( sib') la' la' sol' sol' |
    fa''8-> fa'' fa'' fa'' fa'' fa'' |
    fa''8-> fa'' fa'' fa'' fa'' fa'' |
    la''16-> la'' la'' la'' la''2:16 |
    sol''16-> sol'' sol'' sol'' sol''2:16 |
    fa''4
  }
  \tag #'violino2 {
    sol'8\p sol' fa' fa' mi' mi' |
    mi'( fa') fa'4 r |
    r16 la'( sold' la') sol'( fa' mi' fa') sib'( sol' do'' sib') |
    r la'( sol' fa') mi'( re' dod' re') mib'( do' fa' mib') |
    r re'( fa' re') sib( re' fa' sib') la'( fa' do' fa') |
    mi'!8 sib'4 mi''8 sol'' sol'' |
    fa'' do'' do'' do'' la' la' |
    sib' r sib' r sib' r |
    la' r do'' r la' r |
    sib' r sib' r sib' r |
    la' r do'' r la' r |
    r fad'( sol' la' sib' re'') |
    fa'!4( mi' fa') |
    fa'8( sol') fa' fa' mi' mi' |
    do''-> do'' do'' do'' do'' do'' |
    re''-> re'' re'' re'' re'' re'' |
    fa''16-> fa'' fa'' fa'' fa''2:16 |
    fa''16 fa'' fa'' fa'' mi''-> mi'' mi'' mi'' mi''4:16 |
    fa''4
  }
>> <do''' la'>4\f q |
sib''16 la'' sol'' la'' sol'' fa'' mi'' fa'' mi'' re'' do'' sib' |
la' do'' sib' la' sib' do'' re'' mi'' fa'' sol'' la'' sol'' |
fa'' mi'' re'' do'' re''8 sib16 la sib8 si |
do'2 r4\fermata |
<fa' la' fa''>8 do''~ do''16 fa'' la'' fa'' do'''( la'') re'''( si'') |
do'''8 do''~ do''16 mi'' sol'' mi'' sib''!( sol'') do'''( sib'') |
la''8 fa''~ fa''16 do'' fa'' la'' do'''( la'') si''( re''') |
do'''8 do''~ do''16 mi'' sol'' la'' sib''!( re''') do'''( sib'') |
<<
  \tag #'violino1 {
    la''16 do''' la'' fa'' sol''2\trill |
  }
  \tag #'violino2 {
    la''8 fa''4 fa''8 mi''4\trill |
  }
>>
fa''8 do'' <fa' la>4 r |
<<
  \tag #'violino1 {
    r8 fa''\p fa'' fa'' fa'' fa'' |
    r mib'' mib'' mib'' mib'' mib'' |
    r mib'' mib'' mib'' mib'' mib'' |
    r re'' re'' re'' re'' re'' |
    r sol'' sol'' sol'' mib'' mib'' |
    \ru#3 { re''16( sib') fa''-. fa''-. } |
    \ru#3 { re''16( la') fa''-. fa''-. } |
    \ru#5 { re''16( si') mi''-. mi''-. } si'( mi') re''-. re''-. |
    do''( la') mi''-. mi''-. mi''( do'') la''-. la''-. do'''( la'') mi''-. mi''-. |
    fa''8 fa'' mi'' re'' do'' si' |
    la'16 la'' la'' la'' la''2:16 |
    la''8 re''' do''' si'' la'' sold'' |
    la''4
  }
  \tag #'violino2 {
    r8 re''\p re'' re'' re'' re'' |
    r do'' do'' do'' do'' do'' |
    r do'' do'' do'' do'' do'' |
    r sib' sib' sib' sib' sib' |
    r sib' sib' sib' la' la' |
    sib' r fa' r fa' r |
    fa' r fa' r fa' r |
    mi' r mi' r si' r |
    si' r si' r mi' r |
    mi' r la' r do'' r |
    la' re'' do'' si' la' sold' |
    la'16 re'' re'' re'' re'' re'' re'' re'' dod'' sol'' sol'' sol'' |
    fa''8 fa'' mi'' re'' do'' si' |
    la'4
  }
>> r8 do''-.\f mib''( la') |
do''( sib') r sib'-. re''( sol') |
la'16 do'' sib' la' sib' do'' re'' mi'' fa'' sol'' do'' mib'' |
re''( mi''! fa'') la'-. sib'( do'' re'') fad'-. sol'( la' re') fa'-. |
mi' sol' si' do'' <<
  \tag #'violino1 {
    do'4 r8 do'\p |
  }
  \tag #'violino2 {
    do'8 re'(\p do' sib!) |
  }
>>
