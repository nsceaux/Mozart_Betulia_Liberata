\clef "treble" \transposition fa
r8 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 sol'4 | mi' }
  { do''2 sol'4 | mi' }
>> <<
  \tag #'(corno1 corni) { do''8 do'' do'' do'' | do''4 }
  \tag #'(corno2 corni) { do'8 do' do' do' | do'4 }
>> r4 \twoVoices #'(corno1 corno2 corni) << { sol'4 } { sol' } >> <<
  \tag #'(corno1 corni) { do''4 do''8 do'' do''4 | }
  \tag #'(corno2 corni) { mi'4 mi'8 do' do'4 | }
>>
r4 r \twoVoices #'(corno1 corno2 corni) << { sol'4 } { sol' } >> |
<<
  \tag #'(corno1 corni) { do''2.~ | do''4 }
  \tag #'(corno2 corni) { do'2.~ | do'4 }
>> r4 \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> |
<<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
R2.*4 |
<>\f <<
  \tag #'(corno1 corni) { re''2. }
  \tag #'(corno2 corni) { sol' }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 re''2 | }
  { do''2 sol'4 | }
>>
<<
  \tag #'(corno1 corni) { do''8 mi'' do''4 }
  \tag #'(corno2 corni) { mi'8 sol' do'4 }
>> r4 |
R2.*5 |
r8 <>\f <<
  \tag #'(corno1 corni) { do''8 do'' do'' do'' do'' | do''4 }
  \tag #'(corno2 corni) { do'8 do' do' do' do' | do'4 }
>> r4 r |
R2.*4 |
\tag #'corni <>^"a 2." re''2\f re''8 re'' |
re''4\p r r |
re''2.\f |
re''4\p r r |
r r8 sol'\f sol' sol' |
sol'4 r r |
R2.*14 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2. |
    re''4 sol'2 |
    re''4 re''8 re'' re'' re'' |
    re''4 sol'8 sol' sol' sol' |
    re''4 re''8 re'' re'' re'' |
    re''4 re''2 |
    sol'4 }
  { re''2. |
    sol'2. |
    re''4 re''8 re'' re'' re'' |
    sol'4 sol'8 sol' sol' sol' |
    re''4 re''8 re'' re'' re'' |
    sol'4 re''2 |
    sol'4 }
  { s2.\p | s\f }
>> r4 r |
R2.*4 | \allowPageTurn
R2.*5 |
r8 <>\f <<
  \tag #'(corno1 corni) { do''8 do'' do'' do'' do'' | do''4 }
  \tag #'(corno2 corni) { do'8 do' do' do' do' | do'4 }
>> r4 r |
R2.*4 |
<>\f <<
  \tag #'(corno1 corni) { re''2 re''8 re'' | }
  \tag #'(corno2 corni) { sol'2 sol'8 sol' | }
>>
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 }
  { sol' }
>> r4 r |
r <>\f <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>>
<>\p \twoVoices #'(corno1 corno2 corni) << sol'4 sol' >> r r |
r r8 <>\f <<
  \tag #'(corno1 corni) { do''8 do'' do'' | do''4 }
  \tag #'(corno2 corni) { do'8 do' do' | do'4 }
>> r4 r |
R2.*4 |
<>\p <<
  \tag #'(corno1 corni) { sol'2.~ | sol'~ | sol' | do'' | }
  \tag #'(corno2 corni) { sol2.~ | sol~ | sol | do' | }
>>
R2.*6 |
<>\p \twoVoices #'(corno1 corno2 corni) << sol'2. sol' >> |
<>\f <<
  \tag #'(corno1 corni) {
    do''4 s sol'' |
    fa''4 re''2 |
    do''4 re'' mi'' |
    do''2. |
  }
  \tag #'(corno2 corni) {
    mi'4 s mi'' |
    re''4 sol'2 |
    mi'4 sol' do'' |
    do'2. |
  }
  { s4 r s }
>>
\twoVoices #'(corno1 corno2 corni) << sol'2 sol' >> r4\fermata |
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 do''8 mi''4 re''8 | }
  { mi'4 do''8 do''4 re''8 | }
>>
<<
  \tag #'(corno1 corni) { re''4 re''8 re'' re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol'8 sol' sol' sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''4 do''8 mi''4 re''8 | }
  { do''4 do'8 do''4 re''8 | }
>>
<<
  \tag #'(corno1 corni) { re''4 re''8 re'' re'' re'' | }
  \tag #'(corno2 corni) { sol'4 sol'8 sol' sol' sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 re''2 | }
  { do'4 do'' sol' | }
>>
<<
  \tag #'(corno1 corni) { do''4 do'' }
  \tag #'(corno2 corni) { mi' mi' }
>> r4 |
R2.*14 |
r4 r <>\f <<
  \tag #'(corno1 corni) { re''4 | do''2. | do''4 }
  \tag #'(corno2 corni) { sol'4 | do'2. | do'4 }
>> r4 \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> |
<<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
