\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (flauti #:score-template "score-flauti")
   (corni #:score-template "score-corni"
          #:tag-global () #:instrument "Corni in F."))
