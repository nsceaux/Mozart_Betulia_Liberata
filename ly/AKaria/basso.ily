\clef "bass" r8 |
fa fa fa fa do do |
la, la, fa, fa la fa |
sib sib sib sib do' do' |
fa fa fa fa fa fa |
sib,4 do do |
fa8 fa fa fa la, la, |
sib, sib, sib, sib, sib, si, |
do4 do' r |
fa2(\p do4) |
re2( la,4) |
sib,2( fa,4) |
sol, r r |
do8\f do do do do do |
fa re sib, sib, do do |
fa4 r8 fa\p la do' |
fa fa fa fa do do |
la, la, fa, fa la fa |
sib4 sib do' |
fa fa la |
sib do' do |
fa8\f fa fa fa fa fa |
fa\p fa fa fa fa fa |
mi mi mi mi mi mi |
re re re re sol sol |
do do do re re mi |
fa fa fa fa fa fad |
sol\f sol sol sol sol sol |
sol4\p r r |
r r8 sol\f sol sol |
sol4\p r r |
r8 do\f do do do do |
do'2(\p sol4) |
la2( mi4) |
fa8 fa fa fa fa fa |
mi mi mi mi mi mi |
re re re re sol sol |
do r re r mi r |
re r sol r sol, r |
do r re r mi r |
re r sol r sol, r |
do mi mi mi mi mi |
fa fa sol sol la la |
fa fa sol sol sol sol |
mi-> mi mi mi mi mi |
fa-> fa fa fa fa fa |
sol sol sol sol sol sol |
sol, sol, sol, sol, sol, sol, |
do\f do' do' do' do' do' |
si si si sol la si |
do' do' do' do' do' do' |
si si si sol la si |
do' do sol sol sol, sol, |
do4 r8 do'-.\p re'-. mib'-. |
re4 re re |
sol sol sol |
fad fad fad |
sol do r |
fa!8 fa fa fa do do |
la, la, fa, fa fa la |
sib4 sib do' |
fa fa la |
sib do' do |
fa8\f fa fa fa fa fa |
fa\p fa mib mib mib mib |
re re re re re re |
la, la, la, la, la, la, |
sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, si, |
do\f do do do do do |
do4\p r r |
r r8 do\f do do |
do4\p r r |
r8 fa\f fa fa fa fa |
fa2(\p do4) |
re2( la,4) |
sib,2( fa,4) |
sol,4 sol mi |
fa fa, fa |
sol8 r mi r do r |
fa r la r fa r |
sol r mi r do r |
fa r la r fa r |
sib4 sib, sol |
do' sib la |
sib do' do |
la8-> la la la la la |
sib-> sib sib sib sib sib |
do' do' do' do' do' do' |
do do do do do do |
fa\f fa la la fad fad |
sol sol mi sol do mi |
fa! fa sol sol la la |
la la, sib, sib16 la sib8 si |
do'2 r4\fermata |
fa8 fa fa fa fa fa |
mi mi mi mi mi mi |
fa fa fa fa fa fa |
mi mi mi mi mi mi |
fa re' sib sib do' do |
fa4 fa, r |
sib4\p r r |
sib r r |
la r r |
sib r r |
mib mib fa |
sib,8 r sib r sib r |
la r la r la r sold r sold r sold r |
sold r sold r sold r |
la r la r la r |
re r mi r mi r |
fa r fa r mi r |
re r mi mi mi mi |
la,4 la\f fad |
sol8 sol sol sol mi mi |
fa! fa sol sol la la |
sib sib sib sib sib si |
do'4 do8 sib!(\p la sol) |
