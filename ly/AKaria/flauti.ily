\clef "treble"
<<
  \tag #'flauto1 {
    do''8 |
    do''( fa'') la''4. sib''16( sol'') |
    fa''8( la'') do'''4. do'''8 |
    dod'''( re''') sol''4. la''16( sib'') |
    la''8-. fa''16( la'') do'''!8-. do'''-. do'''4 |
    re'''8( sib'') la''( sol'') do'''( mi'') |
    fa''4 do'''2 |
    re'''2 fa''4 |
    mi'' do''' r |
    la''2\p do'''8( mi'') |
    fa''2 la''8( do'') |
    re''2 fa''8( la') |
    sib'4
  }
  \tag #'flauto2 {
    r8 |
    r4 r r8 do'' |
    do''( fa'') la''4. la''8 |
    fa''4. mi''16( re'') mi''8-. fa''16( sol'') |
    fa''8-. do''16( fa'') la''8-. la''-. la''4 |
    sib''8( re'') do''( sib') la'( sol') |
    fa'4 fa''( mib'') |
    re''2. |
    do''4 mi'' r |
    do''2.\p |
    la' |
    fa' |
    mi'4
  }
>> sib''4\f sib'' |
<<
  \tag #'flauto1 {
    sib''2. |
    la''4 sol''2\trill |
    fa''8 do'' la'4 r8 do''\p |
    do''( fa'') la''4. sib''16( sol'') |
    fa''8( la'') do'''4
  }
  \tag #'flauto2 {
    mi''2. |
    fa''2 mi''4 |
    fa''8 la'' fa''4 r |
    r r r8 do''\p |
    do''( fa'') la''4
  }
>> r4 |
R2.*3 | \allowPageTurn
r8 <<
  \tag #'flauto1 {
    la'4\f sib'16 do'' re'' mi'' fa'' sol'' |
    la''4
  }
  \tag #'flauto2 {
    fa'4\f sol'16 la' sib' do'' re'' mi'' |
    fa''4
  }
>> r4 r |
R2.*4 | \allowPageTurn
<<
  \tag #'flauto1 {
    re''4\f r8 sol'' sol'' sol'' |
    sol''4\p r r |
    r r8 sol''\f sol'' sol'' |
    sol''4\p r r |
    r8 mi''4\f fa''16 sol'' la'' sol'' la'' si'' |
    do'''4
  }
  \tag #'flauto2 {
    si'4\f r8 si' si'-. re''16( si') |
    sol'4\p r r |
    r r8 sol'\f sol' sol' |
    sol'4\p r r |
    r8 do''4\f re''16 mi'' fa'' mi'' fa'' re'' |
    mi''4
  }
>> r4 r |
R2.*13 | \allowPageTurn
r4 <<
  \tag #'flauto1 {
    do'''2\p~ |
    do'''2 si''4 |
    do'''8\f
  }
  \tag #'flauto2 {
    mi''2\p |
    re''2. |
    do''8\f
  }
>> sol''8~ sol''16 do'''( si'' la'') sol''( la'') mi''( fad'') |
sol''8 sol''~ sol''16 la'' re''( mi'') fa''!( re'') sol'' fa'' |
mi''8 sol''~ sol''16 do'''( si'' la'') sol''( la'') mi''( fad'') |
sol''8 sol''~ sol''16 la'' re''( mi'') fa''!( re'') sol'' fa'' |
<<
  \tag #'flauto1 {
    mi''8 do''' re'''2 |
    do'''4 r r |
    R2.*3 | \allowPageTurn
    r4 r r8 do''\p |
    do''( fa'') la''4. sib''16( sol'') |
    fa''8( la'') do'''8-.
  }
  \tag #'flauto2 {
    mi''4 do''' si'' |
    do''' r r |
    R2.*4 |
    r4 r r8 do''\p |
    do''( fa'') la''8-.
  }
>> r8 r4 |
R2.*3 | \allowPageTurn
r8 <>\f <<
  \tag #'flauto1 {
    la'4 sib'16 do'' re'' mi'' fa'' sol'' |
    la''4
  }
  \tag #'flauto2 {
    fa'4 sol'16 la' sib' do'' re'' mi'' |
    fa''4
  }
>> r4 r |
R2.*4 | \allowPageTurn
<>\f <<
  \tag #'flauto1 {
    mi''4 r8 do''' do''' do''' |
    do'''4\p r r |
    r r8 do'''\f do''' do''' |
    do'''4\p r r |
    r8 fa''4\f sol''16 la'' sib'' la'' sib'' sol'' |
    la''4
  }
  \tag #'flauto2 {
    do''4 r8 mi'' mi''-. sol''16( mi'') |
    do''4\p r r |
    r r8 do''\f do'' do'' |
    do''4\p r r |
    r8 la'4\f sib'16 do'' re'' do'' re'' mi'' |
    fa''4
  }
>> r4 r |
R2.*13 | \allowPageTurn
r4 <>\p <<
  \tag #'flauto1 {
    la''2 |
    sol''2. |
    fa''4 do'''2\f |
    sib''2. |
    la''4 mi'' fa'' |
    do''' re'''2 |
    la''
  }
  \tag #'flauto2 {
    fa''2~ |
    fa''4 mi''2 |
    fa''4 la''2\f |
    sol''2. |
    fa''4 sib'' la'' |
    fa''2. |
    fa''2
  }
>> r4\fermata |
fa''8 do''~ do''16 fa'' la'' fa'' do'''( la'') re'''( si'') |
do'''8 do''~ do''16 mi'' sol'' mi'' sib''!( sol'') do'''( sib'') |
la''8 fa''~ fa''16 do'' fa'' la'' do'''( la'') si''( re''') |
do'''8 do''~ do''16 mi'' sol'' la'' sib''!( re''') do'''( sib'') |
<<
  \tag #'flauto1 {
    la''8( fa'') sol''2\trill |
    fa''8 do''' la''4
  }
  \tag #'flauto2 {
    la''8 fa''4 fa''8 mi''4\trill |
    fa''8 la'' fa''4
  }
>> r4 |
R2.*13 | \allowPageTurn
r4 r8 do'''-.\f mib'''( la'') |
do'''( sib'') r sib''-. re'''( sol'') |
la''4 <<
  \tag #'flauto1 {
    sib''8( sol'') fa''4~ |
    fa'' re'''2 |
    do'''4 do''' r8 do''\p |
  }
  \tag #'flauto2 {
    mi''4 do''' |
    re''' sol''4. fa''8 |
    mi''4 mi'' r |
  }
>>
