\clef "alto/treble" r8 |
R2.*14 |
r4 r r8 do' |
do'[ fa'] la'4. sib'16[ sol'] |
fa'8[ la'] do''4 r8 fa' |
fa'[ sol'] sol'4. la'16[ sib'] |
la'8[ sib'] do''4 r8 fa' |
re'8[ sib'] la'[ sol'] do''[ mi'] |
fa'4 fa' r8 r16 do'' la'8. fa'16 do''4. la'8 |
la'[ sol'] sol'4 r8 do'' |
do''[ si'] si'4. fa'8 |
mi'4 r r8 do'' |
do''4\melisma \grace sib'!16 la'8[ \grace la'16 sol'8]\melismaEnd \grace mi'16 re'8 do' |
si4 r r8 sol |
sol'4 fa' mi' |
mi'8[ fa'] fa'4 r8 sol |
fa'4 mi' re' |
re'8[ mi'] mi'4 r |
r r r8 sol' |
mi'4. do'8 sol' sib'! |
la'4 la' r8 la' |
sol'4 do''8[ si'] la'[ sol'] |
fa'2 re'4 |
mi'16[\melisma fa' sol' mi'] fa'[ sol' la' fa'] sol'[ si' do'' sol'] |
fa'[ mi' re' do'] si[ do' re' mi'] fa'8 r |
mi'16[ fa' sol' mi'] fa'[ sol' la' fa'] sol'[ si' do'' sol'] |
fa'[ mi' re' do'] si[ do' re' mi'] fa'8 r |
mi'8[ sol']\melismaEnd sol'4. sib'!8 |
la'4( si') do'' |
\grace si'16 la'8[ sol'16 fa'] mi'4 re' |
sol'2 mi'8[ do'] |
la'2 fa'8 re' |
<< { \voiceOne sol'2. \oneVoice } \new Voice { \voiceTwo sol2. } >> |
re'2\startTrillSpan~ re'8.\stopTrillSpan do'16 |
do'4 r r |
R2.*4 |
r4 r r8 sol' |
fad'4 la'4. do''8 |
sib'[ sol'] re'4. sib'8 |
la'4 la'4. do''8 |
sib'[ la'] sol'4 r8 do' |
do'[ fa'] la'4. sib'16[ sol'] |
fa'8[ la'] do''4 r8 fa' |
sol'4 sol'4. sib'8 |
la'[ sib'] do''4 r8 fa' |
re'8[ sib'] la'[ sol'] do''[ mi'] |
fa'4 fa' r8 r16 fa' fa'4. \grace la'32 sol'16[ fa'] fa'8 fa' |
fa'4.( \grace la'16 sol'8) fa' fa' |
do''4.( la'8) fa' mib' |
re'2 fad'4 |
sol'4.( sib'8) sol' fa'! |
mi'!4 r r8 do' |
do''4 sib' la' |
la'8[ sib'] sib'4 r8 do' |
sib'4 la' sol' |
sol'8[ la'] la'4 r |
r r r8 do' |
fa'2 la'8 do' |
re'4 re' r8 fa' |
mi'4 sol' sib' |
la'2 do''16[ la' sol' fa'] |
mi'[\melisma re' do' re'] mi'[ fa' sol' la'] \grace do''16 sib'8[ la'16 sol'] |
la'[ sol' fa' mi'] fa'[ sol' la' sib'] do''8 r |
mi'16[ re' do' re'] mi'[ fa' sol' la'] \grace do''16 sib'8[ la'16 sol'] |
la'[ sol' fa' mi'] fa'[ sol' la' sib'] do''[ la' fa' mib'] |
re'8[ fad']\melismaEnd sol'4 r8 sib' |
fa'!4\melisma mi' fa' |
re'8[ sib'16 sol']\melismaEnd fa'4 mi' |
fa'2 la'8([ do'']) |
fa'2 sib'8 re'' |
<< { \voiceOne do''2. \oneVoice } \new Voice { \voiceTwo do'2. } >> |
sol'2\startTrillSpan~ sol'8.\stopTrillSpan fa'16 |
fa'4 r r |
R2.*2 |
r4 r r8 fa' |
fa'2 sol'8.\trill\fermata fa'16 |
fa'4 r r |
R2.*4 |
r4 r r8 fa' |
sib'[ fa'] fa'4. sol'8 |
fa'8[ mib'] mib'4. mib'8 |
do''8[ la'] fa'4. mib'8 |
re'[ fa'] fa'4. sib'8 |
sib'[ sol'] sol'4. mib'8 |
\grace mib'16 re'8[ do'16 sib] sib4 r8 re' |
re'4 re'4. fa'8 |
mi'!4 mi' r8 mi' |
si'[ sold'] mi'4. re'8 |
do'4 r la' |
fa'4 mi'8[ re'] do'[ si] |
la4 r la' |
fa'8[ re''] do''[ si'] la'[ sold'] |
la'4 r r |
R2.*3 |
r4 r r8 do' |
