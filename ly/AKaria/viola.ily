\clef "alto" r8 |
la8 la do' do' mi' mi' |
fa' fa' fa' la do' fa' |
sib sib sib sib do' do' |
do'-. la16( do') fa'8 fa' fa' fa' |
sib4 r8 re'( do' sib) |
la16( sib do') fa'-. fa'16( sol') la'-. la'-. la'( sib') do''-. do''-. |
sib'8 sib sib re' re' re' |
sol4 <sol mi'>4 r |
r fa\p sol |
la fa'2 |
r16 fa( mi fa) sol( fa mi fa) fa( la do' la) |
re4 r r |
<mi' sol'>8 q q q q q |
fa' fa' fa' re' do' sib |
la la' fa'4 r |
la8\p la do' do' mi mi |
fa fa fa do'' fa' r |
sib4 sib do' |
fa' fa la |
sib do' do' |
fa'8\f fa' fa' fa' fa' fa' |
fa'\p fa' fa' fa' fa' fa' |
mi' mi' mi' mi' mi' mi' |
re' re' re' re' re' si |
do' do' do' re' re' mi' |
fa' fa' fa' fa' fa' fad' |
sol'\f sol' sol' sol' sol' sol' |
sol'4\p r r |
r r8 sol'\f sol' sol' |
sol'4\p r r |
r8 <>\f <<
  { mi'4 fa'16 sol' la' sol' la' si' | do''4 } \\
  { do'4 re'16 mi' fa' mi' fa' re' | mi'4 }
>> r4 r |
la2(\p mi4) |
fa8 fa' fa' fa' fa' fa' |
mi' mi' mi' mi' mi' mi' |
re' re' re' re' sol' sol' |
do' r re' r mi' r |
re' r sol' r sol r |
do' r re' r mi' r |
re' r sol' r sol r |
do' mi' mi' mi' mi' mi' |
fa' fa' sol' sol' la' la' |
fa' fa' sol' sol' sol' sol' |
mi'-> mi' mi' mi' mi' mi' |
fa'-> fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' |
sol sol sol sol sol sol |
do'\f mi' mi' mi' mi' mi' |
re' re' re' si' do'' re'' |
sol' mi' mi' mi' mi' mi' |
re' re' re' si' do'' re'' |
sol' mi' do' do'' si' si' |
do''4 r8 do'-.\p re'-. mib'-. |
re'8 la' la' la' la' la' |
r sol' re' re' re' re' |
fad' fad' fad' fad' fad' fad' |
r re' mi'!( sib') sol'( mi') |
fa' la do' do' mi' mi' |
fa' do'4 la' fa'8 |
sib4 sib do' |
fa' fa la |
sib do' do |
fa8\f fa' fa' fa' fa' fa' |
fa'\p fa' mib' mib' mib' mib' |
re' re' re' re' re' re' |
fa' fa' fa' fa' fa' fa' |
fa' sib sib sib sib sib |
sib sib sib sib sib si |
do'\f do' do' do' do' do' |
do'4\p r r |
r r8 do'\f do' do' |
do'4\p r r |
r8 <>\f <<
  { fa'4 sol'16 la' sib' la' sib' sol' | la'4 } \\
  { la4 sib16 do' re' do' re' mi' | fa'4 }
>> r4 r |
re'2(\p la4) |
sib2( fa4) |
sol4 sol' mi' |
fa' fa fa' |
sol'8 r mi' r do' r |
fa' r la' r fa' r |
sol' r mi' r do' r |
fa' r la' r fa' r |
sib'4 sib sol' |
la' sol' do' |
sib do' do' |
la8-> la la la la la |
sib-> sib sib sib sib sib |
do' do' do' do' do' do' |
do' do' do' do' do' do' |
do'\f do' fad' fad' <re' la'> q |
re' re' sol' sib' mi' sol' |
do' fa' mi' sib' la' do'' |
do' fa'4 sib16 la sib8 si |
do'2 r4\fermata |
do'8 la' la' la' la' la' |
sol' sol' sol' sol' sol' sol' |
do' la' la' la' la' la' |
sol' sol' sol' sol' sol' sol' |
fa'4. re'8 do' sib |
la la' fa'4 r |
r8 sib\p sib sib sib sib |
r sol' sol' sol' sol' sol' |
r fa' fa' fa' fa' fa' |
r fa' fa' fa' fa' fa' |
r mib' mib' mib' do' do' |
sib r re' r re' r |
re' r re' r re' r |
re' r si r mi' r |
mi' r mi' r si r |
la r do' r mi' r |
r4 r8 mi' mi' re' |
do'16 fa' fa' fa' fa' la' la' la' la'8( dod') |
re'4 r8 mi' mi' mi' |
mi' mi' mi' la'4\f la'8 |
re' re' re' re' sol' mi' |
do' fa'4 fa' fa'8~ |
fa' re'4 re' re'8 |
sol4 <do sol mi'> r8 mi'8*3/4(\p s32) |
