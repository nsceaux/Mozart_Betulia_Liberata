\piecePartSpecs
#`((violino1 #:score-template "score-voix" #:indent 0)
   (violino2 #:score-template "score-voix" #:indent 0)
   (viola #:score-template "score-voix" #:indent 0)
   (basso #:score-template "score-voix" #:indent 0)
   (silence #:on-the-fly-markup ,#{\markup\null#}))
