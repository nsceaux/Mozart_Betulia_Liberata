\clef "bass" sol2~ |
sol1~ |
sol~ |
sol |
sold |
la |
si,2 r8 r16 si, si,4 |
do1 |
sold |
la2 mi~ |
mi fa~ |
fa dod |
r la,~ |
la, re~ |
re1 |
red2 r4 mi |
la,( si,) do8-. r r4 |
do2 fa~ |
fa dod~ |
dod re |
la1~ | \allowPageTurn
la~ |
la2 sib~ |
sib1~ |
sib2 r4 do' |
si!1~ |
si2 do' |
lad1 |
si |
dod2 re~ |
re1~ |
re |
r4 mi\f do!2 |
mi1 |
fa! |
r4 sol\f fad2\p |
re1 |
sol |
la,~ |
la,2 sib,~ |
sib, sold,~ |
sold,1~ |
sold,~ |
sold,2 la,~ |
la, r4 r8 si,-.\p |
mi([ sol si]) mi-. sol([ si mi']) si,-. |
re[ fa! si] si,-. do\f r r r16 do |
do1~ |
do2 la,~ |
la, re~ |
re la,~ |
la, sib,4 la, |
sol,1~ |
sol,2 dod~ |
dod1 |
re |
sold2 la~ |
la1~ |
la2 r4 r8 si, |
sol8 mi red si, mi sol red si, |
mi sol la si mi4 re! |
do r do2~ |
do1 |
fa8 fa mi do fa la mi do |
fa4 r r fa,16 fa la re |
dod4 r la,2~ |
la, re~ |
re sol,~ |
sol, do!8 do' si sol |
do' mi si, sol, do4 r |
do1 |
fa |
sib,2 r4 do |
si,!1~ |
si,2 mi~ |
mi1~ | \allowPageTurn
mi~ |
mi~ |
mi2 la,~ |
la, si,~ |
si, do~ |
do1 |
fa2 sib,~ |
sib,1~ |
sib,2 mib |
r4 fa mi!2 |
do4 mi8 sol sib4 sol8 mi |
dod1~ |
dod2 re~ |
re la, |
sib,8 sib la fa r sib la fa |
sib,4 r sib lab |
sol r si,!2~ |
si,1~ |
si,2 do |
re1 |
sol2 dod~ |
dod re |
sold1~ |
sold |
la2 r4 << si4 \\ si, >> |
do1 |
r4 re mib2~ |
mib1~ |
mib |
r8 lab\p lab lab r lab lab lab |
r lab, do mib lab16 do'\f sib lab sol lab sol fa |
mi!4 r r2 |
do1~ |
do |
fa |
si,!1~ |
si,2 do~ |
do1 |
sib,2 si,~ |
si,1 |
do2 r4 re |
sol1 |
do1~ |
do~ |
do2 fa |
re do |
dod1 |
re2 r4 mi |
red1 |
mi2 r4 fad |
sol1 |
mi~ |
mi2 fa~ |
fa1 |
sib,1 |
si,!2 do~ |
do1~ |
do~ |
do2 sib,~ |
sib, dod~ |
dod1~ |
dod2 re |
r4 mi la,2 |
