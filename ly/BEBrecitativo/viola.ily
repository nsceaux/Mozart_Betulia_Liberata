\clef "alto" sol'2~ |
sol'1 |
mi'2 fad' |
sol'1 |
re'2 mi'~ |
mi'1 |
re'2 r8 r16 si si4 |
sol1 |
si |
la2 mi' |
do'1 |
fa'2 dod' |
do'8 r r4 la2~ |
la re~ |
re1 |
red2 r4 mi~ |
mi( sol) sol8-. r8 r4 |
do'1~ |
do'2 la~ |
la1 |
fa'1~ |
fa' |
do'2 sib~ |
sib do'~ |
do' r4 do' |
si!1~ |
si2 do' |
dod'1 |
si |
dod'2 re'~ |
re'1~ |
re'2 r |
r4 mi'\f do'!2 |
R1*2 | \allowPageTurn
r4 sol'\f fad'2\p |
re'1 |
re'8( sib sol re) mib2( |
fa!1)~ |
fa~ |
fa2 sold~ |
sold1~ |
sold2 mi |
si! la |
fad' r4 r8 si-.\p |
mi'([ sol' si']) mi'-. sol'([ si' mi'']) si-. |
re'[ fa'! si'] si-. do'8\f r r r16 do' |
do'1~ |
do'2 la~ |
la1~ |
la2 fa'~ |
fa' fa'4 re' |
re'1 |
sib2 la~ |
la1 |
la2 si! |
mi'1~ |
mi'2 fad' |
r2 r4 r8 si |
sol' mi' red' si mi' sol' red' si |
mi' sol' la' si' mi'4 re'! |
do'4 r do'2~ do'1 |
fa'8 fa' mi' do' fa' la' mi' do' |
fa'4 r r fa16 fa' la' re' |
la4 r la2~ |
la1~ |
la2 si!2~ |
si2 do'!8 do'' si' sol' |
do'' mi' si sol do'4 r |
do'1 |
do'2 fa' |
fa'4 r r2 |
R1*2 | \allowPageTurn
r2 mi~ |
mi1~ |
mi |
si2 la |
mi' re' |
fa' mi'~ |
mi'1 |
do'2 re'~ |
re' sib~ |
sib1 |
r4 fa' mi'!2 |
do'4 mi'8 sol' sib'4 sol'8 mi'16 mi'32( fa') |
sol'1 |
la~ |
la2 fa |
sib4( do'8) r sib4( do'8) r |
sib4 r sib do' |
re' r fa'2~ |
fa'1~ |
fa'2 mib' |
la'!1 |
sol'2 mi'!~ |
mi' re' |
si!1 |
mi'~ |
mi'4 red' r2 |
R1 | \allowPageTurn
r2 mib'~ |
mib' sol~ |
sol r |
lab1\p~ |
lab16( do') mib4( sol8) lab16 do'\f sib lab sol lab sol fa |
sol4 r r2 |
do'1 |
sol |
fa2 do' |
re'1~ |
re'2 do' |
la!1 |
sib2 si~ |
si fa'! |
mi' r4 re' |
sol r r2 |
R1*21 |
