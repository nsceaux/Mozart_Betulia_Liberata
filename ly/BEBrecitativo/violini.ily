\clef "treble"
<<
  \tag #'violino1 {
    re''2~ |
    re''1 |
    do'' |
    si' |
    si'2 re'' |
    do''1 |
    re''2 r8 r16 re'' re''4 |
    do''1 |
    re'' |
    do'' |
    sib'2 la' |
    \ru#2 { do''16\(( la') do''( la')\) } \ru#2 { mi''\(( la') mi''( la')\) } |
    mi''8 r r4 mi'2~ |
    mi' fa'16( re' fa' re') fa'( re' fa' re') |
    la'( fa' la' fa') la'( fa' la' fa') sold'( si sold' si) sold'( si sold' si) |
    la'( do' la' fad') do''8-. r r4 sold' |
    la' sol'!16( re' sol fa'!) mi'8-. r r4 |
    sol'2 la'~ |
    la'1 |
    sol'2 fa' |
    mib''1~ |
    mib''~ |
    mib''2 re''~ |
    re'' sol'~ |
    sol' r4 sol' |
    sol'1 |
    re''2 mi''~ |
    mi''1 |
    re'' |
    mi''2 fad''~ |
    fad''1 |
    si'2
  }
  \tag #'violino2 {
    si'2( |
    re''1) |
    do'' |
    re' |
    mi'2 si' |
    la'1 |
    sol'2 r8 r16 fa'! fa'4 |
    mi'1 |
    mi' |
    mi'2 sol' |
    sol' fa' |
    \ru#2 { la'16\(( fa') la'( fa')\) } \ru#2 { la'16\(( mi') la'( mi')\) } |
    la'8 r r4 dod'2~ |
    dod' re'16( la re' la) re'( la re' la) |
    fa'( re' fa' re') fa'( re' fa' re') fa'( sold fa' sold) fa'( sold fa' sold) |
    fad'( la fad' do') la'8-. r r4 si |
    do'( re') do'8-. r r4 |
    mi'2 fa'~ |
    fa' mi'~ |
    mi' re' |
    do'1~ |
    do' |
    fa'~ |
    fa'2 mi'!~ |
    mi' r4 mi' |
    re'1 |
    sol' |
    fad'~ |
    fad' |
    la'1~ |
    la' |
    sold'2
  }
>> r2 |
r4 mi'\f do'!2 |
R1*2 | \allowPageTurn
r4 sol'\f fad'2\p |
<<
  \tag #'violino1 {
    la'1 |
    sib'8( sol' re' sib) sib2( |
    mib'1)~ |
    mib'2 re'~ |
    re' mi'!~ |
    mi'1 |
    re' |
    re''2 do''~ |
    do''
  }
  \tag #'violino2 {
    fad'1 |
    sol'8( re' sib sol) sol2( |
    do'1)~ |
    do'2 sib~ |
    sib si~ |
    si1~ |
    si |
    mi' |
    red'2
  }
>> r4 r8 si-.\p |
mi'([ sol' si']) mi'-. sol'([ si' mi'']) si8-. |
re'8([ fa'! si']) si-. <sol mi' do''>8\f r r r16 <<
  \tag #'violino1 {
    sol'16 |
    sol'1~ |
    sol' |
    mi'2 fa'~ |
    fa' mib''~ |
    mib'' re''4 fad' |
    sol'1~ |
    sol'2 la'~ |
    la' sol' |
    fa'! sold' |
    re'' do''~ |
    do''1 |
  }
  \tag #'violino2 {
    mi'16 |
    mi'1~ |
    mi' |
    dod'2 re'~ |
    re' do'!~ |
    do' sib4 do' |
    sib1 |
    re'2 mi'!~ |
    mi'1 |
    re'2 mi' |
    si'! la'~ |
    la' red'' |
  }
>>
r2 r4 r8 si |
<<
  \tag #'violino1 {
    mi'4 fad'8. sol'32 la' sol'8 mi'' fad''8. sol''32 la'' |
    sol''16 mi'' red'' mi'' fad'8 red'' mi' sol'' re'! fa''! |
    <mi'' do'' sol' do'>4 r sol'2~ |
    sol'1 |
    la'8 fa'' sol''8. la''32 sib'' la''8 fa' sol'8. la'32 sib' |
    la'4
  }
  \tag #'violino2 {
    si2:16 si:16 |
    si4 do'8 si16 la sol8 si'4 si'8 |
    <do'' mi' sol>4 r mi'2~ |
    mi'1 |
    fa'16 do' do' do' do'4:16 do'8 do' do' do' |
    do'4
  }
>> r4 r <fa' do'' la''>16-. fa'-. la'-. re'-. |
<<
  \tag #'violino1 {
    dod'4 r mi'2~ |
    mi' fa'~ |
    fa'1~ |
    fa'2 r8 do''! re''8. mi''32 fa'' |
    mi''8 do' re'8. mi'32 fa' mi'4 r |
    sol'1 |
    la'2 do'' |
    re''4 r r2 |
    R1*2 | \allowPageTurn
    r2 sold'~ |
    sold'1~ |
    sold' |
    re''2 do''~ |
    do'' re''~ |
    re'' do''~ |
    do'' sib' |
    la' sib'~ |
    sib' re''~ |
    re'' mib'' |
  }
  \tag #'violino2 {
    mi'4 r dod'2~ |
    dod' re'~ |
    re'1~ |
    re'2 do'!16 sol sol sol sol4:16 |
    sol2:16 sol4 r |
    mi'1 |
    fa'2 la' |
    sib'4 r r2 |
    R1*2 | \allowPageTurn
    r2 si!~ |
    si1~ |
    si |
    sold'2 la'~ |
    la' sol'!~ |
    sol'1~ |
    sol' |
    fa'1~ |
    fa'~ |
    fa'2 sol' |
  }
>>
r4 fa' mi'!2 |
do'8 r16 do'32( re' mi'16)[ mi'32( fa'] sol'16)[ sol'32( la'] sib'8)[ r16 do''32( re''] mi''16[) mi''32( fa''] sol''16[) sol''32( <<
  \tag #'violino1 {
    la''32]) |
    sib''1 |
    sol'2 fa'~ |
    fa' mib'' |
    re''4( mib''8) r re''4( mib''8) r |
    re''32[ re'' re'' re''] do''[ do'' do'' do''] re''[ re'' re'' re''] mib''[ mib'' mib'' mib''] fa''4 lab''8 do'' |
    <si'! re' sol>4 r re''2~ |
    re''1~ |
    re''2 do''~ |
    do''1 |
    sib'2 la'~ |
    la'1 |
    si'! |
    re'' |
    do''2 r |
    R1 | \allowPageTurn
    r2 sib'~ |
    sib' reb'~ |
    reb' r |
    mib'8\p do'4( reb'8) mib' do'4 reb'16( mib'32 fa') |
    mib'16( lab') mib'4( reb'8) do'16-.
  }
  \tag #'violino2 {
    fa''32]) |
    mi''1 |
    mi'2 re'~ |
    re' do'! |
    re'4( mib'8) r re'4( mib'8) r |
    sib32[ sib sib sib] la[ la la la] sib[ sib sib sib] do'[ do' do' do'] re'4 fa' |
    <fa' sol>4 r lab'2~ |
    lab'1 |
    sol' |
    fad' |
    sol'~ |
    sol'2 fa'! |
    mi'1 |
    si' |
    la'4 red'' r2 |
    R1 | \allowPageTurn
    r2 sol'~ |
    sol' sib~ |
    sib r |
    do'8\p lab4( sib8) do' lab4 sib16( do'32 reb') |
    do'16( mib') do'4( sib8) lab16-.
  }
>> do''-.\f sib'-. lab'-. sol'-. lab'-. sol'-. fa'-. |
<mi'! do''>4 r r2 |
<<
  \tag #'violino1 {
    sol'1 |
    sib' |
    lab' |
    sol'~ |
    sol' |
    fad' |
    sol'2 re''~ |
    re''1 |
    do''2
  }
  \tag #'violino2 {
    mi'1~ |
    mi' |
    fa'~ |
    fa'~ |
    fa'2 mib' |
    re'1~ |
    re'2 sol'~ |
    sol'1 |
    sol'2
  }
>> r4 <re' la' fad''>4 |
<sol re' si' sol''>4 r r2 |
R1*21 |
