Ap -- pe -- na
da Be -- tu -- lia par -- tii, che m’ar -- re -- sta -- ro
le guar -- die o -- sti -- li. Ad O -- lo -- fer -- ne in -- nan -- zi
son gui -- da -- ta da lo -- ro e -- gli mi chie -- de
a che ven -- go, e chi son. Par -- te io gli sco -- pro,
tac -- cio par -- te del ve -- ro. Ei non in -- ten -- de,
e ap -- pro -- va i det -- ti mie -- i. Pie -- to -- so, u -- ma -- no
(ma stra -- nie -- ra in quel vol -- to
mi par -- ve la pie -- tà) m’o -- de, m’ac -- co -- glie,
m’ap -- plau -- de, mi con -- so -- la. A lie -- ta ce -- na
se -- co mi vuol. Già su le men -- se e -- let -- te
fu -- ma -- no i va -- si d’or; già vuo -- ta il fol -- le,
fra’ ci -- bi ad or ad or taz -- ze fre -- quen -- ti
di li -- cor ge -- ne -- ro -- so, e a po -- co a po -- co
co -- min -- cia a va -- cil -- lar. Mol -- ti mi -- ni -- stri
e -- ran d’in -- tor -- no a no -- i; ma ad u -- no ad u -- no
tut -- ti si di -- le -- guar. L’ul -- ti -- mo d’es -- si
ri -- ma -- ne -- va, e il peg -- gior. L’u -- scio co -- stui
chiu -- se par -- ten -- do, e mi la -- sciò con lu -- i.

Fie -- ro ci -- men -- to!

O -- gni ci -- men -- to è lie -- ve
ad in -- spi -- ra -- to cor. Scor -- za gran par -- te
e -- ra or -- mai del -- la not -- te. Il cam -- po in -- tor -- no
nel son -- no u -- ni -- ver -- sal ta -- ce -- va op -- pres -- so.
Vin -- to O -- lo -- fer -- ne i -- stes -- so
dal vi -- no, in cui s’im -- mer -- se ol -- tre il co -- stu -- me,
ste -- so dor -- mi -- a su le fu -- ne -- ste piu -- me.
Sor -- go; e ta -- ci -- ta al -- lor co -- là m’ap -- pres -- so,
do -- ve pro -- no ei gia -- ce -- va. Ri -- vol -- ta al Cie -- lo
più col cor, che col lab -- bro. “Ec -- co il i -- stan -- te,”
(dis -- si) “o Dio d’I -- srael, che un col -- po so -- lo
li -- be -- ri il po -- pol tu -- o. Tu’l pro -- met -- te -- sti;
in te fi -- da -- to io l’in -- tra -- pre -- si, e spe -- ro, e spe -- ro,
as -- sis -- ten -- za da te.” Sciol -- go, ciò det -- to,
da’ so -- ste -- gni del let -- to
l’ap -- pe -- so ac -- ciar; lo snu -- do, il crin gli strin -- go
con la si -- ni -- stra man; l’al -- tra soll -- e -- vo
quan -- to il brac -- cio si sten -- de; i vo -- ti a Di -- o
rin -- no -- vo in sì gran pas -- so;
e su l’em -- pia cer -- vi -- ce il col -- po ab -- bas -- so.

Oh co -- rag -- gio!

Oh pe -- ri -- glio!

A -- pre il bar -- ba -- ro il ci -- glio; e in -- cer -- to an -- co -- ra
fra’l son -- no e fra la mor -- te, il fer -- ro im -- mer -- so
sen -- te -- si nel -- la go -- la. Al -- le di -- fe -- se
sol -- le -- var -- si pro -- cu -- ra; e gli el con -- ten -- de
l’im -- pri -- gio -- na -- to crin. Ri -- cor -- re a’ gri -- di;
ma in -- ter -- rot -- te la vo -- ce
tro -- va le vie del lab -- bro, e si dis -- per -- de,
re -- pli -- co il col -- po; ec -- co l’or -- ri -- bil ca -- po
da -- gli o -- me -- ri di -- vi -- so.
Guiz -- za il tron -- co re -- ci -- so
sul san -- gui -- gno ter -- ren; bal -- zar mi sen -- to
il te -- schio se -- mi -- vi -- vo
sot -- te la man, che’l so -- ste -- ne -- a. Quel vol -- to
a un trat -- to sco -- lo -- rir; mu -- te pa -- ro -- le
quel lab -- bro ar -- ti -- co -- lar; quei oc -- chi in -- tor -- no
cer -- car del so -- le i ra -- i,
mo -- ri -- re e mi -- nac -- ciar vi -- di, e tre -- ma -- i.

Tre -- mo in u -- dir -- lo anch’ i -- o.

Re -- spi -- ro al fi -- ne; e del tri -- on -- fo il -- lus -- tre
ren -- do gra -- zie all’ Au -- tor. Svel -- ta dal let -- to
la su -- per -- ba cor -- ti -- na, il ca -- po e -- san -- gue
sol -- le -- ci -- ta n’in -- vol -- go: al -- la mia fi -- da an -- cel -- la lo con -- se -- gno,
che non lun -- gi at -- ten -- de -- a; del du -- ce e -- stin -- to,
m’in -- vo -- lo al pa -- di -- glion, pas -- so fra’ suo -- i
non vi -- sta o ri -- spet -- ta -- ta e tor -- no a vo -- i.

Oh pro -- di -- gio!

Oh por -- ten -- to!

In -- er -- me e so -- la
tan -- to pen -- sar, tan -- to e -- se -- guir po -- te -- sti!
E cre -- der -- ti deg -- g’i -- o?

Cre -- di -- lo a que -- sto
ch’io sco -- pro a -- gli oc -- chi tuo -- i, te -- schio re -- ci -- so.

Oh spa -- ven -- to! È O -- lo -- fer -- ne; io lo rav -- vi -- so.

So -- ste -- ne -- te -- lo, o ser -- vi, il cor gli ag -- ghiac -- cia
l’im -- pro -- vi -- so ter -- ror.

Fug -- ge quell’ al -- ma,
per non ce -- de -- re al ver.

Me -- glio di lui
giu -- di -- chia -- mo, A -- mi -- tal. For -- se quel ve -- lo,
che gli o -- scu -- rò la men -- te,
a un trat -- to or si squar -- ciò. Non fug -- ge il ve -- ro,
ma gli man -- ca il co -- stu -- me
l’im -- pe -- to a so -- ste -- ner di tan -- to lu -- me.
