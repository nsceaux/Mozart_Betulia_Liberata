\clef "alto/treble" r4 r8 re' |
sol' sol' sol' sol' sol'4 la'8 si' |
la'4 r16 la' la' la' la'8 do'!16 do' do'8 re' |
si si r4 r8 re' mi' fa'! |
mi' mi'16 mi' sold'8 sold' r sold'16 si' si'8 re'16 mi' |
do'8 do' r4 mi' mi'8 la' |
sol'! sol' r re'16 mi' fa'!8 fa' r sol'16 re' |
mi'4 r mi' mi'8 mi' |
mi' si r sold'16 si' si'4 re'8 do' |
la la r16 la' si' do'' do''8 sol' r mi' |
sol' sol' sol' la' fa' fa' r fa' |
la' la' r la' la' mi' r mi'16 fa' |
sol'4 sol'8 sib' sol' sol' r mi' |
sol' sol' sol' fa' re'4 r |
la'8 re' r la' sold' sold' r sold' |
la' la' r sold'16 la' la'8 mi' r4 |
r2 r8 mi' mi' fa' |
sol' sol' sib'8 sol'16 la' fa'4 r |
fa'8 fa'16 fa' sol'8 la' la' mi' r4 |
sol'8 sol'16 sol' sol'8 fa' re'4 r16 fa' fa' sol' |
mib'8 mib' r mib' mib' mib' re' mib' |
do'4 do'8 do'16 re' mib'8 mib' r mib'16 sol' |
mib'4 mib'8 fa' re' re' r4 |
r8 fa' sib' sol' mi'! mi' r sib' |
sib' mi' mi' fa' do'4 r |
sol'4 sol'8 sol' sol' re' r4 |
re'8 re'16 mi' fa'8 mi' do' do' r16 mi' fad' sol' |
fad'8 fad' r4 mi'8 mi'16 mi' mi'8 re' |
si4 r re'8 re'16 mi' fad' fad' fad' si' |
la'8 la' r sol'16 fad' re'4 r |
re'4 re'8 mi' fad'4 fad'8 mi'16 re' |
sold'8 sold' r sold' sold' sold' sold' la' |
la' mi' r4
\ffclef "soprano/treble" <>^\markup\character Amital
la'4 la'8 do''! |
do'' sol'! r4
\ffclef "alto/treble" <>^\markup\character Giuditta
sol'8 sol'16 sol' la'8 sib' |
la' la' r la' fa'! fa' fa' mi' |
do'4 r la' fad'8 la' |
la' re' r4 re'8 re' re' do'16 re' |
sib8 sib r4 r8 mib' mib' mib' |
do' do' r do' mib' mib' mib' sol' |
mib' mib' mib' fa' re' re' r4 |
re'4 re'8 fa' mi' mi' r mi' |
sold' sold' r si'! si' mi' mi' fa' |
re' re' re' do'16 re' si!8 si r4 |
si'4 re'8 mi' do' do' r4 |
do''8 red'16 red' red'8 mi' mi' si r4 |
R1 |
r2 sol'8 do' r4 |
r r8 sol' sol'4 sol'8 la' |
sib' sol' mi' re' dod' dod' r mi'16 fa' |
sol'4 sol'8 fa' re' re' r re' |
fa'4 fa'8 sol' mib' mib' r mib'16 mib' |
do'4 r8 mib'16 re' sib8 sib r4 |
r8 re' sol' fad' sol' sol' r4 |
sol'8 re' r16 sol' la' sib' la'4 r16 la' la' la' |
la'8 mi'! r4 sol'8 sol'16 sol' sol'8 fa'! |
re' re' r16 la' la' si'! sold'8 sold' r16 sold' sold' si' |
si'8 re' re' do' la la r mi' |
la' la' r do'' do'' red' r red'16 fad' |
red'4 red'8 mi' si4 r |
R1*2 |
sol'!8 mi' r sol' sol' do' r do'16 re'! |
mi'4 mi'8 fa'! sol' sol' r16 sol' sib' la' |
fa'4 r r2 r4 r8 fa' fa' do' r4 |
r8 mi' mi' la' la' mi' r4 |
sol'8 sol'16 sol' sol'8 fa' re'4 r |
re'4 re'8 mi' fa' fa' r fa'16 la' |
fa'4 fa'8 mi' do'! do' r4 |
r2 r8 sol' sol' sol' |
mi' mi' r do' sol' sol' sol' la' |
fa' fa' r fa'16 do'' do''4 mib'8 fa' |
re' re' r16 sib' mi' fa' fa'8 do' r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
r4 r8 sol16 sol si!8 si
\ffclef "soprano/treble" <>^\markup\character Amital
r8 si'!16 re'' |
re''8 sol' r4
\ffclef "alto/treble" <>^\markup\character Giuditta
r4 si8 si |
mi'8. mi'16 mi'8 fad' sold' sold' r16 sold' la' si' |
si'8 mi' r fa'! re' re' r16 re' re' re' |
si8 si r16 sold' la' si' si'8 re' r4 |
re'8 re'16 re' re'8 do' la la r4 |
mi'4 mi'8 sol'! sol' re' r8 re'16 mi' |
fa'4 fa'8 mi' do' do' r16 mi' mi' fa' |
sol'8 sol' r4 sol'8 sol'16 sol' sib'8 la' |
fa'4 r16 fa' mib' fa' re'8 re' r fa'16 fa' |
re'4 re'8 mib' fa' fa' r4 |
fa'8 fa'16 fa' sol'8 lab' sol' sol' sol' mib'16 re' |
sib8 sib r4 sol' mi'!8 sol' |
sol' do' r4 r2 |
mi'8 mi'16 mi' mi'8 fa' sol' sol' r sib' |
sol'8. sol'16 sol'8 fa' re' re' r re'16 mi' |
fa'4 fa'8 sol' mib'16 mib' do' re' mib'8 mib'16 re' |
sib4 r r2 |
R1 |
r8 fa' fa' sol' lab' lab' r re' |
re' re' re' mib' fa' fa' fa' fa'16 lab' |
fa'8 fa' fa' mib' do' do' r sol' |
fad' fad' r re' la'! la' la' sib' |
sol'4 sol'8 la'16 sib' la'8 la' r mi'! |
sol' sol' sol' la' fa'!4 r16 fa' fa' fa' |
mi'8 mi' r16 mi' mi' fad' sold'4 sold'8 si'! |
si' re' r fa'! re' re' re' mi' |
do' la'16 red' r8 red'16 mi' mi'8 si r4 |
\ffclef "soprano/treble" <>^\markup\character Amital
mi''8 do'' r do''16 mi'' do''4 do''8 si' |
sol' sol' r4
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 sol' sol' sib' |
sib' mib' r mib' mib' mib' mib' fa' |
reb' reb' r sol'16 sib' sib'4 reb'8 do' |
lab4 r r2 |
R1 |
sol'4 sol'8 sol' mi'! mi' r mi'16 mi' |
mi'4 mi'8 fa' sol' sol' r16 sol' sol' lab' |
sib'8 sib' r sib' sib' sib' sib' lab' |
fa'8 fa' r4 fa'8 fa'16 fa' sol'8 lab' |
sol'8 sol' sol' lab' fa' fa' r re'16 mib' |
fa'4 fa'8 mib' do' do' r16 sol' sol' la'! |
fad'8 fad' r fad' la' la' la' sib' |
sol'4 r sol' sol'8 sol' |
sol' re' r re' fa'! fa' r16 fa' fa' sol' |
mi'!8 mi' r16 do'' fad' sol' sol'8 re' r4 |
r
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si16 re' re'8 sol
\ffclef "soprano/treble" <>^\markup\character Cabri
r8 re''16 mi'' |
do''8 do''
\ffclef "bass" <>^\markup\character Achior
r8 sol mi mi r mi |
sol sol sol sol16 la sib4 r |
sib8 sib16 sib sib8 la fa fa r la |
fa fa fa mi sol sol
\ffclef "alto/treble" <>^\markup\character Giuditta
sol'8 la'16 sib' |
la'8 la' r mi' sol' sol' sol' la' |
fa' fa' la' sold'16 la' la'8 mi' r4 |
r
\ffclef "bass" <>^\markup\character Achior
r8 fad16 fad si!8 si r si16 si |
sol8 sol r16 sol lad si si8 fad r4 |
r
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si16 do'! re'8. re'16 re'8 mi' |
do' do' r16 sol sol la sib8 sib r mi'16 sol' |
sol'4 sib8 la fa4
\ffclef "soprano/treble" <>^\markup\character Amital
la'8 la'16 sib' |
do''8 do'' r do''16 re'' mib''4 mib''8 re'' |
sib'4 r
\ffclef "alto/treble" <>^\markup\character Giuditta
fa'8 fa'16 fa' fa'8 sol'16 lab' |
sol'8 sol' r sol' mib' mib' r4 |
sol'4 sol'8 la'! fad' fad' r fad' |
fad' fad' fad' sol' la' la' r re' |
la' la' la' sib' sol'4 r |
r8 sol' la' sib' la' la' r mi'16 mi' |
mi'4 mi'8 fa' sol' sol' r sib' |
sol' sol' sol' la' fa'4 r16 la' sold' la' |
la'8 mi' r4 r2 |
