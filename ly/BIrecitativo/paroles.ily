Di tua vit -- to -- ria un glo -- rio -- so ef -- fet -- to
ve -- di, o Giu -- dit -- ta.

E non il so -- lo. Anch’ io
pec -- ca -- i; mi pen -- to. Il mio ti -- mo -- re of -- fe -- se
la di -- vi -- na pie -- tà. Fra’ ma -- li mie -- i,
mio Dio, non ram -- men -- ta -- i che pu -- oi, chi se -- i.
