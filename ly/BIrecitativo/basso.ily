\clef "bass" dod1~ |
dod2 re |
r4 mi red2~ |
red1~ |
red2 mi~ |
mi1~ |
mi2 dod~ |
dod1 |
re! |
r4 mi la,2 |
