\clef "tenor/G_8" <>^\markup\character Ozia
r8 la la si dod' dod' r dod'16 mi' |
dod'4 si8 la fad' fad' fad' re'16 dod' |
la8 la r4
\ffclef "soprano/treble" <>^\markup\character Amital
r8 si' si' red'' |
si' si' r fad' la'4 r8 dod'' |
la' la' r si' sold' sold' r16 si' si' mi'' |
mi''8 si' r si' sold' sold' r si'16 dod'' |
re''!4 re''8 dod'' la'4 r16 la' la' si' |
dod''8 dod'' r mi'' mi'' sol'! sol' la' |
fad' fad' r fad'' re'' re'' r dod'' |
la' la' r4 r2 |
