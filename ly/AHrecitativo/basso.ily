\clef "bass" si,1~ |
si,~ |
si, |
mib |
sol |
do |
mi!~ |
mi~ |
mi |
fa |
mib~ |
mib~ |
mib2 reb |
fa1 |
mib2 fad~ |
fad sol |
re mi!~ |
mi1~ |
mi2 la,!~ |
la, sib, |
re1 |
sib, |
r4 do dod2 |
fa1 |
la, |
re |
fad |
sol2 dod~ |
dod1 |
re2 si,! |
do! r4 re |
sol1~ |
sol~ |
sol~ |
sol~ |
sol2 mib~ |
mib1~ |
mib~ |
mib |
re2 fad |
sol fa!~ |
fa1 |
mib1~ |
mib2 re~ |
re1 |
mib1~ |
mib2 r4 fa |
fad1 |
sol |
fa!~ |
fa |
mib |
fa |
r4 sol do2 |
