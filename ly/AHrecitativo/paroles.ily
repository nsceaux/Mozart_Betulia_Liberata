E qual pa -- ce spe -- ra -- te
da gen -- te sen -- za leg -- ge e sen -- za fe -- de,
ne -- mi -- ca al no -- stro Di -- o?

Sem -- pre sia me -- glio
be -- ne -- dir -- lo vi -- ven -- ti,
che in ob -- bro -- bri -- o al -- le gen -- ti
mo -- rir, ve -- den -- do ed i con -- sor -- ti, e i fi -- gli
spi -- rar su gli oc -- chi no -- stri.

E se né pu -- re
que -- sta mi -- se -- ra vita a voi las -- cias -- se
la per -- fi -- dia ne -- mi -- ca?

Il ferro al -- me -- no
sol -- le -- ci -- to n’uc -- ci -- da, e non la se -- te
con sì lun -- go mo -- rir. Deh, O -- zì -- a, per quan -- to
han di sa -- cro e di gran -- de e ter -- ra e cie -- lo,
per lui, ch’or ne pu -- nis -- ce,
gran Di -- o de’ pa -- dri no -- stri, all’ ar -- mi As -- si -- re
ren -- da -- si la cit -- tà.

Fi -- gli, che di -- te!

Sì, sì, Be -- tu -- lia in -- te -- ra
par -- la per boc -- ca mi -- a. S’a -- pran le por -- te,
al -- la for -- za si ce -- da. U -- ni -- ti in -- sie -- me,
vo -- lon -- ta -- ri cor -- ria -- mo
al cam -- po d’O -- lo -- fer -- ne. U -- ni -- co scam -- po è que -- sto; o -- gnun lo chie -- de.

Al cam -- po, al cam -- po!

Fer -- ma -- te -- vi, sen -- ti -- te (e -- ter -- no Di -- o,
as -- si -- sten -- za, con -- si -- glio) io non m’op -- pon -- go,
fi -- gli, al vo -- stro pen -- sier; chie -- do che so -- lo
dif -- fe -- rir -- lo vi piac -- cia, e più non chie -- do,
che cin -- que dì. Pren -- de -- te ar -- dir. Fra tan -- to
for -- se id -- dio pla -- che -- ras -- si, e del suo no -- me
la glo -- ria so -- ster -- rà. Se giun -- ge po -- i
sen -- za spe -- me per noi la quin -- ta au -- ro -- ra;
s’a -- pra al -- lor la cit -- tà, ren -- da -- si al -- lo -- ra.

A que -- sta leg -- ge at -- ten -- de -- re -- mo.

Or voi
co’ vo -- stri ac -- com -- pa -- gna -- te
que -- sti che al ciel fer -- vi -- di prie -- ghi in -- vi -- o,
nun -- zi fe -- de -- li in fra’ mor -- ta -- li, e Di -- o.
