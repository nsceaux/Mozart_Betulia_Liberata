\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basso"
      \includeFigures "chiffres"
      \origLayout {
        s1*4\break s1*5\break s1*5\break s1*5\break s1*5\break
        s1*4\break s1*5\break s1*6\break s1*5\break s1*5\pageBreak
      }
    >>
  >>
  \layout {
    indent = \smallindent
    short-indent = 0
  }
  \midi { }
}
