\clef "tenor/G_8" <>^\markup\character Ozia
r8 re'16 re' si8 la16 si sol8 sol r sol |
sol sol sol la si si r16 si si do' |
re'8 re' r si re' re' si sol |
do' do' r4
\ffclef "soprano/treble" <>^\markup\character Amital
do''4 re''8 mib'' |
mib'' sib' r sib'16 do'' reb''4 reb''8 do'' |
lab' lab' r do''16 do'' do''8 do'' do'' reb'' |
sib'4 sib'8 sib' sol'4 r8 sol' |
sol' sol' sol' sol'16 lab' sib'8 sib' r reb'' |
sib' sib' r sol'' sol'' sib' sib' lab' |
fa' fa' r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 do' do' do' |
la! la la la la4 la8 sib |
do'8 do' do' mib' do' do' r la |
do' do' la fa sib sib
\ffclef "soprano/treble" <>^\markup\character Amital
r16 reb'' reb'' do'' |
si'!8 si' r si' re''! re'' si' sol' |
do'' do'' r16 do'' re'' mib'' re''8 re'' r8 la'16 sib' |
do''4 do''8 sib' sol'4 r |
sib' r8 re'' do'' do'' r do'' |
do'' do'' do'' mi''! do'' do'' r do''16 do'' |
do''8 sol'16 sol' sol'8 la'! fa'8 fa' r fa' |
do'' do'' re'' mib'' re'' re'' r fa'' |
fa'' sib' r16 sib' sib' sib' sib'8 fa' r16 sib' re'' sib' |
mi''!8 mi'' r4 mi''8 mi''16 mi'' mi''8 fa'' |
do''4 r
\ffclef "tenor/G_8" <>^\markup\character Ozia
mi'8 la r la |
re' re' r4
\ffclef "soprano/treble" <>^\markup\character Amital
r8 la' re'' fa'' |
fa'' do''16 do'' la'8 la' do'' do''16 do'' mib''8 re'' |
sib' sib' r4 sib' do''8 re'' |
re'' la' r la'16 sib' do''4 do''8 sib' |
sol' sol' r16 sol' la' sib' la'8 la' r la'16 si' |
dod''4 dod''8 mi'' dod'' dod''16 dod'' mi'' mi'' mi'' fa'' |
re''8 re'' r4 re''8 re''16 re'' re''8 fa'' |
mib''8 mib'' r16 mib'' do'' sib' sol'8 sol' r4 |
\ffclef "soprano/treble" <>^\markup\character Coro
r4 r8 re'' re''4 si'!8 sol'' |
sol''4 re''8
\ffclef "tenor/G_8" <>^\markup\character Ozia
re' si si16 si r8 re' |
re' sol r4 r8 re' re' sol' |
sol' re' r8 re'16 re' si8 si r re' |
re' sol r4 sib4 sib8 mib' |
re'8 re' re' re'16 fa' fa'4 lab8 sib |
sol4 r mib' do'8 sib |
la!8 la r la16 sib do'4 do'8 re' |
sib8 sib r16 sib do' re' re'8 la! r16 la do' sib |
sol4 r16 re' re' re' si!4 r8 sol |
si si si do' re'4 re'8 mib' |
do' do' r16 do' do' do' la4 la8 la |
do' do' do' re' sib4 r16 sib sib sib |
lab8 lab r lab16 lab re'8 re'16 fa' fa' lab lab sib |
sol8 sol r sib16 sib mib'4 do'8 sib |
la!4 sol'8 mib'16 re' sib8 sib r4 |
\ffclef "soprano/treble" <>^\markup\character Amital
r8 la' la' re'' re'' la'16 la' do''8 sib' |
sol' sol' r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sol sol la |
si!8 si si do' re' re' re' re'16 fa' |
re'4 si8 si16 do' re'4 re'8 mib' |
do'8 do' r4 do'4 sib8 do' |
lab8 lab r4 r16 fa' re' do' si!8 do' |
do' sol r4 r2 |
