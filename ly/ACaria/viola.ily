\clef "alto" sib8 sib' fa' re' sib4 r |
sib8 sib' fa' re' sib4 r |
sib8 sib sib sib re' re' re' re' |
mib'4 sib r2 |
fa'4 la' sib' reb'' |
fa' la' sib' sib |
fa' r fa' r |
fa' fa' fa' r |
sib2(\p la |
sol fa) |
mib( re |
do) la |
sib4\f fa'8.\trill mib'32 fa' mib'4 mib'8.\trill re'32 mib' |
re'4 sib'8 sib' do'' do'' fa' fa' |
sib4 fa'8.\trill mib'32 fa' mib'4 mib'8.\trill re'32 mib' |
re'4 sib'8 sib' do'' do'' fa' fa' |
sib'4 mib' r2 |
r4 mib'8 mib' fa' fa' fa fa |
sib4 re'8 fa' re' fa' re' fa' |
re'4 fa' fa' r |
sib8\p sib' fa' re' sib4 r |
sib8 sib' fa' re' sib4 r |
sib8 sib sib sib re' re' re' re' |
mib'4 sib r2 |
<<
 { mib'2 reb' | do' reb' | } \\
 { do'2 sib | la sib | }
>>
fa'8 fa' fa' fa' fa' fa' fa' fa' |
fa'4 fa'\f fa' r |
fa'8\p fa' fa' fa' fa' fa' fa' fa' |
sib sib sib sib sib sib do' do' |
re' re' re' re' sol' sol' sol' sol' |
fa'4 re'(\f do') sib( |
la) r la(\p do') |
sib2( do'4) fa'8( la') |
re'4 fa'8( lab') do'2 |
do'8-. la'( sol') fa'-. mi'-. re'-. do'-. mi'-. |
do' do' do' do' re' re' re' re' |
sol4 do'8\f do' <do' do>4 r |
fa'2(\p mi' |
re' do') |
sib( la |
sol) re' |
do'8 do' do' do' do' do' do' do' |
do'4 fa'2\f fa'4 |
fa' do'8.\trill\p sib32 do' re'8 r sib'8.\trill la'32 sib' |
la'4 fa'8\f fa' sol' sol' la la |
fa'4 do'8.\trill\p sib32 do' re'8 r sib'8.\trill la'32 sib' |
la'4 fa'8\f fa' sol' sol' do' do' |
fa'\p fa' fa' fa' fa' fa' fa' fa' |
fa' sib sib sib sib sib sib sib |
fa'4 r r2 |
la8 la la la la la la la |
la4 r r2 |
re'8 re' re' re' re' re' re' re' |
do' do' do' do' do' do' do' do' |
do' do' fa' fa' fa' sib sib sib |
sib sib sib sib sib sib sib sib |
do' do' do' do' do' do' do' do' |
fa' fa' la la sib sib sib sib |
do' do' do' do' do' do' do' do' |
do'\cresc do' do' do' do' do' do' do' |
fa'\f la' do'' la' fa'4 r |
fa'8 la' do'' la' fa'4 r |
fa'8 fa' fa' fa' la' la' la' la' |
sib'4 r r2 | \allowPageTurn
r4 sib'8 sib' do'' do'' do' do' |
fa'4 la'8 do'' la' do'' la' do'' |
la'4 do' do' r |
do'8\p do' do' do' do' do' do' do' |
\ru#32 do' |
do'4 do' lab r |
<sol sol'>1\p~ |
q2 q |
sol'8 sol' sol' sol' sol' sol' sol' sol' |
sol'2\f sol4 r |
mib'8\p mib' mib' mib' mib' mib' mib' mib' |
re' re' re' re' sib sib sib sib |
lab' lab' lab' lab' fa' fa' fa' fa' |
\ru#16 mib' |
mib' mib' mib' mib' mib' mib' mi' mi' |
fa'\f fa' fa' fa' fa'4 r |
sib8\p sib' fa' re' sib4 r |
sib8 sib' fa' re' sib4 r |
sib8 sib sib sib re' re' re' re' |
mib'4 sib r2 |
<<
  { mib'2 reb' | do' reb' | } \\
  { do'2 sib | la sib | }
>>
fa'8 fa' fa' fa' fa' fa' fa' fa' |
fa'4 fa'\f fa r |
sib8(\p re') fa'-. fa'-. fa'( re') fa'4 |
mib'2 do'8 fa'4 do'8 |
sib4 sol' fa'2 |
fa'8 fa' fa' fa' sib sib sib sib |
mib' mib' mib' mib' mib'( do' re' mib') |
re' re' re' re' mib' mib' mib' mib' |
re'( fa' mib' re') la2 |
sol fa |
mib re |
do la4 do' |
do'8 do' do' do' la' la' la' la' |
fa'4\f sib2 sib4 |
sib fa'8.\trill\p mib'32 fa' mib'4 mib'8.\trill re'32 mib' |
re'4 sib'8\f sib' do'' do'' fa' fa' |
sib'4 fa'8.\trill\p mib'32 fa' mib'4 mib'8.\trill re'32 mib' |
re'4 sib'8\f sib' do'' do'' fa' fa' |
sib' sib'\p sib' sib' re' re' re' re' |
mib'4 r mib'8 mib' mib' mib' |
mib' mib' mib' mib' mib' mib' mib' mib' |
re'4 r re'8 re' re' re' |
re' re' re' re' re' re' re' re' |
do'-. sib-. la-. sib-. do'-. re'-. mib'-. r |
re'-. do'-. sib-. do'-. re'-. mib'-. fa'-. r |
do'-. sib-. la-. sib-. do'-. re'-. mib'-. r |
fa' r sib r sib r do' r |
fa'4 re' do'( re'8) mib'-. |
fa' fa' fa' fa' fa fa fa fa |
r sib re' fa' r sib' fa' re' |
r do' sol' mib' r do'' sol' mib' |
fa' fa' fa' fa' fa' fa' fa' fa' |
fa\cresc fa fa fa fa fa fa fa |
sib\f sib mib' mib' re' re' la' la' |
sib' sib' lab' lab' sol' sol' re' re' |
mib'4 r r r8 mi' |
fa'1\fermata |
sib8 sib' fa' re' sib4 r |
sib8 sib' fa' re' sib4 r |
sib8 sib sib sib re' re' re' re' |
mib'4 r r2 |
r4 mib'8 mib' fa' fa' fa' fa' |
sib4 re'8 fa' re' fa' re' fa' |
re'4 fa' fa' r |
sib1\p |
do' |
sib |
mib'8 lab4 fa'8 sib sib sib sib |
sib sol'\f sol' sol' sol' sol' si' si' |
do'' sol'\p sol' sol' sol' sol' sol' sol' |
lab' lab' sol' sol' fa' fa' sol' sol' |
la'! la' la' la' la' la' la' la' |
la' la' sol' sol' sol'4\f do' |
sib\p sib'8( la') sol'( fa' mi' re') |
dod'8 sol'4 la' mib'8( fa' la') |
la' la' la' la' la' la' la' la' |
la'4 la r la'\f |
la' r fa'8\p fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
la' la' la' la' la' la' la' la' |
sib'4 r r2 |
fa'8 fa' fa' fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' sol' sol' |
la' la' la' la' la' la' la' la' |
la la la la la la la la |
re'4 r r re'\f |
sib sol r do'' |
la' fa' r fa' |
sol'8 sol' sol' sol' sol' sol' sol' sol' |
do'4 <do' fa' la'> q r |
