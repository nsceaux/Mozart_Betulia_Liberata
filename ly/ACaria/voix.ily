\clef "tenor/G_8" R1*20 |
sib2~ sib8[ re'] fa'16[ re' mib' do'] |
sib2~ sib8[ re'] fa'16[ re'] mib'[ do'] |
sib2~ sib8[ re'] re' fa' |
mib'4 re' r2 |
fa2 fa' |
fa'8[ do'] do'4 r r8 sib |
la4( do') re' mi' |
fa' fa r2 |
do'2. fa'4 |
\grace mib'8 re'4 re'2 do'4 |
sib2\melisma \grace do'8 sib[ la16 sib]\melismaEnd do'8 sib |
la4 r r2 |
fa'2 mib'! |
re' do' |
si sib |
la8[ do'] do'4 r do'8 do' |
fa'2~ fa'16[ sol' la' sol'] fa'8 la |
sol4 r r2 |
R1*2 |
fa'2. \grace mib'8 re'4 |
sib sib r r8 sib |
sol'2~ sol'8[ mi'] do' sib |
la4 do' r2 |
do'2 re' |
do'8[ la] fa4 r2 |
r4 do'4 re'8[ sib] re' sib |
fa'4 fa r do'8 do' |
fa'[ do'] do' re' mib'![ fa'16 sol'] fa'8 mib' |
re'16[\melisma do' sib la] sib[ do' re' mib'] fa'[ sol' fa' re'] mib'[ fa' sol' la'] |
sib'4 r re'16[ mi' fa' mi'] \grace sol'16 fa'8[ mi'16 re'] |
do'2~ do'8[ re'16 mi'] fa'[ mi' fa' sol'] |
la'4 r la'16[ sol' fa' sol'] fa'[ mi' re' do'] |
si2~ si16[ sol la si] do'[ re' mi' fa'] |
sol'[ la' sol' la'] sib'!4 sib!4.\trill la16[ sib] |
la8[ do']\melismaEnd fa' do' re'2~ |
re'16[\melisma mi' fa' mi'] fa'[ mi' fa' mi'] \grace sol'16 fa'8[ mi'16 re']\melismaEnd \grace mi'16 re'8 do'16[ sib] |
la2 sol4. sol8 |
la'4 fa'8 fa' re'[ fa'] sol sib |
la\melisma do'4 fa' la' sib'16[ sol']\melismaEnd |
sol'2..\startTrillSpan fa'8\stopTrillSpan |
fa'4 r r2 |
R1*8 |
fa'2 do' |
lab4 do' r r8 do' |
do'2~ do'8[ reb'] do' sib |
lab4 fa r2 |
sol'2. mib'8[ do'] |
si4 re' r r8 sol' |
sol'2 mib'4 do' |
si sol r2 |
sib!2. mib'4 |
mib'8[ re'] re'2 fa'4 |
fa'2~ fa'8[ re'] sib lab |
sol4 sol'2 mib'4 |
re'8[ do'] do'2 mib'8[ re'] |
\grace re'8 do'4 do'2 sib4 |
la r r2 |
sib2~ sib8[ re'] fa'16[ re' mib' do'] |
sib2~ sib8[ re'] fa'16[ re'] mib'[ do'] |
sib2~ sib8[ re'] re' fa' |
mib'4 re' r2 |
fa2. fa'4 |
fa'8[ do'] do'4 r r8 sib |
la4( do') re' mi' |
fa' fa r2 |
sib2 lab |
sol fa4. fa'8 |
mi'2( mib'8[ re'16 mib']) fa'8 mib' |
re'2 lab4. lab8 |
sol2 sol'8[ mib'] re'[ do'] |
sib2 do'4.\trill sib8 |
sib4 r r2 |
R1 |
sib2. si4 |
do'8[ re'] mib'4 r mib' |
fa2~ fa8[ la] do' mib' |
re'4 fa' r2 |
sib2 sol' |
fa'8[ re'] sib4 r r8 fa' |
sib2 mib'4. sol'8 |
fa'8[ re'] sib4 r fa'8 la |
sib8[ re'] re' fa' lab'[ fa'] re' lab |
sol8[\melisma la!16 sib] do'[ re' mib' fa'] sol'[ fa' mib' re'] mib'[ fa' sol' la'] |
sib'4 r r2 |
fa8[ sol16 la] sib[ do' re' mib'] fa'[ sol' fa' re'] mib'[ fa' sol' la'] |
sib'4 r r fa' |
mib'16[ fa' re' mib'] do'[ mib' re' fa'] mib'[ fa' re' mib'] do'8 r |
fa'16[ sol' mib' fa'] re'[ fa' mib' sol'] fa'[ sol' mib' fa'] re'8 r |
mib'16[ fa' re' mib'] do'[ mib' re' fa'] mib'[ fa' re' mib'] do'8 r |
re'16[ mib' re' sib] mib'[ fa' mib' do'] fa'[ sol' fa' re'] mib'[ fa' sol' la'] |
sib'4\melismaEnd fa'8 re' sol'[ mib'] re' do' |
sib2 \grace re'8 do'4 sib8[ la] |
fa'2 re'4 sib |
sol'2 mib'4 do' |
fa4( fa'2 re'8[ sib]) |
do'2..\trill sib8 |
sib4 r r2 |
r r4 sib8 sib |
sib2 sib4 sib |
sib2 do'4.\trill sib8 |
sib4 r r2 |
R1*5 |
r2 r4 sib8 sib |
mib'4 sib r r8 mib' |
do'4 do' r r8 mib' |
mib'[ sib] sib4 mib'2 |
mib'8[ do'] do' mib' re'[ do'] sib lab |
sol4 mib r2 |
r4 do'2 sol'8[ mib'] |
re'4 re' r r8 re' |
re'8[ la!] la2 sib8[ do'] |
do'4 sib r2 |
sol'2. mi'8[ re'] |
dod'4 dod' r re' |
mi'8[ dod'] la2 sol'4 |
fa' mi' r2 |
re'2. fa'8[ re'] |
sib4 sib r re'8[ sib] la4( re') \grace fa'8 mi'4 re'8[ dod'] |
re'4 r r2 |
re'2. fa'8[ re'] |
sib4 sib r sol'8[ mi'] |
la1 |
mi'2..\trill re'8 |
re'4 r r2 |
R1*4 |
