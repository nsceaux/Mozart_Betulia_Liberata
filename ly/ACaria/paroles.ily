D’o -- gni __ col -- pa la col -- pa mag -- gio -- re
è l’ec -- ces -- so d’un em -- pio ti -- mo -- re,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
d’o -- gni __ col -- pa la col -- pa mag -- gio -- re
è l’ec -- ces -- so d’un em -- pio ti -- mo -- re,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà, __
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà.

D’o -- gni col -- pa la col -- pa mag -- gio -- re
è l’ec -- ces -- so d’un em -- pio ti -- mo -- re,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà.
D’o -- gni __ col -- pa la col -- pa mag -- gio -- re
è l’ec -- ces -- so d’un em -- pio ti -- mo -- re,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà.

D’o -- gni col -- pa la col -- pa mag -- gio -- re
è l’ec -- ces -- so d’un em -- pio ti -- mo -- re,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà, __
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà,
ol -- trag -- gio -- so all’ e -- ter -- na pie -- tà.


Chi di -- spe -- ra, non a -- ma, non cre -- de, 
non a -- ma, non a -- ma, non cre -- de,
che la fe -- de, l’a -- mo -- re, la spe -- me
son tre fa -- ci, che splen -- do -- no in -- sie -- me,
né u -- na ha lu -- ce se l’al -- tra non l’ha,
né u -- na ha lu -- ce se l’al -- tra non l’ha.
