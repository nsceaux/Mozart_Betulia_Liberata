\clef "treble" <>
\ru#2 {
  \twoVoices #'(oboe1 oboe2 oboi) << { sib'2 } { sib' } >>
  <<
    \tag #'(oboe1 oboi) { re''4 }
    \tag #'(oboe2 oboi) { fa' }
  >> r4 |
}
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 fa'' | }
  { sib'1 | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 fa''8 fa'' fa''4 }
  \tag #'(oboe2 oboi) { mib''4 re''8 re'' re''4 }
>> r4 |
<<
  \tag #'(oboe1 oboi) { fa''2 reb'' | do''4 fa'' }
  \tag #'(oboe2 oboi) { do''2 sib' | la'4 do'' }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { reb''4 }
  { fa'' }
>> <<
  \tag #'(oboe1 oboi) {
    sib''4 |
    la'' s fa'' s |
    fa'' la'' fa''
  }
  \tag #'(oboe2 oboi) {
    reb''!4 |
    do'' s la' s |
    la' do'' la'
  }
  { s4 | s r s r | }
>> r4 |
R1*5 |
<>\f <<
  \tag #'(oboe1 oboi) { fa''2 mib'' | }
  \tag #'(oboe2 oboi) { re''2 do'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 mib''4 sol'' | }
  { sib'1 | }
>>
<<
  \tag #'(oboe1 oboi) { fa''2 mib'' | re''4 }
  \tag #'(oboe2 oboi) { re''2 do'' | sib'4 }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { sol''4 fa'' }
  \tag #'(oboe2 oboi) { mib'' re'' }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 | sib' }
  { do'' | sib' }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { sib'' sib'' }
  \tag #'(oboe2 oboi) { re'' re'' }
>> r4 |
R1*3 |
r4 <>\p <<
  \tag #'(oboe1 oboi) { fa''4 re'' }
  \tag #'(oboe2 oboi) { re'' sib' }
>> r4 |
R1*3 |
r4 <>\f <<
  \tag #'(oboe1 oboi) { fa''4 fa'' }
  \tag #'(oboe2 oboi) { la' la' }
>> r4 |
R1*3 |
\tag #'oboi <>^"a 2." fa''2\f mi''4 re'' |
mi'' r r2 |
R1*4 |
r4 <<
  \tag #'(oboe1 oboi) { sol''4 sol'' }
  \tag #'(oboe2 oboi) { mi'' mi'' }
>> r4 |
R1*7 |
<>\f <<
  \tag #'(oboe1 oboi) { do'''2 sib'' | }
  \tag #'(oboe2 oboi) { la''2 mi'' | }
>>
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 sib'' | }
  { fa''4 do'' re''2 | }
>>
<>\f <<
  \tag #'(oboe1 oboi) { la''2 sib'' | la''4 }
  \tag #'(oboe2 oboi) { do''2 mi'' | fa''4 }
>> r4 r2 |
R1*10 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 | sol''1 | fa''2 s | fa'' s | la''2 fa''4 mib''! | }
  { fa''2~ | fa'' mi'' | fa'' s | fa'' s | do''2 do'' | }
  { s2\p | s1 | s2\f r | s r | }
>>
<<
  \tag #'(oboe1 oboi) { re''4 }
  \tag #'(oboe2 oboi) { sib' }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { sib'' la'' sol'' | }
  \tag #'(oboe2 oboi) { sol'' fa'' mi'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''4 s fa'' s | fa'' }
  { fa''4 s fa'' s | fa'' }
  { s4 r s r | }
>> <<
  \tag #'(oboe1 oboi) { la''4 fa'' }
  \tag #'(oboe2 oboi) { do'' la' }
>> r4 |
R1 |
\tag #'oboi <>^"a 2." do''1\p~ |
do''~ |
do''~ |
do''4 r r2 |
R1 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''2 mib'' | re'' }
  \tag #'(oboe2 oboi) { re''2 do'' | si' }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2 |
    si'4 re'' mib'' fad'' | }
  { mib''2 |
    re''4 si' do''2 | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 sol'' sol'' }
  \tag #'(oboe2 oboi) { si' si' si' }
>> r4 |
R1*6 | \allowPageTurn
r4 <>\f <<
  \tag #'(oboe1 oboi) { fa''4 fa'' }
  \tag #'(oboe2 oboi) { la' la' }
>> r4 |
R1*3 |
r4 <>\p <<
  \tag #'(oboe1 oboi) { sib''4 sib'' }
  \tag #'(oboe2 oboi) { re'' re'' }
>> r4 |
R1*3 |
r4 <>\f <<
  \tag #'(oboe1 oboi) { la''4 fa'' }
  \tag #'(oboe2 oboi) { do'' la' }
>> r4 |
R1*13 |
<>\f <<
  \tag #'(oboe1 oboi) { fa''2 mib'' | }
  \tag #'(oboe2 oboi) { re''2 do'' | }
>>
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 mib'' | }
  { sib'1 }
>>
<<
  \tag #'(oboe1 oboi) { fa''2 mib'' | re''4 }
  \tag #'(oboe2 oboi) { re''2 la' | sib'4 }
  { s1\f | s4\p }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) { sol''1 | fa''4 s2. | fa''1 | mib''8 }
  \tag #'(oboe2 oboi) { sib'1 | sib'4 s2. | sib'1 | la'8 }
  { s1\p | s4 r r2 | }
>> r8 r4 r2 |
R1*7 |
r2 <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | do''1 | sib'4 }
  { sib'2~ | sib' la' | sib'4 }
>> <>\f <<
  \tag #'(oboe1 oboi) { la''4 sib'' do''' | re''' }
  \tag #'(oboe2 oboi) { do'' re'' fa'' | fa'' }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { re'' mib'' sib' | sib' }
  { sib'' sib' fa'' | sol'' }
>> r4 r r8 <<
  \tag #'(oboe1 oboi) { sib''8 | sib''2 }
  \tag #'(oboe2 oboi) { re''8 | re''2 }
>> r2\fermata |
\ru#2 {
  \twoVoices #'(oboe1 oboe2 oboi) << { sib'2 } { sib' } >>
  <<
    \tag #'(oboe1 oboi) { re''4 }
    \tag #'(oboe2 oboi) { fa' }
  >> r4 |
}
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2. fad''4 | }
  { sib'1 | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 }
  \tag #'(oboe2 oboi) { sib' }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { sol''4 fa''! }
  \tag #'(oboe2 oboi) { mib'' re'' }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { la' | sib' }
  { do'' | sib' }
>> r4 <<
  \tag #'(oboe1 oboi) { sib''4 s | sib'' sib'' sib'' }
  \tag #'(oboe2 oboi) { re''4 s | re'' re'' re'' }
  { s4 r | }
>> r4 |
R1*4 |
r2 <>\f <<
  \tag #'(oboe1 oboi) { sol''4( fa'') | mib'' }
  \tag #'(oboe2 oboi) { sib'( si') | do'' }
>> r4 r2 |
R1*13 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2 | mi''1 | re'' | mib''! | re'' | }
  { re''2~ | re'' dod'' | re''1~ | re''2 do''!~ | do'' sib' | }
  { s2\p | s1 | s\f }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''2 sib'' |
    la''4 fa'' fa''
  }
  \tag #'(oboe2 oboi) {
    do''2 sol'' |
    fa''4 la' la'
  }
>> r4 |
