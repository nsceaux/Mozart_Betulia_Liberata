\clef "bass" sib,8 sib fa re sib,4 r |
sib,8 sib fa re sib,4 r |
sib,8 sib, sib, sib, re re re re |
mib4 sib, r2 |
fa4 la sib reb' |
fa la sib sib, |
fa r fa r |
fa fa fa r |
sib2(\p la |
sol fa) |
mib( re |
do) la |
sib8\f sib sib sib sol sol mib mib |
sib, sib, sib sib do' do' fa fa |
sib sib sib sib sol sol mib mib |
sib, sib, sib sib do' do' fa fa |
sib4 mib r2 |
r4 mib8 mib fa fa fa, fa, |
sib,4 sib8 re' sib re' sib re' |
sib4 sib, sib, r |
sib,8\p sib fa re sib,4 r |
sib,8 sib fa re sib,4 r |
sib,8 sib, sib, sib, re re re re |
mib4 sib, r2 |
fa4 la sib reb' |
fa la sib sib, |
fa8 fa fa fa fa fa fa fa |
fa4 fa\f fa r |
fa8\p fa fa fa fa fa fa fa |
sib sib sib sib sib sib la la |
sol sol sol sol do do do do |
re4 re'8\f re' do' do' sib sib |
la4 r la,\p r |
sib, r do r |
re r mi r |
fa4 r r2 |
fa8 fa fa fa si, si, si, si, |
do do'\f sol mi do4 r |
fa2(\p mi |
re do) |
sib,( la, |
sol,) sol4 fa |
mi8 mi mi mi mi mi mi mi |
fa4 r r2 |
fa8\fp fa fa fa re re sib, sib, |
fa\f fa fa fa sol sol do do |
fa\fp fa fa fa re re sib, sib, |
fa\f fa fa fa sol sol do do |
fa\p fa fa fa la la la la |
\ru#16 sib |
\ru#16 la |
sol sol sol sol fa fa fa fa |
mi mi mi mi mi mi mi mi |
fa fa fa fa sib, sib, sib, sib, |
sib,4 r r2 |
do'8 do' do' do' do do do do |
fa fa la la sib sib sib sib |
do' do' do' do' do' do' do' do' |
do\cresc do do do do do do do |
fa\f la do' la fa4 r |
fa8 la do' la fa4 r |
fa8 fa fa fa la la la la |
sib4 r r2 |
r4 sib8 sib do' do' do do |
fa4 fa8 la fa la fa la |
fa4 fa fa r |
R1 | \allowPageTurn
r4 fa8\p r mi r sol r |
fa r lab r sol r sib r |
lab r fa r mi r sol r |
fa fa fa fa mi mi mi mi |
fa fa fa fa fa fa fad\f fad |
sol4\p si do' mib' |
sol si do' do |
sol8 sol sol sol sol sol sol sol |
sol2\f sol,4 r |
mib!8\p mib mib mib mib mib mib mib |
fa fa fa fa fa fa fa fa |
sib, sib, sib, sib, sib, sib, sib, sib, |
mib mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
mib mib mib mib mib mib mi mi |
fa\f fa fa fa fa4 r |
sib,8\p sib fa re sib,4 r |
sib,8 sib fa re sib,4 r |
sib,8 sib, sib, sib, re re re re |
mib4 sib, r2 |
fa4 la sib reb' |
fa la sib reb' |
fa8 fa fa fa fa fa fa fa |
fa4 fa\f fa, r |
sib,\p r re r |
mib r fa r |
sol r la r |
sib8 sib sib sib re re re re |
mib mib mib mib mib mib mib mib |
fa fa fa fa fa fa fa fa |
sib2( la) |
sol( fa) |
mib re |
do la4 sol |
fa8 fa fa fa fa fa fa fa |
sib4\f sib, r2 |
sib8\p sib sib sib sol sol mib mib |
sib, sib, sib\f sib do' do' fa fa |
sib8\p sib sib sib sol sol mib mib |
sib, sib, sib\f sib do' do' fa fa |
sib\p sib sib sib re re re re |
mib4 r mib8 mib mib mib |
mib mib mib mib mib mib mib mib |
re4 r re8 re re re |
re re re re re re re re |
do sib, la, sib, do sib, la, r |
re do sib, do re do sib, r |
do sib, la, sib, do sib, la, r |
sib, r sib, r sib, r sib, r |
sib, sib, sib, sib, mib mib mib mib |
fa fa fa fa fa, fa, fa, fa, |
re re re re re re re re |
mib mib mib mib mib mib mib mib |
fa fa fa fa fa fa fa fa |
fa\cresc fa fa fa fa fa fa fa |
sib,\f sib, mib mib re re la la |
sib sib lab lab sol sol re re |
mib4 r r r8 mi |
fa1\fermata |
sib,8 sib fa re sib,4 r |
sib,8 sib fa re sib,4 r |
sib,8 sib, sib, sib, re re re re |
mib4 r4 r2 |
r4 mib8 mib fa fa fa fa |
sib,4 sib8 re' sib re' sib re' |
sib4 sib, sib, r4 |
mib8\p mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
lab lab lab lab sib sib sib sib |
mib mib mib mib mib\f mib re re |
do do\p do do do do do do |
fa fa sol sol lab lab sol sol |
fad fad fad fad fad fad fad fad |
sol sol sol sol sol\f sol la la |
sib\p sib sib sib sib sib sib sib |
sib sib la la sol sol fa fa |
dod dod dod dod dod dod dod dod |
re4 la r sol\f |
fa8\p fa fa fa fa fa fa fa |
sol sol sol sol sol sol sol sol |
la la la la la la la la |
sib4 r r2 |
fa8 fa fa fa fa fa fa fa |
sol sol sol sol sol sol sol sol |
la la la la la la la la |
la, la, la, la, la, la, la, la, |
re4 r r2 |
r4 do'\f la fa |
r sib sol re |
mib8 mib mib mib mib mib mi mi |
fa4 fa fa, r |
