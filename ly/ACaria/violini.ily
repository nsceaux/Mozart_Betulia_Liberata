\clef "treble" <re' sib'>2~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
<sib' re'>2~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
sib'8 do''16 re'' mib'' fa'' sol'' la'' sib'' do''' re''' do''' sib''8 r |
sol''8.\trill fa''32 sol'' fa''4 r \grace mib''16 re''8 do''16 sib' |
<<
  \tag #'violino1 {
    fa''4 r8 fa'' fa''( reb'') r sib' |
    la'-. fa'-. r fa''
  }
  \tag #'violino2 {
    fa'16( sol' la' sib') do''( re'' mib'' do'') reb''( do'' sib' la') sib'( reb'' do'' sib') do''( sib' la' sol') fa'( la' do'' mib'')
  }
>> \grace mib''16 reb''8 do''16 sib' \grace mib''16 reb''8 do''16 sib' |
fa''4 la'16 do'' fa'' la'' <fa'' la'>4 la'16 do'' fa'' la'' |
<la' fa''>4 <fa' la> q r |
<<
  \tag #'violino1 {
    \grace sol''8 fa''4\p mib''8 re'' re''4 re'' |
    \grace mib''8 re''4 do''8 sib' sib'2 |
    \grace do'''8 sib''4 la''8 sol'' sol''4 sol'' |
    \grace la''8 sol''4 fa''8 mib'' mib''2 |
    re''16\f sib'' sib'' sib'' sib''4:16 sib''2:16 |
    sib''4
  }
  \tag #'violino2 {
    sib8(\p re' fa' re') do'( re' fad' re') |
    sib( re' sol' re') sib( re' lab' re') |
    mib'( sol' sib' sol') sol( fa' si' fa') |
    sol( mib' re' do') do'( mib' fa' do') |
    sib4\f re''8.\trill do''32 re'' sol''4 sol''8.\trill fa''32 sol'' |
    fa''4
  }
>> re''16 mib'' fa'' re'' mib'' re'' do'' sib' la' sol' fa' mib' |
<<
  \tag #'violino1 {
    re'16 sib'' sib'' sib'' sib''4:16 sib''2:16 |
    sib''4
  }
  \tag #'violino2 {
    re'4 re''8.\trill do''32 re'' sol''4 sol''8.\trill fa''32 sol'' |
    fa''4
  }
>> re''16 mib'' fa'' re'' mib'' re'' do'' sib' la' sol' fa' mib' |
re'4 fad''16( sol''8.) re''16( mib''8.) si'16( do''8.) |
fad'16( sol'8.) sol''32( la'' sib''8.) re''32( mib'' fa''!8.) la'32( sib' do''8.) |
sib'4 sib''16( la'' sib'' do''') sib''( la'' sib'' do''') sib''( la'' sib'' do''') |
sib''4 <sib' re'>4 q r |
<re' sib'>2\p~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
<sib' re'>2~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
sib'8 do''16 re'' mib'' fa'' sol'' la'' sib'' do''' re''' do''' sib''8 r |
sol''8.\trill fa''32 sol'' fa''4 r \grace mib''16 re''8 do''16 sib' |
<<
  \tag #'violino1 {
    fa''4 r8 fa'' fa''( reb'') r sib' |
    la'-. fa'-. r fa''
  }
  \tag #'violino2 {
    fa'16( sol' la' sib') do''( re'' mib'' do'') reb''( fa'' mib'' reb'') do''( sib' la' sib') la'( sib' do'' la') fa'( la' do'' mib'')
  }
>> \grace mib''16 reb''8 do''16 sib' \grace mib''16 reb''8 do''16 sib' |
<<
  \tag #'violino1 {
    la'4:16 do'':16 re''!:16 mi'':16 |
    fa''8.
  }
  \tag #'violino2 {
    la'2:16 sib'4:16 sol':16 |
    la'8.
  }
>> do''16\f la'( do'' fa'' la'') <fa'' la'>4 r |
<<
  \tag #'violino1 {
    do''8\p do'' do'' do'' do'' do'' do'' do'' |
    re'' re'' re'' re'' re'' re'' do'' do'' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    la'4 fa''16(\f sol'' la'' fa'') mi''8 r re''16( mi'' fa'' re'') |
    do''8 r fa''\p fa'' fa''(\fp mib''!) mib''-. mib''-. |
    mib''(\fp re'') re''-. re''-. re''(\fp do'') do''-. do''-. |
    do''(\fp si') si'-. si'-. sol''(\fp mi'') sib'-. sib'-. |
    la'8 r fa''8.\trill mi''32 fa'' sol''8 r sib'8.\trill la'32 sib' |
    la'8 fa'' fa'' fa'' fa'' fa'' fa'' la' |
    sol'4 do'''16\f( si'' do''' re''') do'''4-. r |
    \grace re'''8 do'''4\p sib''8 la'' la''4 la'' |
    \grace sib''8 la''4 sol''8 fa'' fa''2 |
    \grace sol''8 fa''4 mib''8 re'' re''4 re'' |
    \grace mib''8 re''4 do''8 sib' sib'2 |
    sol''8 sol'' sol'' sol'' sib' sib' sib' sib' |
    la'-. la'(\f sib') do''-. re''-. do''-. re''-. mi''-. |
    fa''2:16\fp fa'':16 |
    fa''4
  }
  \tag #'violino2 {
    la'8\p la' la' la' la' la' la' la' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' mi' mi' mi' mi' |
    fa'8 fa''4\f fa'' fa'' fa''8 |
    fa''32( sol'' la''8.) r4 r8 fa'16(\p sol' fa'8) fa'-. |
    r8 fa'16( sol' fa'8) fa'-. r8 fa'16( sol' la'8) fa'-. |
    r8 fa'16( sol' lab'8) fa' r sol'16( fa' mi'8) sol' |
    fa'8-. do''( sib') la'-. sol'-. fa'-. mi'-. sol'-. |
    fa' la' la' la' la' la' la' fa' |
    mi'4 mi''16(-\sug\f re'' mi'' fa'') mi''4 r |
    fa'8\p( la' do'' la') sol'( la' dod'' la') |
    fa'8( la' re'' re') mib'!( fa' la' mib') |
    re'( fa' sib' fa') fa'( la' do'' fad') |
    sol'( sib' la' sol') fad'( sol') fad'( sol') |
    sib'8 sib' sib' sib' sol' sol' sol' sol' |
    fa'!-. fa'(\f sol') la'-. sib'-. la'-. sib'-. sol'-. |
    fa'4 la''8.\p\trill sol''32 la'' sib''8 r re''8.^\trillNatural do''32 re'' |
    do''4
  }
>> la''16\f sib'' do''' la'' sib'' la'' sol'' fa'' mi'' re'' do'' sib' |
<<
  \tag #'violino1 {
    la'16\p fa'' fa'' fa'' fa''4:16 fa''2:16 |
    fa''4
  }
  \tag #'violino2 {
    la'4 la''8.\p\trill sol''32 la'' sib''8 r re''8.^\trillNatural do''32 re'' |
    do''4
  }
>> la''16\f sib'' do''' la'' sib'' la'' sol'' fa'' mi'' re'' do'' sib' |
la'8\p <<
  \tag #'violino1 {
    fa''8 fa'' fa'' mib''! mib'' mib'' mib'' |
    re'' re'' re'' re'' fa'' fa'' fa'' fa'' |
    r sib''-. fa''-. re''-. sib'-. fa'-. re'-. sib-. |
    do' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    r8 fa'-. la'-. do''-. fa''-. la''-. fa''-. do''-. |
    si' si' si' si' si' si' si' si' |
    sib'! sib' sib' sib' sib' sib' sib' sib' |
    la' la' do'' do'' re'' re'' re'' re'' |
    re'' r fa'' r fa'' r re'' r |
    la''8 la'' la'' la'' sol'' sol'' sol'' sol'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    la''16-> la'' la'' la'' la'' la'' la'' la'' la''16-> la'' la'' la'' la'' la'' la'' la'' |
    sol''16->\cresc sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol''16-> sol'' sol'' sol'' sol'' sol'' sol'' sol'' 
  }
  \tag #'violino2 {
    la'8 la' la' do'' do'' do'' do'' |
    fa' fa' fa' fa' re'' re'' re'' re'' |
    re''4 r r2 | \allowPageTurn
    fa'8 do'' do'' do'' do'' do'' do'' do'' |
    do''4 r r2 |
    \ru#16 sol'8 |
    fa'8 fa' la' la' sib' fa' fa' fa' |
    fa' r re'' r re'' r fa'' r |
    fa'' fa'' fa'' fa'' mi'' mi'' mi'' mi'' |
    la'' la'' do'' do'' re'' re'' re'' re'' |
    fa''16-> fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa''-> fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa''16->\cresc fa'' fa'' fa'' fa'' fa'' fa'' fa'' mi''-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
  }
>>
<fa' la' fa''>2\f~ fa''8 la''16( sib'') do'''( la'') sib''( sol'') |
<fa'' la'>2~ fa''8 la''16( sib'') do'''( la'') sib''( sol'') |
fa''( sol'' fa'' sol'') la''( sib'' la'' sib'') do'''( sib'' la'') sib''-. la'' sol'' fa'' mib''! |
re''4 dod'''16( re'''8.) la''16( sib''8.) fad''16( sol''8.) |
dod''16( re''8.) sol''32( la'' sib''8.) la''32( sib'' do'''!8.) mi''32( fa''! sol''8.) |
fa''4 fa''16( mi'' fa'' sol'') fa''( mi'' fa'' sol'') fa''( mi'' fa'' sol'') |
fa''4 <fa' la>4 q r |
<<
  \tag #'violino1 {
    R1 | \allowPageTurn
    r4 lab''8\p r sol'' r sib'' r |
    lab''? r fa'' r mi'' r sol'' r |
    fa'' r lab'' r sol'' r sib'' r |
    lab''?8 fa''4 do'' reb''8 do'' sib' |
    lab' sol' fa'4 r \grace fa''16 mib''8\f re''!16 do'' |
    si'4\p r8 sol'' sol''( mib'') r do'' |
    si'-. sol'-. r sol''
  }
  \tag #'violino2 {
    <>\p \ru#32 do'8 |
    do'4 lab' sol'2 |
    fa'4 lab do' la'!\f |
    sol'16(-\sug\p la' si' do'') re''( mib'' fa'' re'') mib''( re'' do'' si') do''( sol' fa' mib') |
    re'( do' si la) sol-. re''( mib'' fa'')
  }
>> \grace fa''16 mib''8 re''16 do'' \grace fa'' mib''8 re''16 do'' |
<<
  \tag #'violino1 {
    si'4:16 re'':16 mib'':16 fad'':16 |
    sol''8\f <sol' si> q q q4 r |
    sib'!8\p sib' sib' sib' sib' sib' sib' sib' |
    sib' sib' sib' sib' re'' re'' re'' re'' |
    fa'' fa'' fa'' fa'' lab'' lab'' lab'' lab'' |
    \ru#24 sol'' |
    fa''8.
  }
  \tag #'violino2 {
    si'2:16 do'':16 |
    si'8\f <re' si>8 q q q4 r |
    sol'8\p sol' sol' sol' sol' sol' sol' sol' |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    re' re' re' re' re'' re'' re'' re'' |
    mib'' sib' sib' sib' sib' sib' si' si' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' sib' sib' |
    la'8.
  }
>> do''16\f la' do'' fa'' la'' <fa'' la'>4 r |
<re' sib'>2\p~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
<sib' re'>2~ sib'8 re''16( mib'') fa''( re'' mib'' do'') |
sib'8 do''16 re'' mib'' fa'' sol'' la'' sib'' do''' re''' do''' sib''8 r |
sol''8.\trill fa''32 sol'' fa''4 r \grace mib''16 re''8 do''16 sib' |
<<
  \tag #'violino1 {
    fa''4 r8 fa'' fa''( reb'') r sib' |
    la'-. fa'-. r fa'' fa''( reb'') r sib' |
    la'4:16 do'':16 re''!:16 mi'':16 |
    fa''8.
  }
  \tag #'violino2 {
    fa'16( sol' la' sib') do''( re'' mib'' do'') reb''( do'' sib' la') sib'( reb'' do'' sib') |
    la'( sib' do'' la') fa'( la' do'' mib'') reb''( do'' sib' la') sib'( do'' reb'' sib') |
    do''4:16 la':16 sib':16 sol':16 |
    la'8.
  }
>> do''16\f la' do'' fa'' la'' <fa'' la'>4 r |
<<
  \tag #'violino1 {
    fa''8(\fp re'') sib'-. sib'-. sib(\fp fa') lab'-. lab'-. |
    sol'8(\fp sib') sol''-. sol''-. fa''(\fp do'') la'-. fa''-. |
    sib''(\fp mi'') mi''-. mi''-. do'''(\fp la'') fa''-. mib''-. |
    re'' re'' re'' re'' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol''( mib'' re'' do'') |
    sib' sib' sib' sib' do''4 r |
    \grace sol''8 fa''4 mib''8 re'' re''4 re'' |
    \grace mib''8 re''4 do''8 sib' sib'2 |
    \grace do'''8 sib''4 la''8 sol'' sol''4 sol'' |
    \grace la''8 sol''4 fa''8 mib'' mib''2 |
    mib''8 mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re''-.\f re''-. mib''-. fa''-. sol''-. fa''-. sol''-. la''-. |
    sib''2:16\fp sib'':16 |
    sib''4
  }
  \tag #'violino2 {
    r8 sib16(\p do' sib8) sib-. r sib16( do' sib8) sib-. |
    r8 sib16( do' sib8) sib-. r la16( sib do'8) la-. |
    r sib16( do' reb'8) sib-. r do'16( re' mib'8) do'-. |
    sib sib sib sib fa' fa' fa' fa' |
    sib sib sib sib do'( mib' fa' sol') |
    fa' fa' fa' fa' la' la' la' la' |
    sib'( re' do' sib) do'( re' fad' do') |
    sib( sib' fad' sol') lab'( fa' mib' re') |
    mib'( sol' re' mib') fa'( re' do' si) |
    sol( mib' re' do') do'( fa' mib' sib!) |
    la la la la do'' do'' do'' do'' |
    sib'-.\f sib'-. do''-. re''-. mib''-. re''-. mib''-. do''-. |
    re''4 re''8.\trill\p do''32 re'' sol''4 sol''8.\trill fa''32 sol'' |
    fa''4
  }
>> re''16\f mib'' fa'' re'' mib'' re'' do'' sib' la' sol' fa' mib' |
<<
  \tag #'violino1 {
    re'16 sib''\p sib'' sib'' sib''4:16 sib''2:16 |
    sib''4
  }
  \tag #'violino2 {
    re'4 re''8.\p\trill do''32 re'' sol''4 sol''8.\trill fa''32 sol'' |
    fa''4
  }
>> re''16\f mib'' fa'' re'' mib'' re'' do'' sib' la' sol' fa' mib' |
re'8 <<
  \tag #'violino1 {
    fa''8\p fa'' fa'' fa'' fa'' fa'' fa'' |
    sol''4 r sol''8 sol'' sol'' sol'' |
    sib'' r sol'' r mib'' r sib' r |
    fa'4 r fa''8 fa'' fa'' fa'' |
    sib'' r fa'' r re'' r sib' r |
    mib''-. re''-. do''-. re''-. mib''-. re''-. do''16( re'' mib'' do'') |
    fa''8-. mib''-. re''-. mib''-. fa''-. mib''-. re''16( mib'' fa'' re'') |
    mib''8-. re''-. do''-. re''-. mib''-. re''-. do''16( re'' mib'' do'') |
    re''8 r mib'' r fa'' r la'' r |
    sib''( fa'') fa''( re'') sol''( mib'') re''-. do''-. |
    sib'-. re''([ fa'' re'']) \grace re''8 do''4 sib'8 la' |
    fa''8-. re''[ re'' sib'] fa''-. re''[ re'' sib'] |
    sol''-. mib''[ mib'' do''] sol''-. mib''[ mib'' do''] |
    re''2:16 re'':16 |
    do'':16\cresc do'':16 |
  }
  \tag #'violino2 {
    re''8\p re'' re'' sib' sib' sib' sib' |
    sib'4 r sib'8 sib' sib' sib' |
    \ru#8 { sol'16 sib' } |
    sib4 r sib'8 sib' sib' sib' |
    \ru#8 { sib16 re' } |
    \ru#6 fa'2:16 |
    sib'8 r do'' r re'' r mib'' r |
    re''4 sib'4. sol'8( fa') mib'-. |
    re'4. fa'8 \grace fa'8 mib'4 re'8 do' |
    \ru#4 sib2:16 |
    sib:16 sib':16 |
    sib':16\cresc la':16 |
  }
>>
sib'4\f <fa' do'' la''> <fa' re'' sib''> do'''8.\trill sib''32 do''' |
re'''4 <re'' fa' sib> <sib sol' mib''> fa''8.\trill <mib''>32 fa'' |
sol''4 sol''16( la'' sib'' la'') sib''( la'') sib''( la'') sib''8 mi' |
fa'2 r\fermata |
<re' sib'>2~ sib'8 re''16( mib'' fa'') re''( mib'' do'') |
<sib' re'>2~ sib'8 re''16( mib'' fa'') re''( mib'' do'') |
sib'8 do''16 re'' mib'' fa'' sol'' la'' sib'' do''' re''' do''' sib'' lab'' sol'' fad'' |
sol''4 fad''16 sol''8. re''16( mib''8.) si'16( do''8.) |
fad'16( sol'8.) sol''32( la'' sib''8.) re''32( mib'' fa''!8.) la'32( sib' do''8.) |
<sib' re'>4 sib''16( la'' sib'' do''') sib''( la'' sib'' do''') sib''( la'' sib'' do''') |
sib''4 <sib' re'> q r |
<<
  \tag #'violino1 {
    r8 mib'-.\p sol'-. sib'-. mib''-. sol''-. mib''-. sib'-. |
    r mib'-. lab'-. do''-. mib''-. lab''-. mib''-. do''-. |
    r mib'-. sol'-. sib'-. mib''-. sol''-. sib''-. sib'-. |
    do'' do'' do'' mib''-. re''( do'' sib') lab'-. |
    sol'4
  }
  \tag #'violino2 {
    sol'1\p |
    lab' |
    sol' |
    lab'8 mib' mib' do'' sib'( lab' sol') fa'-. |
    mib'4
  }
>> r8 sib'16(\f do'') sib'8 sol'' fa''8.\trill mib''32 fa'' |
mib''8 <<
  \tag #'violino1 {
    mib''8\p mib'' mib'' mib''( do'') sol''( mib'') |
    \ru#16 re'' |
    re'' re'' re'' re'' re''(\f sol'') fad''( la'') |
    sol''(\p la'' sib'' la'') sol''( fa''! mi'' re'') |
    dod''8 dod''4 dod'' sib''( la''8) |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    fa''4 mi''16\f
  }
  \tag #'violino2 {
    do''8\p do'' do'' do'' do'' do'' do'' |
    do'' do'4 do' do' do'8 |
    do' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' sib' sib' sib'\f re''4 re'8 |
    re'(\p fad' sol' la') sib'( la' sol' fa') |
    mi'8 mi'4 mi' dod''( re''8) |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re''4 dod''16\f 
  }
>> la' si' dod'' re'' mi'' fa'' sol'' la'' si'' dod''' la'' |
re'''8 re''\p re'' re'' <<
  \tag #'violino1 {
    re''8 re'' re'' re'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re''4 fa'' \grace la''8 sol''4 fa''8( mi'') |
    re''8\f \grace mi''16 re'' dod'' re''8 mi'' fa'' \grace sol''16 fa'' mi'' fa''8 sol'' |
    la''\p la'' la'' la'' la'' la'' la'' la'' |
    sib'' sib'' sib'' sib'' sib'' sib'' sib'' sib'' |
    fa''2:16 fa'':16 |
    mi'':16 mi'':16 |
    re''4 r r2 |
    mib''!2\f~ mib''8 do'''16 sib'' la'' sol'' fa'' mib'' |
    re''2~ re''8 sib''16 la'' sol'' fa'' mib'' re'' |
  }
  \tag #'violino2 {
    la'8 la' la' la' |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    la'4 re'' \grace fa''8 mi''4 re''8( dod'') |
    re''4 r re''8\f \grace mi''16 re'' dod'' re''8 mi'' |
    fa''\p re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re''2:16 re'':16 |
    re'':16 dod'':16 |
    re''8\f re'' re'' re'' re'' re'' re'' re'' |
    re'' sol' la' sib' do''2~ |
    do''8 fa' sol' la' sib'2 |
  }
>>
do''16( re'' do'' re'') mib''( fa'' mib'' fa'') sol''( la'' sol'' la'') sib''8 sib'-. |
la'16 do'' fa'' la'' fa'' do'' la' do'' <fa'' la'>4 r |
