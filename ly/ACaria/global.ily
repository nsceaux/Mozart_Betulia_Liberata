\tag #'all \key sib \major
\tempo "Allegro aperto" \midiTempo#120
\time 4/4 s1*99 \bar "||" \segnoMark \grace s8
s1*36 \bar "|."
\once\override Score.RehearsalMark.break-visibility = #begin-of-line-invisible
\mark\markup\musicglyph #"scripts.ufermata"
s1*26 \bar "|." \segnoMark
