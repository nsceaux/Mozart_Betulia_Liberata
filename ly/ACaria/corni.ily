\clef "treble" \transposition sib <>
<<
  \tag #'(corno1 corni) {
    do''2 do''4 s |
    do''2 do''4 s |
  }
  \tag #'(corno2 corni) {
    mi'2 mi'4 s |
    mi'2 mi'4 s |
  }
  { s2. r4 | s2. r4 | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 do''4 do'' |
    do''8 do'' do'' mi'' mi''4 }
  { do''2 do''4 do'' |
    do''8 do'' do'' do'' do''4 }
>> r4 |
<<
  \tag #'(corno1 corni) {
    sol''1~ |
    sol'' |
    sol''4 s re'' s |
    re'' re'' re''
  }
  \tag #'(corno2 corni) {
    sol'1~ |
    sol' |
    sol'4 s sol' s |
    sol' sol' sol'
  }
  { s1*2 | s4 r s r | }
>> r4 |
R1*4 |
r8 \tag #'corni <>^"a 2." do''\f do'' do'' do'' do'' do'' do'' |
do''4 r r2 |
r8 do'' do'' do'' do'' do'' do'' do'' |
<<
  \tag #'(corno1 corni) { mi''2 re'' | do''4 }
  \tag #'(corno2 corni) { do''2 sol' | do'4 }
>> r4 r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { do''4 mi'' re'' | }
  { do''2 sol'4 | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 mi''8 mi'' mi'' mi'' mi'' mi'' |
    mi''4 mi'' mi''
  }
  \tag #'(corno2 corni) {
    mi'4 do''8 do'' do'' do'' do'' do'' |
    do''4 do'' do'' 
  }
>> r4 |
R1*3 |
r4 <>\p <<
  \tag #'(corno1 corni) { mi''4 mi'' }
  \tag #'(corno2 corni) { do'' do'' }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1~ | sol' | sol'' | }
  { sol'1~ | sol' | sol' }
>>
<<
  \tag #'(corno1 corni) { sol''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol' }
  { s4 s\f }
>> r4 |
R1*3 |
<<
  \tag #'(corno1 corni) { sol''1~ | sol''4 }
  \tag #'(corno2 corni) { sol'1~ | sol'4 }
>> r4 r2 |
R1*4 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re'' re'' }
  { re'' re'' }
>> r4 |
R1*5 |
<<
  \tag #'(corno1 corni) { sol''1~ | sol''~ | sol''4 }
  \tag #'(corno2 corni) { sol'1~ | sol'~ | sol'4 }
>> r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 }
  { re'' }
>> r4 |
r8 <>\p <<
  \tag #'(corno1 corni) {
    sol''8 sol'' sol'' sol'' sol'' sol'' sol'' | sol''2
  }
  \tag #'(corno2 corni) {
    sol'8 sol' sol' sol' sol' sol' sol' | sol'2
  }
  { s4. s2 | s2\f }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | }
  { re'' }
>>
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
R1*11 | \allowPageTurn
<<
  \tag #'(corno1 corni) {
    re''1 |
    sol''2 s |
    sol'' s |
    re''2 re''8 re'' re'' re'' |
    mi''4 s2. |
    s4 mi''
  }
  \tag #'(corno2 corni) {
    re''1 |
    sol'2 s |
    sol' s |
    sol'2 sol'8 sol' sol' sol' |
    do''4 s2. |
    s4 do''
  }
  { s1\p |
    s2\f r |
    s r |
    s1 |
    s4 r r2 |
    r4 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' }
  { re'' re'' }
>>
<<
  \tag #'(corno1 corni) {
    re''4 s re'' s |
    re'' re'' re'' s |
    re''1 |
  }
  \tag #'(corno2 corni) {
    sol'4 s sol' s |
    sol' sol' sol' s |
    re''1 |
  }
  { s4 r s r | s2. r4 | s1\p | }
>>
R1*4 |
r4 <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
R1*10 |
r4 <>\f <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
R1*3 |
r4 <>\p <<
  \tag #'(corno1 corni) { mi''4 mi'' }
  \tag #'(corno2 corni) { do'' do'' }
>> r4 |
<<
  \tag #'(corno1 corni) {
    sol'1~ |
    sol' |
    sol'' |
    sol''4 re'' re''
  }
  \tag #'(corno2 corni) {
    sol'1~ |
    sol' |
    sol' |
    sol'4 sol' sol'
  }
  { s1*3 | s4 s\f }
>> r4 |
R1*11 |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1 | s8 do'' do'' do'' do'' do'' do'' do'' | }
  { do'1 | s8 do'' do'' do'' do'' do'' do'' do'' | }
  { s1\f | r8 s\p }
>>
<>\f <<
  \tag #'(corno1 corni) { mi''2 re'' | }
  \tag #'(corno2 corni) { do''2 sol' | }
>>
r8 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { do''8 do'' do'' do'' do'' do'' do'' | }
  { do''8 do'' do'' do'' do'' do'' do'' | }
>>
<>\f <<
  \tag #'(corno1 corni) { mi''2 re'' | }
  \tag #'(corno2 corni) { do'' sol' | }
>>
<>\f 
\twoVoices #'(corno1 corno2 corni) << { do''4 } { do'' } >> r4 r2 |
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { sol'1~ | sol'~ | sol' | do'' | }
  \tag #'(corno2 corni) { sol1~ | sol~ | sol | do' | }
>>
R1*5 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''4 re'' mi'' sol'' |
    mi''
  }
  \tag #'(corno2 corni) {
    sol'1 |
    mi'4 sol' do'' re'' |
    do''
  }
  { s1\p | s4 s2.\f }
>> \twoVoices #'(corno1 corno2 corni) <<
  { do''4 do'' do'' | do'' }
  { do'' do'' do'' | do'' }
>> r4 r r8 <<
  \tag #'(corno1 corni) { mi''8 | mi''2 }
  \tag #'(corno2 corni) { do''8 | do''2 }
>> r2\fermata |
<<
  \tag #'(corno1 corni) {
    do''2 do''4 s |
    do''2 do''4 s |
    do''1 |
    do''4
  }
  \tag #'(corno2 corni) {
    mi'2 mi'4 s |
    mi'2 mi'4 s |
    do'1 |
    do'4
  }
  { s2. r4 | s2. r4 | }    
>> r4 r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { do''4 mi'' re'' | }
  { do''2 sol'4 | }
>> <<
  \tag #'(corno1 corni) {
    do''4 s mi''4 s |
    mi'' mi'' mi''
  }
  \tag #'(corno2 corni) {
    mi'4 s do''4 s |
    do'' do'' do''
  }
  { s4 r s r }
>> r4 |
R1*22 |
r2 <>\f <<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>>
r2 \twoVoices #'(corno1 corno2 corni) <<
  { do''2 | re''1 | }
  { do''2 | re''1 }
>>
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol' }
>> r4 |
