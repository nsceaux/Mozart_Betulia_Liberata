\clef "soprano/treble" <>^\markup\character Cabri
r8 sib' sib' la' do''4 r |
\ffclef "soprano/treble" <>^\markup\character Amital
do''8 do''16 do'' re''8 mib'' re'' re'' r16 la' la' sib' |
do''8 do'' do'' mib'' do'' do'' r la' |
fad' fad' r la'16 sib' do''4 re''8 la' |
sib'8 sib' r16 sol' sol' la' sib'8 sib' r sib'16 sib' |
sib'4 do''8 re'' do'' do'' r16 do'' do'' re'' |
sib'8 sib' r sol' sib' sib' do'' sol' |
la'4 r16 la' la' si'! dod''8 dod'' r dod''16 mi'' |
dod''4 si'8 la' re'' re'' r re''16 re'' |
re''4 do''!8 re'' sib' sib' r16 sib' sib' la' |
do''8 do'' r4
\ffclef "soprano/treble" <>^\markup\character Cabri
do''8 do''16 do'' do''8 fa'' |
fa'' do'' r do'' mib'' mib'' mib'' reb'' |
sib'4 r16 reb'' reb'' do'' si'8 si' r si' |
re''! re'' re'' mib'' do''4 r8 do'' |
do''4 do''8 mib'' re'' re'' r la'16 sib' |
do''4 do''8 sib' sol' sol' r4 |
re''4 re''8 mi''! dod'' dod'' dod'' re'' |
mi''4 mi''8 fa'' re''4 r |
r8 re'' re'' do'' si'! si' r4 |
si'8 si'16 si' si'8 do'' re'' re'' fa'' mib'' |
do'' do'' r do'' lab' lab' r16 lab' lab' lab' |
fa'8 fa' r reb'' sib' sib' r16 sib' sib' sib' |
sol'8 sol' r16 sol' sol' lab' sib'8 sib' r sib'16 sib' |
sib'4 do''8 reb'' do'' do'' sib' do'' |
lab' lab' r do''16 fa'' fa''8 si'!16 si' r8 do'' |
do'' sol' r4 r8 la'! la' sib' |
do'' do'' do'' do''16 sib' sol'4 r |
sol'8 sol'16 sol' sol'8 sol' dod'' dod'' r dod'' |
dod'' dod'' re'' mi'' mi'' sol' r sol'16 la' |
sib'8 sib' r sib'16 la' fa'8 fa' r16 la' la' re'' |
re''8 la' r16 do''! do'' re'' sib'4 r |
sol''8 mi''16 re'' dod''8 re'' re'' la' r4 |
R1 |
