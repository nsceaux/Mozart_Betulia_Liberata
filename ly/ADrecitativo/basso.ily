\clef "bass" sol2 fa~ |
fa fad~ |
fad1~ |
fad |
sol~ |
sol2 mi~ |
mi1 |
dod1~ |
dod2 fa~ |
fa sol |
fa1~ |
fa2 fa |
reb fa~ |
fa mib~ |
mib fad~ |
fad sol~ |
sol1~ |
sol2 fa~ |
fa1~ |
fa |
mib2 do |
reb re |
mib1~ |
mib2 mi |
fa1 |
r4 sol fad2~ |
fad dod~ |
dod sib,2~ |
sib, la,~ |
la, re |
fad sol~ |
sol r4 la |
re1 |
