E in che spe -- rar?

Nel -- la di -- fe -- sa for -- se
di no -- stre schie -- re in -- de -- bo -- li -- te, e sce -- me
dall’ as -- si -- dua fa -- ti -- ca? es -- te -- nu -- a -- te
dal -- lo scar -- so a -- li -- men -- to? in -- ti -- mo -- ri -- te
dal pian -- to u -- ni -- ver -- sal? Fi -- dar pos -- sia -- mo
ne’ vi -- ci -- ni già vin -- ti?
Ne -- gli a -- mi -- ci im -- po -- ten -- ti? in Dio sde -- gna -- to?

Scor -- ri per o -- gni la -- to
la mi -- se -- ra cit -- tà; non tro -- ve -- ra -- i
che og -- get -- ti di ter -- ror. Gli or -- di -- ni u -- sa -- ti
son ne -- glet -- ti, o con -- fu -- si. Al -- tri s’a -- di -- ra
con -- tro il ciel, con -- tro te; pian -- gen -- do ac -- cu -- sa
al -- tri le pro -- prie col -- pe an -- ti -- che e nuo -- ve;
chi cor -- re, e non sa do -- ve;
chi ge -- me, e non fa -- vel -- la; e lo spa -- ven -- to
come in a -- ri -- da sel -- va ap -- pre -- sa fiam -- ma
si co -- mu -- ni -- ca, e cres -- ce. O -- gnun si cre -- de
pres -- so a mo -- rir. Già ne’ con -- ge -- di e -- stre -- mi,
s’ab -- brac -- cia -- no a vi -- cen -- da
i con -- giun -- ti, gli a -- mi -- ci; ed è de -- ri -- so
chi o -- sten -- ta an -- cor qual -- che fer -- mez -- za in vi -- so.
