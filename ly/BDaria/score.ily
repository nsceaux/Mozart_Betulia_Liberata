\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = "Corni in B."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Amital
      shortInstrumentName = \markup\character Am.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\break s1*6\pageBreak
        s1*5\break s1*5\break s1*6\pageBreak
        s1*5\break s1*7\break s1*6\pageBreak
        s1*6\break s1*6\break s1*7\pageBreak
        s1*7\break s1*5\break s1*7\pageBreak
        s1*6\break s1*6\break s1*8\pageBreak
        s1*5\break s1*3 s2.*3\break s2.*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
