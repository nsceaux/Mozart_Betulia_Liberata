\tag #'all \key sib \major
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*69 \bar "|." \segnoMark
s1*42 \bar "|." \fermataMark
\time 3/4 s2.*14 \bar "||"
\tempo "Tempo primo"
\time 4/4 s1*4 \bar "|." \dalSegnoMark
