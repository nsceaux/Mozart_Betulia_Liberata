\clef "treble" <re' sib'>2 <fa' re''> |
fa''4 r8 sib' \grace re''8 do''4 sib'8 la' |
sib'( fa') <<
  \tag #'violino1 {
    re''8-. fa''-. \grace fa''8 mib''4 re''8 do'' |
    sib'8 do''16 re'' mib'' fa'' sol'' la'' sib''4 lab'' |
    \ru#2 { sol''16( fa'' mib'' re'') mib''8-. r } |
    \ru#2 { re''16( do'' sib' la') sib'8-. r } |
  }
  \tag #'violino2 {
    sib'8-. re''-. \grace re''8 do''4 sib'8 la' |
    <sib' re'>2 fa'8 fa' fa' fa' |
    sol' r sol'16( fa' mib' re') mib'8-. r mib'16( fa' sol' la') |
    sib'8-. r fa'16( mib' re' do') sib8-. r sib16( re' fa' sib') |
  }
>>
sol''16( la'') sib''-. sib''-. sib''4:16^\dotFour sib''8 sib' la' sol' |
<<
  \tag #'violino1 {
    \ru#2 { la'16 do'' fa'' fa'' fa''4:16 fa''8-. re''( do'' sib') | }

  }
  \tag #'violino2 {
    \ru#2 { la'16 do'' fa'' fa'' fa''4:16 fa''8-. sib'( la' sol') | }
  }
>>
la'4 <fa' la' fa''> <fa' la> r |
<>\p <<
  \tag #'violino1 {
    re''8 \grace mib''32 re''16( do''32 re'' mib''8) do''-. fa'' r sib'' r |
    mib''8 \grace fa''32 mib''16( re''32 mib'' re''8) fa''-. do'' r fa' r |
    re''8 \grace mib''32 re''16( do''32 re'' mib''8) do''-. fa''-. r sib''-. r |
    mib''8 \grace fa''32 mib''16( re''32 mib'' re''8) fa''-. do''-. r fa'-. r |
  }
  \tag #'violino2 {
    re'8 \grace mib'32 re'16( do'32 re' mib'8) do'-. fa' r sib' r |
    mib'8 \grace fa'32 mib'16( re'32 mib' re'8) fa'-. do'8 r la r |
    re'8 \grace mib'32 re'16( do'32 re' mib'8) do'-. fa'-. r sib'-. r |
    mib'8 \grace fa'32 mib'16( re'32 mib' re'8) fa'-. do'8-. r la-. r |
  }
>>
re''16\f do'' sib' la' sib' do'' re'' mi'' fa'' sol'' fa'' re'' mib'' fa'' mib'' do'' |
re'' do'' sib' la' sib' do'' re'' mi'' fa'' sol'' fa'' re'' mib'' fa'' mib'' do'' |
re'' fa'' mib'' re'' mib'' fa'' sol'' la'' sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
lab' sol' fa' mib' re' do' sib lab sol4 sol'' |
fa' <<
  \tag #'violino1 {
    sib''16( fa'') sib''( fa'') re''8.( fa''16) \grace fa''16 mib''8 re''16 do'' |
  }
  \tag #'violino2 {
    fa''16( re'') fa''( re'') sib'8.( re''16) \grace re''16 do''8 sib'16 la' |
  }
>>
<sib' re'>4 r8 <<
  \tag #'violino1 {
    sib''8 re''8. fa''16 \grace fa''16 mib''8 re''16 do'' |
    <re'' fa'>4 r8 sib'' re''8. fa''16 \grace fa''16 mib''8 re''16 do'' |
  }
  \tag #'violino2 {
    fa''16( re'') sib'8. re''16 \grace re''16 do''8 sib'16 la' |
    <sib' re'>4 r8 re'' sib'8. re''16 \grace re''16 do''8 sib'16 la' |
  }
>>
sib'4 <re' sib' sib''>4 q r |
<<
  \tag #'violino1 {
    <re' sib'>2\p <fa' re''> |
    fa''4 r8 sib' \grace re''8 do''4 sib'8( la') sib'( fa') re''-. fa''-. fa''( mib'') re''-. do''-. |
    sib'8\f do''16 re'' mib'' fa'' sol'' la'' sib''4 lab'' |
    <>\p \ru#2 { sol''16( fa'' mib'' re'') mib''8 r } |
    \ru#2 { re''16( do'' sib' la') sib'8 r } |
    \ru#2 { sol''16( fa'' mib'' re'') mib''8 r } |
    \ru#2 { re''16( do'' sib' la') sib'8 r } |
    do''8 do'' do'' do'' do'' do'' sib' sib' |
    la'4 do'2 re'8( mib') |
    re'4-. sib'( la' sol') |
    fa' do'2 re'8( mib') |
    re'4-. sib'( la' sol') |
    fa' r r2\fermata | \allowPageTurn
    la'8\p \grace sib'32 la'16( sol'32 la' sib'8) sol'-. do''-. r fa''-. r |
    mi''8 \grace fa''32 mi''16( re''32 mi'' fa''8) re''-. sol'' r sib'-. r |
    la'-. \grace re''32 do''16( sib'32 do'' fa''8) do''-. la''-. r fa''-. r |
    mi''8 \grace fa''32 mi''16( re''32 mi'' fa''8) re''-. sol''-. r sib'-. r |
    la'8 la' la' la' do'' do'' do'' do'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    mi''4 r sol'' r |
    sib''8 sib'' sib'' sib'' sib' sib' sib' sib' |
    la'4 r fa'' r |
    la''8 la'' la'' la'' la' la' la' la' |
    sib'4 r mi'' r |
    sol''8 sol'' sol'' sol'' <sib' sol'>8 q q q |
    la'8 do'' fa'' do'' re'' fa'' sol' sib' |
    la' la'' la'' la'' sol'' sol'' sol'' sol'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    la' la' la' la' la' la' la' la' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
  }
  \tag #'violino2 {
    <sib fa'>2\p <re' sib'> |
    re''4 r8 sib' sol'( mib') re'( do') |
    fa'( re') fa'-. re''-. re''( do'') sib'-. la'-. |
    <sib' re'>2\f fa'8\f fa' fa' fa' |
    sol'\p r sol'16( fa' mib' re') mib'8 r mib'16( fa' sol' la') |
    sib'8 r fa'16( mib' re' do') sib8 r sib'16( lab' sol' fa') |
    mib'8 r sol'16( fa' mib' re') mib'8 r mib'16( fa' sol' la'!) |
    sib'8 r fa'16( mib' re' do') sib8 r sib'16( do'' sib' la') |
    sol'8 sol' sol' sol' sol' sol' sol' sol' |
    fa'4 la2 sib8( do') |
    sib4-. re'( do' sib) |
    la la2 sib8( do') |
    sib4-. re'( do' sib) |
    la r r2\fermata | \allowPageTurn
    la8\p \grace sib32 la16( sol32 la sib8) sol-. do'-. r fa'-. r |
    mi'8 \grace fa'32 mi'16( re'32 mi' fa'8) re'-. sol'-. r sib-. r |
    la8 \grace re'32 do'16( sib32 do' fa'8) do'-. la'-. r fa'-. r |
    mi'8 \grace fa'32 mi'16( re'32 mi' fa'8) re'-. sol'-. r sib-. r |
    la8 fa' fa' fa' la' la' la' la' |
    la' la' la' la' si' si' si' si' |
    do''4 r mi'' r |
    mi''8 mi'' mi'' mi'' mi' mi' mi' mi' |
    fa'4 r la' r |
    fa''8 fa'' fa'' fa'' la' la' la' la' |
    sol'4 r sol' r |
    sib'8 sib' sib' sib' mi' mi' mi' mi' |
    fa' fa'4 fa' fa' fa'8 |
    fa' fa'' fa'' fa'' mi'' mi'' mi'' mi'' |
    fa'' fa'' fa'' fa'' do'' do'' do'' do'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' fa' fa' fa' mi' mi' mi' mi' |
  }
>>
fa'16\f fa'' la'' sol'' fa'' mi'' re'' do'' sib' mi'' sol'' fa'' mi'' re'' do'' sib' |
la' fa'' la'' sol'' fa'' mi'' re'' do'' sib' mi'' sol'' fa'' mi'' re'' do'' sib' |
la' do'' sib' la' sib' do'' re'' mi'' fa'' mi'' re'' do'' sib' la' sol' fa' |
<<
  \tag #'violino1 {
    mib'!2:16 re'4 re''' |
    do'4 fa''16( do'') fa''( do'') la'8. do''16 \grace do''16 sib'8 la'16 sol' |
    la'4 r8 la''16( fa'') do''8. fa''16 \grace do''16 sib'8 la'16 sol' |
    la'4 r8 do'''16( la'') fa''8. la''16 \grace la''16 sol''8 fa''16 mi'' |
    fa''4
  }
  \tag #'violino2 {
    do'2:16 sib4 fa'' |
    do'4 do''16( la') do''( la') fa'8. la'16 \grace la'16 sol'8 fa'16 mi' |
    fa'4 r8 do''16( la') fa'8. la'16 \grace la'16 sol'8 fa'16 mi' |
    fa'4 r8 la''16( do'') la'8. do''16 \grace do''16 sib'8 la'16 sol' |
    fa'4
  }
>> <fa' la' fa''>4 q r |
<<
  \tag #'violino1 {
    fa''2:16\p re'':16 |
    si'4:16 re'':16 si':16 sol':16 |
    <sol fa'>2:16 q:16 |
    q8 si'4 re'' re'' fa''8 |
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re''( sib'!) re''( fa'') mib''( re'') do''( sib') |
    la'8 <la' fa''>\f q q q4 r |
  }
  \tag #'violino2 {
    lab'2:16\p fa':16 |
    re'4:16 fa':16 re':16 si:16 |
    <si re'>2:16 q:16 |
    q8 re'4 fa' si' re''8 |
    do'' sol' sol' sol' la' la' la' la' |
    sib'4 sib2 sol'4 |
    fa'8 <fa' la'>\f q q <la' fa' do'>4 r |
  }
>>
<<
  \tag #'violino1 {
    <re' sib'>2\p <fa' re''> |
    fa''4 r8 sib' \grace re''8 do''4 sib'8( la') |
    sib'( fa') re''-. fa'' fa''( mib'') re''( do'') |
    sib'8\f do''16 re'' mib'' fa'' sol'' la'' sib''4 lab'' |
    <>\p \ru#2 { sol''16( fa'' mib'' re'') mib''8-. r } |
    \ru#2 { re''16( do'' sib' la') sib'8-. r } |
    \ru#2 { sol''16( fa'' mib'' re'') mib''8-. r } |
    re''16( do'' sib' la') sib'8 r fa''16( mib'' re'' do'') re''8-. r |
    do''8 do'' do'' do'' sib' sib' sib' sib' |
    la'4 do'2 re'8( mib') |
    re'4 sib'( la' sol') |
    fa' do'2 re'8( mib') |
    re'4 sib'( la' sol') |
    fa'4 r r2\fermata |
    re''8\p \grace mib''32 re''16( do''32 re'' mib''8) do''-. fa''-. r sib''-. r |
    la''8 \grace sib''32 la''16( sol''32 la'' sib''8) sol''-. do'''-. r mib''-. r |
    re''8 \grace mib''32 re''16( do''32 re'' mib''8) do''-. fa''-. r sib'-. r |
    mib'' \grace fa''32 mib''16( re''32 mib'' re''8) fa''-. do''-. r fa'-. r |
    re''8 re'' re'' re'' re'' re'' re'' re'' |
    fa'' fa'' fa'' fa'' fa'' fa'' lab'' lab'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    r sol''( mib'' sib') r mib''( sib' sol'') |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    r fa''( re'' sib') r re''( sib' fa') |
    \ru#2 { mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' } |
    re'' r fa'' r mib'' r mib'' r |
    re'' r re'' r mib'' r mib'' r |
    re'' fa'' re'' sib' sol' sol'' mib'' do'' |
    re'' re'' re'' re'' do'' do'' do'' do'' |
    fa'' fa'' fa'' fa'' fa'' fa'' fa'' fa'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
  }
  \tag #'violino2 {
    <sib fa'>2\p <re' sib'> |
    re''8 re' re' re' sol'( mib') re'( do') |
    fa'4. re''8 re''( do'') sib'( la') |
    sib'4 r fa'8\f fa' fa' fa' |
    sol'\p r sol'16( fa' mib' re') mib'8-. r mib'16( fa' sol' la') |
    sib'8-. r fa'16( mib' re' do') sib8-. r re'16( mib' fa' re') |
    mib'8-. r sol'16( fa' mib' re') mib'8-. r mib'16( fa' sol' la') |
    sib'8-. r fa'16( mib' re' do') sib8-. r sib'16( la' sol' fa') |
    sol'8 sol' sol' sol' sol' sol' sol' sol' |
    fa'4 la2 sib8( do') |
    sib4 re'( do' sib) |
    la la2 sib8( do') |
    sib4 re'( do' sib) |
    la4 r r2\fermata |
    re'8\p \grace mib'32 re'16( do'32 re' mib'8) do'-. fa'-. r sib'-. r |
    la'8 \grace sib'32 la'16( sol'32 la' sib'8) sol'-. do''-. r mib'-. r |
    re'8 \grace mib'32 re'16( do'32 re' mib'8) do'-. fa'-. r sib-. r |
    mib'8 \grace fa'32 mib'16( re'32 mib' re'8) fa'-. do'-. r do'-. r |
    sib8 fa' fa' fa' fa' sib' sib' sib' |
    \ru#2 { sib' sib' sib' sib' sib' sib' sib' sib' } |
    r mib''( sib' sol') r sib'( sol' mib') |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    r re''( sib' fa') r sib( fa' re') |
    la8 la la la la' la' sol' sol' |
    fa' fa' fa' fa' fa' fa' fa' fa' |
    fa' r sib' r la' r do'' r |
    sib' r fa' r do'' r do'' r |
    sib'8 sib4 sib sib sib8 |
    sib' sib' sib' sib' la' la' la' la' |
    \ru#2 { sib' sib' sib' sib' sib' sib' sib' sib' } |
  }
>>
mi'8\f fa' sol' la' sib' sol' fa' mi' |
fa'1\fermata |
sib'16 do'' sib' la' sib' re'' sol'' fa'' mib'' re'' do'' sib' la' do'' fa'' mib'' |
re'' do'' sib' la' sib' re'' sol'' fa'' mib'' re'' do'' sib' la' do'' fa'' mib'' |
re'' fa'' mib'' re'' mib'' fa'' sol'' la'' sib'' la'' sol'' fa'' mib'' re'' do'' sib' |
lab' sol' fa' mib' re' do' sib lab sol4 sol'' |
fa' <<
  \tag #'violino1 {
    sib''16( fa'') sib''( fa'') re''8. fa''16 \grace fa''16 mib''8 re''16 do'' |
    <sib' re'>4 r8 sib'' re''8. fa''16 \grace fa''16 mib''8 re''16 do'' |
    <re'' fa'>4 r8 sib'' re''8. fa''16 \grace fa''16 mib''8 re''16 do'' |
    sib'4 <re' sib' sib''>4 q r |
  }
  \tag #'violino2 {
    fa''16( re'') fa''( re'') sib'8. re''16 \grace re'' do''8 sib'16 la' |
    <sib' re'>4 r8 fa''16( re'') sib'8. re''16 \grace re'' do''8 sib'16 la' |
    <sib' re'>4 r8 fa''16( re'') sib'8. re''16 \grace re''16 do''8 sib'16 la' |
    sib'4 <sib fa' re''>4 q r |
  }
>>
<<
  \tag #'violino1 {
    r16 re''(\p sib' re'') r re''( sib' re'') r re''( sib' re'') |
    r fad''( do'' fad'') r fad''( do'' fad'') r fad''( do'' fad'') |
    r sol''( re'' sol'') r sol''( sib' sol'') r sol''( re'' sol'') |
    r la''( fad'' re'') r re''( la' re'') r re'( la' do'') |
    r sib'( re'' sib') r sib'( sol'' sib') r sol''-. sol''( dod'') |
    re''4 r\fermata r |
    sib'16 sib'8 sib' sib' sib' sib' sib'16 |
    sib' sib'8 sib' sib' sib' sib' sib'16 |
    la' la'8 la' la' la' la' la'16 |
    sib'16 sib'8 sib' sib' sib' reb'' reb''16 |
    do''8 do''4 reb''16 sib' lab'8 sol' |
    fa''( mib''!) r mib'' r mib'' |
    mib''( reb'') r reb'' r fa''16( reb'') |
    r8 do'' r do'' r mi' |
    fa'4 lab''(\f si'8) r r4 |
    r sol''4( la'!8) r r4 |
    sib'!16 do'' re'' mib'' fa'' sol'' la'' sib'' sol'' fa'' mib'' re'' do'' sib' la' sib' |
    la'8 <la' fa''> q q q4 r |
  }
  \tag #'violino2 {
    r16 sib'(\p sol' sib') r sib'( sol' sib') r sib'( sol' sib') |
    \ru#3 { r do''( re' do'') } |
    r16 re''( sib' re'') r re''( re' re'') r re''( sib' re'') |
    r re''( la' fad') r la'( fad' re') r la'( re' la') |
    r re'( sib' sol') r sol'( sib' sol') r sib'-. sib'( sol') |
    fad'4 r\fermata r |
    fa'!16 fa'8 fa' fa' fa' fa' fa'16 |
    solb' solb'8 solb' solb' solb' solb' solb'16 |
    mib' mib'8 mib' mib' mib' mib' mib'16 |
    reb' reb'8 reb' reb' reb' mi' mi'16 |
    fa'8 fa'4 sol'8 fa' mi' |
    fa' fa'4 fa' fa'8~ |
    fa' fa'4 fa' fa'8 |
    r lab' r lab' r sol' |
    fa'4 lab'(\f si8) r r4 |
    r sol'4( la!8) r r4 |
    fa'4 sib'!2 sol'4 |
    fa'8 <fa' la'> q q q4 r |
  }
>>
