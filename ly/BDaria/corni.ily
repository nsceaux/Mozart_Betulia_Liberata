\clef "treble" \transposition sib <>
<<
  \tag #'(corno1 corni) { do''2 do'' | do''4 }
  \tag #'(corno2 corni) { mi'2 mi' | mi'4 }
>> r4 r \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 |
    do'' mi''8 sol'' sol'' fa'' mi'' re'' | }
  { sol'4 |
    do''4. mi''8 mi'' re'' do'' sol' | }
>>
<<
  \tag #'(corno1 corni) { do''1~ | do''~ | do''~ | do''2 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'~ | do'2 }
>> r4 r8 \twoVoices #'(corno1 corno2 corni) << re''8 re'' >> |
<<
  \tag #'(corno1 corni) {
    sol''1~ |
    sol''~ |
    sol''4 re'' re''
  }
  \tag #'(corno2 corni) {
    sol'1~ |
    sol'~ |
    sol'4 sol' sol'
  }
>> r4 |
R1*4 |
<>\f <<
  \tag #'(corno1 corni) {
    do''4 s8 mi'' re''4 s8 re'' |
    mi''4 s8 mi'' re''4 s8 re'' |
  }
  \tag #'(corno2 corni) {
    do'4 s8 do'' sol'4 s8 sol' |
    do''4 s8 do'' sol'4 s8 sol' |
  }
  { s4 r8 s s4 r8 s | s4 r8 s s4 r8 s | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 do''4 }
  { do''2 do''4 }
>> r4 |
r8 <<
  \tag #'(corno1 corni) { do''8 do'' do'' do''4 }
  \tag #'(corno2 corni) { do'8 do' do' do'4 }
>> \twoVoices #'(corno1 corno2 corni) << do''4 do'' >> |
<<
  \tag #'(corno1 corni) { mi''2~ mi''8 sol'' fa'' re'' | }
  \tag #'(corno2 corni) { do''2~ do''8 mi'' re'' sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 r }
  { do'8 mi' sol' do'' }
>> <<
  \tag #'(corno1 corni) { sol''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do'8 mi' sol' do'' sol'4 }
  { do'8 mi' sol' do'' sol'4 }
>> r4 |
<<
  \tag #'(corno1 corni) { do''4 do'' do'' }
  \tag #'(corno2 corni) { mi' mi' mi' }
>> r4 |
R1*3 |
<>\f <<
  \tag #'(corno1 corni) { do''1 }
  \tag #'(corno2 corni) { do' }
>>
R1*5 |
<<
  \tag #'(corno1 corni) {
    sol''1~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''4
  }
  \tag #'(corno2 corni) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4
  }
>> r4 r2\fermata |
R1*17 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 |
    re''2 re'' |
    re'' re'' |
    re''4 re'' re'' }
  { re''1 |
    sol'2 re'' |
    sol' re'' |
    sol'4 re'' sol' }
  { s1 s\f }
>> r4 |
<<
  \tag #'(corno1 corni) {
    sol''2 sol''4 s |
    re''1 |
    re''4
  }
  \tag #'(corno2 corni) {
    sol'2 sol'4 s |
    re''1 |
    sol'4
  }
  { s2. r4 }
>> r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' re'' | }
  { re''8 re'' re'' re'' | }
>>
<<
  \tag #'(corno1 corni) { sol''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''8 re'' re'' re'' | }
  { re''8 re'' re'' re'' | }
>> <<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol' }
>> r4 |
R1*6 | \allowPageTurn
r8 <>\f <<
  \tag #'(corno1 corni) { re''8 re'' re'' re''4 }
  \tag #'(corno2 corni) { sol'8 sol' sol' sol'4 }
>> r4 |
R1*3 |
<>\f <<
  \tag #'(corno1 corni) { do''1 }
  \tag #'(corno2 corni) { do' }
>>
R1*5 |
<<
  \tag #'(corno1 corni) {
    sol''1~ |
    sol''~ |
    sol''~ |
    sol''~ |
    sol''4
  }
  \tag #'(corno2 corni) {
    sol'1~ |
    sol'~ |
    sol'~ |
    sol'~ |
    sol'4
  }
>> r4 r2\fermata |
R1*18 |
<>\f <<
  \tag #'(corno1 corni) { mi''1~ | mi''2 }
  \tag #'(corno2 corni) { do''1~ | do''2 }
>> r2\fermata |
<<
  \tag #'(corno1 corni) { mi''2 fa'' | }
  \tag #'(corno2 corni) { do'' re'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re'' | do''4 }
  { do''2. sol'4 | do'' }
>> <<
  \tag #'(corno1 corni) {
    re''4 mi'' s |
    do''1 |
    mi''2. re''4 |
  }
  \tag #'(corno2 corni) {
    sol'4 do'' s |
    do'1 |
    do''2. sol'4 |
  }
  { s2 r4 | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 r sol'' }
  { do'8 mi' sol' do'' sol'4 }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { do'8 mi' sol' do'' sol'4 }
  { do'8 mi' sol' do'' sol'4 }
>> r4 |
<<
  \tag #'(corno1 corni) { do''4 mi'' mi'' }
  \tag #'(corno2 corni) { mi'4 do'' do'' }
>> r4 |
R2.*5 |
R2.^\fermataMarkup |
R2.*8 |
R1 |
r2 <<
  \tag #'(corno1 corni) {
    re''2 |
    mi'' re'' |
    re''8 re'' re'' re'' re''4
  }
  \tag #'(corno2 corni) {
    sol'2 |
    do'' do'' |
    sol'8 sol' sol' sol' sol'4
  }
>> r4 |
