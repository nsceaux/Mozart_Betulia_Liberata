\clef "treble"
<<
  \tag #'oboe1 {
    sib'2 re'' |
    fa''4 r r2 |
    r8 sib' re'' fa'' \grace fa''8 mib''4 re''8 do'' sib'2 fa'' |
    sol''1 |
    fa'' |
    sol''2 r8 re'' do'' sib' |
    la'-. do''( re'' mib'') re''4 r |
    r8 do''( re'' mib'') re''4 r |
    do'' fa'' fa'' r |
  }
  \tag #'oboe2 {
    re'2 sib' |
    re''4 r r2 |
    r8 fa' sib' re'' \grace re''8 do''4 sib'8 la' |
    sib'2 re'' |
    mib''1 |
    re''2 sib'~ |
    sib' r8 sib' la' sol' |
    fa'-. la'( sib' do'') sib'4 r |
    r8 la'( sib' do'') sib'4 r |
    la' la' la' r |
  }
>>
R1*2 |
<<
  \tag #'oboe1 {
    fa''1\p~ |
    fa'' |
    re''4.\f mi''8 fa''4 mib'' |
    re''4. mi''8 fa''4 mib'' |
    re''4 la' sib' fa'' |
    lab''2 sol''4 sib' |
    re''2. do''4 |
    sib' r re'' do'' |
    re'' r re'' do'' |
    sib' sib'' sib'' r |
  }
  \tag #'oboe2 {
    fa'1\p~ |
    fa' |
    sib'4.\f sol'8 fa' la' sib' do'' |
    sib'4. sol'8 fa' la' sib' do'' |
    sib'4 mib'' re''2 |
    fa'' sib'4 sol'' |
    sib'2. la'4 |
    sib' r sib' la' |
    sib' r sib' la' |
    sib' re'' re'' r |
  }
>>
R1*3 |
<>\f <<
  \tag #'oboe1 { fa''1 | }
  \tag #'oboe2 { re'' | }
>>
R1*5 |
<<
  \tag #'oboe1 {
    r4 do''2 re''8( mib'') |
    re''4-. sib''( la'' sol'') |
    fa'' do''2 re''8( mib'') |
    re''4-. sib''( la'' sol'') |
    fa''
  }
  \tag #'oboe2 {
    r4 la'2 sib'8( do'') |
    sib'4-. re''( do'' sib') |
    la' la'2 sib'8( do'') |
    sib'4-. re''( do'' sib') |
    la'
  }
>> r4 r2\fermata |
R1*4 |
<>\p <<
  \tag #'oboe1 { la''1 }
  \tag #'oboe2 { fa'' }
>>
R1*9 |
<>\p <<
  \tag #'oboe1 {
    fa''1~ |
    fa'' |
    r2 la'' |
    sol''1 |
    fa''2\f mi'' |
    fa'' mi'' |
    fa''4 mi'' fa'' r |
    mib''!2 re''4 r |
    la''2.( sol''4) |
    fa'' r la'( sol') |
    la'-. r do''( sib') |
    la'-. fa'' fa'' r |
  }
  \tag #'oboe2 {
    la'2 do'' |
    re''1 |
    r2 fa''~ |
    fa'' mi'' |
    fa''4\f la' sib'2 |
    la'4 do'' sib'2 |
    la'4 sib' do'' r |
    do''2 sib'4 r |
    fa''2.( mi''4) |
    fa'' r fa'( mi') |
    fa'-. r la'( sol') |
    fa'-. la' la' r |
  }
>>
R1*2 | \allowPageTurn
<>\p <<
  \tag #'oboe1 { fa''1 }
  \tag #'oboe2 { re'' }
>>
R1*3 |
r8 <>\f <<
  \tag #'oboe1 { fa''8 fa'' fa'' fa''4 }
  \tag #'oboe2 { la'8 la' la' la'4 }
>> r4 |
R1*3 |
<>\f <<
  \tag #'oboe1 { fa''1 | }
  \tag #'oboe2 { re'' }
>>
R1*5 |
r4 <<
  \tag #'oboe1 {
    do''2 re''8( mib'') |
    re''4 sib''( la'' sol'') |
    fa'' do''2 re''8( mib'') |
    re''4( sib'' la'' sol'') |
    fa''
  }
  \tag #'oboe2 {
    la'2 sib'8( do'') |
    sib'4 re''( do'' sib') |
    la' la'2 sib'8( do'') |
    sib'4( re'' do'' sib') |
    la'
  }
>> r4 r2\fermata |
R1*4 |
<>\p <<
  \tag #'oboe1 { fa''1 | lab'' | sol''4 }
  \tag #'oboe2 { re''1~ | re'' | mib''4 }
>> r4 r2 |
R1*6 |
<>1\p <<
  \tag #'oboe1 { fa''1 }
  \tag #'oboe2 { fa' }
>>
R1*2 |
<>\p <<
  \tag #'oboe1 { fa''1 | sol'' | sib''\f~ | sib''2 }
  \tag #'oboe2 { sib'1 | sib' | re''\f~ | re''2 }
>> r2\fermata |
<<
  \tag #'oboe1 {
    fa''2 mib'' |
    re''4 fa'' mib''2 |
    re''4 la' sib'2 |
    lab'' sol'' |
    re''2. do''4 |
    sib' r re'' do'' |
    re'' r sib'' la'' |
    sib'' sib'' sib'' r |
  }
  \tag #'oboe2 {
    re''2 la' |
    sib'2. la'4 |
    sib' mib'' re''2 |
    fa'' mib'' |
    sib'2. la'4 |
    sib' r sib' la' |
    sib' r re'' do'' |
    sib' re'' re'' r |
  }
>>
R2.*5 |
R2.^\fermataMarkup |
R2.*8 |
<<
  \tag #'oboe1 {
    lab''2\f si' |
    sol'' la'! |
    sib'!4 fa'' sol''2 |
    fa''8 fa'' fa'' fa'' fa''4 r |
  }
  \tag #'oboe2 {
    r2 re''\f |
    sol' do'' |
    fa'4 sib'2 sib'4 |
    la'8 la' la' la' la'4 r |
  }
>>
