\clef "bass" sib,8 sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sol sol mib mib fa fa |
re re sib, sib, fa fa fa, fa, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, re re re re |
mib mib mib mib mi mi mi mi |
fa fa fa fa fa fa fa fa |
fa fa fa fa fa fa fa fa |
fa4 fa fa r |
sib,8\p r do r r re-. sib,-. re-. |
do r sib, r r fa-. la-. fa-. |
sib, r do r r re-. sib,-. re-. |
do r sib, r r fa-. la-. fa-. |
sib\f sib sib sib la fa sol la |
sib sib sib sib la fa sol la |
sib sib, do do re4 r |
re8 re re re mib4 r |
fa8 fa fa fa fa fa fa fa |
sib,16 do re mib fa sol la sib fa4 r |
sib,16 do re mib fa sol la sib fa4 r |
sib,4 sib, sib, r |
sib,8\p sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sol sol mib mib fa fa |
re re sib, sib, fa fa fa fa |
sib,\f sib, sib, sib, sib, sib, sib, sib, |
sib,\p sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
mib mib mib mib mi mi mi mi |
\ru#4 { fa fa fa fa fa fa fa fa | } |
fa4 r r2\fermata |
fa8 r sol r r la-. fa-. la-. |
sol r fa r r mi-. sol-. do-. |
fa r la r r fa-. la-. fa-. |
sol r fa r r mi-. sol-. do-. |
fa fa fa fa fa fa fa fa |
re re re re re re re re |
do4 r do r |
do8 do do do do do do do |
do4 r do r |
do8 do do do do do do do |
do4 r do r |
do8 do do do do do do do |
fa4 la, sib, sib, |
do8 do do do do do do do |
fa fa fa fa la la la la |
sib sib sib sib sib sib sib sib |
do' do' do' do' do' do' do' do' |
do do do do do do do do |
fa\f fa fa fa sol sol do do |
fa fa fa fa sol sol do do |
fa fa sol sol la4 r |
la,8 la, la, la, sib,4 r |
do8 do do do do do do do |
fa16 sol la sib do' re' mi' fa' do'4 r |
fa16 sol la sib do' re' mi' fa' do'4 r |
fa fa, fa r |
R1*2 | \allowPageTurn
r8 sol(\p fad sol lab sol fad sol) |
sol sol sol sol sol sol sol sol |
do do do do fa! fa fa fa |
sib, sib, re re mib mib mi mi |
fa\f fa fa fa fa4 r |
sib,8\p sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sol sol mib mib fa fa |
re re sib, sib, fa fa fa fa |
sib,\f sib, sib, sib, sib, sib, sib, sib, |
sib,\p sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, re re re re |
mib mib mib mib mib mib mi mi |
\ru#4 { fa fa fa fa fa fa fa fa } |
fa4 r r2\fermata |
sib,8\p r do r r re8-. sib,-. re-. |
fa r sol r r la-. fa-. la-. |
sib r sib, r r sib,-. re-. sib,-. |
do r sib, r r fa-. la-. fa-. |
sib sib sib sib sib sib sib sib |
sib, sib, sib, sib, sib, sib, sib, sib, |
mib mib mib mib mib mib mib mib |
mib4 r mib r |
re8 re re re re re re re |
re4 r re r |
do8 do do do do do sib, sib, |
la,4 r la, r |
sib,8 r re r do r fa r |
sib, r sib r la r fa r |
sib8 sib sib sib mib mib mib mib |
fa fa fa fa fa fa fa fa |
re re re re re re re re |
mib mib mib mib mib mib mib mib |
mi\f fa sol la sib sol fa mi |
fa1\fermata |
sib,8 sib, sib, sib, do do fa fa |
sib, sib, re re do do fa fa |
sib, sib, do do re4 r |
re8 re re re mib mib mib mib |
fa fa fa fa fa fa fa fa |
sib,16 do re mib fa sol la sib fa4 r |
sib,16 do re mib fa sol la sib fa4 r |
sib, sib, sib, r |
sol8\p sol sol sol sol sol |
la la la la la la |
sib sib sib sib sib sib |
fad fad fad fad fad fad |
sol sol sol sol mib mib |
re4 r\fermata r |
re8 re re re re re |
mib mib mib mib mib mib |
fa! fa fa fa fa fa |
solb solb solb solb sol sol |
lab lab lab sib do' do |
la! la la la la la |
sib sib sib sib sib sib |
do' do' do' do' do do |
fa4\f r4 r8 re-. sol-. fa-. |
mib!4 r r8 do-. fa-. mib-. |
re8 re re re mib mib mi mi |
fa fa fa fa fa4 r |
