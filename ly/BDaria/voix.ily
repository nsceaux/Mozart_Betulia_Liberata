\clef "soprano/treble" R1*22 |
sib'2 re'' |
fa''4 r8 sib' \grace re''8 do''4 sib'8[ la'] |
sib'[ fa'] re'' fa'' fa''[ mib''] re''[ do''] |
sib'4 sib' r2 |
sib'~ sib'8[ mib''] do''[ la'] |
sib'4 sib' r r8 sib' |
sib'2~ sib'8[ mib''] do''[ la'] |
sib'[ re''] re''4 r re''8 re'' |
do''[ mib''] mib''[ sol''] sol''4. sib'8 |
fa''1~ |
fa''~ |
fa''~ |
fa''~ |
fa''4.\melisma mi''16[ fa''] fa'4\fermata\melismaEnd r4 |
r2 r4 do''8 fa'' |
mi'' r re'' r do'' r sib' r |
la'4 do'' r do''8 fa'' |
mi'' r re'' r do'' r sib' r |
la'4 do'' r do''8 do'' |
fa''4. mi''16[ re''] re''8[ do''] do''[ si'] |
do''16[\melisma re'' do'' si'] do''[ re'' do'' si'] do''[ mi'' re'' fa''] mi''[ sol'' fa'' la''] |
sol''2 sib'! |
la'16[ do'' sib' la'] sib'[ do'' re'' mi''] fa''[ do'' re'' mi''] fa''[ mi'' fa'' sol''] |
la''2 la' |
sib'16[ do'' sib' la'] sib'[ sol' la' sol'] mi''[ fa'' mi'' re''] mi''[ do'' re'' do''] |
sol''2 sib' |
la'8[ do'']\melismaEnd fa'' do'' re''[ fa''] sol' sib' |
la'2 sol'\trill |
fa'4 r fa''4. fa''8 |
fa''8[ re''] re''4 r re'' |
do''1 |
sol'\trill |
fa'4 r r2 |
R1*7 |
fa''2 re'' |
si'4 re'' si' sol' |
fa'4 fa' r sol'8 sol' |
re''[ sol'] sol'4 r8 re'' re'' fa'' |
mib''4 mib'' r do''8 mib'' |
re''[ sib'] re''[ fa''] mib''[ re''] do''[ sib'] |
fa''4 r r2 |
sib'2 re'' |
fa''4 r8 sib' \grace re''8 do''4 sib'8[ la'] |
sib'[ fa'] re'' fa'' fa''[ mib''] re''[ do''] |
sib'4 sib' r2 |
sib'~ sib'8[ mib''] do''[ la'] |
sib'4 sib' r r8 sib' |
sib'2~ sib'8[ mib''] do''[ la'] |
sib'4 sib' r re''8 re'' |
do''[ mib''] mib''[ sol''] sol''4 sib' |
fa''1~ |
fa''~ |
fa''~ |
fa''~ |
fa''4.\melisma mi''16[ fa''] fa'4\fermata\melismaEnd r |
r2 r4 sib'8 sib' |
la'4 sib' do'' mib'' |
re'' sib' r sib'8 sib' |
la'4 sib' do'' fa'' |
re'' sib' r re''8 fa'' |
fa''4. re''8 sib'4 lab' |
sol'16[\melisma sib' do'' sib'] do''[ sib' do'' sib'] mib''4 r |
sol''2 sol' |
fa'16[ sib' do'' sib'] do''[ sib' do'' sib'] re''4 r |
fa''2 fa' |
mib'8[ fa'16 sol'] la'[ sib' do'' re''] mib''4 r |
mib''2 << { \voiceOne mib'2 \oneVoice } \new Voice { \voiceTwo do' } >> |
re'4 \grace mib''16 re''8[ do''16 sib'] la'8 r \grace fa''16 mib''8[ re''16 do''] |
re''8 r \grace sol''16 fa''8[ mib''16 re''] do''8 r \grace re''16 do''8[ sib'16 la'] |
sib'8[ fa'']\melismaEnd re'' sib' sol'[ sol''] mib'' do'' |
sib'2 do''\trill |
sib' sib'4. sib'8 sib'8[ sol''] sol''4 r2 |
r r4 sib' |
sib'2( \once\outsideSlur do''4.)\trill\fermata sib'8 |
sib'4 r r2 |
R1*6 |
r2 r4 re''8 re'' |
sol''4. re''8 sib' sol' |
fad'4 fad' fad'8 fad' |
sol'4. re''8 \grace mib''16 re''8 do''16[ sib'] |
la'4 la' la'8 la' |
sib'4 r8 sol' sol'' dod'' |
re''4 r\fermata fa''!8 re'' |
sib'4. sib'8 sib' sib' |
sib'4 solb' mib'8 mib' |
do'4 mib'' reb''16[ do''] sib'[ la'] |
sib'4 sib' reb''8 reb'' |
do''4. reb''16[ sib'] lab'8 sol' |
fa''[ mib''!] r4 mib''8 mib'' |
mib''[ reb''] r4 reb'' |
do'' r mi' |
fa' r r2 |
R1*3 |
