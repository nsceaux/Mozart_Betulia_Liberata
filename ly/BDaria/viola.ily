\clef "alto" R1*3 |
sib8 sib sib sib re' re' re' re' |
mib' sib sib sib sib sib sib sib |
sib sib sib sib re' re' re' re' |
mib' mib' mib' mib' mi' mi' mi' mi' |
fa' fa' fa' fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' fa' fa' |
fa'4 fa' fa' r |
sib8\p r do' r r re'-. sib-. re'-. |
do' r sib r r fa'-. la'-. fa'-. |
sib r do' r r re'-. sib-. re'-. |
do' r sib r r fa'-. la'-. fa'-. |
sib'\f sib' sib' sib' la' fa' sol' la' |
sib' sib' sib' sib' la' fa' sol' la' |
sib' sib do' do' re'4 r |
re'8 re' re' re' mib'4 r |
fa'8 fa' fa' fa' fa' fa' fa' fa' |
sib16 do' re' mib' fa' sol' la' sib' fa'4 r |
sib16 do' re' mib' fa' sol' la' sib' fa'4 r |
sib4 sib sib r |
sib8\p sib sib sib sib sib sib sib |
sib sib sol' sol' mib' mib' fa' fa' |
re' re' sib sib fa' fa' fa' fa' |
sib\f sib sib sib re' re' re' re' |
sib\p sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
mib' mib' mib' mib' mi' mi' mi' mi' |
\ru#4 { fa' fa' fa' fa' fa' fa' fa' fa' | } |
fa'4 r r2\fermata |
fa'8 r sol' r r la'-. fa'-. la'-. |
sol' r fa' r r mi'-. sol'-. do'-. |
fa' r la' r r fa'-. la'-. fa'-. |
sol' r fa' r r mi'-. sol'-. do'-. |
fa' fa' fa' fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
do'4 r do' r |
do'8 do' do' do' do' do' do' do' |
do'4 r do' r |
do'8 do' do' do' do' do' do' do' |
do'4 r do' r |
do'8 do' do' do' do' do' do' do' |
fa'4 la sib sib |
do'8 do' do' do' do' do' do' do' |
fa' fa' fa' fa' la la la la |
sib sib sib sib sib sib sib sib |
do' do' do' do' do' do' do' do' |
do' do' do' do' do' do' do' do' |
fa'\f fa' fa' fa' sol' sol' do' do' |
fa' fa' fa' fa' sol' sol' do' do' |
fa' fa' sol' sol' la'4 r |
la8 la la la sib4 r |
do'8 do' do' do' do' do' do' do' |
fa'16 sol' la' sib' do'' re'' mi'' fa'' do''4 r |
fa'16 sol' la' sib' do'' re'' mi'' fa'' do''4 r |
fa' fa fa' r |
R1*2 | \allowPageTurn
r8 sol'(\p fad' sol' lab' sol' fad' sol') |
sol' sol' sol' sol' sol' sol' sol' sol' |
do' do' do' do' fa'! fa' fa' fa' |
sib sib re' re' mib' mib' mi' mi' |
fa'\f fa' fa' fa' fa'4 r |
sib8\p sib sib sib sib sib sib sib |
sib sib sol' sol' mib' mib' fa' fa' |
re' re' sib sib fa' fa' fa' fa' |
sib\f sib sib sib re' re' re' re' |
sib\p sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib re' re' re' re' |
mib' mib' mib' mib' mib' mib' mi' mi' |
\ru#4 { fa' fa' fa' fa' fa' fa' fa' fa' } |
fa'4 r r2\fermata |
sib8\p r do' r r re'8-. sib-. re'-. |
fa' r sol' r r la'-. fa'-. la'-. |
sib' r sib r r sib-. re'-. sib-. |
do' r sib r r fa'-. la'-. fa'-. |
sib' sib' sib' sib' sib' sib' sib' sib' |
sib sib sib sib sib sib sib sib |
mib' mib' mib' mib' mib' mib' mib' mib' |
mib'4 r mib' r |
re'8 re' re' re' re' re' re' re' |
re'4 r re' r |
do'8 do' do' do' do' do' sib sib |
la4 r la r |
sib8 r re' r do' r fa' r |
sib r sib' r la' r fa' r |
sib'8 sib' sib' sib' mib' mib' mib' mib' |
fa' fa' fa' fa' fa' fa' fa' fa' |
re' re' re' re' re' re' re' re' |
mib' mib' mib' mib' mib' mib' mib' mib' |
mi'\f fa' sol' la' sib' sol' fa' mi' |
fa'1\fermata |
sib8 sib sib sib do' do' fa' fa' |
sib sib re' re' do' do' fa' fa' |
sib sib do' do' re'4 r |
re'8 re' re' re' mib' mib' mib' mib' |
fa' fa' fa' fa' fa' fa' fa' fa' |
sib16 do' re' mib' fa' sol' la' sib' fa'4 r |
sib16 do' re' mib' fa' sol' la' sib' fa'4 r |
sib sib sib r |
sol2.\p |
la |
sib |
fad |
sol2 mib4 |
re r\fermata r |
re2. |
mib |
fa! |
solb2 sol4 |
lab4. sib8 do'4 |
do'2. |
sib |
do'2 do4 |
fa-\sug\f r4 r8 re'-. sol'-. fa'-. |
mib'!4 r r8 do'-. fa'-. mib'-. |
re'8 re' re' re' mib' mib' mi' mi' |
fa' fa' fa' fa' fa'4 r |
