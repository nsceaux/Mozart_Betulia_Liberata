\tag #'all \key sol \major
\tempo "Allegro" \midiTempo#120
\time 4/4 s1*70 \bar "||" \segnoMark
s1*45 \bar "|." \fermataMark
\time 2/2 \tempo "Adagio" s1*20
\bar "||" \time 4/4 \tempo "Tempo primo" s1*4 \bar "|." \dalSegnoMark
