Par -- to, par -- to in -- er -- me, e non pa -- ven -- to,
e non, e non pa -- ven -- to;
so -- la par -- to, e son __ si -- cu -- ra, 
e son __ si -- cu -- ra;
vo per l’om -- bre e or -- ror non ho,
vo per l’om -- bre e or -- ror non ho,
par -- to in -- er -- me e non pa -- ven -- to,
so -- la par -- to e son si -- cu -- ra, 
vo per l’om -- bre e or -- ror non ho,
vo per l’om -- bre,
vo per l’om -- bre e or -- ror non ho.

Par -- to in -- er -- me, e non __ pa -- ven -- to,
so -- la par -- to, e son __ si -- cu -- ra, 
e son si -- cu -- ra,
vo per l’om -- bre e or -- ror non ho,
vo per l’om -- bre e or -- ror non ho,
par -- to in -- er -- me e non pa -- ven -- to, e non pa -- ven -- to,
so -- la par -- to, e son si -- cu -- ra, 
vo per l’om -- bre,
vo per l’om -- bre e or -- ror non ho,
or -- ror non ho,
e or -- ror non __ ho.

Chi m’ac -- ce -- se al gran ci -- men -- to,
m’ac -- com -- pa -- gna e m’as -- si -- cu -- ra,
m’ac -- com -- pa -- gna e m’as -- si -- cu -- ra;
l’ho nell’ al -- ma, ed io lo sen -- to
re -- pli -- car, __ che vin -- ce -- rò,
chi m’ac -- ce -- se al gran ci -- men -- to,
m’ac -- com -- pa -- gna e m’as -- si -- cu -- ra,
l’ho nell’ al -- ma, ed io lo sen -- to
re -- pli -- car, __ che vin -- ce -- rò,
re -- pli -- car, __ che __ vin -- ce -- rò.
