\clef "treble" <sol' sol>4. si'16 re'' sol''8 sol'' sol'' sol'' |
<sol' sol>4. si'16 re'' sol''8 sol'' sol'' sol'' |
sol' la'16 si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
\grace re''16 do''8.\trill si'16 do''4 r2 |
<la' re'>4. do''16 fad'' la''8 la'' la'' la'' |
<do'' re'>4. fad''16 la'' do'''8 do''' do''' do''' |
do'''16 si'' la'' si'' la'' sol'' fad'' sol'' fad'' sol'' la'' sol'' fad'' mi'' re'' do'' |
\grace re''16 do''8( si') si'4 r2 |
<mi' si' sol''>2 fa''4.\trill mi''16 fa'' |
mi'' re'' do'' si' do'' re'' mi'' fad''! sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
do'' re'' mi'' re'' do'' si' la' sol' fad' fad'' la'' sol'' fad'' mi'' re'' do'' |
si' sol'' si'' la'' sol'' fad'' mi'' re'' do'' la'' do''' si'' la'' sol'' fad'' mi'' |
re'' si'' re''' do''' si'' la'' sol'' fad'' mi'' re'' do'' si' la' sol' fad' sol' |
fad' la' re'' fad'' la''( fad'') la''( fad'') <re'' re'>4 r |
<<
  \tag #'violino1 {
    sib'2~ sib'8 re''4( fad'!8) |
    sol'2~ sol'8 sib'4( re'8) |
    mib'2~ mib'8 sol'4( si!8) |
    do'4 r la''16 sol'' fad'' mi''! re'' do'' si'! la' |
  }
  \tag #'violino2 {
    r2 fad'!4:16 sib'16 sib sib sib |
    sib4 r re'16 sol' sol' sol' sol' sol sol sol |
    sol4 r sol2:16 |
    do'2:16 do'4:16 r4 |
  }
>>
si'16 re'' do'' si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
mi'' fad'' sol'' fad'' mi'' re'' do'' si' do'' re'' mi'' re'' do'' si' la' sold' |
la' mi' fad' sold' la' si' do'' re'' mi'' red'' mi'' red'' mi'' do'' si' la' |
si' re''! sol'' fad'' sol'' re'' si' sol' re'4 la''8.\trill sol''32 la'' |
sol''4
<<
  \tag #'violino1 {
    re''16 re' do'' re' si'4 si'16 re' la' re' |
    sol'4
  }
  \tag #'violino2 {
    si'16 re' la' re' sol'4 sol'16 re' do' re' |
    si4
  }
>> <re' si' sol''>4 <sol re' si' sol''>4 r |
r2 <<
  \tag #'violino1 {
    si4\p do'8.\trill si32 do' |
    re'4 si-. do'-. re'-. |
    mi' r fa' re'8.\trill do'32 re' |
    mi'4 re'-. mi'-. fad'!-. |
    sol'4 r r2\fermata |
    si'16\p sol' la' si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
    do''4 r r2 |
    r16 la' si' do'' re'' mi'' fad'' sol'' la'' sol'' fad'' mi'' re'' do'' si' do'' |
    si'8 si' si' si' si' si' si' si' |
    la' la' do'' mi'' re'' do'' si' la' |
  }
  \tag #'violino2 {
    sol4\p la8.\trill sol32 la |
    si4 sol-. la-. si-. |
    do' r re' si8.\trill la32 si |
    do'4 si-. do'-. la-. |
    si r r2\fermata |
    R1 |
    r16 do'\p re' mi' fad' sol' la' si' do'' re'' mi'' re'' do'' si' la' sol' |
    fad'4 r r2 |
    re'8 re' re' re' sol' sol' sol' sol' |
    sol' sol'4 do''8 si' la' sol' fad' |
  }
>>
sol'8.\f sol''16 si'' sol'' re'' si' sol'4 r |
<<
  \tag #'violino1 {
    fad''8\p fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sol'' sol'' sol'' sol'' sol'' sol'' sol'' sol'' |
    <sol'' la'> q q q q q q q |
    fad'' fad'' fad'' fad'' mi'' mi'' mi'' mi'' |
    re'' re'' re'' re'' <re'' mi'> q q q |
    <mi' dod''>4 r fa''8\f la''4 dod''8 |
    re''2~ re''8 fa''4 la'8 |
    sib'8->\p sib' sib' sib' dod''-> dod'' dod'' dod'' |
    re'' fa'' la'' re''' sib'' sol'' mi'' re'' |
    dod''4 r fa''8\f la''4 do''8 |
    re''2~ re''8 fa''4 la'8 |
    sib'8->\p sib' sib' sib' dod''-> dod'' dod'' dod'' |
    re'' fa'' fa'' la'' sib'' sol'' mi'' re'' |
    dod''
  }
  \tag #'violino2 {
    si'8\p si' si' si' si' si' si' si' |
    si' si' si' si' si' si' si' si' |
    <la' mi''> q q q q q q q |
    la' la' la' la' la' la' la' la' |
    la' la' la' la' <sold' si> q q q |
    <la la'>4 r dod''4:16\f fa''16 fa' fa' fa' |
    fad'4 r la16 la' la' la' la' re' re' re' |
    re'8->\p re' re' re' sib-> sib sib sib |
    la la re' re' re' sib sib' sib' |
    mi'4 r dod'':16\f fa''16 fa' fa' fa' |
    fa'4 r la16 la' la' la' la' re' re' re' |
    re'8->\p re' re' re' sib-> sib sib sib |
    la la re' re' re' re' sib' sib' |
    mi'
  }
>> la8(\f sib sold) la4 r |
<<
  \tag #'violino1 {
    <re' re''>2.\fp sol''4 |
    fad''8-> fad'' fad'' fad'' sol''-> sol'' sol'' sol'' |
    la''-> la'' la'' la'' la''2:16 |
    si'':16 <dod''' mi''>:16 |
    re'''8
  }
  \tag #'violino2 {
    r16 re'\p mi' fad' sol' la' si' dod'' re''8 la' si'16 re'' dod'' mi'' |
    re''8-> re'' re'' re'' mi''-> mi'' mi'' mi'' |
    fad''-> fad'' fad'' fad'' fad''2:16 |
    sol'':16 <sol'' la'>:16 |
    fad''8
  }
>> fad''8\f fad'' fad'' \grace sol''16 fad''8 mi''16 re'' \grace la''16 sol''8 fad''16 mi'' |
re''8\p <<
  \tag #'violino1 {
    re''8 re'' re'' re'' re'' do''! do'' |
    si' si' si' si' sol'' sol'' sol'' sol'' |
    fad''16-> fad'' fad'' fad'' fad'' fad'' fad'' fad'' mi''-> mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re''( dod'') re''-. mi''-. fad''( mi'') fad''-. sol''-. la''8 la'' la'' la'' |
    si''-> si'' si'' si'' si''-> si'' si'' si'' |
    la''2:16 si'':16 |
    fad'':16 fad'':16 |
    mi'':16 mi'':16 |
  }
  \tag #'violino2 {
    la'8 la' la' la' la' la' la' |
    re' re' re' re' si' si' si' si' |
    re''16-> re'' re'' re'' re'' re'' re'' re'' dod''-> dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    re''4 r re''8 re'' re'' re'' |
    re''-> re'' re'' re'' re''-> re'' re'' re'' |
    re''2:16 re'':16 |
    re'':16 re'':16 |
    re'':16 dod'':16 |
  }
>>
re''16\f re' mi' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' si'' la'' |
sol'' la'' si'' la'' sol'' fad'' mi'' re'' dod'' re'' mi'' re'' dod'' si' la' sol' |
fad' la' sol' fad' sol' la' si' dod'' re'' mi'' fad'' sol'' la'' fad'' si'' la'' |
sol'' la'' si'' sol'' mi'' si' dod'' re'' dod'' re'' mi'' dod'' la' mi' fad' sol' |
fad' la' si' dod'' re'' dod'' si' la' si' lad'' si'' lad'' si'' mi'' sol'' mi'' |
fad'' re'' la''! fad'' re''' la'' fad'' re'' la'4 mi''8.\trill re''32 mi'' |
re''4 <<
  \tag #'violino1 {
    la''16 la' sol'' la' fad''4 fad''16 la' mi'' la' |
    re''4
  }
  \tag #'violino2 {
    fad''16 la' mi'' la' re''4 la'16 la sol' la |
    fad'4
  }
>> <re' la' fad''>4 q r |
<<
  \tag #'violino1 {
    r4 red'(\p mi' fad') |
    sol'( si' la' fad') |
    mi'8 mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' red'' red'' |
    mi''4
  }
  \tag #'violino2 {
    r4 si(\p dod' red') |
    mi'( sol' fad' red') |
    mi'8 sol' sol' sol' si' si' si' si' |
    do'' do'' do'' do'' sol' sol' fad' fad' |
    mi'4
  }
>> mi'4(\f re'! do') |
si <<
  \tag #'violino1 {
    si(\p do' re') |
    mi'( re' mi' fa') |
    mi'8 mi' mi' mi' sol'' sol'' sol'' sol'' |
    la'' la'' la'' la'' mi'' mi'' re'' re'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' mi'' mi'' sol'' sol'' |
    fad''!4 r4 sib''8\f re'''4 fad''8 |
    sol''2~ sol''8 sib''4 re''8 |
    mib''8\p mib'' mib'' mib'' si'! si' si' si' |
    do'' do'' do'' do'' dod'' dod'' dod'' dod'' |
    re''4 r sib''8\f re'''4 fad''8 |
    sol''2~ sol''8 sib''4 re''8 |
    mib''8\p mib'' mib'' mib'' fad''! fad'' fad'' fad'' |
    sol'' sol'' la'' la'' sib'' sib'' dod'' dod'' |
    re''8\f
  }
  \tag #'violino2 {
    sol4\p( la si) |
    do'( si do' re') |
    do'8 do' do' do' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' si' si' |
    do'' sol' sol' sol' sol' sol' sold' sold' |
    la' la' la' la' la' la' la' la' |
    la'4 r fad':16\f sib'16 sib sib sib |
    sib4 r re':16 re'16 sol' sol sol |
    sol8 sol' sol' sol' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
    la'!4 r fad'!:16\f sib'16 sib sib sib |
    sib4 r re':16 re'16 sol' sol sol |
    sol8\p sol' sol' sol' do'' do'' do'' do'' |
    sib' sib' re' re' re' sol sol' sol' |
    fad'\f
  }
>> re'8( mib' dod') re'4 r |
<<
  \tag #'violino1 {
    <sol sol'>2.\fp mi''!8( do'') |
    si'( re'' sol'') si''-. si''( la'') sol''( fad'') |
    sol''( re'') si''( re''') re'''( do''') si''( la'') |
    sol'' sol'' \grace la''16 sol''8.\trill\f fad''32 sol'' si''8( sol'') do'''8.\trill si''32 do''' |
    re'''8(\p sol'') sol''-. sol''-. r fa'' fa'' fa'' |
    r mi'' mi'' mi'' r mi'' do''' la'' |
    sol'' sol'' sol'' sol'' fad''! fad'' fad'' fad'' |
    sol''( re'') re''-. re''-. re''( sol'') sol''( si'') |
    si''( la'') la''-. re''-. re''( la'') la''( do''') |
    si''( re''') sol''( si'') mi''( sol'') do''( mi'') |
    la' la' la' la' do''' do''' do''' do''' |
    si''2:16 si'':16 |
    la'':16 la'':16 |
  }
  \tag #'violino2 {
    r16 sol\p la si do'! re' mi'! fad' sol' fad' mi' re' do' re' mi' fad' |
    sol'4 r8 re''-. re''( do'') si'( la') |
    re'( sol') sol''( si'') si''( la'') sol''( fad'') |
    sol'' sol'' sol''\f sol'' sol'' sol'' sol'' sol'' |
    sol''(\p re'') re''-. re''-. r re'' re'' re'' |
    r sol' sol' sol' r sol' mi'' do'' |
    si' si' si' si' la' la' la' la' |
    sol'( si') si'-. si'-. si'( re'') si'( sol') |
    sol'( fad') fad'-. re''-. do''( la') fad'( la') |
    sol'8( si') mi'( sol') do'( mi') la( do') |
    fad' fad' fad' fad' la' la' la' la' |
    sol'16 sol'' sol'' sol'' sol''4:16 sol''2:16 |
    sol'':16 fad'':16 |
  }
>>
sol''16 sol' la' si' do'' re'' mi'' fad'' sol'' si' do'' re'' mi'' fad'' sol'' la'' |
si'' re'' mi'' fad'' sol'' la'' si'' do''' re'''4 re'' |
mi''16 re'' do'' si' do'' re'' mi'' fad'' sol''4 dod' |
re'2 r\fermata |
si'16 sol' la' si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' |
do'' re'' mi'' re'' do'' si' la' sol' fad' fad'' la'' sol'' fad'' mi'' re'' do'' |
si' sol' la' si' do'' re'' mi'' fad'' sol'' fad'' sol'' la'' sol'' fa'' mi'' re'' |
mi'' fad'' sol'' fad'' mi'' re'' do'' si' do'' re'' mi'' re'' do'' si' la' sold' |
la' mi' fad' sol'! la' si' do'' re'' mi'' red'' mi'' red'' mi'' do'' si' la' |
si' re''! sol'' si'' sol'' re'' si' sol' re'4 la''8.\trill sol''32 la'' |
sol''4 <<
  \tag #'violino1 {
    re''16 re' do'' re' si'4 si'16 re' la' re' |
    sol'4
  }
  \tag #'violino2 {
    si'16 re' la' re' sol'4 sol'16 re' do' re' |
    si4
  }
>> <re' si' sol''>4 <sol re' si' sol''> r |
<<
  \tag #'violino1 {
    r8 sol' r sol' r sol' r sol' |
    r fa' r fa' r la' r re'' |
    r re'' r fa' r re' r si' |
    r do'' r mi'' r sol'' r sol' |
    r la' r la' r mi' r re' |
    r do' r mi' r sol' r do'' r mi'' r la'' r sol'' fad''! mi'' |
    r red'' r red'' r fad'' r la'' |
    r sol'' r sol'' fad'' mi''4 red''8 |
    r mi'' r si' r sol'' r mi'' |
    r dod'' r dod'' r dod'' r dod'' |
    r dod'' r re'' r la' r la' |
    r si' r re'' r fa'' r si' |
    r do''! r mi'' r sol'' r mi'' |
    r mi'' r sol' r mi'' r sol'' |
    r fad''! r red'' r red'' r red'' |
    r mi'' r mi'' r mi'' r red'' |
    r mi'' r sol' r re''! r mi' |
    r do'' r do'' r mi'' r mi'' |
    r mi'' r mi'' r red'' r red'' |
    mi''4 r sol''8\f re'''4 fa''8 |
    mi''2~ mi''8 si''4 re''8 |
    do''16 si' la' sold'
  }
  \tag #'violino2 {
    r8 mi' r mi' r mi' r mi' |
    r la r la r fa' r fa' |
    r fa' r re' r si r re' |
    r mi' r sol' r do'' r do' |
    r do' r do' r do' r si |
    r do' r sol r mi' r mi' |
    r la' r mi'' r lad' r lad' |
    r fad'! r fad' r si' r si |
    r si r sol' do''16( la') sol'4 fad'8 |
    r sol' r sol' r si' r sol' |
    r mi' r mi' r mi' r mi' |
    r mi' r fa' r fa' r fa' |
    r re' r si' r si' r re' |
    r mi' r sol' r mi'' r sol' |
    r sol' r mi' r sol' r dod'' |
    r do''! r do'' r fad'! r fad' |
    r si' r sol' r fad' r fad' |
    r mi' r mi' r mi' r si |
    r mi' r mi' r la' r la' |
    r sol' r sol' r fad' r fad' |
    mi'4 r re'16\f sol' sol' sol' sol'4:16 |
    sol4 r mi'16-> mi' mi' mi' mi' mi' mi' mi' |
    mi'4
  }
>> la'16 si' do'' re'' mi'' re'' do'' si' la' sol'! fad' sol' |
fad' la' re'' fad'' la''( fad'') la''( fad'') re''4 r |
