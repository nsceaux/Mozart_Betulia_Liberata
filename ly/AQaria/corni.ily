\clef "treble" \transposition sol <>
<<
  \tag #'(corno1 corni) { do''1~ | do''~ | do''2 }
  \tag #'(corno2 corni) { do'1~ | do'~ | do'2 }
>> r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' re'' |
    re''1~ |
    re'' |
    sol'1 |
    s8 mi' mi' mi' mi' mi' mi' mi' |
    mi'4 }
  { re''4 re'' re'' |
    sol'1~ |
    sol' |
    sol |
    s8 mi' mi' mi' mi' mi' mi' mi' |
    mi'4 }
  { s2. | s1*3 | r8 }
>> r4 r <<
  \tag #'(corno1 corni) {
    do''4 |
    do'' s do'' s |
    s2 re'' |
    do''4 s2. |
    s4 do'' do''
  }
  \tag #'(corno2 corni) {
    do'4 |
    do' s do' s |
    s2 sol' |
    do'4 s2. |
    s4 do' do'
  }
  { s4 | s r s r | r2 s | s4 r r2 | r4 }
>> \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> |
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol'4 sol sol }
>> r4 |
r2 <<
  \tag #'(corno1 corni) { sol'2 }
  \tag #'(corno2 corni) { sol }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do'4 s do'2 |
    do'4 s do'2 | }
  { do'4 s do'2 |
    do'4 s do'2 | }
  { s4 r s2 | s4 r s2 | }
>>
r2 <<
  \tag #'(corno1 corni) {
    re''2 |
    do''1 |
    do''4 s8 do'' do''4
  }
  \tag #'(corno2 corni) {
    sol2 |
    do'1 |
    do'4 s8 do' do'4
  }
  { s2 | s1 | s4 r8 }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re'' | }
  { do''2. sol'4 | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 sol''8 fa'' mi''4 mi''8 re'' |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    mi'4 mi''8 re'' do''4 do''8 sol' |
    mi'4 do' do'
  }
>> r4 |
<>\p <<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do''~ |
    do''~ |
    do''4
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do'~ |
    do'~ |
    do'4
  }
>> r4 r2\fermata |
R1*5 |
r4 <>\f <<
  \tag #'(corno1 corni) { do''4 do'' }
  \tag #'(corno2 corni) { mi' mi' }
>> r4 |
R1*5 |
<>\fp \twoVoices #'(corno1 corno2 corni) <<
  { re''1 | sol'4 s sol'2 | }
  { re''1 | sol'4 s sol'2 | }
  { s1 | s4 r }
>>
R1*2 |
r2 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | sol'4 s sol'2 | }
  { re''2 | sol'4 s sol'2 | }
  { s2 | s4 r }
>>
R1*2 |
r2 \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> r |
<>\fp <<
  \tag #'(corno1 corni) { sol'2 }
  \tag #'(corno2 corni) { sol }
>> r2 |
R1*3 |
r4 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 sol' sol' | sol' }
  { sol'4 sol' sol' | sol' }
>> r4 r2 |
R1*2 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'2 }
  { sol' }
>> r2 |
R1*3 | \allowPageTurn
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | re'' | mi''2 re'' | sol'1 |
    mi''4 s re''2 |
    re'' mi'' |
    re''1 |
    re''4 s8 re'' re''4 s8 re'' | }
  { re''1 | sol' | do''2 re'' | sol'1 |
    do''4 s re''2 |
    sol'2 do'4 do'' |
    re''1 |
    sol'4 s8 re'' sol'4 s8 re'' | }
  { s1\p | s\f | s1*2 | s4 r s2 | s1*2 | s4 r8 s s4 r8 }
>>
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol' }
>> r4 |
<>\fp <<
  \tag #'(corno1 corni) { mi''1 | mi'' }
  \tag #'(corno2 corni) { mi'1~ | mi' }
>>
R1*3 |
<>\p <<
  \tag #'(corno1 corni) { do''1~ | do'' }
  \tag #'(corno2 corni) { do'~ | do' }
>>
R1*4 |
r2 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 | do'4 s do'2 | }
  { sol'2 | do'4 s do'2 | }
  { s2 | s4 r }
>>
R1*2 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 | do'4 s do'2 | }
  { sol2 | do'4 s do'2 | }
  { s2 | s4 r }
>>
R1*2 |
r2 \twoVoices #'(corno1 corno2 corni) << sol'4 sol' >> r |
<>\fp <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*2 |
r4 <>\f <<
  \tag #'(corno1 corni) { do''4 do''8 do'' do'' do'' | do''4 }
  \tag #'(corno2 corni) { do'4 do'8 do' do' do' | do'4 }
>> r4 r2 |
R1 |
<>\p <<
  \tag #'(corno1 corni) { mi''2 re'' | do''4 }
  \tag #'(corno2 corni) { do''2 sol' | mi'4 }
>> r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1 | do'4 }
  { sol1 | do'4 }
>> r4 r2 |
R1*2 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''4 s do'' s |
    do'' s do''2 |
    do''4 s s do'' |
  }
  \tag #'(corno2 corni) {
    sol'1 |
    mi'4 s mi' s |
    mi' s do'2 |
    do'4 s s do' |
  }
  { s1 | s4\f r s r | s r s2 | s4 r r s }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol'2 }
  { sol' }
>> r2\fermata |
<<
  \tag #'(corno1 corni) {
    do''1 |
    s2 re'' |
    do''1 |
    do''4 s8 do'' do''4
  }
  \tag #'(corno2 corni) {
    do'1 |
    s2 sol' |
    do'1 |
    do'4 s8 do' do'4
  }
  { s1 | r2 s | s1 | s4 r8 }
>> r4 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re'' | }
  { do''2. sol'4 | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 s8 re'' mi''4 s8 re'' |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    mi'4 s8 sol' do''4 s8 sol' |
    mi'4 mi' mi'
  }
  { s4 r8 s s4 r8 s | }
>> r4 |
R1*20 |
r2 <>\f <<
  \tag #'(corno1 corni) { do''2 | do''4 }
  \tag #'(corno2 corni) { do'2 | do'4 }
>> r4 r2 |
r2 r4 \twoVoices #'(corno1 corno2 corni) << re''4 re'' >> |
<<
  \tag #'(corno1 corni) { re''8 re'' re'' re'' re''4 }
  \tag #'(corno2 corni) { sol'8 sol' sol' sol' sol'4 }
>> r4 |
