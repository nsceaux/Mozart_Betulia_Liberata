\clef "alto/treble" R1*24 |
sol'1~ |
sol'~ |
sol'~ |
sol'~ |
sol'4.\trill\melisma fad'8\melismaEnd sol'4\fermata r4 |
re'2. sol'4 |
fad' fad' r r8 la' |
re'2. do'4 |
si si r sol' |
mi'( la'8) do'' si'[ la' sol'] fad'8 |
sol'4 sol' r2 |
si'2. la'4 |
sol' sol' r sol' |
la2~ la8[ si'] la'[ sol'] |
fad'[ la'] la'4 r la' |
si'4~ si'16[ dod'' si' dod'']\melisma re''4\melismaEnd r8 re' |
dod'4 <la la'>4 r2 |
R1 |
sib'2 dod' |
re'8[ fa'] fa'[ la'] sib'[ sol'] mi'[ re'] |
la'4 r r2 |
R1 |
sib'2 dod' |
re'8[ fa'] fa'[ la'] sib'[ sol'] mi'[ re'] |
dod'4 r r2 |
re'2. sol'4 |
fad'8[ re'] fad'[ la'] sol'[ mi'] sol'[ si'] |
la'[ fad'] fad'4 la'4. do''!8 |
si'[ la'] sol'[ fad'] sol'[ la'] si'[ dod''] |
re''4 re' r2 |
re'2. do'!4 |
si si' \grace la'8 sol'4 \grace fad'8 mi'4 |
re'2 dod' |
re'4 r la'8[ fad'] fad'[ re'] |
si'[ sol'] sol'[ mi'] si'[ dod''] dod''[ re''] |
re''4 la' si' << 
  { \voiceOne si' | la'1 \oneVoice }
  \new Voice { \voiceTwo si4 | la1 }
>>
mi'1\trill |
re'4 r r2 |
R1*7 |
si'1~ |
si'2. red'4 |
mi' mi' r mi' |
fad'4.( sol'16[ la'] sol'4) fad' |
mi' mi' r2 |
sol'1~ |
sol'2. si4 |
do' do' r r8 do' |
re'4.( mi'16[ fa'] mi'4) re' |
do'8[ mi'] mi'2 do''8[ si'] \grace si'8 la'2. sol'4 |
fad'! re' r2 |
R1 |
sol'2 si! |
do'4 sol' sib' dod' |
re' r r2 |
R1 |
mib'2 fad'! |
sol'4 la' sib' dod' |
re' r r2 |
sol'2. mi'!8[ do'] |
si[ re'] sol'[ si'] si'[ la'] sol'[ fad'] |
sol'[ re'] si'[ re''] re''[ do''] si'[ la'] |
sol'4 sol' r2 |
sol'2. fa'4 |
mi' mi' r do''8[ la'] |
sol'2 fad'! |
sol'4 sol' re'8[ sol'] sol'[ si'] |
si'[ la'] la'4 re'8[ la'] la'[ do''] |
si'[ re''] sol'[ si'] mi'[ sol'] do'[ mi'] |
la2 do'' |
re'1 |
la'\trill |
sol'4 r r2 |
R1 |
r2 r4 sol' |
sol'2 \once\override Script.avoid-slur = #'outside la'4.(\trill\fermata sol'8) |
sol'4 r r2 |
R1*6 |
r2 r4 sol'8 sol' |
do'[ do''] do''4. si'8 la' sol' |
fa'4 fa' r \grace sol'16 fa'8 \grace mi'16 re'8 |
\grace do'8 si4. la'8 \grace la'8 sol'4. fa'8 |
fa'16[ mi' la' sol'] sol'4 r \grace la'16 sol'8 fa'16[ mi'] |
\grace mi'8 re'4~ re'16[ la'] fa'[ re'] \grace re'8 do'4. re'8 |
do' do' r4 r mi'8 mi' |
mi'[ la'] la'4. sol'8 fad'! mi' |
red'[ do''] do''4 r fad'8 la' |
sol'8.([ si'16] la'[ sol']) fad'[ mi'] \grace sol'16 fad'8 mi'4 red'8 |
mi'4 r r \grace la'16 sol'8 \grace fa'16 mi'8 |
\grace re'8 dod'4. sib'8 \grace sib'8 la'4. sol'8 |
sol'4 fa' r \grace sol'16 fa'8 \grace mi'16 re'8 |
\grace do'8 si4. la'8 \grace la' sol'4. fa'8 |
\grace sol'16 fa'8[ mi'] mi'4 r mi'8 mi' |
mi'4. sol'8 \grace la'16 sol'8 fad'!4 mi'8 |
red'[ do''] do''2 \grace si'16 la'8 sol'16[ fad'] |
si'4( \grace la'16 sol'8) fad'16[ mi'] fad'8.[ sol'32 la' sol'8] fad' |
mi'4 r re'!4. re'8 |
do'4( do''2) si'16[ la' sol' fad'] |
mi'2( \once\override Script.avoid-slur = #'outside fad'4.)\trill mi'8 |
mi'4 r r2 |
R1*3 |
