\clef "treble" <>
<<
  \tag #'(oboe1 oboi) {
    sol''1~ |
    sol''~ |
    sol'' |
    mi'' |
    do'' |
    la' |
    fad''~ |
    fad'' |
  }
  \tag #'(oboe2 oboi) {
    si'1~ |
    si' |
    si' |
    do'' |
    la' |
    fad' |
    la'~ |
    la' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4 sol'' fa''4.\trill mi''16 fa'' |
    mi''4. fad''!8 sol''2~ |
    sol'' fad'' |
    sol''4 mi''2 fad''4 |
    fad'' sol''2 sol''4 |
    fad'' la'' fad'' r |
    sib''1~ |
    sib''4 sol''2 re''4( |
    mib''2.) si'4 |
    do'' }
  { sol'2 la'4 si' |
    do'' la' re'2 |
    mi'4 do''2 la'4 |
    si'2 do'' |
    re'' mi'' |
    re''4 fad'' re''2~ |
    re''4 r fad''!2 |
    sol''4 r re'' sol'' |
    sol'' r sol''2 |
    fad''4 }
>> r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 |
    sol''4 fad'' sol''2~ |
    sol''~ sol''4. sold''8 |
    la''1 |
    sol''2. fad''4 | }
  { do''2 |
    si'4 do'' re''2 |
    mi''4. si'8 do''4. si'8 |
    mi'2 mi''4 do'' |
    si'2 la' | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''4 re''8 do'' si'4 si''8 la'' |
    sol''4 sol'' sol''
  }
  \tag #'(oboe2 oboi) {
    sol'4 si'8 la' sol'4 re''8 do'' |
    si'4 si' si'
  }
>> r4 |
R1 |
r4 <>\p <<
  \tag #'(oboe1 oboi) { si'( do'' re'') | mi'' }
  \tag #'(oboe2 oboi) { sol'4( la' si') | do'' }
>> r4 r2 |
r4 <<
  \tag #'(oboe1 oboi) { re''4( mi'' fad''!) | sol'' }
  \tag #'(oboe2 oboi) { si'( do'' la') | si' }
>> r4 r2\fermata |
R1*3 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1 }
  \tag #'(oboe2 oboi) { si' }
>>
R1 |
r4 <>\f <<
  \tag #'(oboe1 oboi) { sol''4 sol'' }
  \tag #'(oboe2 oboi) { si' si' }
>> r4 |
R1*3 |
<>\p <<
  \tag #'(oboe1 oboi) { la''2( sol'') }
  \tag #'(oboe2 oboi) { fad''( mi'') }
>>
R1 |
r2 <>\f <<
  \tag #'(oboe1 oboi) { fa''2~ | fa'' re'' | }
  \tag #'(oboe2 oboi) {
    \tag#'oboi \once\slurDown dod''2( |
    re'') la' |
  }
>>
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2\f dod'' | re''1 | }
  { r2 \tag#'oboi \once\tieUp fa''\f~ | fa'' la' | }
>>
R1*2 |
r8 <>\f <<
  \tag #'(oboe1 oboi) { la''8( sib'' sold'') la''4 }
  \tag #'(oboe2 oboi) { la'8( sib' sold') la'4 }
>> r4 |
<>\fp <<
  \tag #'(oboe1 oboi) { re''2 }
  \tag #'(oboe2 oboi) { re' }
>> r2 |
R1*3 |
r2 <>\f <<
  \tag #'(oboe1 oboi) { fad''4 sol'' | la'' }
  \tag #'(oboe2 oboi) { re''4 re'' | re'' }
>> r4 r2 |
R1*4 | \allowPageTurn
<>\p <<
  \tag #'(oboe1 oboi) { la''2( sol'') | }
  \tag #'(oboe2 oboi) { fad''2( mi'') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1 |
    mi'' |
    re'' |
    re''4 sol''2 mi''4 |
    fad'' sol'' la''2 |
    sol''4 mi''2 sol'4 |
    fad' la' si' sol'' |
    fad''2 mi'' |
    re''4 }
  { re''1~ |
    re''2 dod'' |
    re''2 fad'4 la' |
    si'2 dod'' |
    re''1 |
    re''2 dod''4 la'~ |
    la' re'2 si'4 |
    re''2. dod''4 |
    re''4 }
  { s1*2 | s1\f }
>> <<
  \tag #'(oboe1 oboi) {
    fad''8 mi'' re''4 la''8 sol'' |
    fad''4 la'' fad''
  }
  \tag #'(oboe2 oboi) {
    la'8 sol' fad'4 fad''8 mi'' |
    re''4 fad'' re''
  }
>> r4 |
r <>\p <<
  \tag #'(oboe1 oboi) {
    red''4( mi'' fad'') |
    sol''( si'' la'' fad'') |
  }
  \tag #'(oboe2 oboi) {
    si'( dod'' red''!) |
    mi''( sol'' fad'' red'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) << mi''4 mi'' >> r4 r2 |
R1*2 |
r4 <>\p <<
  \tag #'(oboe1 oboi) {
    si'4( do'' re''!) |
    mi''( re'' mi'' fa'') |
    mi''
  }
  \tag #'(oboe2 oboi) {
    sol'( la' si') |
    do''( si' do'' re'') |
    do''
  }
>> r4 r2 |
R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r2 sib''\f | sib''4 sol''2 sol''4 | }
  { re''2\f fad''! | sol''4 r re''2 | }
>>
R1*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r2 sib''\f | sib''4 sol''2 sol''4 | }
  { re''2\fp fad''! | sol'' re''2 | }
>>
R1*2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8( mib'' dod'') re''4 }
  { re''8( mib'' dod'') re''4 }
>> r4 |
<>\fp <<
  \tag #'(oboe1 oboi) { sol''2 }
  \tag #'(oboe2 oboi) { sol' }
>> r2 |
R1*2 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 si''4 do''' | re''' }
  { sol''1~ | sol''4 }
>> r4 r2 |
R1*6 |
r2 <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { si''2 | la''1 | sol''4 }
  { sol''2~ | sol'' fad'' | sol''4 }
  { s2 s1 s4\f }
>> r4 <<
  \tag #'(oboe1 oboi) {
    sol''4 s |
    sol'' s re''2 |
    mi''4 s s si'' |
    si''2
  }
  \tag #'(oboe2 oboi) {
    si'4 s |
    si' s si'2 |
    do''4 s s sol'' |
    sol''2
  }
  { s4 r | s r s2 | s4 r r }
>> r2\fermata |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1~ |
    sol''2 fad'' |
    sol''2. fa''4 |
    mi''4. si'8 do''4. sold'8 |
    la'2. la''4 |
    sol''2. fad''4 | }
  { si'2 re'' |
    do'' la' |
    si' re'' |
    sol'~ sol'4. si'8 |
    mi'2 mi''4 do'' |
    si'2 la' | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''4 re''8 do'' si'4 si''8 la'' |
    sol''4 sol'' sol''
  }
  \tag #'(oboe2 oboi) {
    si' si'8 la' sol'4 re''8 do'' |
    si'4 si' si'
  }
>> r4 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1 |
    fa''~ |
    fa'' |
    mi''2 sol'' |
    la''2 mi''4( re'') |
    do''4 }
  { mi''1 |
    la' si' |
    do'' |
    do''2. si'4 |
    do''4 }
>> r4 r2 |
R1*3 |
<<
  \tag #'(oboe1 oboi) { sol''1 }
  \tag #'(oboe2 oboi) { mi'' }
>>
R1*3 |
<<
  \tag #'(oboe1 oboi) { sol''1~ | sol''4 }
  \tag #'(oboe2 oboi) { mi''1~ | mi''4 }
>> r4 r2 |
R1*3
<>\p <<
  \tag #'(oboe1 oboi) { mi''2 la'' | sol'' fad'' | }
  \tag #'(oboe2 oboi) { do''2 mi'' | mi'' red'' | }
>>
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2 sol''~ |
    sol''4 mi''2 mi''4~ |
    mi'' sold'' la'' sol'' | }
  { mi''2 re''! |
    mi''4 r si'2 |
    do''4 re'' mi''2 | }
  { s2 s\f }
>>
<<
  \tag #'(oboe1 oboi) { fad''4 la'' fad'' }
  \tag #'(oboe2 oboi) { la'4 fad'' re'' }
>> r4 |
