\clef "alto" sol8 sol' fad' mi' re' mi' re' do' |
si mi' re' do' si do' si la |
sol sol sol sol sol' sol' sol' sol' |
la' la la' si' do'' si' la' sol' |
fad' la' fad' re' fad' re' fad' la' |
re' la' do'' si' la' sol' fad' mi' |
re' re' re' re' re' re' re' re' |
red' red' red' red' red' red' red' red' |
mi' mi' mi' mi' re'! re' re' re' |
do' do' do' do' si si si si |
la la la la re' re' re' re' |
sol' sol' sol' sol' sol' sol' fad' la' |
la' la' sol' si' si' la' la' la |
la4 la' <fad' la> r |
r2 re:16 |
mib4 r sib2:16 |
do'4 r si!2:16 |
fad!4:16 mib:16 re r |
sol'8 sol' la' la' si' si' si' si' |
do'' do'' do'' sol' mi' mi' mi' re' |
do' do' do' do' do' do' do' do' |
re' re' re' re' re' re' re' re' |
sol'4 r8 re'' sol'4 r8 re' |
sol'4 sol sol r |
sol8\p sol sol sol sol sol sol sol |
\ru#3 { sol sol sol sol sol sol sol sol | }
sol4 r r2\fermata |
re'8\p re' re' re' si si si si |
fad' fad' fad' fad' fad' fad' fad' fad' |
la' fad' fad' fad' <fad' la'> q q q |
sol' sol' sol' sol' mi' mi' mi' mi' |
do' do' do' do' re' re' re' re' |
sol' sol' sol' sol' sol' sol'\p fad' mi' |
red' red' red' red' red' red' red' red' |
mi' mi' mi' mi' mi' mi' mi' mi' |
dod' dod' dod' dod' dod' dod' dod' dod' |
re'! re' re' re' dod' dod' dod' dod' |
si si si si mi' mi' mi' mi' |
la'4 r la2:16\f |
sib4 r fa2:16 |
sol8\p sol sol sol mi' mi' mi' mi' |
re'4 fa sol8 sol sol sol |
la4 r la2:16\f |
sib4 r fa8 fa fa fa |
sol\p sol sol sol mi' mi' mi' mi' |
fa' fa' fa' fa' sol' sol' sol' sol' |
la' la(\f sib sold) la4 r |
\ru#32 re'8 |
re'4 r re'\f mi' |
fad'8\p fad' fad' fad' fad' fad' fad' fad' |
sol' sol' sol' sol' sol' sol' sol' sol' |
la' la' la' la' la la la la |
re' re' re' re' fad' fad' fad' fad' |
sol' sol' sol' sol' sol' sol' sol' sol' |
fad' fad' fad' fad' sol' sol' sol' sol' |
la' la' la' la' la' la' la' la' |
la la la la la la la la |
re'\f re' re' re' re' re' fad' fad' |
mi' mi' mi' mi' la' la' la' la' |
re' re' mi' mi' fad' fad' fad' fad' |
mi' mi' mi' sold' la' la' dod' dod' |
re' re' fad' fad' sol' sol' sol' sol' |
la' la' la' la' la la la la |
re'4 r8 la' re'4 r8 la |
re'4 re' re' r |
si8\p si si si si si si si |
si si si si si si si si |
do' do' do' do' sol sol sol sol |
la la la la si si si si |
si4 mi'(\f re' do') |
si8 sol\p sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol mi' mi' mi' mi' |
fa' fa' fa' fa' sol' sol' sol' sol' |
do' do' do' do' do' do' do' do' |
do' do' do' do' do' do' dod' dod' |
re'4 r re2:16\f |
mib4 r sib2:16 |
do'8\p do' do' do' fa' fa' fa' fa' |
mib' mib' mib' mib' sib' sib' sib' sib' |
fad'!4 r re2:16\f |
mib4 r sib2:16 |
do'8\p do' do' do' re' re' re' re' |
mib' mib' fad' fad' sol' sol' mib' mib' |
re'-.\f re'( mib' dod') re'4 r |
sol8\p sol sol sol sol sol sol sol |
sol' sol' sol' sol' re' re' re' re' |
si si sol sol re' re' re' re' |
sol'\f sol' sol' sol' sol' sol' la' la' |
si'4\p r si r |
do' r do' r |
re' r re' r |
sol'8 sol' sol' sol' sol sol sol sol |
re' re' re' re' re' re' re' re' |
sol'4 r r2 |
r do'8 do' do' do' |
re' re' re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' |
sol'\f sol' fad' mi' re' mi' re' do' |
si do' si la sol sol sol sol |
do' do' do' do' do'4 dod' |
re'1\fermata |
re'8 re' re' re' re' re' re' re' |
mi' mi' mi' mi' re' re' re' re' |
re' re' re' re' sol' sol' sol' sol' |
sol' sol'4 fa'8 mi' mi' mi' mi' |
mi' mi' mi' mi' sol' sol' sol' sol' |
sol' sol' sol' sol' re' re' do' do' |
si4 r8 re' re'4 r8 re' |
re'4 re' re' r |
do'8 r do' r do' r do' r |
re' r re' r re' r re' r |
sol' r sol' r sol' r sol' r |
do' r do' r mi' r mi' r |
fa' r fa' r sol' r sol r |
do' r do' r do' r do' r |
do' r do' r do' r do' r |
si r la' r red' r red' r |
mi' r mi' r do' r si r |
si r mi' r mi' r mi' r |
la r la r la r la r |
re' r re' r re' r re' r |
sol r sol r sol r sol r |
do'! r do' r do' r si r |
dod' r dod' r dod' r mi' r |
red' r fad'! r do'! r do' r |
sol r do' r la r si r |
do' r do' r sold r sold r |
la r la r do' r do' r |
si r si r si r si r |
mi'4 r si2:16\f |
do'4 r sold2:16 |
la8 mi'4 mi' mi' la'8 |
la'4 <la' re'> <fad' la> r |
