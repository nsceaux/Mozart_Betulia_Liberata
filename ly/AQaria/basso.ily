\clef "bass" sol,8 sol fad mi re mi re do |
si, mi re do si, do si, la, |
sol, sol, sol, sol, sol sol sol sol |
la la, la si do' si la sol |
fad la fad re fad re fad la |
re la do' si la sol fad mi |
re re re re re re re re |
red red red red red red red red |
mi mi mi mi re! re re re |
do do do do si, si, si, si, |
la, la, la, la, re re re re |
sol sol sol sol la la la la |
si si si si do' do' do' dod' |
re' re' re' re' re4 r |
r2 re( |
mib4) r sib,2( |
do4) r sol,2 |
la,4( sol, fad,) r |
sol8 sol la la si si si si |
do' do' do' sol mi mi mi re |
do do do do do do do do |
re re re re re re re re |
sol4 r8 re' sol4 r8 re |
sol4 sol, sol, r |
sol\p r r2 |
R1 |
sol4 r r2 |
R1 |
sol4 r r2\fermata |
sol8\p sol sol sol sol sol sol sol |
la la la la la la la la |
re re re re re re re re |
sol sol sol sol mi mi mi mi |
do do do do re re re re |
sol\f sol sol sol sol sol\p fad mi |
red red red red red red red red |
mi mi mi mi mi mi mi mi |
dod dod dod dod dod dod dod dod |
re! re re re dod dod dod dod |
si, si, si, si, mi mi mi mi |
la4 r la2\f( |
sib4) r fa2 |
sol8\p sol sol sol sol sol sol sol |
fa fa fa fa sol sol sol sol |
la4 r la2(\f |
sib4) r fa2 |
sol8\p sol sol sol sol sol sol sol |
fa fa fa fa sol sol sol sol |
la la(\f sib sold) la sol\p fad mi |
\ru#32 re |
re4 r re\f mi |
fad8\p fad fad fad fad fad fad fad |
sol sol sol sol sol sol sol sol |
la la la la la, la, la, la, |
re re re re fad fad fad fad |
sol sol sol sol sol sol sol sol |
fad fad fad fad sol sol sol sol |
la la la la la la la la |
la, la, la, la, la, la, la, la, |
re\f re re re re re fad fad |
mi mi mi mi la la la la |
re re mi mi fad fad fad fad |
mi mi mi sold la la dod dod |
re re fad fad sol sol sol sol |
la la la la la, la, la, la, |
re4 r8 la re4 r8 la, |
re4 re re r | \allowPageTurn
si,\p r r2 |
R1 |
do'8 do' do' do' sol sol sol sol |
la la la la si si si si |
mi4 mi'(\f re' do') |
si r r2 |
R1 |
do8\p do do do mi mi mi mi |
fa fa fa fa sol sol sol sol |
do do do do do do do do |
do do do do do do dod dod |
re4 r re2(\f |
mib4) r sib,2 |
do8\p do do do re re re re |
mib mib mib mib mib mib mib mib |
re4 r re2\f( |
mib4) r sib,2 |
do8\p do do do re re re re |
mib mib fad fad sol sol mib mib |
re-.\f re( mib dod) re do'!-.\p si!-. la-. |
sol sol sol sol sol sol sol sol |
sol sol sol sol re re re re |
si, si, sol, sol, re re re re |
sol\f sol sol sol sol sol la la |
si4\p r si, r |
do r do r |
re r re r |
sol8 sol sol sol sol, sol, sol, sol, |
re re re re re re re re |
sol4 r r2 |
r do8 do do do |
re re re re re re re re |
re re re re re re re re |
sol\f sol fad mi re mi re do |
si, do si, la, sol, sol, sol, sol, |
do do do do do4 dod |
re1\fermata |
sol8 sol sol sol si si si si |
la la la la re re re re |
sol sol sol sol si, si, si, si, |
do do do re mi mi mi re |
do do do do do do do do |
re re re re re re re re |
sol4 r8 re' sol4 r8 re |
sol4 sol, sol, r |
do8 r do r do r do r |
re r re r re r re r |
sol r sol r sol r sol r |
do r do r mi r mi r |
fa r fa r sol r sol, r |
do r do r do r do r |
do r do r do r do r |
si, r la r red r red r |
mi r mi r la r si r |
mi r mi r mi r mi r |
la, r la, r la, r la, r |
re r re r re r re r |
sol, r sol, r sol, r sol, r |
do! r do r do r si, r |
lad, r lad, r lad, r lad, r |
la,! r la, r la r la r |
sol r do r la, r si, r |
do r do r sold, r sold, r |
la, r la, r do r do r |
si, r si, r si, r si, r |
mi4 r si,2\f( |
do4) r sold2( |
la8) la la, si, do do do dod |
re4 re re r |
