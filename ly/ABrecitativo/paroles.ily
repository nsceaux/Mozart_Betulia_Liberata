Po -- po -- li di Be -- tu -- lia, ah qual v’in -- gom -- bra
ver -- go -- gno -- sa vil -- tà! Pal -- li -- di, af -- fli -- ti,
tut -- ti mi sie -- te in -- tor -- no! E ver, ne strin -- ge
d’as -- se -- dio per -- ti -- na -- ce il cam -- po As -- si -- ro:
ma non siam vin -- ti an -- cor. Dun -- que sì pre -- sto
ce -- de -- te al -- le sven -- tu -- re? io, più di lo -- ro,
te -- mo il vo -- stro ti -- mor. De’ no -- stri ma -- li
que -- sto, que -- sto è il peg -- gior: que -- sto ci ren -- de
in -- a -- bi -- li a’ ri -- pa -- ri. O -- gni tem -- pe -- sta
al noc -- chier, che di -- spe -- ra,
è tem -- pe -- sta fa -- tal, ben -- chè leg -- ge -- ra.
