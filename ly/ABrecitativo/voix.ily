\clef "tenor/G_8" <>^\markup\character Ozia
re'8 re'16 re' re'8 fad' re'8 re' r4 |
re'4 re'8 re' re' la r la16 sib |
do'4 do'8 sib sol4 r |
sib8 sib16 sib r8 re' si si r4 |
re'8 re'16 mib' fa'8 mib' do' do' r4 |
r8 sol do' mib' do' do' r do' |
do' do' sib do' la la la sib |
do' do' r4 r16 do' do' re' mib'8 re' |
sib4 r sib sib8 re' |
do' do' r sol sib sib do' sol |
la la r4 la la8 si! |
dod' dod' dod' re' mi'!4 mi'8 fa' |
re'4 r16 re' do'! re' si!8 si r4 |
re'8 re' fa' fa'16 mi' do'4 r |
do'4 re'8 mi' re' re' r la |
do' do' do'8. re'16 si8 si r4 |
sol sol8 la sib sib r sib16 sib |
mi'4 mi'8 sol' sol' sib r sol16 la |
sib4 sib8 do' la4 r |
re'4 <<
  { \voiceOne mi'8 fa' do' do' \oneVoice }
  \new Voice \with { autoBeaming = ##f } {
    \voiceTwo sib8 la fa fa
  }
>> r4 |
R1 |
