\clef "bass" re1~ |
re~ |
re2 sib,~ |
sib, fa~ |
fa mib |
mib1~ |
mib~ |
mib |
re |
mi! |
dod~ |
dod |
re2 sol~ |
sol mi~ |
mi fad~ |
fad sol~ |
sol mi~ |
mi1~ |
mi2 fa |
sib, r4 do |
fa,1 |
