\clef "bass" dod2 re |
r4 mi fa!2~ |
fa1~ |
fa2 re |
fad1 |
sol2 mi |
fa!1 |
re2 do~ |
do mi~ |
mi fa~ |
fa fad~ |
fad1 |
sol~ |
sol |
r4 la sold2~ |
sold1~ |
sold |
la2 red~ |
red mi |
fad1~ |
fad |
sol |
r4 la sib2~ |
sib1~ |
sib~ |
sib2 mib~ |
mib fa~ |
fa mi!~ |
mi1 |
la,2 mi~ |
mi fa |
fad1~ |
fad~ |
fad2 sol |
re1~ |
re2 mib~ |
mib fad~ |
fad sol |
dod1~ |
dod2 re |
la,1 |
la, |
sib,2 fad~ |
fad sol |
mi fa!~ |
fa1~ |
fa2 r4 sol |
fad1~ |
fad |
sol~ |
sol |
mib |
fa2 r4 sol |
mi!1~ |
mi2 fa~ |
fa reb~ |
reb do |
