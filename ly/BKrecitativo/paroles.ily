Quan -- ta cu -- ra hai di noi, bon -- tà di -- vi -- na!

Fu -- ro, o san -- ta E -- ro -- i -- na,
ve -- ri i pre -- sa -- gi tu -- oi: gli As -- si -- ri op -- pres -- se,
ec -- ci -- dio u -- ni -- ver -- sal.

For -- se è lu -- sin -- ga
del tuo de -- si -- o.

No; del fe -- li -- ce e -- ven -- to
par -- te vid’ i -- o; da’ trat -- te -- nu -- ti il re -- sto
fug -- gi -- ti -- vi rac -- col -- si. In su le mu -- ra,
co -- me im -- po -- se Giu -- dit -- ta al suo ri -- tor -- no,
de -- stai di gri -- da e d’ar -- mi
stre -- pi -- to -- so tu -- mul -- to.

E qui s’in -- te -- se

Te -- mon le guar -- die o -- sti -- li
d’un as -- sal -- to not -- tur -- no, ed O -- lo -- fer -- ne
cor -- ro -- no ad av -- ver -- tir -- ne. Il tron -- co in -- for -- me
tro -- van co -- là nel pro -- prio san -- gue in -- vol -- to;
tor -- nan gri -- dan -- do in -- die -- tro. II ca -- so a -- tro -- ce
spar -- ge -- si fra le schie -- re, in -- ti -- mo -- ri -- te
già da’ no -- stri tu -- mul -- ti; ec -- co cia -- scu -- no
pre -- ci -- pi -- ta al -- la fu -- ga, e nel -- la fu -- ga
l’un l’al -- tro ur -- ta, im -- pe -- di -- sce. In -- ciam -- pa, e ca -- de
so -- pra il ca -- du -- to il fug -- gi -- ti -- vo: im -- mer -- ge
sto -- li -- do in sen l’in -- vo -- lon -- ta -- rio ac -- cia -- ro
al com -- pa -- gno il com -- pa -- gno; op -- pri -- me op -- pres -- so,
nel sol -- le -- var l’a -- mi -- co, il fi -- do a -- mi -- co.
Or -- ri -- bil -- men -- te il cam -- po
tut -- to rim -- bom -- ba in -- tor -- no. E -- scon dal chiu -- so
spa -- ven -- ta -- ti i de -- strie -- ri, e van -- no anch’ es -- si
cal -- pe -- stan -- do per l’om -- bre
gli e -- stin -- ti, i se -- mi -- vi -- vi a’ lor ni -- tri -- ti
mi -- ste de -- gli em -- pi e le be -- stem -- mie, e i vo -- ti
dis -- si -- pa il ven -- to. A -- pre al -- la mor -- te il ca -- so
cen -- to in -- so -- li -- te vi -- e. Del pa -- ri o -- gnu -- no
te -- me, fug -- ge, pe -- ris -- ce; e o -- gnun del pa -- ri
i -- gno -- ra in quell’ or -- ro -- re
di che te -- me, o -- ve fug -- ge, e per -- chè muo -- re.

Oh Di -- o! So -- gno, o son de -- sto?

O -- di, o Si -- gnor, quel mor -- mo -- rio fu -- ne -- sto?
