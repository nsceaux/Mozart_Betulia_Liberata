\ffclef "soprano/treble" <>^\markup\character Cabri
r8 dod''16 mi'' mi''8 sol'!16 la' fad'8 fad'' re'' dod'' |
la' la' r4
\ffclef "soprano/treble" <>^\markup\character Carmi
la'8 la' r la' |
la' la' la' do''! do'' fa'! r4 |
la'8 la'16 sib' do''8 re'' sib' sib' r16 sib' do'' re'' |
re''8 la' r la' do'' do'' do'' sib' |
sol'4
\ffclef "tenor/G_8" <>^\markup\character Ozia
sib8 do'16 re' do'8 do' r16 do' sib do' |
la8 la
\ffclef "soprano/treble" <>^\markup\character Carmi
do''4 r8 do''16 do'' si'!8 la' |
sold' sold' si' si'16 do'' la'8 la' r la' |
la' la' si' do'' do'' sol'! r sol'16 la' |
sib'4 sib'8 la' fa' fa' r4 |
r8 do'' re'' mib'' mib'' la' r la'16 la' |
la'4 la'8 sib' do'' do'' r16 do'' do'' re'' |
sib'8 sib' r16 sib' do'' re'' re''8 sol' r sol' |
dod'' dod'' r dod''16 mi''! dod''4 dod''8 re'' |
re'' la' r4
\ffclef "soprano/treble" <>^\markup\character Amital
r8 si' si' mi'' |
mi'' si' r4
\ffclef "soprano/treble" <>^\markup\character Carmi
si'8 si'16 si' si'8 do'' |
re'' re'' r re''16 fa'' re''4 re''8 do'' |
la' la' r16 la' si' do'' si'8 si' r4 |
la'8 la'16 la' la'8 sol' mi' mi' r16 si' si' mi'' |
re''8 re'' r4 la' la'8 si' |
do'' do'' do'' mi'' do''4 do''8 si' |
sol' sol' r4 dod''8 dod''16 dod'' dod''8 re'' |
re'' la' r4 r8 fa'! sib' la' |
sib' sib' r4 re''8 re''16 re'' do''!8 re'' |
sib' sib' r16 sib' sib' do'' lab'8 lab' r re''16 fa'' |
fa''4 lab'8 sib' sol' sol' r4 |
mib''4 re''8 do'' si'! si' r si' |
si'8. si'16 do''8 re'' re'' sold' r16 sold' sold' la' |
si'8 si' r16 si' si' do'' re''8 re'' r re''16 do'' |
la'8 la' r16 la' si' do'' do''8 sol' sol' sol'16 la' |
sib'8 sib' r16 re'' sib' la' fa'8 fa' r16 do'' re'' mib'' |
mib''8 la' la' la' fad'4 r |
la'8 la'16 la' la'8 sib' do'' do'' r do''16 mib'' |
do''4 do''8 sib' sol' sol' r16 sib' sib' do'' |
lab'8 lab' r re'' re'' re'' mib'' fa'' |
fa'' lab' r16 lab' lab' sib' sol'8 sol' r4 |
sib'8 sib'16 sib' sib'8 mib'' re'' re'' r4 |
do''8 do''16 do'' do''8 sib' sol' sol' r4 |
la'4 la'8 si'! dod'' dod'' r dod''16 mi''! |
mi''4 sol'8 la' fa' fa' r16 re'' mi'' fa'' |
fa''8 do''! r do''16 do'' la'4 la'8 sib' |
do''8 do'' fa'' do'' mib'' mib'' mib'' re'' |
sib' sib' r16 sib' do'' re'' re''8 la' la' la'16 sib' |
do''8 do'' do'' sib' sol' sol' r re'' |
do''! do'' sib' sib'16 do'' la'8 la' r4 |
do''8 do''16 do'' fa''8 re'' si'! si' r si'16 re'' |
si'4 si'8 do'' do'' sol' r4 |
r8 la' la' re'' re'' la' r4 |
fad''8 re'' r4 do''8 do'' r re'' |
si' si' r4 r8 sol' sol' la' |
si' si' r sol' re'' re'' re'' mib'' |
do'' do'' r sol'16 sol' do''8 do'' r do''16 do'' |
lab'8 lab' r16 do'' si'! do'' do''8 sol' r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
r4 r8 sol do' do' r4 |
reb'8 sib r sib16 do' lab8 lab r4 |
\ffclef "soprano/treble" <>^\markup\character Carmi
lab'8 lab' r lab'16 do'' fa'4 r8 lab' |
fa' fa' fa' mi'! sol' sol' r4 |
