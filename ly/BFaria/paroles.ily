Pri -- gio -- nier, __ che fa ri -- tor -- no
da -- gli or -- ro -- ri al dì se -- re -- no,
da -- gli or -- ro -- ri al dì se -- re -- no,
chiu -- de i lu -- mi a’ rai del gior -- no,
a’ rai del gior -- no,
e pur tan -- to il so -- spi -- rò.
Pri -- gio -- nier, che fa __ ri -- tor -- no
da -- gli or -- ro -- ri al dì __ se -- re -- no,
chiu -- de i lu -- mi ai rai del gior -- no,
e pur tan -- to il so -- spi -- rò,
chiu -- de i lu -- mi ai rai del gior -- no,
e pur tan -- to il so -- spi -- rò,
e pur tan -- to il so -- spi -- rò.

Pri -- gio -- nier, che fa, che fa ri -- tor -- no
da -- gli or -- ro -- ri al dì se -- re -- no,
da -- gli or -- ro -- ri al dì __ se -- re -- no,
pri -- gio -- nier, __ che fa ri -- tor -- no
da -- gli or -- ro -- ri al dì se -- re -- no,
da -- gli or -- ro -- ri al dì se -- re -- no,
chiu -- de i lu -- mi ai rai del gior -- no,
ai rai del gior -- no,
e pur tan -- to il so -- spi -- rò.

Pri -- gio -- nier, che fa ri -- tor -- no
da -- gli or -- ro -- ri al dì se -- re -- no,
chiu -- de i lu -- mi ai rai del gior -- no
e pur tan -- to il so -- spi -- rò,
chiu -- de i lu -- mi ai rai del gior -- no,
e pur tan -- to il so -- spi -- rò,
e pur tan -- to il so -- spi -- rò,
e pur tan -- to il so -- spi -- rò.

Ma co -- sì fra po -- co ar -- ri -- va
a sof -- frir la chia -- ra lu -- ce,
a sof -- frir la chia -- ra lu -- ce,
ché l’av -- vi -- va e lo con -- du -- ce
lo splen -- dor, che l’ab -- ba -- gliò,
lo splen -- dor, che l’ab -- ba -- gliò, che l’ab -- ba -- gliò.
