\clef "treble" \transposition re <>
r4 <<
  \tag #'(corno1 corni) { do''1 | re'' | }
  \tag #'(corno2 corni) { mi'1 | sol' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 fa'' | mi''4 do'' re''2 | }
  { sol'1 | do'2 sol' | }
>>
<<
  \tag #'(corno1 corni) { mi''2 re'' | do''1 }
  \tag #'(corno2 corni) { do''2 sol' | do'1 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 s re'' re'' | }
  { do''4 s re'' re'' | }
  { s4 r }
>>
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol }
>> r4 |
R1*3 |
<>\f <<
  \tag #'(corno1 corni) {
    do''1 |
    do''4 s2. |
    mi''2 re'' |
    do''4 s4 sol''8 mi'' fa'' re'' |
    mi''4 s s sol' |
    do'' do'' do''
  }
  \tag #'(corno2 corni) {
    do'1 |
    do'4 s2. |
    do''2 sol' |
    do'4 s mi''8 do'' re'' sol' |
    do''4 s s sol |
    do' do' do'
  }
  { s1 | s4 r r2 | s1 | s4 r s2 | s4 r r s | }
>> r4 |
R1*5 |
<>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*4 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { re''2 re''4 }
  { re''2 re''4 }
>> r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 re'' |
    re''1 |
    re''2 re'' |
    re''1 |
    re''4 }
  { sol'2 re'' |
    re''1 |
    sol'2 re'' |
    re''1 |
    sol'4 }
  { s1\f s\p s\f s\p }
>> r4 r2 |
R1 |
r4 <>\f \twoVoices #'(corno1 corno2 corni) <<
  { re'' re'' }
  { re'' re'' }
>> r4 |
R1*7 |
<>\f <<
  \tag #'(corno1 corni) { re''1 | }
  \tag #'(corno2 corni) { sol' | }
>>
R1*4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 |
    re''2 re'' |
    re'' re'' | }
  { re''1 |
    sol'2 re'' |
    sol' re'' | }
  { s1\p s\f }
>>
<<
  \tag #'(corno1 corni) { sol''2 sol''8 }
  \tag #'(corno2 corni) { sol'2 sol'8 }
>> r8 r4 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 |
    re''4 s s re'' |
    re'' s s re'' | }
  { re''1 |
    sol'4 s s re'' |
    sol' s s re'' | }
  { s1 | s4 r r s | s r r s | }
>>
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol' }
>> r4 |
R1 |
<>\f <<
  \tag #'(corno1 corni) { mi''2~ mi''4 }
  \tag #'(corno2 corni) { mi'2~ mi'4 }
>> r4 |
R1 |
<>\f <<
  \tag #'(corno1 corni) { mi''1 | }
  \tag #'(corno2 corni) { do'' }
>>
R1*3 |
r8 <>\f <<
  \tag #'(corno1 corni) { re''8 re'' re'' re''4 }
  \tag #'(corno2 corni) { sol'8 sol' sol' sol'4 }
>> r4 |
R1*5 |
<>\f <<
  \tag #'(corno1 corni) { do''2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
R1*4 |
<>\f <<
  \tag #'(corno1 corni) { re''2 re''4 }
  \tag #'(corno2 corni) { sol'2 sol4 }
>> r4 |
<>\f <<
  \tag #'(corno1 corni) { do''2 sol' | mi'4 }
  \tag #'(corno2 corni) { do'2 sol | do'4 }
  { s1 s4\p }
>> \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 sol'4 | }
  { sol'2 sol'4 | }
>>
<>\f <<
  \tag #'(corno1 corni) { do''2 sol' | mi'4 }
  \tag #'(corno2 corni) { do'2 sol | do'4 }
  { s1 s4\p }
>> \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 sol'4 | }
  { sol'2 sol'4 | }
>>
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { do' }
>> r4 r2 |
R1 |
r4 <>\f <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
R1*7 |
<>\f <<
  \tag #'(corno1 corni) { do''1 }
  \tag #'(corno2 corni) { do' }
>>
R1*4 |
<>\p <<
  \tag #'(corno1 corni) { sol'1 | do'' | do'' | }
  \tag #'(corno2 corni) { sol'1 | do' | do' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol'4 }
  { sol' }
>> r4 r2\fermata |
<<
  \tag #'(corno1 corni) {
    mi''2 re'' |
    do'' re'' |
    do'' s |
    mi'' re'' |
    do''4 s sol''8 mi'' fa'' re'' |
    mi''4 s s sol' |
    do'' do'' do''
  }
  \tag #'(corno2 corni) {
    do''2 sol' |
    mi' sol' |
    do' s |
    do'' sol' |
    mi'4 s mi''8 do'' re'' sol' |
    do''4 s s sol |
    do' mi' mi'
  }
  { s1*2 s2 r | s1 | s4 r s2 | s4 r r s | }
>> r4 |
R4.*25 |
<>\f <<
  \tag #'(corno1 corni) { mi''4 }
  \tag #'(corno2 corni) { mi' }
>> r8\fermata |
<<
  \tag #'(corno1 corni) {
    do''4 re'' mi'' do'' |
    do''
  }
  \tag #'(corno2 corni) {
    mi'4 sol' do'' do' |
    do'
  }
>> r4 \twoVoices #'(corno1 corno2 corni) << re''2 re'' >> |
<<
  \tag #'(corno1 corni) { re''4 re'' re'' }
  \tag #'(corno2 corni) { sol' sol' sol }
>> r4 |
