\clef "alto" r4 |
re'8( fad' re' fad') re'( fad' re' fad') |
mi'( sol' mi' sol') mi'( sol' mi' sol') |
la( mi' dod' mi') la( dod' mi' dod') |
re'16( mi' re' dod') re'( fad' si' la') sol'( fad' mi' re') dod'( mi' la' sol') |
fad'4 re'2 dod'16-. sol'( fad' mi') |
re'8 re' re' re' fad' fad' fad' fad' |
sol'4 sol' sol' sold' |
la' la la r |
r fad'4\p( si'2) |
r4 mi'( la'2) |
re'4 re' re' r |
re'16(\f mi' fad' sol') la'( si' do'' si') la'( sol' fad' mi') re'( mi' fad' re') |
sol'4 r r8 fad'( sol') sold'-. |
la'8 la' la' la' la la la la |
re'4 re'' r la' |
re' re'' r la' |
re' re' re' r |
re'8(\p fad' re' fad') re'( fad' re' fad') |
mi'( sol' mi' sol') mi'( sol' mi' sol') |
la( dod' la dod') la( dod' la dod') |
re'4 re' fad' fad' |
sol' sol' la' la |
re'\f re' re' r |
dod'\p dod' dod' dod' |
si si mi' mi' |
la la dod' dod' |
re' re' red' red' |
mi' mi'\f mi' r |
<<
  { dod'2 re' |
    dod' re' |
    dod' re' |
    dod' re' |
    dod'4 } \\
  { la2\f si |
    la\p si |
    la\f si |
    la\p si |
    la4 }
>> la4 la la |
fad'8 fad' fad' fad' fad' fad' fad' fad' |
mi'4 mi'\f mi' r |
r dod4\p fad2 |
r4 si mi2 |
la4 la dod' dod' |
re' re' mi' mi' |
la\mf dod4( fad2) |
r4 si(\p mi2) |
la8 la la la la4 r |
la16(\f si dod' re') mi'( fad' sol'! fad') sol'( fad' mi' re') dod'( si la dod') |
re'8\p re' re' re' mi' mi' mi' mi' |
la' la' la' la' dod' dod' dod' dod' |
re'8 re' re' re' fad' fad' fad' fad' |
mi' mi' mi' mi' mi' mi' mi' mi' |
mi' mi' mi' mi' mi' mi' mi' mi' |
la'16(\f mi' re' dod') si( la sold la) sold( la si dod') re'( si mi' re') |
dod'4 la si2 |
la4 dod' re' dod'8 red' |
mi'8 mi' mi' mi' mi' mi' mi' mi' |
la4 la' r mi' |
la la' r mi' |
la la la r |
la'8\p la' la' la' la' la' sol'! sol' |
fad'\f fad' fad' fad' fad'\p fad' fad' fad' |
sol' re' re' mi' fad' fad' fad' fad' |
si\f si si si si si si si |
re'\p re' re' re' re' re' re' re' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sold' sold' sold' sold' |
la' la\f la la la4 r |
re'8(\p fad' re' fad') re'( fad' re' fad') |
mi'( sol' mi' sol') dod'( mi' dod' mi') |
la( dod' la dod') la( dod' la dod') |
re'4 re' fad' fad' |
sol' sol' la' la' |
re'\f re' re' r |
re'\p re' do' do' |
si si fad' fad' |
sol' sol' sol' sol' |
sol' sol' sol sold |
la la'\f la r |
r4 re'\f dod' la |
r re'\p dod' la |
r re'\f dod' la |
r re'\p dod' la |
re' re' si' si' |
sol' sol' sol' sold' |
la' la\f la r |
r fad'4(\p si'2) |
r4 mi'( la'2) |
re'4 re' fad' fad' |
sol' sol' la' la |
r4 fad'( si'2) |
r4 mi'4( la'2) |
re'4 re' re' re' |
re'16\f( mi' fad' sol') la'( si' do'' si') do''( si' la' sol') fad'( mi' re' fad') |
sol'8\p sol' sol' sol' la' la' la' la' |
re' re' re' re' fad' fad' fad' fad' |
sol' sol' sol' sol' si' si' si' si' |
la' la' la' la' la' la' la' la' |
la la la la la la la la |
re'\f re' re' re' si' si' si' si' |
fad' fad' sol' la' si' la' sol' sold' |
la'1\fermata |
re'16( la' sol' fad') mi'( re' dod' re') dod'( re' mi' fad') sol'( mi' la' sol') |
fad'2 sol'4 mi'16( sol' fad' mi') |
re'4 fad' sol' fad'8 sold' |
la' la' la' la' la la la la |
re'4 re'' r la' |
re' re'' r la' |
re' re' re' r |
sol'8 si' sol' |
la'4 sol'8 |
fad' fad' re' |
sol' sol' la' |
si' la' sol' |
fad' re' sol' |
do' re' do' |
si sol re' |
si' la' sol' |
fad' re' sol' |
do' re' re' |
sol-. sol'-. si'-. |
red'4. |
mi' |
lad |
si8 si dod'! |
re' re' re' |
mi' mi' mi' |
mi' fad' fad' |
sol' sol' sol' |
red' red' red' |
mi' mi' mi' |
mi' fad' fad' |
si'\f re'' fad' |
sol' si' mid' |
fad'4 r8\fermata |
si4 dod' re' re' |
sol' sol' sol' sold' |
la' la la r |
