\clef "treble"
<<
  \tag #'violino1 {
    la''8. la''16 |
    \grace sol''8 fad''2~ fad''8 la'' \grace sol''16 fad''8 \grace mi''16 re''8 |
    \grace mi''8 re''4 dod'' r sol''8 \grace fad''16 mi''8 |
    \grace re''8 dod''2~ dod''8 mi'' \grace la''16 sol''8 \grace fad''16 mi''8 |
    re''4 <la' la''>2 q4 |
    la''16-. fad''( mi'' re'') dod''( re'') si''( la'') sol''( fad'' mi'' re'') dod''-. si''( la'' sol'') |
    fad''4 la''2 lad''4 |
    lad''16( si'') fad''-. fad''-. fad''( sol'') red''-. red''-. red''( mi'') si'-. si'-. si'( dod'') dod''( re''!) |
    dod''( si' la' si') la'8-. la'-. <la' mi' dod'>4 r |
    la'8(\p la''8) la''2 sol''16( fad'' mi'' red'') |
    mi''8( sol'') sol''2 fad''16( mi'' re''! dod'') |
    re''8( fad'') la''-. la''-. la''-. la''-. la''16( sol'' la'' si'') |
    <re'' do'''>8\f q4 q q q8 |
    si''4 dod'''!8.\trill si''32 dod''' re'''8 la''( si'') sold-. |
    la2 mi''\trill |
    re''4 r8 \tuplet 3/2 { sol''16( la'' si'') } la''8-. fad''-. sol''-. mi''-. |
    <fad'' la' re'>4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    re''4 <fad'' la' re'>4 q la''8.\p la''16 |
    \grace sol''8 fad''2~ fad''8 la'' \grace sol''16 fad''8 \grace mi''16 re''8 |
    \grace re''8 dod''4 dod'' r mi''8 mi'' |
    sol''2~ sol''8( mi'') sol''( mi'') |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    mi''8 mi'' mi'' sol''16( mi'') re''8 re'' re'' dod'' |
    re'' la'16(\f si' la'8) re''16( mi'' re''8) fad''\p fad'' fad'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' mi'' mi'' mi'' mi'' |
    fad'' fad'' fad'' sold'' la'' la'' la'' dod'' |
    si'4
  }
  \tag #'violino2 {
    r4 |
    \ru#4 { re'16( fad') la'-. la'-. } |
    \ru#4 { la16( sol') la'-. la'-. } |
    \ru#4 { la16( mi') sol'-. sol'-. } |
    fad'16( mi' re' dod') re'( fad' si' la') sol'( fad' mi' re') dod'( mi' la' sol') |
    fad'4 la'2 la'4 |
    la'8 re''4 re' re'' re'8 |
    re'4 si'2 mi'4 |
    mi'16( re' dod' re') dod'8-. dod'-. <la mi' la'>4 r |
    r4 do''(\p red') fad'8( la') |
    sol'4 si'( dod'! mi'8 sol') |
    fad'8( la') fad'-. fad'-. fad'-. fad'-. fad'16( mi' fad' sol') |
    la'8\f <la' fad''>4 q q q8 |
    <re' si' sol''>4 mi''8.\trill re''32 mi'' fad''8 re''4 si8-. |
    la16 re' fad' re' la' fad' re' fad' la mi' dod' mi' sol' mi' dod' mi' |
    fad'4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    <re'' re'>4 r8 \tuplet 3/2 { sol'16( la' si') } la'8-. fad'-. sol'-. mi'-. |
    re'4 <re' la' fad''>4 q r |
    <>\p \ru#4 { re'16( fad') la'-. la'-. } |
    \ru#4 { la16( sol') la'-. la'-. } |
    \ru#4 { la16( dod') mi'-. mi'-. } |
    la16( re') fad'-. fad'-. re'( fad') la'-. la'-. la'8 la' la' la' |
    si' si' si' si' fad' fad' fad' mi' |
    fad' fad'16(\f sol' fad'8) fad'16( sol' fad'8) la'\p la' la' |
    la' la' la' la' la' la' la' la' |
    la' sold' sold' sold' sold' sold' sold' sold' |
    la' mi' mi' mi' la' la' la' la' |
    la' la' la' si' dod'' dod'' dod'' la' |
    sold'4
  }
>> mi'4\f <mi' si' sold''> r |
<<
  \tag #'violino1 {
    R1 |
    dod''8(\p la') r mi''16( dod'') si'8 sold' r re''16( si') |
    dod''4 r r2 |
    dod''8( la') r mi''16( dod'') si'8( sold') r re''16( si') |
    dod''8 dod'' dod'' dod'' dod'' dod'' dod'' dod'' |
    la''2:16 la'':16 |
    sold''4 <mi'' si' sold'>\f q r |
    mi'8\p( mi'') mi''2 \grace re''16 dod''8( si'16 lad') |
    si'8( re'') fad''2 \grace mi''16 re''8( dod''16 si') |
    dod''8( mi'') la''-. la''-. la'' la'' la'' la'' |
    la''-. la''( fad'') re''-. dod''-. dod''( si') mi'-. |
    mi'(\mf mi'') mi''2 \grace re''16 dod''8(\p si'16 lad') |
    si'8( re'') re''2 \grace dod''16 si'8( la'!16 sold') |
    la'8( dod'') mi''4~ mi''16\cresc sold'( la' si') dod''( re'' mi'' fad'') |
    sol''!8\f <sol'' la'>4 q q q8 |
    <la' fad''>-. fad''-.\p fad''( re'') dod'' dod'' si' si' |
    la'16 la''( sold'' la'') si''( la'' sold'' la'') la' mi''( re'' mi'') fad''( mi'' re'' mi'') |
    la'16 fad''([ mi'' fad'']) sol''( fad'' mi'' fad'') la' re''([ dod'' re'']) mi''( re'' dod'' re'') |
    dod''2:16 dod'':16 |
    si':16 si':16 |
    la'4\f <<
      { mi''2 mi''4 | mi''16 } \\
      { mi''2 mi''4 | mi''16 }
    >> mi''16([ re'' dod'']) si'( la' sold' la') sold'( la' si' dod'') re''( fad'' mi'' re'') |
    dod''( mi'') mi''4 mid''8 fad'' sold''( la'') red'-. |
    mi'2 si''\trill |
    la''4 r8 \tuplet 3/2 { re''16( mi'' fad'') } mi''8-. dod''-. re''-. si'-. |
    dod''4 r8 \tuplet 3/2 { si'16( dod'' re'') } dod''8-. la'-. si'-. sold'-. |
    la'4 <mi' dod'' la''>4 q r |
  }
  \tag #'violino2 {
    <>\f \ru#2 { dod'16( la) mi'-. mi'-. } \ru#2 { re'16( si) mi'-. mi'-. } |
    <>\p \ru#2 { dod'16( la) mi'-. mi'-. } \ru#2 { re'16( si) mi'-. mi'-. } |
    <>\f \ru#2 { dod'16( la) mi'-. mi'-. } \ru#2 { re'16( si) mi'-. mi'-. } |
    <>\p \ru#2 { dod'16( la) mi'-. mi'-. } \ru#2 { re'16( si) mi'-. mi'-. } |
    mi'8 mi' mi' mi' la' la' la' la' |
    dod''2:16 red'':16 |
    mi''4 <mi'' si' mi'>4\f q r |
    r lad(\p dod' mi') |
    re'( si sold sold') |
    la'8( dod'') dod'-. dod'-. mi' mi' mi' mi' |
    fad'-. fad'( re') si'-. la'-. la'( sold') sold'-. |
    la'4\mf sol'!( lad mi') |
    re'-. fad'(\p sold re') |
    dod'8 dod' dod' dod' dod'4 r |
    <la' mi''>8\f q4 q q q8 |
    re'' re''[-.\p re''( si')] la' la' sold' sold' |
    la'8 la'4 la' la' la'8~ |
    la' la'4 la' la' la'8 |
    la'2:16 la':16 |
    la':16 sold':16 |
    la'16(\f mi' re' dod') si( la sold la) sold( la si dod') re'( si mi' re') |
    dod'4 mi'2 mi'4 |
    mi'8 la'4 la si8( mi') fad'-. |
    mi'16 la' dod'' la' mi'' la' dod'' la' mi' sold' si' sold' re'' sold' si' sold' |
    la'4 r8 \tuplet 3/2 { si16( dod' re') } dod'8-. la-. si-. sold-. |
    la4 r8 \tuplet 3/2 { re'16( mi' fad') } mi'8-. dod'-. re'-. si-. |
    dod'4 <la mi' dod''>4 q r |
  }
>>
la'2:16\p la'4:16 sol'!:16 |
fad'2:16\f fad'4:16 <<
  \tag #'violino1 {
    fad'16\p mi''( dod'' lad') |
    si'8 fad'' r sol'' r fad'' r mi'' |
    r re''16(\f mi'' re''8) fad''16( sol'' fad''8) si''16( dod''' si''8) r |
    <fad' la>8\p q4 q q q8 |
    sol' re''16( mi'' re''8) sol''-. r re''16( mi'' re''8) si''-. |
    r8 mi''16( fad'' mi''8) si''-. r mi''16( fad'' mi''8) si''-. |
    la''8 la'\f la' la' <la' mi' dod'>4 la''8. la''16 |
    \grace sol''8 fad''2\p~ fad''8 la'' \grace sol''16 fad''8 \grace mi''16 re''8 |
    dod''4 dod'' r mi''8 mi'' |
    sol''4. mi''8 sol''( mi'') sol''( mi'') |
    re'' re'' re'' re'' re'' re'' re'' re''
    mi'' mi'' mi'' sol''16( mi'') re''8 re'' re'' dod'' |
    re''\f la'16( si' la'8) re''16( mi'' re''8) fad''\p fad'' fad'' |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sol'' sol'' sol'' sol'' la'' la'' la'' la'' |
    sol'' si' si' si' si' si' si' si' |
    si' si' si' si' mi' mi''4( re''8) |
    dod''4 <mi' dod'' la''>\f la r | \allowPageTurn
    R1 |
    re''8(\p fad'') r fad''16( re'') mi''8( sol'') r sol''16( mi'') |
    re''4 r r2 |
    re''8(\p fad'') r fad''16( re'') mi''8( sol'') r sol''16( mi'') |
    fad''8 fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    mi''8 mi'' mi'' mi'' mi'' mi'' mi'' re'' |
    dod''4 <la' mi' dod'>\f q r |
    la'8\p( la'') la''2 sol''16( fad'' mi'' red'') |
    mi''8( sol'') sol''2 \grace fad''16 mi''8( re''!16 dod'') |
    re''8( fad'') la''-. la''-. la'' la'' la'' lad'' |
    si'' si''4 sol''16( mi'') re''8 re'' dod'' la' |
    la'( la'') la''2 sol''16( fad'' mi'' red'') |
    mi''8( sol'') sol''2 \grace fad''16 mi''8( re''!16 dod'') |
    re''8( fad'') la''-. la''-. la''-. la''-. la''16( sol'' la'' si'') |
    <do''' re''>8\f q4 q q q8 |
    si''8\p si'' si'' si'' fad'' fad'' mi'' mi'' |
    re''16 la''([ sol'' fad'']) mi''( re'' dod'' re'') r re''( dod'' re'') mi''( re'' dod'' re'') |
    si'' re''([ dod'' re'']) mi''( re'' dod'' re'') re' re''([ dod'' re'']) mi''( re'' dod'' re'') |
    fad''2:16 fad'':16 |
    mi'':16 mi'':16 |
    re''16-.\f fad''( mi'' fad'') sol''( fad'' mi'' fad'') r sol''( fad'' sol'') la''( sol'' fad'' sol'') |
    la''( do''') do'''( si'') si''( la'') la''( sol'') sol''( fad'') fad''( mi'') mi'' mi'' mi'' mi'' |
    fad''4 r r2\fermata |
    re''4 <la' la''>2 q4 |
    q16 la''([ sol'' fad'']) mi''( re'' dod'' re'') dod''( re'' mi'' fad'') sol''( si'' la'' sol'') |
    fad''( la'') la''4( lad''8) si'' dod''' re''' sold |
    la2 mi''2\trill |
    re''4 r8 \tuplet 3/2 { sol''16( la'' si'') } la''8-. fad''-. sol''-. mi''-. |
    <fad'' la' re'>4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    re''4 <fad'' la' re'>4 q r |
  }
  \tag #'violino2 {
    mi'4:16\p |
    re'8 si' r si' r si' r lad' |
    r si'16(\f dod'' si'8) re''16( mi'' re''8) re''16( mi'' re''8) r |
    <do' re'>8\p q4 q q q8 |
    si8 si'16( dod'' si'8) re''-. r si'16( dod'' si'8) re''-. |
    r si' sol' mi' r si' si' re'' |
    dod'' <mi' dod'>\f q q <dod' mi' la'>4 r |
    <>\p \ru#4 { re'16( fad') la'-. la'-. } |
    \ru#4 { la16( sol') la'-. la'-. } |
    \ru#4 { la16( dod') mi'-. mi'-. } |
    la16( re') fad'-. fad'-. re'( fad') la'-. la'-. la'8 la' la' la' |
    si' si' si' si' fad' fad' fad' mi' |
    re'\f fad'16( sol' fad'8) fad'16( sol' fad'8) la'\p la' la' |
    la' la' la' la' <la' re'> q q q |
    re' re' re' re' <re' do''> q q q |
    si' re' re' re' re' re' re' re' |
    mi' mi' mi' mi' si' si' si' si' |
    la'4 <mi' dod''! la''> la r |
    <>\f \ru#2 { la'16( fad') la'-. la'-. } \ru#2 { sol'16( mi') la'-. la'-. } |
    <>\p \ru#2 { fad'( re') la'-. la'-. } \ru#2 { sol'16( mi') la'-. la'-. } |
    <>\f \ru#2 { fad'( re') la'-. la'-. } \ru#2 { sol'16( mi') la'-. la'-. } |
    <>\p \ru#2 { re'16( fad') la'-. la'-. } \ru#2 { mi'16( sol') la'-. la'-. } |
    la'8 re'' re'' re'' re'' re'' re'' re'' |
    re''8 re'' re'' re'' si' si' si' si' |
    la'4 <la' mi' dod'>\f q r |
    r do''(\p red' fad'8 la') |
    sol'4 si'( dod'! mi'8 sol') |
    fad'( la') fad'-. fad'-. re'! re''4 re'8~ |
    re' re''4 si'16( sol') fad'8 fad' mi' dod' |
    re'4 do''( red' fad'8 la') |
    sol'4 si'( dod'! mi'8 sol') |
    fad'( la') fad'-. fad'-. fad'-. fad'-. fad'16( mi' fad' sol') |
    la'8\f <la' fad''>4 q q q8 |
    re''8\p re'' re'' re'' re'' re'' dod'' dod'' |
    fad' fad' fad' fad' la' la' la' la' |
    re' si' si' si' sol' sol' sol' sol' |
    re'16 re'' re'' re'' re''4:16 re''2:16 |
    re'':16 dod'':16 |
    re''16-.\f re''( dod'' re'') mi''( re'' dod'' re'') r8 re'' re'' re'' |
    re''16 re''8 re'' re'' re''16~ re'' re''8 re'' re'' re''16 |
    re''4 r r2\fermata |
    re''16( la' sol' fad') mi'( re' dod' re') dod'( re' mi' fad') sol'( mi' la' sol') |
    fad'4 la'2 la'4 |
    la'8 re''4 re'' mi''8 fad'' si |
    la16 re' fad' re' la' fad' re' fad' la dod' mi' dod' sol' mi' dod' mi' |
    fad'4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    <re'' re'>4 r8 \tuplet 3/2 { sol'16( la' si') } la'8-. fad'-. sol'-. mi'-. |
    fad'4 <re' la' fad''>4 q r |
  }
>>
<<
  \tag #'violino1 {
    re''4 sol''8 |
    sol''( fad'') mi''-. |
    re''4 do'''8 |
    si'' re'''16[( si'') la''( fad'')] |
    sol''8( fad'' mi'') |
    re''( do''') si''-. |
    la''( sol'' fad'') |
    sol''( si'') la''-. |
    sol''( fad'' mi'') |
    re''( do''') si''-. |
    la''( sol'' fad'') |
    sol''-. sol'-. si'-. |
    red' si' la' |
    sol'16-. si'-. mi''-. si'-. sol''-. mi''-. |
    fad''4. |
    r16 si''-. re''([ fad'' mi'' dod''!]) |
    si'4.:16 |
    si':16 |
    sol''16 sol'' fad'' fad'' mi'' mi'' |
    re''4.:16 |
    la':16 |
    sol':16 |
    sol''16 sol'' fad'' fad'' lad' lad' |
  }
  \tag #'violino2 {
    r8 re'' si' |
    do''4 si'8 |
    la'4 fad'8 |
    sol' si' do'' |
    re''( do'' si') |
    la'4 sol'8 |
    mi'16([ do'']) si'8([ la']) |
    re'8( sol') fad'-. |
    re''( do'' si') |
    la'4 sol'8 |
    mi'16([ do'']) si'8([ la']) |
    sol'8-. sol'-. si'-. |
    red'8 fad'4 |
    si4. |
    dod'! |
    fad'4( lad'8) |
    si'16 fad' fad' fad' fad' fad' |
    sol'4.:16 |
    si'16 si' re'' re'' dod'' dod'' |
    si'4.:16 |
    fad':16 |
    mi':16 |
    mi''16 mi'' re'' re'' dod'' dod'' |
  }
>>
si'16\f si' re'' re'' fad' fad' |
sol' sol' si' si' mid' mid' |
fad'4 r8\fermata |
re''16 si' fad''( re'') mi''( la') la''( sol''!) <<
  \tag #'violino1 {
    fad''16( la'') la''4 si''16( do''') |
    si''8 si'~ si'16 dod''32([ re'' mi'' fad'' sol'' la''] si''16) la''( sol'' fad'') mi''( re'' dod'' re'') |
    dod''( si' la' si') la'8 la' la'4 r |
  }
  \tag #'violino2 {
    fad''8 fad''4 sol''16( la'') |
    sol''4 si'2 si4 |
    mi'16( re' dod' re') dod'8 dod' dod'4 r |
  }
>>
