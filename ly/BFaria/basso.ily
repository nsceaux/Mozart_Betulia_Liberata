\clef "bass" r4 |
re re re re |
mi mi mi mi |
mi mi la, la, |
re fad mi la |
re fad mi la |
re8 re re re fad fad fad fad |
sol4 sol sol sold |
la la, la, r |
r4 fad(\p si2) |
r4 mi( la2) |
re4 re re r |
re16(\f mi fad sol) la( si do' si) la( sol fad mi) re( mi fad re) |
sol4 r r8 fad( sol) sold-. |
la la la la la, la, la, la, |
re4 re' r la |
re re' r la |
re re re r |
re\p re re re |
mi mi mi mi |
la, la, la, la, |
re re fad fad |
sol sol la la, |
re\f re re r |
dod\p dod dod dod |
si, si, mi mi |
la, la, dod dod |
re re red red |
mi mi\f mi r |
r4 la\f sold mi |
r la\p sold mi |
r la\f sold mi |
r la\p sold mi |
la la la la |
fad8 fad fad fad fad fad fad fad |
mi4 mi\f mi r |
r dod\p fad2 |
r4 si, mi2 |
la,4 la, dod dod |
re re mi mi |
la\mf dod( fad2) |
r4 si,(\p mi2) |
la,8 la, la, la, la,4 r |
la,16(\f si, dod re) mi( fad sol! fad) sol( fad mi re) dod( si, la, dod) |
re8\p re re re mi mi mi mi |
la la la la dod dod dod dod |
re8 re re re fad fad fad fad |
mi mi mi mi mi mi mi mi |
mi mi mi mi mi mi mi mi |
la4 dod' si mi |
la dod' si mi |
la dod re dod8 red |
mi mi mi mi mi mi mi mi |
la,4 la r mi |
la, la r mi |
la, la, la, r |
la8\p la la la la la sol! sol |
fad\f fad fad fad fad\p fad fad fad |
sol re re mi fad fad fad fad |
si,\f si, si, si, si, si, si, si, |
re\p re re re re re re re |
sol sol sol sol sol sol sol sol |
sol sol sol sol sold sold sold sold |
la la,\f la, la, la,4 r |
re4\p re re re |
mi mi mi mi |
la, la, la, la, |
re re fad fad |
sol sol la la |
re\f re re r |
re\p re do do |
si, si, fad fad |
sol sol sol sol |
sol sol sol, sold, |
la, la\f la, r |
r4 re\f dod la, |
r re\p dod la, |
r re\f dod la, |
r re\p dod la, |
re4 re si si |
sol sol sol sold |
la la,\f la, r |
r fad(\p si2) |
r4 mi( la2) |
re4 re fad fad |
sol sol la la, |
r4 fad( si2) |
r4 mi( la2) |
re4 re re re |
re16\f( mi fad sol) la( si do' si) do'( si la sol) fad( mi re fad) |
sol8\p sol sol sol la la la la |
re re re re fad fad fad fad |
sol sol sol sol si si si si |
la la la la la la la la |
la, la, la, la, la, la, la, la, |
re\f re re re si si si si |
fad fad sol la si la sol sold |
la1\fermata |
re4 fad mi la |
re re mi la, |
re fad sol fad8 sold |
la la la la la, la, la, la, |
re4 re' r la |
re re' r la |
re re re r |
sol8 si sol |
la4 sol8 |
fad fad re |
sol sol la |
si la sol |
fad re sol |
do re do |
si, sol, re |
si la sol |
fad re sol |
do re re |
sol,-. sol-. si-. |
red4. |
mi |
lad, |
si,8 si, dod! |
re re re |
mi mi mi |
mi fad fad |
sol sol sol |
red red red |
mi mi mi |
mi fad fad |
si\f re' fad |
sol si mid |
fad4 r8\fermata |
si,4 dod re re |
sol sol sol sold |
la la, la, r4 |
