\clef "alto/treble" r4 |
R1*16 |
r2 r4 la'8 la' \grace sol'8 fad'2~ fad'8 la' fad' re' |
\grace re'8 dod'4 dod' r mi'8 mi' |
sol'4. mi'8 sol'[ mi'] sol'[ mi'] |
re'4 re' r la'8 fad' |
mi'4. sol'16[ mi'] \grace mi'8 re'4. dod'8 |
re'4 re' r fad'8 re' |
la'[ mi'] mi' mi' mi'4. la'8 |
la'[ sold'] sold'8 si' si'4. re'8 |
dod'[ mi'] mi'4 r la'8 mi' |
fad'4. sold'8 la'4. dod'8 |
si4 r r2 |
R1 |
la'2 mi' |
dod'4 r r mi' |
la'4.( si'16[ dod''] si'4) re' |
dod' dod' r mi'8 mi' |
la'4. la'8 dod''[ la' fad'] red' |
mi'4 mi' r2 |
r r4 dod'8 lad |
si[ re'] fad'4. fad'8 re' si |
dod'[ mi'] la'2 mi'8 mi' |
fad'[ la'] fad'[ re'] dod'4 si |
la r r dod'8 lad |
si8[ re'] re'4. re'8 re' sold' |
la'[ mi'] mi'4 r2 |
sol'!2. sol'4 |
fad'8[ la'] fad'[ re'] dod'4 si |
dod''2 la'4. la'8 |
fad'2 la' |
mi'1\melisma |
<<
  { \voiceOne si'2\startTrillSpan~ si'4..\stopTrillSpan\melismaEnd la'16 |
    la'4 \oneVoice }
  \new Voice { \voiceTwo si2~ si4.. la16 | la4 }
>> r4 r2 |
R1*6 |
la'2. sol'!4 |
fad'2 r4 r8 fad' |
si'4. sol'8 \grace sol'8 fad'4. lad8 |
si4 si r re'8 re' |
do'4. la'8 la'4. do'8 |
si4 si'2 sol'8 fad' |
mi'4. sol'16[ fad'] mi'8[ sold' si'] re' |
dod'4 dod' r la'8 la' |
\grace sol'8 fad'2~ fad'8 la' fad' re' |
dod'4 dod' r mi'8 mi' |
sol'4. mi'8 sol'[ mi'] sol'[ mi'] |
re'4 re' r la'8 fad' |
mi'4. sol'16[ mi'] \grace mi'8 re'4. dod'8 |
re' re' r4 r re'8 re' |
fad'[ re'] la'[ fad'] do''[ la'] fad'[ re'] |
sol'4 sol'8 si' \grace si'8 la'4. do'8 |
si4 si r sol'8 fad' |
mi'4. mi'8 si'4. re'8 |
dod'!4 r r2 |
R1 |
la'2. dod'4 |
re' r r r8 la' |
la'2. dod'4 |
re' re' r re'8 re' |
si4. si'8 si'4. re'8 |
dod'[ si] la4 r2 |
r r4 fad'8 red' |
mi'[ sol'] sol'4. sol'8 mi' dod' |
re'[ fad'] la'2 la'8 lad' |
si'4. sol'16[ mi'] re'4 dod' |
re' r r fad'8 red' |
mi'[ sol'] sol'4. sol'8 \grace fad'16 mi'8 dod' |
re'![ fad'] la'4 r2 |
re'2 do'' |
si'4. sol'16[ mi'] re'4 dod'! |
re'2 la'4. la'8 |
si'2 sol' |
fad'4\melisma la'2 fad'8[ re'] |
mi'2\startTrillSpan~ mi'4..\stopTrillSpan\melismaEnd re'16 |
re'4 r r2 |
r r8 re'16 re' re'8 re' |
re'2( \outsideSlur mi'4.\trill\fermata) re'8 |
re' r r4 r2 |
R1*6 |
re'4 sol'8 |
sol'[ fad'] mi' |
re'4 do''8 |
si' sol' r |
sol'[ la'] si' |
re'[ do''] si' |
la'[ sol'] fad' |
sol'[ si'] la' |
sol'[ la'] si' |
re'[ do''] si' |
la'[ sol'] fad' |
sol' sol' r |
r si' la' |
sol' sol' r |
r16 fad' dod''!8 mi' |
re' fad' r |
r si' fad' |
sol'4 r8 |
sol' fad' lad' |
si'4 r8 |
r8 si' fad' |
sol'4 r8 |
sol' fad' lad |
si4 r8 |
r r si' |
si' dod'4\trill\fermata |
si4 r r2 |
R1*2 |
