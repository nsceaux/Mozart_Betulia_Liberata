\clef "treble" r4 |
<<
  \tag #'oboe1 {
    la'1~ |
    la'~ |
    la'2 dod'' |
    re''4 la''2 sol''4 |
    fad'' la'' sol''2 |
    fad''4 la''2 lad''4 |
    si''2~ si''4 re'' |
    dod'' la' la'
  }
  \tag #'oboe2 {
    fad'1 |
    sol'~ |
    sol'2 mi' |
    fad'4 re'' dod'' mi'' |
    re''2. dod''4 |
    re'' re''2 re''4 |
    re'' si' si' si' |
    la' la' la'
  }
>> r4 |
R1*3 |
<>\f <<
  \tag #'oboe1 {
    fad''1 |
    sol''4 r r r8 mi'' |
    fad''2 mi''\trill |
    re''4 r4 r2 |
    r4 r8 \tuplet 3/2 { sol''16( la'' si'') } la''8-. fad''-. sol''-. mi''-. |
    re''4 fad'' fad''
  }
  \tag #'oboe2 {
    la'1 |
    si'4 r r r8 re'' |
    re''2 dod''\trill |
    re''4 r4 r2 |
    r4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    re''4 la' la'
  }
>> r4 |
R1*5 |
<>\f <<
  \tag #'oboe1 { fad''2 }
  \tag #'oboe2 { re'' }
>> r |
R1*4 |
<>\f <<
  \tag #'oboe1 { sold''2 sold''4 }
  \tag #'oboe2 { si'2 si'4 }
>> r4 |
R1 |
<<
  \tag #'oboe1 {
    la''2\p mi'' |
    dod''4 r r2 |
    la''2\p mi'' |
    dod''4 r r2 |
    R1 |
    r4 sold''\f mi'' r |
  }
  \tag #'oboe2 {
    dod''2\p si' |
    la'4 r r2 |
    dod''\p si' |
    la'4 r r2 |
    R1 |
    r4 si'\f sold' r |
  }
>>
R1*7 |
<>\f <<
  \tag #'oboe1 { sol''!1 | }
  \tag #'oboe2 { mi'' | }
>>
R1*3 |
r2 <>\p <<
  \tag #'oboe1 {
    la''2~ |
    la'' sold'' |
    la''\f sold'' |
    la''4 mi''2 re''4 |
    dod''8 mi''4 mid''8( fad'' sold'') la''4 |
    la''2 sold'' |
    la''4 r r sold'' |
    la'' r8 \tuplet 3/2 { re''16( mi'' fad'') } mi''8-. dod''-. re''-. si'-. |
    la'4 la'' la''
  }
  \tag #'oboe2 {
    dod''2 |
    si'1 |
    dod''4\f mi'' re'' si' |
    dod'' la' sold'2 |
    la'~ la'8 si'( dod'' si') |
    dod''2 si' |
    la'4 r r si' |
    dod'' r8 \tuplet 3/2 { si'16( dod'' re'') } dod''8-. la'-. si'-. sold'-. |
    la'4 dod'' dod''
  }
>> r4 |
R1 |
<>\f <<
  \tag #'oboe1 { fad''2~ fad''4 }
  \tag #'oboe2 { fad'2~ fad'4 }
>> r4 |
R1 |
<>\f <<
  \tag #'oboe1 { si''1 }
  \tag #'oboe2 { re'' }
>>
R1*3 |
r8 <>\f <<
  \tag #'oboe1 { la''8 la'' la'' la''4 }
  \tag #'oboe2 { dod''8 dod'' dod'' dod''4 }
>> r4 |
R1*5 |
<>\f <<
  \tag #'oboe1 { fad''2 }
  \tag #'oboe2 { re'' }
>> r2 |
R1*4 | \allowPageTurn
<>\f <<
  \tag #'oboe1 { mi''2 la''4 }
  \tag #'oboe2 { dod''2 dod''4 }
>> r4 |
R1 |
<<
  \tag #'oboe1 {
    fad''2(\p sol'') |
    fad''4 r r2 |
    fad''2(\p sol'') |
    fad''4 r r2 |
    
  }
  \tag #'oboe2 {
    re''2(\p mi'') |
    re''4 r r2 |
    re''2(\p mi'') |
    re''4 r r2 |
  }
>>
R1 |
r4 <>\f <<
  \tag #'oboe1 { mi''4 mi'' }
  \tag #'oboe2 { dod'' dod'' }
>> r4 |
R1*7 |
<>\f <<
  \tag #'oboe1 { la''1 | }
  \tag #'oboe2 { fad'' | }
>>
R1*3 |
r2 <>\p <<
  \tag #'oboe1 {
    fad''2 |
    mi''1 |
    fad''2\f sol'' |
    la''8 do'''16( si'') si''( la'') la''( sol'') sol''( fad'') fad''( mi'') mi''4 |
    fad''
  }
  \tag #'oboe2 {
    re''2~ |
    re'' dod'' |
    re''1\f |
    re''4 re''2 re''4 |
    re''4
  }
>> r4 r2\fermata |
<<
  \tag #'oboe1 {
    fad''4 la''2 sol''4 |
    fad''2 dod'' |
    re''8 la''4 lad''8 si'' dod''' re''' mi'' |
    fad''2 mi''\trill |
    re''4 r r2 |
    fad''4 r8 \tuplet 3/2 { sol''16( la'' si'') } la''8-. fad''-. sol''-. mi''-. |
    re''4 fad'' fad''
  }
  \tag #'oboe2 {
    re''2 dod'' |
    re''4 la' sol'2 |
    fad'4 re''4. mi''8 re''4 |
    re''2 dod''\trill |
    re''4 r r2 |
    re''4 r8 \tuplet 3/2 { mi''16( fad'' sol'') } fad''8-. re''-. mi''-. dod''-. |
    re''4 la' la'
  }
>> r4 |
R4.*3 |
r8 <>\p <<
  \tag #'oboe1 { sol''( fad'') | sol''-. }
  \tag #'oboe2 { si'( do'') | re'' }
>> r8 r |
R4.*6 |
r8 <<
  \tag #'oboe1 { sol''-. si''-. | si' }
  \tag #'oboe2 { si'-. sol'-. | fad' }
>> r r |
R4.*2 |
r8 <<
  \tag #'oboe1 { fad''( lad') | si' }
  \tag #'oboe2 { si'( mi'') | re'' }
>> r8 r |
R4.*2 |
<>\p <<
  \tag #'oboe1 { si''4. | la'' | sol'' | }
  \tag #'oboe2 { si'4.~ | si' | si' | }
>>
R4. |
<>4\f <<
  \tag #'oboe1 { si''4.~ | si''~ | si''4 }
  \tag #'oboe2 { re''4.~ | re''~ | re''4 }
>> r8\fermata |
<<
  \tag #'oboe1 {
    si''4 la''8 sol''! fad''4 la'' |
    si'' mi''2 re''4 |
    dod'' la'' la''
  }
  \tag #'oboe2 {
    re''4 mi'' re'' fad'' |
    sol'' si'2 si'4 |
    la'4 dod'' dod''
  }
>> r4 |
