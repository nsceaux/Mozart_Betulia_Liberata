\tag #'all \key re \major
\tempo "Adagio." \midiTempo#80
\time 4/4 \partial 4 s4 s1*74 \bar "|." \segnoMark
s1*30 \bar "|." \fermataMark
\time 3/8 s4.*26 \bar "||"
\time 4/4 s1*3 \bar "|." \dalSegnoMark