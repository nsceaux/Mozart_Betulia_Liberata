\clef "bass" si,1~ |
si,~ |
si, |
do2 fa~ |
fa mi~ |
mi mi |
fa1 |
mi |
fa~ |
fa |
sib,1 |
si,!2 do~ |
do sib,~ |
sib, la, |
dod1~ |
dod~ |
dod |
re2 la,~ |
la,1~ |
la,2 sib,~ |
sib, mib~ |
mib r4 fa |
mi!1~ |
mi2 fa |
dod1 |
re1~ |
re |
do |
mi |
fa2 do~ |
do sib,~ |
sib, re~ |
re si,!~ |
si,1 |
do |
sib,2 r4 la, |
fad1~ |
fad2 sol |
la,1 |
sib, |
mib2 dod~ |
dod re |
si,!1 |
do!~ |
do2 r4 re |
sol,1 |
