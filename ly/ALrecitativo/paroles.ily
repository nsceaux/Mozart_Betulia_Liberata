Oh sag -- gia, oh san -- ta, oh ec -- cel -- sa don -- na! Id -- dio
a -- ni -- ma i lab -- bri tuo -- i.

Da ta -- li ac -- cu -- se
chi si può di -- scol -- par?

Deh tu, che se -- i
ca -- ra al Si -- gnor, per noi per -- do -- no im -- plo -- ra;
ne gui -- da, ne con -- si -- glia.

In Dio spe -- ra -- te
sof -- fren -- do i vo -- stri ma -- li. E -- gli in tal gui -- sa
cor -- reg -- ge, e non op -- pri -- me; ei de’ più ca -- ri
co -- sì pro -- va la fe -- de, e A -- bra -- mo, e I -- sac -- co
e Gia -- cob -- be, e Mo -- sè di -- let -- ti a lu -- i
di -- ven -- ne -- ro co -- sì. Ma que -- i che o -- sa -- ro
ol -- trag -- giar mor -- mo -- ran -- do
la sua giu -- sti -- zia, o del -- le ser -- pi il mor -- so,
o il fuo -- co e -- ster -- mi -- nò. Se in giu -- sta lan -- ce,
pe -- sia -- mo il fal -- li no -- stri, as -- sai di lo -- ro
è mi -- no -- re il ca -- sti -- go: on -- de dob -- bia -- mo
gra -- zie a Dio, non que -- re -- le. Ei ne con -- so -- li
se -- con -- do il vo -- ler su -- o. Gran pro -- ve io spe -- ro
del -- la pie -- tà di lu -- i. Voi, che di -- ce -- ste,
che muo -- ve i lab -- bri mie -- i, cre -- de -- te an -- co -- ra
ch’ei de -- sti i miei pen -- sie -- ri. Un gran di -- se -- gno
mi bol -- le in men -- te, e mi tra -- spor -- ta. A -- mi -- ci,
non cu -- ra -- te sa -- per -- lo. Al sol ca -- den -- te
del -- la cit -- tà m’at -- ten -- di,
O -- zi -- a, pres -- so al -- le por -- te. Al -- la grand’ o -- pra
a pre -- pa -- rar -- mi io va -- do. Or, fin ch’io tor -- ni,
voi con prie -- ghi sin -- ce -- ri
se -- con -- da -- te di -- vo -- ti i miei pen -- sie -- ri.
