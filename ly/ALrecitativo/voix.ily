\clef "tenor/G_8" <>^\markup\character Ozia
r4 r8 sol si si r si |
re' re' r16 re' re' fa' re'8 re' r si |
re'4 re' re'8 re'16 re' re'8 mi' |
do' do'
\ffclef "soprano/treble" <>^\markup\character Cabri
r16 do'' re'' mi'' mi''8 la' r la'16 do'' |
la'4 la'8 sold' si'4 r |
\ffclef "tenor/G_8" <>^\markup\character Ozia
si4 si8 mi' dod' dod' mi' mi'16 fa' |
re'4 r8 la la la la re' |
do'! do' r sol' sol' sib r sib16 la |
fa8 fa r4
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 fa' fa' sol' |
la' la' r do'' do'' mib' mib' fa' |
re' re' r4 fa'8 fa'16 fa' fa'8 sol'16 lab' |
sol'8 sol' r16 re' fa' mib' do'8 do' r4 |
sol'4 sol'8 sol' mi'! mi' r mi'16 fa' |
sol'4 sol'8 la' fa' fa' r la' |
la' mi' r mi' dod' dod' r dod'16 re' |
mi'8 mi' r mi'16 mi' sol'8 sol' sol' sib' |
sol'8 sol' r mi' sol' sol' sol' fa' |
re'4 r8 fa' fa' fa' r fa' |
la' la' sib' do'' mib'4 mib'8 mib' |
do' do' r16 mib'16 mib' fa' re'8 re' r4 |
fa'8 fa'16 fa' sol'8 lab' sol' sol' r mib' |
la'! la' la' sib' fa'4 r |
r8 sol' sol' sol' mi'! mi' r mi' |
sol' sol' sol' la' fa' fa' r16 fa' sol' la' |
la'8 mi' r mi'16 fa' sol'4 sol'8 fa' |
re'8 re' r4 la' la'8 si'! |
sold' sold' sold' la' si'4 si'8 do'' |
la' la' r4 la' la'8 do'' |
do'' sol'! r sol' sib' sib' sib' la' |
fa' fa' r16 la' sol' la' fad'8 fad' r4 |
la'8 la'16 la' la'8 sib' sol' sol' r4 |
sol'4 sol'8 sib' sib' fa'! r fa' |
lab' lab' lab' fa' sol' sol' r16 sol' sol' lab' |
fa'8 fa' r re' fa' fa' fa' mib' |
do' do' r16 sol' sol' la'! fad'8 fad'16 fad' la'8 sib' |
sol'8 sol' sib' sol'16 fa'! re'8 re' r4 |
r4 r8 la' la' re' r la'16 sib' |
do''4 do''8 sib' sol' sol' r16 re' re' sol' |
fa'!8 fa' r4 do''8 mib'16 mib' mib'8 fa' |
re'8 re'16 fa' lab'8 lab' lab'4 lab'8 sol' |
mib' mib' r16 sol' sol' sib' la'!8 la' r16 mi' mi' fa' |
sol'4 sol'8 fa' re' re' r16 fa' sol' lab' |
sol'8 sol' r4 re'8 re' fa' fa'16 sol' |
mib'8 mib' r sol'16 sol' do''4 lab'8 sol' |
fad'8 fad' r16 fad' fad' sol' sol'8 re' r4 |
R1 |
