\tag #'ozia {
  Pie -- tà se i -- ra -- to se -- i,
  pie -- tà se i -- ra -- to se -- i,
  pie -- tà, Si -- gnor, di no -- i;
  ab -- bian ca -- sti -- go i re -- i,
  ab -- bian ca -- sti -- go i re -- i,
  ma l’ab -- bi -- a -- no da te,
}
\tag #'(vsoprano valto vtenor vbasso) {
  Ab -- bian ca -- sti -- go i re -- i
  ma l’ab -- bia -- no da te.
}
\tag #'ozia {
  Se op -- pres -- so chi t’a -- do -- ra
  sof -- fri da chi __ t’i -- gno -- ra,
  gli em -- pi di -- ran -- no po -- i:
  que -- sto lor Dio dov’ è?
}
\tag #'(vsoprano valto vtenor vbasso) {
  Gli em -- pi di -- ran -- no po -- i:
  que -- sto lor Dio dov’ è? dov’ è? dov’ è?
}
\tag #'ozia {
  Pie -- tà, se i -- ra -- to se -- i,
  pie -- tà, se i -- ra -- to se -- i,
  pie -- tà, Si -- gnor, di no -- i;
  ab -- bian ca -- sti -- go i re -- i,
  ab -- bian ca -- sti -- go i re -- i,
  ma l’ab -- bi -- a -- no da te,
}
\tag #'(vsoprano valto vtenor vbasso) {
  Ab -- bian ca -- sti -- go i re -- i
  ab -- bian ca -- sti -- go i re -- i
  ma l’ab -- bia -- no da te,
  ma l’ab -- bia -- no da te.
}
