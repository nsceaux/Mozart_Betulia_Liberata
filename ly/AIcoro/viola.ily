\clef "alto" r4 |
<sol mib'>1 |
<lab fa'> |
<sol sol'> |
<<
  { fa'4 mib'2 mib'4 |
    re' do' sol'2 |
    mib'4 lab' sol'8( mib') fa'( re') |
    mib'1 |
    fa' |
    mib' |
    fa'2 sol' |
    sol'1 |
    sol'4 } \\
  { do'2 do' |
    sol sol |
    sol4 fa' mib'8( do') re'( si) |
    sol2\p do' |
    lab1 |
    sol |
    lab2 sol |
    sol1 |
    sol4 }
>> r4 sol r |
<<
  { re'1 |
    mib'2 mib'~ |
    mib'1~ |
    mib'4 re' re'2 | } \\
  { si1 |
    si2 do'~ |
    do'1~ |
    do'2 fad | }
>>
sol4 mib re2 |
<<
  { re8 sol'( fad' sol') la'( sol' fad' sol') | } \\
  { s8 sib(\f la sib) do'( sib la sib) }
>>
sol'4\f sol' sol' sol' |
lab'!8 lab' lab' lab' mib'2:16 |
re'4 sol'8 mib' re' re' re' re' |
re'1\p |
mib'2 la |
sib4 la sol re' |
mib'2 la |
sol4 <<
  { mib'8( do') re'( sib) do'( la!) |
    sib1 |
    do' |
    reb'4 fa'2 mi'4 |
    fa'2 do' |
    do'~ do'4. re'8 |
    mib'1 |
    mib' |
    re'8 si( re' sol') si'4 } \\
  { do'8(\f la) sib( sol) la( fad) |
    sol1\p |
    lab!2 la |
    sib4 do' reb'!2~ |
    reb'4 do' la2 |
    lab!2~ lab4. si8 |
    do'1 |
    do' |
    sol8 sol(\f si! re') sol'4 }
>> r4 |
fa'4 fa' mib' mib' |
re'( do') do'' sol' |
lab' lab' lab' lab' |
sol' sol' sol' sol' |
sol' sol' sol' sol' |
sol' r r2\fermata |
<<
  { sol'8( mib') sol'( mib') fa'( re') fa'( re') |
    mib'1 |
    fa' |
    mib'2 do' |
    fa'2 sol' |
    sol'1 | } \\
  { mib'8(\p do') mib'( do') re'( si) re'( si) |
    do'1 |
    lab |
    sol |
    lab2 sol |
    sol1 | }
>>
sol'4 r sol r |
<<
  { mi'1 |
    fa'2 mib'! |
    la'1 |
    sol'2 fa' |
    mib'4 lab'! sol'2 |
    sol'8 mib'( re' mib') fa'( mib' re' mib') | } \\
  { reb'1 |
    do'~ |
    do' |
    si4 do' re'2 |
    do'2. re'4 |
    mib'8 do'(\f si do') re'( do' si do') | }
>>
sol'8\f sol' sol' sol' sol' sol' sol' sol' |
lab' lab' lab' lab' lab' lab' lab' lab' |
la' la' la' la' la' la' la' la' |
si' si si si si si si si |
sol' sol' sol' sol' sol' sol' sol' sol' |
mib' mib' mib' mib' fa' fa' fa' fa' |
sol' sol' sol' sol' sol sol sol sol |
sol2\p sol |
lab lab |
la2. sol4~ |
sol lab!2( sol4) |
sol sol sol sol |
sol2 r |
