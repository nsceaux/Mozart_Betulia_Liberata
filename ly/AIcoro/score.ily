\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = "Corni in Es."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ozia
      shortInstrumentName = \markup\character Oz.
    } \withLyrics <<
      \global \keepWithTag #'ozia \includeNotes "voix"
    >> \keepWithTag #'ozia \includeLyrics "paroles"
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro
      \haraKiriFirst
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenore \includeNotes "voix"
      >> \keepWithTag #'vtenore \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasso \includeNotes "voix"
      >> \keepWithTag #'vbasso \includeLyrics "paroles"
    >>
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s4 s1*7\break s1*8\pageBreak
        s1*7\break s1*7\pageBreak
        s1*7\break s1*8\pageBreak
        \grace s8 s1*7\break s1*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
