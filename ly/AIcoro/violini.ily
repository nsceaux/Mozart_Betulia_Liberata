\clef "treble" <<
  \tag #'violino1 {
    sol'4 |
    do''4. re''8 \grace fa''16 mib''4 re''8( do'') |
    do''4( si') r si' |
    do''4. re''8 \grace fa''16 mib''4 re''8( do'') |
    \grace sib'!16 lab'4 sol'2( fad'4) |
    fa'!4 mib' \appoggiatura mib'8 re'2\trill |
    do'4 r r2 |
    r8 <>^"pizz." mib' sol' si' r do'' re'' mib'' |
    r fa'' re'' si' r re'' fa'' si' |
    r do'' do'' re'' r mib'' re'' do'' |
    r lab' lab' lab' r sol' sol' do'' |
    r <si' re'> r <mib' do''> r <re'' sol'> r <sol' mib''> |
    r mib'' re'' sol'' sol4 r |
    r8 re'' si' re'' r re'' si' re'' |
    r mib'' mib'' mib'' r mib'' do'' mib'' |
    r mib'' do''' mib'' r mib'' do'' mib'' |
    r8 la'' fad'' re'' r do'' la' do'' |
    r sib' do'' mib'' r sib' la' la' |
    sol'4 r8 <>^"arco" sol'-. la'( sol' fad' sol') |
    sol'\f mib'' mib'' mib'' mib'' mib'' mib'' re'' |
    do'' mib'' mib'' mib'' fad''2:16 |
    sol''8[ re'']
  }
  \tag #'violino2 {
    r4 |
    r8 sol'4 do' mib' sol'8 |
    fa' fa'4 re' si lab8 |
    sol sol'4 sol sol' mib'8 |
    do' do'4 mib' do' mib'8 |
    re' sol4 do' do'8 si si |
    do'4 r r2 |
    r8 <>^"pizz." do' mib' fa' r mib' fa' sol' |
    r lab' fa' re' r fa' lab' fa' |
    r sol' sol' sol' r sol' fa' mib' |
    r do' do' do' r do' mib' sol' |
    r <fa' sol> r <mib' sol> r <sol sol'> r <sol' do''> |
    r do'' si' re'' sol4 r |
    r8 si' sol' si' r si' sol' si' |
    r si' si' si' r do'' mib' do'' |
    r do'' mib'' do'' r do'' mib' do'' |
    r do'' la' fad' r la' re' la' |
    r re' sol' sol r sol' fad' fad' |
    sol'4 r8 <>^"arco" sib'-. do''( sib' la' sib') |
    sib'\f sib' sib' sib' sib' sib' sib' si' |
    do'' do'' do'' do'' do''2:16 |
    re''8[ sol'']
  }
>> \grace fa''16 mib''8 re''16 do'' sib'8 sol'' la' fad'' |
<<
  \tag #'violino1 {
    sol''4 r r sol''\p~ |
    sol'' la' r re''~ |
    re'' sol' r sol''~ |
    sol'' la' r re''8( fad') |
    sol'4 r r2 |
    r8 <>^"pizz." sib' mib'' sol'' r sib' mib'' reb'' |
    r do'' do'' do'' r do'' fa'' mib'' |
    r reb'' do'' fa'' r fa'' mi'' sib'' |
    r8 sib'' la'' la' r fa'' do'' la' |
    r do'' lab'! do'' r lab' do'' re'' |
    r mib'' mib'' do'' r mib'' do''' mib'' |
    r do'' fad'' do'' r fad'' do'' fad'' |
    sol''4 r r8 <>^"arco" si'\f( re'' fa''!) |
    lab''8 lab'' lab'' lab'' sol'' sol'' sol'' sol'' |
    fa''4( mib'') mib''8 sol''4 si'8 |
    do'' do'' do'' do'' fad''2:16 |
    sol''8 sol'' re'' fa''! mib'' sol'' mib'' do'' |
    si' sol'' re'' fa'' mib'' sol'' mib'' do'' |
    si'4 r r2\fermata |
    R1 |
    r8 <>^"pizz." sol' do'' re'' r mib'' re'' do'' |
    r do'' si' re'' r fa'' re'' si' |
    r do'' do'' re'' r mib'' re'' do'' |
    r lab' lab' lab' r sol' mib'' do'' |
    r <si' re'> r <mib' do''> r <re'' sol'> r <sol' mib''> |
    r mib'' re'' sol'' sol4 r |
    r8 mi'' sol'' sib'' r sib'' sol'' mi'' |
    r fa'' fa'' fa'' r mib''! mib'' mib'' |
    r mib'' re'' mib'' r mib'' re'' do'' |
    r si' do'' mib'' r fa'' lab'' fa'' |
    r sol'' fa'' re'' r do'' re'' si' |
    do''4 r8 <>^"arco" mib'-. fa'( mib' re' mib') |
    reb''8\f reb'' reb'' reb'' mi'' mi'' mi'' mi'' |
    fa'' fa'' fa'' fa'' lab''( fa'') do'''( lab'') |
    fad'' fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    sol'' sol'' sol'' sol'' fa''! fa'' fa'' fa'' |
    mib'' mib'' mib'' mib'' re'' re'' re'' re'' |
    do''8 do'' do'' do'' lab'' lab'' lab'' lab'' |
    sol''( do'') mib''( sol'') sol''( fa'') mib''( re'') |
    do''4 r r do'''4\p~ |
    do''' re'' r sol''~ |
    sol'' fad''2 fa''4 |
    mib''2 re''\trill |
    do''4 <mib' sol> q q |
    q2 r |
  }
  \tag #'violino2 {
    sol''8 sol'( sib' sol') fad'(\p sol' fad' sol') |
    r mib'( do'' la') fad'( la' re' fad') |
    r sol'( do'' mib'') re''( sib' sol' re') |
    r mib'( do'' la') fad'( la' re' do') |
    sib4 r r2 |
    r8 <>^"pizz." sol' sib' mib'' r mib' sib' sib |
    r mib' mib' mi' r fa' do'' fa' |
    r fa' fa'' fa' r sib' sib mi' |
    r reb'' do'' fa'' r do'' la' fa' |
    r lab'! do' lab' r fa' lab' si |
    r do' do'' la' r do'' mib'' do'' |
    r mib'' do'' mib' r do'' mib' do'' |
    si'4 r r8 <>^"arco" sol'(\f si' re'') |
    si' si' si' si' do'' do'' do'' do'' |
    si'4( do'') do''8 mib'' re'' re' |
    do' mib'' mib'' mib'' do''2:16 |
    si'4 si do'8 mib' sol' mib' |
    re'4 si do'8 mib' sol' mib' |
    re'4 r r2\fermata |
    R1 |
    r8 <>^"pizz." mib' mib' fa' r sol' fa' mib' |
    r fa' re' fa' r lab' fa' lab' |
    r sol' sol' si' r do'' sol' mib' |
    r do' reb' si r do' do'' sol' |
    r <fa' sol> r <sol mib'> r <fa' sol> r <mib' do''> |
    r do'' si' re'' sol4 r |
    r8 sib'! reb'' sib' r reb'' sib' reb'' |
    r do'' do'' do'' r do'' do'' do'' |
    r do'' si' do'' r do'' la' la' |
    r sol' sol' sol r re'' fa'' re'' |
    r mib'' do'' fa' r mib' fa' re' |
    mib'4 r8 <>^"arco" do'8-. re'( do' si do') |
    mi'\f mi' mi' mi' reb'' reb'' reb'' reb'' |
    do'' do'' do'' do'' fa''( do'') lab''( fa'') |
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    re'' re'' do'' do'' do'' do'' si' si' |
    do'' do'' do'' do'' do'' do'' do'' do'' |
    mib''( sol') do''( mib'') mib''( re'') do''( si') |
    do'' sol'(\p mib' re') do'( mib' sol' mib') |
    r re'( lab' fa') re'( si fa' re') |
    r do'( mib' re') do'( mib' re' sol) |
    r sol'( do'' do') lab'-. do'4( si8) |
    do'4 do' do' do' |
    do'2 r |
  }
>>
