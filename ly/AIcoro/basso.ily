\clef "bass" r4 |
do do do do |
re re re re |
mib mib mib mib |
fa sol lab la |
si do' sol sol, |
do r r sol |
do\p do do do |
re re re re |
mib mib mib mib |
fa fa mib do |
re do si, do |
sol sol, r2 |
sol4 sol sol sol |
sol sol lab lab |
lab lab sol sol |
fad fad fad fad |
sol mib re re |
sol r r2 |
sol4\f sol sol sol |
lab! lab la la |
sib do' re' re |
sol sol sib,\p sib, |
do do re re |
mib mib sib, sib, |
do do re re |
sol, r r re\f |
sol\p sol sol sol |
lab lab la la |
sib lab! solb solb |
fa fa fa fa |
fa fa fa fa |
fad fad sol sol |
lab lab lab lab |
sol r r2 |
fa4\f fa mib mib |
re( do) do' sol |
lab lab lab lab |
sol sol sol sol |
sol sol sol sol |
sol r r2\fermata |
do'2\p sol |
do4 do do do |
re re re re |
mib mib mib mib |
fa fa mib mib |
re do si, do |
sol sol, r2 |
sol4 sol sol sol |
lab lab sol sol fad fad fad fad |
fa! mib si, si, |
do lab! sol sol, |
do r r2 |
sib4\f sib sib sib |
lab lab lab lab |
do' do' do' do' |
si si si si |
do' do sol sol, |
lab,2 fa8 fa fa fa |
sol sol sol sol sol, sol, sol, sol, |
do4 do\p mib mib |
fa fa sol sol |
la la la si |
do' lab! fa sol |
do do do do |
do2 r |
