\clef "treble" r4 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''1 |
    fa'' |
    mib''2. re''8 do'' |
    lab''4 sol''2 fad''4 |
    fa''! mib'' re''2\trill |
    do''4 lab'' sol''8( mib'') fa''( re'') |
    do''4 }
  { do''1 |
    lab' |
    sol' |
    do'' |
    re''4 do''2 si'4 |
    do'' fa'' mib''8( do'') re''( si') |
    do''4 }
>> r4 r2 |
R1*4 |
r2 r8 <>\p <<
  \tag #'(oboe1 oboi) { re''8(-. re''-. re''-.) | re''4 }
  \tag #'(oboe2 oboi) { si'8(-. si'-. si'-.) | si'4 }
>> r4 r2 |
R1*4 |
r8 <>\f <<
  \tag #'(oboe1 oboi) {
    sol''8( fad'' sol'') la''( sol'' fad'' sol'') |
    mib''2~ mib''4. re''8 |
  }
  \tag #'(oboe2 oboi) {
    sib'8( la' sib') do''( sib' la' sib') |
    sib'2~ sib'4. si'8 |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2 fad'' | }
  { do''2 mib'' | }
>>
<<
  \tag #'(oboe1 oboi) { sol''4 la'' sol'' fad'' | }
  \tag #'(oboe2 oboi) { re''4 do'' sib' la' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1\p~ |
    sol''2 fad'' |
    sol''1~ |
    sol''2 fad'' |
    sol''4 }
  { sib'2 re''\p |
    mib'' do'' |
    sib'4 do'' re''2 |
    mib'' la' |
    sib'4 }
>> <>\f <<
  \tag #'(oboe1 oboi) {
    mib''8( do'') re''( sib') do''( la'!) |
    sib'4
  }
  \tag #'(oboe2 oboi) {
    do''8( la') sib'( sol') la'( fad') |
    sol'4
  }
>> r4 r2 |
R1*6 |
r8 <>\f <<
  \tag #'(oboe1 oboi) { si'8( re'' sol'') si''4 }
  \tag #'(oboe2 oboi) { sol'8( si' re'') sol''4 }
>> r4 |
<<
  \tag #'(oboe1 oboi) {
    lab''2 sol'' |
    fa''4 mib''2 re''4 |
  }
  \tag #'(oboe2 oboi) {
    si'2 do'' |
    si'4 do''2 si'4 |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''2 }
  { do'' }
>> <<
  \tag #'(oboe1 oboi) {
    fad''2 |
    sol'' mib'' |
    re'' mib'' |
    re''4
  }
  \tag #'(oboe2 oboi) {
    do''2 |
    si' do'' |
    si' do'' |
    si'4
  }
>> r4 r2\fermata |
<>\p <<
  \tag #'(oboe1 oboi) { sol''2 fa'' | mib''4 }
  \tag #'(oboe2 oboi) { mib''2 re'' | do''4 }
>> r4 r2 |
R1*4 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 sol'' fa'' | mi'' }
  { si'2.( | sib'!4) }
>> r4 r2 |
R1*4 |
r8 <>\f <<
  \tag #'(oboe1 oboi) {
    mib''8( re'' mib'') fa''( mib'' re'' mib'') |
    mi''1 |
    fa'' |
    fad'' |
  }
  \tag #'(oboe2 oboi) {
    do''8( si' do'') re''( do'' si' do'') |
    reb''1 |
    do'' |
    mib''! |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 fa''! |
    mib'' re'' |
    do'' }
  { re''1~ |
    re''4 do''2 si'4 |
    do''2 }
>> <<
  \tag #'(oboe1 oboi) {
    lab''2 |
    sol'' sol''8( fa'') mib''( re'') |
    mib''2 sol'' |
    lab'' fa'' |
    mib''2. re''4~ |
    re'' do''2
  }
  \tag #'(oboe2 oboi) {
    do''2 |
    mib'' mib''8( re'') do''( si') |
    do''2 \tag #'oboi \once\tieDown do''~ |
    do'' si' |
    do''2. sol'4~ |
    sol' lab'2
  }
  { s2 | s1 | s2\p }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'4 | }
  { sol'8( fa') | }
>>
<<
  \tag #'(oboe1 oboi) { do''1~ | do''2 }
  \tag #'(oboe2 oboi) { mib'1~ | mib'2 }
>> r2 |
