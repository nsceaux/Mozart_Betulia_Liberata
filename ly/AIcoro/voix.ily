<<
  \tag #'ozia {
    \clef "tenor/G_8" r4 |
    R1*5 |
    r2 r4 sol |
    do'4. re'8 \grace fa'8 mib'4 re'8[ do'] |
    do'4 si r si |
    do'4. re'8 \grace fa'8 mib'4 re'8[ do'] |
    \grace sib!8 lab2 sol4 r8 do' |
    si4 do' re' mib' |
    mib' re' r2 |
    re'4 re'8 re' re'4 re' |
    mib'2 do'4 r |
    do'4 do'8 do' do'4. mib'8 |
    mib'4 re' do'2 |
    \grace do'16 sib8[ la16 sib] \grace re'16 do'8 sib16[ la] sol4 la\trill |
    sol4 r r2 |
    R1*7 |
    r2 r4 re' |
    mib'4. mib'8 mib'4. reb'8 |
    do'4 do' r2 |
    reb'4 do'8 do' sib[ do'16 reb' do'8] sib |
    sib4 la r2 |
    do'4 do'8 do' do'4. re'8 |
    mib'4 mib' r2 |
    do'4 do'8 do' do'4. si8 |
    re'4 r r2 |
    R1*6 |
    r2 r4 sol |
    do'4. re'8 \grace fa'8 mib'4 re'8[ do'] |
    do'4 si r si |
    do'4. re'8 \grace fa'8 mib'4 re'8[ do'] |
    \grace sib8 lab2 sol4 r8 do' |
    si4 do' re' mib' |
    mib' re' r2 |
    reb'4 reb'8 reb' reb'4 reb' |
    do'8[ fa'] fa'4 mib'2 |
    mib'4( re'!8) mib'-. mib'4( re'8) do'-. |
    si4 do' lab'2 |
    sol'8 do'4 fa'16[ re'] do'4 si |
    do' r r2 |
    R1*13 |
  }
  \tag #'vsoprano {
    \clef "soprano/treble" r4 |
    R1*18 |
    mib''4 mib''8 mib'' mib''4. sol''16[ si'] |
    do''4 do'' mib''2 |
    re''4 \grace fa''16 mib''8[ re''16 do''] sib'4 la' |
    sol'4 r r2 |
    R1*12 |
    si'4 si'8 si' do''4. sol''8 |
    fa''4 mib'' r2 |
    do''4 do''8 do'' do''4. si'8 |
    re''4 r r mib''8[ do''] |
    si'4 r r mib''8[ do''] |
    si'4 r r2\fermata |
    R1*13 |
    reb''4 reb''8 reb'' reb''4 reb'' |
    do''8[ lab'] fa'4 r2 |
    mib''4 mib''8 mib'' mib''4 mib'' |
    re''8[ si'] sol'4 fa''2 |
    mib''4. mib''8 re''4. re''8 |
    do''2 lab' |
    sol'8[ do''] do''[ mib''] mib''[ re''] do''[ si'] |
    do''4 r r2 |
    R1*5 |
  }
  \tag #'valto {
    \clef "alto/treble" r4 |
    R1*18 |
    sol'4 sol'8 sib' sol'4. fa'8 |
    mib'4 mib' fad'2 |
    sol'4. sol'8 sol'4 fad' |
    sol'4 r r2 |
    R1*12 |
    lab'4 lab'8 lab' sol'4. sol'8 |
    sol'4 sol' r2 |
    fad'4 fad'8 fad' fad'4. fad'8 |
    sol'4 r r sol' |
    sol' r r sol' |
    sol' r r2\fermata |
    R1*13 |
    sol'4 sol'8 sol' sol'4 sol' |
    lab'8[ fa'] do'4 r2 |
    fad'4 fad'8 fad' fad'4 fad' |
    sol'8[ re'] si4 sol'2 |
    sol'4. sol'8 sol'4. sol'8 |
    mib'2 fa' |
    mib'8[ sol'] mib'[ sol'] sol'[ fa'] mib'[ re'] |
    mib'4 r r2 |
    R1*5 |
  }
  \tag #'vtenore {
    \clef "tenor/G_8" r4 |
    R1*18 |
    sib4 sib8 sol sib4. re'8 |
    do'4 do' do'2 |
    sib4 la8[ mib'] re'4 re' |
    re' r r2 |
    R1*12 |
    re'4 re'8 re' mib'4. do'8 |
    si4 do' r2 |
    do'4 do'8 do' mib'4 do' |
    si r r do'8[ mib'] |
    re'4 r r do'8[ mib'] |
    re'4 r r2\fermata |
    R1*13 |
    mi'4 mi'8 mi' mi'4 mi' |
    fa'8[ do'] lab4 r2 |
    la4 la8 la la4 la |
    si8[ sol] re'4 re'2 |
    re'4 do' do' si |
    do'2 do' |
    do'4. do'8 sol4 sol |
    sol r r2 |
    R1*5 |
  }
  \tag #'vbasso {
    \clef "bass" r4 |
    R1*18 |
    sol4 sol8 sol sol4. sol8 |
    lab4 lab la2 |
    sib4 do' re' re |
    sol r r2 |
    R1*12 |
    fa4 fa8 fa mib4. mib8 |
    re4 do r2 |
    lab4 lab8 lab lab4. lab8 |
    sol4 r r sol |
    sol r r sol |
    sol r r2\fermata |
    R1*13 |
    sib4 sib8 sib sib4 sib |
    lab lab, r2 |
    do4 do8 do do4 do |
    si, si, si2 |
    do'4 do sol sol, |
    lab,2 fa |
    sol4 sol sol4. sol8 |
    do4 r r2 |
    R1*5 |
  }
>>