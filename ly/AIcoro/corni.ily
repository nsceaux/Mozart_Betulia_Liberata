\clef "treble" \transposition mib
r4 |
<<
  \tag #'(corno1 corni) { mi''1 | fa'' | mi'' | }
  \tag #'(corno2 corni) { do''1 | re'' | do'' }
>>
R1 |
r4 <<
  \tag #'(corno1 corni) { mi''4 mi''2 | }
  \tag #'(corno2 corni) { mi'4 mi'2 | }
>>
r2 r4 <<
  \tag #'(corno1 corni) { mi''4 | mi'' }
  \tag #'(corno2 corni) { mi'4 | mi' }
>> r4 r2 |
R1*27 |
r2 <>\p <<
  \tag #'(corno1 corni) { mi''2 | mi'' }
  \tag #'(corno2 corni) { mi' | mi' }
>> r2 |
R1 |
<<
  \tag #'(corno1 corni) {
    mi''2 mi'' |
    mi'' mi'' |
    mi''4
  }
  \tag #'(corno2 corni) {
    mi'2 mi' |
    mi' mi' |
    mi'4
  }
>> r4 r2\fermata |
R1*13 |
r2 \twoVoices #'(corno1 corno2 corni) <<
  { mi''2 }
  { mi'' }
>>
<<
  \tag #'(corno1 corni) { fa''1 }
  \tag #'(corno2 corni) { re'' }
>>
R1 |
<<
  \tag #'(corno1 corni) {
    mi''1 |
    mi''2 mi'' |
  }
  \tag #'(corno2 corni) {
    mi'1 |
    mi'2 mi' |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 }
  { do'' }
>> r2 |
<<
  \tag #'(corno1 corni) { mi''2 mi'' | mi''4 }
  \tag #'(corno2 corni) { mi'2 mi' | mi'4 }
>> r4 r2 |
R1*3 |
r2 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { mi'2 }
  { mi' }
>>
<<
  \tag #'(corno1 corni) { mi'2 }
  \tag #'(corno2 corni) { do' }
>> r2 |
