\tag #'all \key mib \major
\time 4/4 \midiTempo#120 s1*48 \bar "||"
\segnoMark s1*30 \bar "|." \endMark "[Fine.]"
\time 3/8 \midiTempo#60 s4.*24 \bar "||"
\time 4/4 \midiTempo#120 s1*4 \bar "|." \endMark "[Dal Segno.]"
