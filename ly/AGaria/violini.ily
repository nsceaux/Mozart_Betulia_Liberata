\clef "treble" <sol mib'>4 mib'16 re' mib' fa' <<
  \tag #'violino1 {
    sol'4 r |
    sib''8-. lab''-. sol''-. fa''-. mib''-. re''-. do''-. sib'-. |
    <fa' sib>4 fa'16 mib' fa' sol' lab'4 r |
    lab''8-. sol''-. fa''-. mib''-. re''-. do''-. sib'-. lab'-. |
    sol'16 sol' sol' sol' sol''4:16 lab'':16 lab':16 |
    sol':16 sol'':16 lab'':16 lab':16 |
  }
  \tag #'violino2 {
    mib'4 r |
    sol''8-. fa''-. mib''-. re''-. do''-. sib'-. lab'-. sol'-. |
    <re' sib>4 re'16 do' re' mib' fa'4 r |
    fa''8-. mib''-. re''-. do''-. sib'-. lab'-. sol'-. fa'-. |
    mib'16 mib' mib' mib' mib''4:16 re'':16 fa':16 |
    mib':16 mib'':16 re'':16 fa':16 |
  }
>>
sol':16 mib'':16 fa':16 re''4:16 |
mib''4 <<
  \tag #'violino1 {
    sol'16( fa' sol' lab') sol'4 mib''16( re'' mib'' fa'') |
    mib''4
  }
  \tag #'violino2 {
    mib'16( re' mib' fa') mib'4 sol'16( fa' sol' lab') |
    sol'4
  }
>> r4 r2 |
<mib' sol>4\p mib'16( re' mib' fa') <<
  \tag #'violino1 { sol'4 }
  \tag #'violino2 { mib' }
>> r4 |
<mib' sol>4 <<
  \tag #'violino1 {
    mib'16( re' mib' fa') sol'4 r |
    <fa' sib>4 fa'16( sol' fa' mib') fa'4 r |
    fa'8( sib' re'' sib') fa'( sib' re'' sib') |
    <fa' sib>4 fa'16( mib' fa' sol') lab'4 r |
    lab'8( sol') fa'( sol') lab'( sib' do'' re'') |
    mib''-. sib'( do'' re'') mib''( re'') mib''( fa'') |
    solb''2:16\f la'':16 |
    sib''4
  }
  \tag #'violino2 {
    mib'16( fa' mib' re') mib'4 r |
    <re' sib>4 re'16( mib' re' do') re'4 r |
    re'8( fa' sib' fa') re'( fa' sib' fa') |
    <re' sib>4 re'16( do' re' mib') fa'4 r |
    fa'8( mib') re'( mib') fa'( sol' lab' fa') |
    sol'-. sol'( lab' sib') do''( sib') do''( re'') |
    mib''2:16\f mib'':16 |
    re''4
  }
>> <sib' re'>4 q r |
R1 |
<>\p <<
  \tag #'violino1 {
    mib''2:16 reb'':16 |
    do''4 r r2 |
    mib''2:16 reb'':16 |
    do''8( si') do''( reb'') mib''( reb'') mib''( fa'') |
    solb''( fa'' mib'' fa'') mib''( reb'' do'' sib') |
    la' fa''(\f solb'' fa'') mi''( fa'' la'' fa'') |
    sib'' sib'\p sib' sib' r sib' sib' sib' |
    |
    r sib' sib' sib' r do''-. do''( mib'') |
    mib''( re'') sib'-. re''-. re''( do'') sib'( la') |
    sib'8. sib'16\f sib'8.\trill la'32 sib' re''8 sib' mib'' sib' |
    fa''\p fa'' fa'' fa'' r fa'' fa'' fa'' |
    r sol'' sol'' sol'' r sol'' do'' mib'' |
    re''( fa'') sib'( re'') do''( mib'') do''( la') |
    fa''->\fp fa'' fa'' fa'' sol''->\fp sol'' sol'' sol'' |
    re''2*1/2:16\p s4\cresc re''2:16 |
    do'':16 do'':16 |
    
  }
  \tag #'violino2 {
    do''2:16 sib':16 |
    la'4 r r2 |
    do''2:16 sib':16 |
    solb'4 do'8( reb') mib'( reb') mib'( fa') |
    solb'( fa' mib' fa') mib'( reb' do' sib) |
    la fa'(\f solb' fa') mi'( fa' la' fa') |
    sib' fa'\p fa' fa' r fa' fa' fa' |
    r sol'! sol' sol' r sol' sol' sol' |
    sol'( fa') re'-. fa'-. fa'( mib') re'( do') |
    sib4 sib'\f la' sol' |
    sib'8\p sib' sib' sib' r sib' sib' sib' |
    r sib' sib' sib' r do'' sol' do'' |
    sib'( re'') re'( sib') sol'( do'') mib'( do') |
    sib'->\fp sib' sib' sib' sib'->\fp sib' sib' sib' |
    sib'2*1/2:16\p s4\cresc sib'2:16 |
    sib':16 la':16 |
  }
>>
<re' sib'>4\f sib'16( la' sib' do'') re''4 re''16( do'' re'' mib'') |
fa''4 re''16( do'' re'' mib'') fa''4 sol''16( fa'' sol'' la'') |
sib''4 r sib-. lab!8.\trill sol32 lab |
sol4 r r2 |
<<
  \tag #'violino1 {
    mi'8(\p sol' mi' sol') mi'( sol' mi' sol') |
    mi'( sol' mi' sol') mi'( sol' mi' sol') |
    fa'( lab' fa' lab') lab'( do'' lab' do'') |
    re'' re'' re'' re'' fa'' fa'' fa'' fa'' |
    mib'' sib' sib' sib' sib'-. sib'-. sib'( do'') |
    reb''( do'' sib' lab') sol'-. fa''( mib'' reb'') |
    do''2:16 re''!:16 |
    mib''8\cresc mib'' fa'' fa'' solb'' solb'' la' la' |
    sib'4\f <sib' re'> q r |
  }
  \tag #'violino2 {
    do'8(\p mi' do' mi') do'( mi' do' mi') |
    do'( mi' do' mi') do'( mi' do' mi') |
    do'( fa' do' fa') fa'( lab' fa' lab') |
    lab' lab' lab' lab' lab' lab' lab' lab' |
    sol' sol' sol' sol' sol'-. sol'-. sol'( lab') |
    sib'( lab' sol' fa' mib') reb'( do' sib) |
    lab2:16 sib:16 |
    sib8\cresc sib4 sib sib8 mib'[ mib'] |
    re'!4\f <sib fa'>4 q r |
  }
>> \allowPageTurn
R1 |
<mib' sol>4\p mib'16( re' mib' fa') sol'4 r |
<mib' sol>4 <<
  \tag #'violino1 {
    mib'16( re' mib' fa') sol'4 r |
    <fa' sib>4 fa'16( sol' fa' mib') re'4 r |
    <fa' sib>4 fa'16( mib' fa' sol') lab'4 r |
    <sib fa' re''>4 re''16( do'' re'' mib'') fa''4 r |
    r re''16( do'' re'' mib'') fa''8( re'') fa''( re'') |
    r4 mib''16( re'' mib'' fa'') sol''8( mib'') sol''( mib'') |
    fa'' fa'' fa'' fa'' lab'' lab'' lab'' lab'' |
    sol''4 r r2 |
    fa''2:16\p solb'':16 |
    fa''4 r r2 |
    solb'':16\p solb':16 |
    fa'8( sol'! lab' sib') do''( re'' mib'' fa'') |
    sol''!( fa'' mib'' re'') mib''( reb'' do'' si') |
    do''-. do''( si' do'') mib''( do'') mib''( do'') |
    sib'! sib' sib' sib' r sib'-. sib'( do'') |
    r sib' sib' sib' r sib' sib' sib' |
    sib'( mib'') mib''( sol'') sol''( fa'') mib''( re'') |
    mib''8. mib'16\f mib'8.\trill re'32 mib' sol'8-. mib'-. lab'-. mib'-. |
    sib' mib''\p mib'' mib'' r reb'' reb'' reb'' |
    r do'' do'' do'' do''( lab') mib''( do'') |
    sib'( mib'') mib''( sol'') sol''( fa'') mib''( re''!) |
    mib'' mib'' mib'' mib'' mib'' mib'' mib'' mib'' |
    sol''2:16 sol'':16 |
    fa'':16\cresc fa'':16 |
  }
  \tag #'violino2 {
    mib'16( fa' mib' re') mib'4 r |
    <re' sib>4 re'16( mib' re' do') sib4 r |
    <sib re'>4 re'16( do' re' mib') fa'4 r |
    <fa' sib>4 fa'16( mib' fa' sol') lab'4 r |
    r sib'16( lab' sib' do'') re''8( sib') re''( sib') |
    r4 sib' mib''8( sib') mib''( sib') |
    sib' sib' sib' sib' sib' sib' sib' sib' |
    sib'4 r r2 |
    re''2:16\p mib'':16 |
    re''4 r r2 |
    mib''2:16\p mib':16 |
    re'4 lab8( sib) do'( re' mib' fa') |
    sol'!( fa' mib' re') mib'( reb' do' si) |
    do'-. do'( si do') mib'( do') mib'( do') |
    sol' sol' sol' sol' r sol' sol' sol' |
    r fa'( sol' lab') r lab'( sol' fa') |
    sol'( sib') mib''( sib') sib'( lab') sol'( fa') |
    mib'2\f re'4 do' |
    sib8 sib'\p sib' sib' r sib' sib' sib' |
    r mib' mib' mib' mib''( do'') do''( lab') |
    sol'( sib) sol'( sib') sib'( lab') sol'( fa') |
    sib' sib' sib' sib' do'' do'' do'' do'' |
    mib''2:16 mib'':16 |
    mib'':16\cresc re'':16 |
  }
>>
mib''4 mib'16(\f re' mib' fa' mib'4) <<
  \tag #'violino1 {
    sol'16( fa' sol' lab' |
    sol'4) mib''16( re'' mib'' fa'' mib''4) sol''16( fa'' sol'' lab'' |
    sol''4)
  }
  \tag #'violino2 {
    mib'16( re' mib' fa' |
    mib'4) sol'16( fa' sol' lab' sol'4) mib''16( re'' mib'' fa'' |
    mib''4)
  }
>> <mib'' mib'>4 q q |
<mib'' mib' sol>2 r |
<<
  \tag #'violino1 {
    sol''4.\p~ |
    sol'' |
    sol''4 \grace fa''16 mib''8 |
    re''( do'') r |
    reb''( do'' si') |
    si'16( do'') do''8 lab'~ |
    lab' sol' fa'' |
    mib''16( re'' do''8) r |
    r r sib'!16( mib'') |
    mib''8( re''16 do'' sib' lab') |
    sol'8( sib') mib''16( sol'')~ |
    sol''8( fa''16 mib'' re''32[ do'' sib' lab']) |
    sol'8( mib') r |
    sol'4 sol''8 |
    sol''8.( fa''32 mib'') mib''[( re'') re''( dod'')] |
    re''8 re'' r |
    mib''4 mib''8 |
    re''( fad'' sol'') |
    \grace fa''32 mib''16( re''32 do'') sib'8[ la'] |
    sol'16 sol''\f( fad'' sol'' fad'' sol'') |
    fad''4.:16\p |
    sol''16 sol'' sol''[\f sol'' sol'' sol''] |
    sol''4.:16 |
    <sol'' sib' re'>4 r8\fermata |
  }
  \tag #'violino2 {
    r8 mib'(\p do') |
    r re'( sol) |
    do'16 mib'8 sol' si'16~ |
    si' do''8 sol' lab'16~ |
    lab' lab8 sol fa'16~ |
    fa' mib'8 do'' mib''16~ |
    mib'' re''8 si re'16( |
    do') sol'8 do'' lab'16( |
    sol') sib'8 sol' sib'16~ |
    sib' lab'8 fa' sib16~ |
    sib sol'8 sib' mib''16~ |
    mib'' do''8 fa' re'16( |
    mib') sol'8 sib re'16 |
    mib' sol'8 sib' sol'16 |
    sol16 dod'8 sol' sib'16 |
    fad'16 la'8 fad' re'16 |
    la fad'8 la' fad'16~ |
    fad' sol'8 do'' sib'16 |
    \grace re''32 do''16( sib'32 la') sol'8[ fad'] |
    sol'16 sib'(\f la' sib' la' sib') |
    la'4.:16\p |
    sol'16[ sol'] sol''\f sol'' fa''! fa'' |
    mib'' mib'' re'' re'' dod'' dod'' |
    re''4 r8\fermata |
  }
>>
<re' sib' sol''>4 <<
  \tag #'violino1 {
    sol''16( fad'' sol'' la'') sib''4 r |
    lab''!4 fa''!16( mib'' fa'' sol'') lab''4 r |
    sol'' mib''16( re'' mib'' fa'') sol''4 sol''16( fa'' sol'' la'') |
    sib''4
  }
  \tag #'violino2 {
    sol''16( la'' sol'' fa''!) mi''4 r |
    fa''4 lab''!16( sol'' fa'' mib''!) re''4 r |
    mib'' mib''16( fa'' mib'' re'') mib''4 mib''16( fa'' sol'' mib'') |
    re''4
  }
>> <sib' re'> q r |
