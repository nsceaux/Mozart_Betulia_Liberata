\clef "treble" \transposition mib <>
<<
  \tag #'(corno1 corni) {
    do''1 |
    do''4 s2. |
    sol'1 |
    sol'4 s2. |
    do''2 re'' |
    mi'' re'' |
  }
  \tag #'(corno2 corni) {
    do'1 |
    do'4 s2. |
    sol1 |
    sol4 s2. |
    mi'2 sol' |
    do'' sol' |
  }
  { s1 | s4 r r2 | s1 | s4 r r2 | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''2 re'' | }
  { do''2. sol'4 | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 do'' do'' do'' |
    do'' s2. |
    do''1~ |
    do'' |
    sol'4
  }
  \tag #'(corno2 corni) {
    do'4 do' do' do' |
    do' s2. |
    do'1~ |
    do' |
    sol4
  }
  { s1 | s4 r r2 | s1\p }
>> r4 r2 |
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1 }
  { sol }
>>
R1*2 |
<>\f <<
  \tag #'(corno1 corni) { do''1 | re''4 re'' re'' }
  \tag #'(corno2 corni) { do'1 | sol'4 sol' sol' }
>> r4 |
R1 |
\tag #'corni <>^"a 2." re''1\p~ |
re''4 r r2 |
re''1 |
R1*6 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'1 }
  { sol }
>>
R1*5 |
\tag #'corni <>^"a 2." re''1\p |
<<
  \tag #'(corno1 corni) {
    re''4 s re''2 |
    re''
  }
  \tag #'(corno2 corni) {
    sol'4 s sol'2 |
    sol'
  }
  { s4\f r }
>> \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 sol' | sol' }
  { sol' sol' | sol' }
>> r4 r2 |
<>\fp <<
  \tag #'(corno1 corni) { mi''2. }
  \tag #'(corno2 corni) { mi'2. }
>> r4 |
R1*3 |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1 | do'4 }
  { sol1 | do'4 }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { do''1 | }
  \tag #'(corno2 corni) { do' | }
>>
R1 |
r2 <<
  \tag #'(corno1 corni) { do''2 | re''4 re'' re'' }
  \tag #'(corno2 corni) { do'2 | sol'4 sol' sol' }
  { s2 | s4\f }
>> r4 |
R1 |
<>\p <<
  \tag #'(corno1 corni) { do''1 | do'' | sol'~ | sol'~ | sol'4 }
  \tag #'(corno2 corni) { do'1 | do' | sol~ | sol~ | sol4 }
>> r4 r2 |
r4 <>\p <<
  \tag #'(corno1 corni) { re''4 re'' }
  \tag #'(corno2 corni) { sol' sol' }
>> r4 |
r <<
  \tag #'(corno1 corni) { mi''4 mi'' }
  \tag #'(corno2 corni) { do'' do'' }
>> r |
\twoVoices #'(corno1 corno2 corni) <<
  { sol'1~ | sol'4 }
  { sol'1~ | sol'4 }
>> r4 r2 |
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 }
  { sol' }
>> <<
  \tag #'(corno1 corni) { do''2 | sol'4 }
  \tag #'(corno2 corni) { do'2 | sol4 }
>> r4 r2 |
<>\p \twoVoices #'(corno1 corno2 corni) <<
  { do''1 | sol' | do''~ | do''4 }
  { do'1 | sol' | do'~ | do'4 }
>> r4 r2 |
R1*3 |
\tag #'corni <>^"a 2." do''1\f~ |
do''4 r r2 |
R1*4 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''4 s do'' s |
    do'' s do'' s |
    do'' do'' do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    sol'1 |
    mi'4 s mi' s |
    mi' s mi' s |
    mi'4 mi' mi' mi' |
    mi'2
  }
  { s1\p | s4\f r s r | s r s r | }
>> r2 |
R4.*12 |
<>\p <<
  \tag #'(corno1 corni) { do''4 re''8 | mi'' }
  \tag #'(corno2 corni) { mi'4 sol'8 | do'' }
>> r r |
R4.*7 |
<>\f <<
  \tag #'(corno1 corni) { mi''4. | mi'' | mi''4 }
  \tag #'(corno2 corni) { mi'4. | mi' | mi'4 }
>> r8\fermata |
<<
  \tag #'(corno1 corni) { mi''4 }
  \tag #'(corno2 corni) { mi' }
>> r4 r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''4 }
  { re'' }
>> r4 <<
  \tag #'(corno1 corni) {
    re''8 re'' re'' re'' |
    do''1 |
    re''4 re'' re''
  }
  \tag #'(corno2 corni) {
    sol'8 sol' sol' sol' |
    do'1 |
    sol'4 sol' sol'
  }
>> r4 |
