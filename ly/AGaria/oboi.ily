\clef "treble" <>
<<
  \tag #'(oboe1 oboi) { mib''2 sol'' | sib''4 }
  \tag #'(oboe2 oboi) { sol'2 mib'' | sol''4 }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { re''2 fa'' | lab''4 }
  \tag #'(oboe2 oboi) { sib'2 re'' | fa''4 }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) {
    sol''4 s s lab'' |
    sol'' r r lab'' |
  }
  \tag #'(oboe2 oboi) {
    mib''4 s s fa'' |
    mib'' s s fa'' |
  }
  { s4 r r s | s r r s | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 fa'' | mib''4 }
  { mib''2. re''4 | mib'' }
>> <<
  \tag #'(oboe1 oboi) {
    sol''16( fa'' sol'' lab'') sol''4 sol'' |
    sol''
  }
  \tag #'(oboe2 oboi) {
    mib''16( re'' mib'' fa'') mib''4 mib'' |
    mib''
  }
>> r4 r2 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''1~ | sol'' | fa''4 }
  \tag #'(oboe2 oboi) { mib''1~ | mib'' | re''4 }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) { fa''1 }
  \tag #'(oboe2 oboi) { re'' }
>>
R1*2 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { solb''2 la'' | }
  { mib''1 | }
>>
<<
  \tag #'(oboe1 oboi) { sib''4 sib'' sib'' }
  \tag #'(oboe2 oboi) { re'' re'' re'' }
>> r4 |
\tag #'oboi <>^"a 2." fa''2\f reb''4 sib' |
la' r r2 |
fa''2 reb''4 sib' |
la' r r2 |
R1*3 |
<>\p <<
  \tag #'(oboe1 oboi) { fa''1 | sol''!2 }
  \tag #'(oboe2 oboi) { sib'1~ | sib'2 }
>> r2 |
R1*2 |
<>\p <<
  \tag #'(oboe1 oboi) { lab''!1 | sol''2 }
  \tag #'(oboe2 oboi) { fa''1 | mib''2 }
>> r2 |
R1*2 |
r2 <>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | do''1 | sib'2 re'' | }
  { sib'2~ | sib' la' | sib'1 | }
  { s2 | s1 | s\f }
>>
<<
  \tag #'(oboe1 oboi) { fa''4 s fa'' }
  \tag #'(oboe2 oboi) { re'' s re'' }
  { s4 r }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''16( fa'' sol'' la'') | }
  { mib''4 | }
>>
<<
  \tag #'(oboe1 oboi) { sib''4 }
  \tag #'(oboe2 oboi) { re'' }
>> r4 r2 |
<>\p \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2( fa'') | mi''4 }
  { si'1 | do''4 }
>> r4 r2 |
R1 |
<<
  \tag #'(oboe1 oboi) { lab''!1~ | lab'' | sol''4 }
  \tag #'(oboe2 oboi) { fa''1 | re'' | mib''4 }
>> r4 r2 |
R1*3 |
r4 <>\f <<
  \tag #'(oboe1 oboi) { sib''4 sib'' }
  \tag #'(oboe2 oboi) { re'' re'' }
>> r4 | \allowPageTurn
R1 |
<>\p <<
  \tag #'(oboe1 oboi) {
    sol''1 |
    sol'' |
    fa''~ |
    fa''~ |
    fa''4 s s2 |
    s4 fa'' fa'' s |
    s sol'' sol'' s |
  }
  \tag #'(oboe2 oboi) {
    mib''1 |
    mib'' |
    re''~ |
    re''~ |
    re''4 s s2 |
    s4 re'' re'' s |
    s mib'' mib'' s |
  }
  { s1*4 |
    s4 r r2 |
    r4 s\p s r |
    r4 s s r | }
>>
R1 |
\tag #'oboi <>^"a 2." sib''2\f solb''4 mib'' |
re'' r r2 |
sib''2\f solb''4 re'' |
mib'' r r2 |
R1*6 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { mib''2 sol''!4 lab'' | sib'' }
  { mib''2 re''4 do'' | sib' }
>> r4 r2 |
R1*3 |
r2 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 | fa''1 | mib''4 }
  { mib''2~ | mib'' re'' | mib''4 }
  { s2\p | s1 | s4\f }
>> r4 <<
  \tag #'(oboe1 oboi) {
    sol''4 s |
    sol'' s sol'' s |
    sol'' sol'' sol'' sol'' |
    sol''2
  }
  \tag #'(oboe2 oboi) {
    mib''4 s |
    mib'' s mib'' s |
    mib''4 mib'' mib'' mib'' |
    mib''2
  }
  { s4 r |
    s r s r |
  }
>> r2 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''4.( | fa'') | mib''8 }
  \tag #'(oboe2 oboi) { mib''4.( | si') | do''8 }
>> r8 r |
R4.*5 |
<>\p <<
  \tag #'(oboe1 oboi) { sol''4.( | lab'') | sol''8 }
  \tag #'(oboe2 oboi) {
    \tag #'oboi \tieDown sib'!4.~ | sib'~ | sib'8 \tag #'oboi \tieNeutral
  }
>> r r |
R4. |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''4( re''8) | mib'' }
  { sol''4( lab''8) | sol'' }
>> r r |
R4.*7 |
\mergeDifferentlyDottedOn
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4. | sol''4 sib''8 | sib'4 }
  { sol''4 fa''!8 | mib'' re'' dod'' | re''4 }
>> r8\fermata |
<<
  \tag #'(oboe1 oboi) {
    sol''4 s sib''8( sol'') sib''( sol'') |
    lab''!4 s lab''8( fa'') lab''( fa'') |
  }
  \tag #'(oboe2 oboi) {
    sib'4 s sol''8( mi'') sol''( mi'') |
    fa''4 s fa''8( re'') fa''( re'') |
  }
  { s4 r s2 | s4 r s2 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2. sol''16( fa'' sol'' la'') | }
  { mib''!1 | }
>>
<<
  \tag #'(oboe1 oboi) { sib''4 sib'' sib'' }
  \tag #'(oboe2 oboi) { re'' re'' re'' }
>> r4 |
