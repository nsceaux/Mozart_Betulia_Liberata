\clef "soprano/treble" R1*8 |
mib''2 sib' |
sol'4 r r2 |
r4 sib' mib'' sol'' |
fa'' sib' r sib'8 sib' |
fa''4. re''8 fa''[ re''] fa''[ re''] |
sib'4 sib' r sib'8 sib' |
lab'[ sol'] fa'[ sol'] lab'[ sib'] do''[ re''] |
mib''[ sib'] do''[ re''] mib''[ re''] mib''[ fa''] |
solb''4 mib'' dob'' la' |
sib'4 r r2 |
R1 |
mib''2 reb'' |
do''4 r r fa' |
mib''2 reb'' |
do''8[ si'] do''[ reb''] mib''[ reb''] mib''[ fa''] |
solb''[ fa''] mib''[ fa''] mib''[ reb''] do''[ sib'] |
la'4 la' r2 |
sib'2. fa''8[ re''!] |
do''4 do'' r do''8 mib'' |
mib''[ re''] sib'[ re''] re''[ do''] sib'[ la'] |
sib'4 r r2 |
fa''2. lab'!4 |
sol'8[ sol''] sol''2 do''8 mib'' |
re''[ fa''] sib'[ re''] do''[ mib''] do''[ la'] |
fa''[ re''] re'' sib' sol''[ mib''] mib'' do'' |
fa'1 |
do''\trill |
sib'4 r r2 |
R1*2 |
sol''2 si' |
do''4 r r2 |
r4 do'' sol'' sib'! |
lab'4 do'' r do''8 do'' |
re''4. fa''8 fa''4. lab'8 |
sol'4 sib' r sib'8 do'' |
reb''[ do''] sib'[ lab'] sol'[ fa''] mib''[ reb''] |
do''[ sib'] lab'4 re''!4. re''8 |
mib''4 fa'' solb'' la' |
sib'4 r r2 |
mib''2 sib' |
sol'4 r r2 |
r4 sib' mib'' sol'' |
fa''8[ re''] sib'4 r sib'8 sib' |
fa''4. re''8 fa''[ re''] fa''[ re''] |
sib'4 sib' r sib'8 sib' |
re''4 re'' r sib'8 sib' |
mib''4 mib'' r mib''8 sol'' |
fa''4 re'' sib'4. lab'8 |
sol'4 r r2 |
fa''2 solb'' |
fa''4 r r sib' |
mib''2 solb' |
fa'8[ sol'!] lab'[ sib'] do''[ re''] mib''[ fa''] |
sol''![ fa''] mib''[ re''] mib''[ reb''] do''[ si'] |
do''4 do'' r2 |
sib'!2. mib''8[ do''] |
sib'4 sib' r sib'8 sib' |
sib'[ mib''] mib''[ sol''] sol''[ fa''] mib''[ re''] |
mib''4 r r2 |
mib''2. reb''4 |
do'' do'' r do''8 do'' |
sib'8[ mib''] mib''[ sol''] sol''[ fa''] mib''[ re''!] |
mib''[ sol''] mib'' sib' do''[ fa''] fa'' lab'' |
sib'1 |
fa''\trill |
mib''4 r r2 |
R1*3 |
sol''4.~ |
sol''~ |
sol''4 \grace fa''16 mib''8 |
re'' do'' r |
reb'' do'' si' |
si'16[ do''] do''8 lab' |
lab' sol' fa'' |
mib''16[ re''] do''8 r |
r r sib'!16 mib'' |
mib''8[ re''16] do'' sib' lab' |
sol'8 sib' mib''16 sol''~ |
sol''8 fa''16 mib'' re''32[ do''] sib'[ lab'] |
sol'8 mib' r |
sol'4 sol''8 |
sol''8. fa''32[ mib''] mib''[ re''] re''[ dod''] |
re''8 re'' r |
mib''4 mib''8 |
re'' fad'' sol'' |
\grace fa''!32 mib''16[ re''32 do'' sib'8] la' |
sol'4 r8 |
fad''4 fad''8 |
sol'' sol' r |
r r sol' |
sol' la'4\trill\fermata |
sol'4 r r2 |
R1*3 |
