\clef "alto" mib'8 mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
sib8 sib sib sib sib sib sib sib |
sib4 r r2 |
mib'8 mib' mib' mib' fa' fa' re' re' |
mib' mib' mib' mib' fa' fa' re' re' |
mib' mib' do' do' lab lab sib sib |
mib'4 r mib' r |
mib' r r2 |
mib'8\p mib' mib' mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' mib' mib' |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib4 r r2 |
mib'4 r r2 |
dob'8\f dob' dob' dob' dob' dob' dob' dob' |
sib4 sib' sib r |
fa'2\f reb'4 sib |
la r r2 |
fa'2 reb'4 sib |
la r r2 |
mib'4 r mib' r |
mib' r mib' mi' |
fa' r r mib'!\f |
re'!\p r re' r |
mib' r mib' r |
fa'1 |
sol'2\f fa'4 mib' |
re'\p r re' r |
mib' r mib' r |
fa' sol' mib' fa' |
re'8->\fp re' re' re' mib'->\fp mib' mib' mib' |
fa'\p fa' fa' fa' fa' fa' fa' fa' |
fa\cresc fa fa fa fa fa fa fa |
sib\f sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib4 r sib lab!8.\trill sol32 lab |
sol4 r r2 |
<sol mi'>1\p~ |
q |
<lab fa'> |
q |
<sol mib'>8 mib' mib' mib' mib' mib' mib' mib' |
mib'4 r r2 |
lab8 lab lab lab lab' lab' lab' lab' |
solb'\cresc solb' re' re' mib' mib' dob' dob' |
sib4\f sib' sib r |
R1 |
mib'8-\sug\p mib' mib' mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' mib' mib' |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib sib sib sib |
sib sib sib sib sib4 r |
r lab'\f lab' r |
r sol' sol' r |
re' r r2 |
sib'2\f solb'4 mib' |
re' r r2 |
sib'2\f solb'4 re' |
mib' r r2 |
sib4\p fa8( sol!) lab( sib do' re') |
mib'( fa' sol' fa') mib'4 mib |
mib8-. lab( sol lab) do'( lab) do'( lab) |
mib'4 r mib' r |
re' r sib r |
mib' sol' sib' sib |
do'2\f sib4 lab |
sol r r8 mib'\p mib' mib' |
lab'4 r r2 |
sib'8 sib' sib' sib' sib sib sib sib |
sol' sol' sol' sol' lab' lab' lab' lab' |
sib' sib' sib' sib' sib' sib' sib' sib' |
sib sib sib sib sib sib sib sib |
mib'8\f mib' mib' mib' mib' mib' mib' mib' |
mib' mib' mib' mib' mib' mib' mib' mib' |
mib' sol' sib' sol' mib' sol' sib' sol' |
mib'2 r |
do'4\p r8 |
sol4 r8 |
R4.*14 |
do'8 do' do' |
sib la sol |
la re'4 |
mib' r8 |
do'(\p re' mib') |
re'4.:16\f |
do'8 re' mib' |
re'4 r8\fermata |
sol'8 sol' sol' sol' do' do' do' do' |
fa' fa' fa' fa' sib sib sib sib |
mib' mib' mib' mib' do' do' do' do' |
sib4 sib' sib r |
