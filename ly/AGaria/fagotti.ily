\clef "bass" \tag #'fagotti <>^"a 2."
mib8 mib mib mib mib mib mib mib |
mib4 r r2 |
sib,8 sib, sib, sib, sib, sib, sib, sib, |
sib,4 r r2 |
sib1~ |
sib~ |
sib4 do'2 sib4 |
mib r mib r |
mib r r2 |
R1*2 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1~ | sib~ | sib~ | sib( | mib) | }
  { sib,1~ | sib,~ | sib,~ | sib,( | mib,) }
>>
\tag #'fagotti <>^"a 2." dob8\f dob dob dob dob dob dob dob |
sib,4 sib sib, r |
fa'2\f reb'4 sib |
la r r2 |
fa'2 reb'4 sib |
la4 r r2 |
R1*2 |
r2 r4 mib\f |
re!\p r r2 |
mib4 r r2 |
fa1 |
sol2\f fa4 mib |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1 | sib2 }
  { re1 | mib2 }
>> \tag #'fagotti <>^"a 2." mib4 r |
fa4 sol mib fa |
re2-> mib-> |
fa1 |
fa, |
sib,8\f sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib,4 r sib lab! |
sol r r2 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { do'1~ | do' | fa | sib | }
  { do~ | do | fa | sib, | }
>>
\tag #'fagotti <>^"a 2." mib4 r r2 |
mib4 r r2 |
R1*2 |
sib,4\f sib sib, r | \allowPageTurn
R1*3 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib1~ | sib~ | sib | sib~ | sib | sib4 }
  { sib,1~ | sib,~ | sib, | lab | sol | re4 }
>> r4 r2 |
\tag #'fagotti <>^"a 2." sib2\f solb4 mib |
re r r2 |
sib2\f solb4 re |
mib4 r r2 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { lab!1 | sol! | lab4 }
  { lab,!1 | sol,! | lab,4 }
>> r4 r2 |
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sol4 }
  { sol, }
>> r4 \tag #'fagotti <>^"a 2." mib4 r |
re r sib, r |
mib sol sib sib, |
do2\f sib,4 lab, |
sol, r sol\p r |
lab r r2 |
sib8 sib sib sib sib, sib, sib, sib, |
sol sol sol sol lab lab lab lab |
sib sib sib sib sib sib sib sib |
sib, sib, sib, sib, sib, sib, sib, sib, |
mib\f mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
mib8 sol sib sol mib sol sib sol |
mib2 r |
do'4.\p |
sol |
R4.*10 |
<>\p \twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { sib8 mib' re' | mib'4 }
  { sol4 fa8 | sol4 }
>> r8 |
R4.*2 |
<<
  \tag #'(fagotto1 fagotti) { mib'4. | re'4 }
  \tag #'(fagotto2 fagotti) { do'4. | sib4 }
>> r8 |
R4.*2 |
mib'8\p re' do' |
sib sib,\f si, |
do re mib |
re4 r8\fermata |
R1*4 |
