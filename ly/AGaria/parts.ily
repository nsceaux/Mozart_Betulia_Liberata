\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (corni #:score-template "score-corni"
          #:tag-global () #:instrument "Corni in Es.")
   (fagotti #:music , #{
\quoteBasso "AGbasso"
s1*9
\cue "AGbasso" { \mmRestUp <>_\markup\tiny "Basso" s1*2 }
s1*11
\cue "AGbasso" { <>_\markup\tiny "Basso" s1*2 s2 }
s2 s1*20
\cue "AGbasso" { <>_\markup\tiny "Basso" s1*2 }
s1
\mmRestCenter s1
\cue "AGbasso" { \mmRestUp <>_\markup\tiny "Basso" s1*2 }
s1*27
s4.*2
\cue "AGbasso" { \mmRestDown <>_\markup\tiny "Basso" s4.*7 \mmRestUp s4. \mmRestDown s4.*2 }
s4.*2
\cue "AGbasso" { \mmRestUp <>_\markup\tiny "Basso" \voiceThree s4.*2 }
s4.*2
\cue "AGbasso" { \mmRestDown <>_\markup\tiny "Basso" \voiceThree s4.*2 }
s4.*4
\cue "AGbasso" { <>_\markup\tiny "Basso" s1*4 }
#}))
