\clef "bass" mib8 mib mib mib mib mib mib mib |
mib4 r r2 |
sib,8 sib, sib, sib, sib, sib, sib, sib, |
sib,4 r r2 |
mib8 mib mib mib fa fa sib, sib, |
mib mib mib mib fa fa sib, sib, |
mib mib do' do' lab lab sib sib |
mib4 r mib r |
mib r r2 |
mib8\p mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib,4 r r2 |
mib4 r r2 |
dob8\f dob dob dob dob dob dob dob |
sib,4 sib sib, r |
R1 |
fa8\p fa fa fa fa fa fa fa |
fa4 r r2 |
fa8 fa fa fa fa fa fa fa |
mib4 r mib r |
mib r mib mi |
fa r r mib!\f |
re!\p r re r |
mib r mib r |
fa1 |
sol2\f fa4 mib |
re\p r re r |
mib r mib r |
fa sol mib fa |
re8->\fp re re re mib->\fp mib mib mib |
fa\p fa fa fa fa fa fa fa |
fa,\cresc fa, fa, fa, fa, fa, fa, fa, |
sib,8\f sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib,4 r sib lab! |
sol r r2 |
do4-\sug\p r r2 |
do4 r r2 |
fa4 r r2 |
sib,8 sib, sib, sib, sib, sib, sib, sib, |
mib mib mib mib mib mib mib mib |
mib4 r r2 |
lab,8 lab, lab, lab, lab lab lab lab |
solb\cresc solb re re mib mib dob dob |
sib,4\f sib sib, r |
R1 |
mib8-\sug\p mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib, sib, sib, sib, |
sib, sib, sib, sib, sib,4 r |
r lab\f lab r |
r sol sol r |
re8\p re re re re re re re |
mib4 r r2 |
sib8\p sib sib sib la la la la |
sib4 r r2 |
dob8\p dob dob dob la, la, la, la, |
lab,!4 r r2 |
sol,4 r sol r |
lab r r2 |
sol4 r mib r |
re r sib, r |
mib sol sib sib, |
do2\f sib,4 lab, |
sol, r sol\p r |
lab r r2 |
sib8 sib sib sib sib, sib, sib, sib, |
sol sol sol sol lab lab lab lab |
sib sib sib sib sib sib sib sib |
sib, sib, sib, sib, sib, sib, sib, sib, |
mib\f mib mib mib mib mib mib mib |
mib mib mib mib mib mib mib mib |
mib8 sol sib sol mib sol sib sol |
mib2 r |
R4.*2 |
do8\p mib sol |
lab mib fa |
fa mib re |
do lab do' |
si sol si, |
do do re |
mib sib! sol |
fa sib, re |
mib sol mib |
lab fa sib |
mib sol sib |
mib mib mib |
mib mib mib |
re re re |
do' do' do' |
sib la sol |
do' re' re |
mib4 r8 |
mib'8 re' do' |
sib sib,\f si, |
do re mib |
re4 r8\fermata |
sol8 sol sol sol do do do do |
fa fa fa fa sib, sib, sib, sib, |
mib mib mib mib do do do do |
sib,4 sib sib, r |
