- scol -- to, O -- zi -- ̀a!

Be -- tu -- lia, ai -- mè, che a -- scol -- to! All’ ar -- mi As -- si -- re
dun -- que a -- pri -- rem le por -- te, o -- ve non giun -- ga
soc -- cor -- so in cin -- que dì! Mi -- se -- ri! e que -- sta
e la vi -- a d’im -- pe -- trar -- lo? Ah tut -- ti sie -- te
col pe -- vo -- li e -- gual -- men -- te. Ad un e -- stre -- mo
li po -- po -- lo tra -- scor -- se; e chi lo reg -- ge
nell’ al -- tro ru -- i -- nò. Quel -- lo di -- spe -- ra
del -- la pie -- tà di -- vi -- na; ar -- di -- sce que -- sto
li -- mi -- tar -- le i con -- fi -- ni. Il pri -- mo è vi -- le,
te -- me -- ra -- rio il se -- con -- do. A chi la spe -- me,
a chi man -- ca il ti -- mor:
né in que -- sto o in quel -- la
mi -- su -- ra si ser -- bò. Vi -- zio, ed ec -- ces -- so
non è di -- ver -- so. Al -- la vir -- tù pre -- scrit -- ti
so -- no i cer -- ti con -- fi -- ni; e ca -- de o -- gnu -- no,
che per qual -- un -- que via da lor si sco -- sta,
in col -- pa e -- gual, ben -- chè tal -- vol -- ta op -- po -- sta.
