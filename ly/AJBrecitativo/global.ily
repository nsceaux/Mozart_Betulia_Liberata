\set Score.currentBarNumber = #27 \bar ""
\key do \major
\midiTempo#100
\time 4/4 s1*5
\tempo "Andante" s1*10
\tempo "Allegro" s1*10 s2 s8
\tempo "Andante" s4. s1*13 \bar "|."
