\clef "alto/treble" sol'8 sol' r si' sol' sol' r re' |
sol' sol' r la' fa'4 r8 re' |
si si r16 sold' sold' la' si'8 si' r4 |
si'8 mi'16 mi' mi'8 fa' re' re' re' re'16 re' |
sold'8 sold' r sold' si' si' si' do'' |
la'4 r r2 |
r do''8 fad'16 fad' r8 fad' |
fad' fad' fad' sol' la' la' si' fad' |
sol' sol' r4 r2 si'4 si'8 la' sold' sold' r sold' |
si' si'16 si' si'8 do'' la' la' r4 |
r2 r8 la' la' si' |
sol' sol' r mi' sol' sol' sol' fa' |
re' re' r4 r2 |
r8 fa' fa' sol' lab' lab'16 do'' lab' lab' lab' sol' |
mib'4 r r2 |
do''4 sol'8 sol' mi'! mi' r4 |
sol'8 sol'16 sol' sib'8 lab' fa' fa' r4 |
r2 r8 sol' sol' lab' |
fa' fa' r re'16 mib' fa'4 fa'8 mib' |
do' do' r4 r2 |
r r8 sol' lab' sib' |
sib' mi'! r4 r8 sol'16 sol' sib'8 sib'16 lab' |
fa'8 fa' r4 r2 |
r8 mi' mi' fad' sold' sold' r sold'16 la' |
si'!4 si'8 do'' la'!4 r |
R1 |
r8 sol' sol' mi' dod' dod' r sib' |
sol' sol' sol' la' fa'4 r |
sold'4 sold'8 si'! si' mi' r16 mi' si' do'' |
la'8 la' r4 r2 |
r fad'8 fad'16 fad' fad'8 la' |
la' red' r fad'16 sol' la'4 la'8 sol' |
mi' mi' r4 r2 |
r8 sol' sol' sib' la' la' r mi' |
mi' mi' mi' fa'! sol' sol' sol' la' |
fa' fa' r4 r8 si' si' la' |
sold'4 r la'8 la'16 la' sold'8 la' |
la' mi' r4 r2 |
