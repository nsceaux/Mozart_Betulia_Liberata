\clef "alto" r2 r8 r16 sol sol4 |
r2 si~ |
si1~ |
si2 mi'~ |
mi'1 |
mi'8 la si8.( do'32 si) la8 la' si'8.( do''32 si') |
la'16-. si'( do'' sold') la'-. mi'( fa' mi') red'4 r |
R1 |
r8 sol fad si r sol fad si |
si4 r r2 |
r r8 do' si mi' |
r8 do' si mi' mi'4 r |
la1 |
la8 fa'16( re') mi'8 la la re'16( sib) do'!8 fa |
fa4 r r2 |
sib4 r8 fa' sol'4 r8 fa' |
sol'4 r r2 |
r do'4 r8 sib |
do'4 r8 do' <re'! sol>4 r |
R1 | \allowPageTurn
do'4 r8 mib' re'4 r8 do' |
sib4 r8 lab sol4 r |
do' r8 reb' do'4 r |
do'4 r8 lab' re' fa' si! re' |
re'4 r r2 |
r r8 la!32(\f si do' re') mi'!8-. mi'-. |
fa'4( sold8) r r2 |
sib4\f r r2 |
R1 |
fa'4 r r2 |
r8 la si8.( do'32 si) la8 la' si'8.( do''32 si') |
la'16-. si'( do'' sold') la'( sol' fa' mi') red'4 r |
R1 |
mi4( re!) do8 sol4 sol8 |
sol4 r r8 dod' dod-. r |
R1 |
r8 fa re' mi' fa'!4 r |
r8 re re' fa' fad'4 r |
r mi' mi2 |
