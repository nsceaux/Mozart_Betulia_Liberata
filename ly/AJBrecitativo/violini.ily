\clef "treble" <>
<<
  \tag #'violino1 {
    r2 r8 r16 re' re'4 |
    r2 re''~ |
    re''1~ |
    re''~ |
    re'' |
    do''16 mi'8 mi' mi' mi'16 mi' mi''8 mi'' mi'' mi''16 |
    mi''-.
  }
  \tag #'violino2 {
    r2 r8 r16 si si4 |
    r2 fa'~ |
    fa' mi'~ |
    mi' si'~ |
    si'1 |
    la'8 do' re'8.( mi'32 re') do'8 do'' re''8.( mi''32 re'') |
    do''16-.
  }
>> si'16( do'' sold') la'-. mi'( fa' mi') <red' do''>4 r |
R1 |
<<
  \tag #'violino1 {
    si'16( do'' si'8)-. r16 si( fad' la') sol'( do'' si'8)-. r16 si( fad' la') |
    sol'4 r r2 |
    r mi''16( fa'' mi''8)-. r16 mi'( si' re'') |
    do''16( fa'' mi''8)-. r16 mi'( si' re'') dod''4 r |
    mi''1 |
    re''16( sib'' la''8)-. r16 la'( mi'' sol'') fa''16( sol'' fa''8)-. r16 fa'( do'' mib'') |
    re''4 r r2 |
    r8 sib'32( do'' sib' la' sib'8) re''-. mib''-. sib'32( do'' sib' la' sib'8) reb''-. |
    do''4 r r2 |
    r r8 fa''32( sol'' fa'' mi''! fa''8) reb''-. |
    do''-. fa''32( sol'' fa'' mi'' fa''8) do''-. <si'! re'! sol>4 r |
    R1 | \allowPageTurn
    r8 do'''32( re''' do''' si'' do'''8) do''-. sib'-. sib''32( do''' sib'' la'' sib''8) lab'-. |
    sol'-. sol''32( lab'' sol'' fa'' sol''8) fa'-. mib'4-. r |
    r8 sib'32( do'' sib' la'! sib'8) sib''-. sib4-. r |
    r8 fa''32( sol'' fa'' mi''!) fa''8-. lab' re' fa' si! re' |
    sold4
  }
  \tag #'violino2 {
    r8 si16( do') si8( red'16 fad') mi'8 si16( do') si8( red'16 fad') |
    mi'4 r r2 |
    r r8 mi'16( fa'!) mi'8( sold'16 si') |
    la'8 mi'16( fa') mi'8( sold'16 si') la'4 r |
    sol'!1 |
    fa'8 la16( sib) la8( dod'16 mi') re'8 fa'16( sol') fa'8( la'16 do''!) |
    sib'4 r r2 |
    sol'4 r8 lab' sib'4 r8 sib' |
    sib4 r r2 |
    r lab'4 r8 fa' |
    fa'4 r8 fa' <fa' sol>4 r |
    R1 | \allowPageTurn
    mib'4 r8 sol' fa'4 r8 mib' |
    re'4 r8 do' sib4 r |
    <sol mi'!>4 r8 q q4 r |
    lab'4 r8 lab' re' fa' si! re' |
    mi'4
  }
>> r4 r2 | \allowPageTurn
r r8 la!32(\f si do' re') mi'!8-. mi'-. |
fa'4( sold8) <>\p <<
  \tag #'violino1 {
    re''8( do'' si' la'! sold') |
    sol'!4\f r r2 |
    r r16 la'( fa'' dod'') re''( do'' sib' la') |
    sold'4 r r2 |
    la'16 mi'8 mi' mi' mi'16 mi' mi''8 mi'' mi'' mi''16~ |
    mi''
  }
  \tag #'violino2 {
    fa'8( mi' re' do' si) |
    mi'4\f r r2 |
    r2 r8 r16 la( fa' mi' re' do') |
    si!4 r r2 |
    r8 do' re'8.( mi'32 re') do'8 do'' re''8.( mi''32 re'') |
    do''16-.
  }
>> si'( do'' sold') la'( sol' fa' mi') <red' do''>4 r |
R1 |
<<
  \tag #'violino1 {
    sol''2~ sol''16 mi'' do''8~ do''16 mi''( re'' si') |
    do''4 r r8 mi''16( dod'') la'8-. r |
    R1 |
    la''16( fa'') re''4( do''!8) si'4 r |
    r8 re''16( si') sold'8-. si''-. do'''4-. r |
    r si' <do'' mi' la>2 |
  }
  \tag #'violino2 {
    r16 sol'( mi' do') si-. fa'!( re'! si) do' sol mi'8~ mi'16 sol'( fa' re') |
    mi'4 r r8 la16( dod') mi'8-. r |
    R1 |
    r16 la la' la' la' la' la' la' la'4 r |
    r8 fa'16( re') si8-. sold''-. la''4-. r |
    r sold' <la' mi' do'>2 |
  }
>>
