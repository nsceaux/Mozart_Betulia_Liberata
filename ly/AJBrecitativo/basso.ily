\clef "bass" sol2 r8 r16 sol sol4 |
r2 sold~ |
sold1~ |
sold2 sold~ |
sold1 |
la8 la sold mi r la sold mi |
la16-. si( do' sold) la-. mi( fa mi) red4 r |
R1 | \allowPageTurn
r8 mi red si, r mi red si, |
mi4 r r2 |
r r8 la sold mi |
r la sold mi sol!4 r |
dod1 |
re8 re' dod' la re sib la fa |
sib,4 r r2 |
mib4 r8 fa sol4 r8 fa |
mi!4 r r2 | \allowPageTurn
r fa4 r8 sib |
lab4 r8 lab sol4 r |
R1 | \allowPageTurn
do4 r8 mib' re'4 r8 do' |
sib4 r8 lab sol4 r |
do r8 reb do4 r |
fa4 r8 lab re fa si,! re |
sold,4 r r2 |
r r8 la,!32(\f si, do re) mi!8-. mi-. |
fa4( sold,8) r r2 |
dod4 r r2 |
R1 | \allowPageTurn
re4 r mi2 |
la,8 la sold mi r la sold mi |
la16-. si( do' sold) la( sol fa mi) red4 r |
si,1 |
mi4 re! do8 do mi sol |
do4 r dod r |
R1 | \allowPageTurn
r8 fa! fa mi re4 r |
re r8 re red4 r |
r mi la,2 |
