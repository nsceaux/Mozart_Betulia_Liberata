\ffclef "bass" <>^\markup\character Achior
r8 re re mi fad4 r |
fad4 fad8 sol la la r la |
la4 la8 si do' do' do' si |
sol4 r
\ffclef "tenor/G_8" <>^\markup\character Ozia
re'4 si8 re' |
re' sol r4 re'8 re'16 re' re'8 mi' |
do' do' r16 mi' do' si sol4 r |
si8 si16 si si8 red' si si red' red'16 mi' |
fad'4 r8 fad' fad' si si do' |
la la la la16 si sol4 r16 si si do' |
re'4 re'8 mi' do' do' r do'16 la |
red'4 red'8 mi' mi' si r4 |
R1 |
\ffclef "soprano/treble" <>^\markup\character Amital
r4 r8 sol' do'' do'' r mi'' |
re'' re'' r la'16 si' do''4 re''8 la' |
si' si' r16 si' si' si' si'8 fad' r16 fad' fad' sol' |
la'4 la'8 la'16 la' la'4 la'8 do'' |
la' la' r fad' la' la' la' sol' |
mi' mi' r mi''16 dod'' lad'4 lad'8 si' |
si' fad' r4 r8 sol' sol' la' |
si' si' r4 si'8 si'16 do''! re''8 mi'' |
do''4 r do'' re''8 mi'' |
re'' si' r si' re'' re'' re'' do'' |
la' la' r16 la' si' do'' do''8 sol' r sol'16 la' |
sib'4 sib'8 la' fa'4 r |
la'4 la'8 do'' do'' fa' r la'16 sib' |
do''4 do''8 re'' sib' sib' r4 |
sib'4 sib'8 do'' lab' lab' r re''16 fa'' |
fa''4 lab'8 sib' sol'4 sol''8 mib''16 re'' |
sib'8 sib' r4 r2 |
