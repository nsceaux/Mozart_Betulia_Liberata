Con -- fu -- so io son; sen -- to se -- dur -- mi, e pu -- re
ri -- tor -- no a du -- bi -- tar.

Quan -- do il co -- stu -- me
al -- la ra -- gion con -- tra -- sta,
av -- vien co -- sì. Tal di ne -- glet -- ta ce -- tra
mu -- si -- ca man le ab -- ban -- do -- na -- te cor -- de
sten -- ta a tem -- prar, per -- chè vi -- bra -- te ap -- pe -- na
si ral -- len -- tan di nuo -- vo.

Ah dim -- mi, O -- zi -- a,
che si fa, che si pen -- sa? Io non in -- ten -- do,
che vo -- glia dir que -- sto si -- len -- zio e -- stre -- mo,
a cui pas -- sò Be -- tu -- lia
dall’ e -- stre -- mo tu -- mul -- to. Il no -- stro sta -- to
pun -- to non mi -- glio -- rò. Cre -- sco -- no i ma -- li;
e sce -- man le que -- re -- le, og -- nun chie -- de -- a
ier’ ai -- ta, e pie -- tà: stu -- pi -- do o -- gnu -- no
og -- gi pas -- sa e non par -- la. Ah par -- mi que -- sto
un pre -- sa -- gio per noi trop -- po fu -- nes -- to!
