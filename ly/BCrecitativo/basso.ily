\clef "bass" fad1~ |
fad~ |
fad |
sol~ |
sol |
mi2 r4 re |
red1~ |
red~ |
red2 mi~ |
mi la,~ |
la, r4 si, |
mi1 |
do |
fad |
red~ |
red~ |
red |
mi |
r4 fad sol2~ |
sol1 |
mi |
sold |
do2 mi~ |
mi fa~ |
fa1~ |
fa2 re~ |
re sib,2~ |
sib, mib |
r4 fa sib,2 |
