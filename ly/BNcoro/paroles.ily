\tag #'(vsoprano valto vtenore vbasso) {
  Lo -- di al gran Di -- o, che op -- pres -- se
  gli em -- pi ne -- mi -- ci suo -- i,
  che com -- bat -- té per noi,
  \tag #'(vsoprano vbasso) {
    che tri -- on -- fò co -- sì.
  }
  \tag #'valto {
    che tri -- on -- fò __ co -- sì.
  }
  \tag #'vtenore {
    che tri -- on -- fò,
    che tri -- on -- fò __ co -- sì.
  }
}
\tag #'giuditta {
  Ven -- ne l’As -- si -- ro, e in -- tor -- no
  con le fa -- lan -- gi Per -- se,
  con __ le fa -- lan -- gi Per -- se,
  le val -- li ri -- co -- per -- se,
  i fiumi i -- na -- ri -- dì,
  i fiumi i -- na -- ri -- dì.
  Par -- ve o -- scu -- ra -- to il gior -- no,
  par -- ve o -- scu -- ra -- to il gior -- no,
  par -- ve con quel cru -- de -- le
  al ti -- mi -- do I -- sra -- e -- le
  giun -- to l’e -- stre -- mo dì,
  giun -- to l’e -- stre -- mo dì.
}
\tag #'(vsoprano valto vtenore vbasso) {
  Lo -- di al gran Di -- o, che op -- pres -- se
  gli em -- pi ne -- mi -- ci suo -- i,
  che com -- bat -- té per noi,
  \tag #'(vsoprano vtenore vbasso) {
    che tri -- on -- fò co -- sì.
  }
  \tag #'valto {
    che tri -- on -- fò __ co -- sì.
  }
}
\tag #'giuditta {
  Fiam -- me, ca -- te -- ne, e mor -- te
  ne mi -- nac -- ciò fe -- ro -- ce,
  al -- la ter -- ri -- bil vo -- ce
  Be -- tu -- lia im -- pal -- li -- dì,
  Be -- tu -- lia im -- pal -- li -- dì.
  Ma in -- a -- spet -- ta -- ta sor -- te
  l’e -- stin -- se in un mo -- men -- to,
  e co -- me neb -- bia al ven -- to,
  tan -- to fu -- ror spa -- rì,
  tan -- to fu -- ror spa -- rì.
}
\tag #'(vsoprano valto vtenore vbasso) {
  Lo -- di al gran Di -- o, che op -- pres -- se
  gli em -- pi ne -- mi -- ci suo -- i,
  che com -- bat -- té per
  \tag #'(vsoprano valto) noi,
  \tag #'(vtenore vbasso) { no -- i, }
  \tag #'(vsoprano vtenore vbasso) {
    che tri -- on -- fò co -- sì.
  }
  \tag #'valto {
    che tri -- on -- fò __ co -- sì.
  }
}
\tag #'giuditta {
  Di -- sper -- si, ab -- ban -- do -- na -- ti
  i bar -- ba -- ri fug -- gi -- ro;
  si spa -- ven -- tò l’As -- si -- ro,
  il Me -- do in -- or -- ri -- dì.
  Ne fur Gi -- gan -- ti u -- sa -- ti
  ad as -- sa -- lir le stel -- le,
  ad as -- sa -- lir le stel -- le,
  fu don -- na so -- la e im -- bel -- le
  quel -- la, che gli at -- ter -- rì,
  che gli at -- ter -- rì.
}
\tag #'(vsoprano valto vtenore vbasso) {
  Lo -- di al gran Di -- o,
  \tag #'vsoprano { che op -- pres -- se }
  \tag #'(valto vtenore vbasso) { che op -- pres -- se }
  gli em -- pi ne -- mi -- ci suo -- i,
  che com -- bat -- té per no -- i,
  che tri -- on -- fò,
  \tag #'(vsoprano valto) {
    che tri -- on -- fò __ co -- sì.
  }
  \tag #'(vtenore vbasso) {
    che tri -- on -- fò co -- sì.
  }
}
\tag #'(vsoprano valto vtenore vbasso) {
  So -- lo di tan -- te squa -- dre
  veg -- ga -- si il du -- ce e -- stin -- to,
  veg -- ga -- si il du -- ce e -- stin -- to;
  sciol -- ta è Be -- tu -- lia,
  sciol -- ta è Be -- tu -- lia,
  o -- gni ne -- mi -- co è vin -- to,
  o -- gni ne -- mi -- co è vin -- to, è vin -- to.
  Al -- ma, i ne -- mi -- ci re -- i
  che t’in -- si -- di -- an la lu -- ce
  i vi -- zi son, ma la su -- per -- bia è il du -- ce.
  Spe -- gni -- la, spe -- gni -- la, e spen -- to in le -- i
  tut -- to il se -- gua -- ce stuo -- lo,
  tut -- to il se -- gua -- ce stuo -- lo,
  mie -- te -- rai mil -- le pal -- me a un col -- po so -- lo,
  mie -- te -- ra -- i mil -- le pal -- me a un col -- po so -- lo,
  a un col -- po so -- lo.
}
