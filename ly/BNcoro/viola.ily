\clef "alto" sol'8 si'~ si'16( la' sol' fad') mi'8 mi' sol' si' |
fad' red'4 fad'8 si sol' mi' sol' |
fad' red'4 fad'8 si4 do'8 re' |
mi'4 mi'16-. re'-. do'-. si-. la-. fad-. si-. la-. sol8 sol' |
fad'4 mi'16( si) la( fad) mi8 si do' si |
mi' mi' mi' mi' fad' fad' fad' fad' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
mi' mi' mi' mi' do' do' sol' sol' |
re' re' re' re' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
fad' fad' fad' fad' dod' dod' dod' dod' |
do'! do' do' do' si si la la |
si si si si si si si si |
mi' mi' mi' mi' fad' fad' fad' fad' |
sol' sol' si si do' do' re' re' |
sol8 sol\p re' re' re' re' re' re' |
<re' si>1 |
<mi' do'>~ |
q |
<re' si> |
la8 la la la re' re' re' re' |
sol\f sol sol sol sol sol'\p sol' sol' |
fad' fad' fad' fad' fad' fad' fad' fad' |
mi' mi' mi' mi' mi' mi' re' re' |
dod' dod' dod' dod' dod' dod' dod' dod' |
re' re' re' re' fad' fad' fad' fad' |
sol' sol' sol' sol' mi' mi' mi' mi' |
la\f la la la la la la la |
la2:16\p la:16 |
sib:16 sib:16 |
fad!:16 fad:16 |
sol:16 sol:16 |
sib:16 sib:16 |
mi':16 si!:16 |
sol'8 sol' sol' sol' sol' sol' sol' sol' |
mi' mi' mi' mi' fad' fad' fad' fad' |
dod' dod' dod' dod' re' re' re' re' |
sol' sol' sol' sol' la' la' la' la' |
si' si' sol' sol' fad' fad' sol' sol' |
la' la' la' la' la la la la |
\tuplet 3/2 { re'8( fad' la') } la'4 \tuplet 3/2 { sol'8( mi' dod') } dod'4 |
re'4 <re' la'> fad' red' |
mi'2 \tuplet 3/2 { do''8( si' la') } \tuplet 3/2 { sol'( fad' mi') } |
si4 si si si |
mi'8 mi' mi' mi' fad' fad' fad' fad' |
sol' sol' sol' sol' mi' mi' mi' mi' |
si si si si do' do' do' do' |
re' re' re' re' mi' mi' re' re' |
do' do' do' do' re' re' re' re' |
mi' mi' mi' mi' mi' mi' mi' mi' |
do' do' do' do' dod' dod' dod' dod' |
re' re' re' re' mi' mi' mi' mi' |
re' re' re' re' re' re' re' re' |
red' red' red' red' mi' mi' mi' mi' |
si si si si si si si si |
mi'4 r r dod' |
dod' r r fad' |
fad' fad' sol' dod' |
si16\p re' re' re' re'4:16 re'2:16 |
red':16 red'4:16 si:16 |
si8 r red' r mi' r si r |
<si mi>16\f q q q q4:16 q2:16 |
<sol si>1\p |
\ru#2 \tuplet 3/2 { sol8-. sol-. sol-. } mi'8*2/3 mi' mi' mi' mi' mi' |
fa' fa' fa' fa' fa' fa' sol' sol' sol' sol' sol' sol' |
la' la' la' la' la' la' mi' mi' mi' mi' mi' mi' |
fa' fa' fa' fa' fa' fa' fa fa fa fa fa fa |
sol sol sol sol sol sol sol sol sol sol sol sol |
<sol do>1\f |
<mi mi'>\p | \allowPageTurn
la8 la la la la la la la |
red' red' red' red' red' red' red' red' |
mi' mi' mi' mi' mi' mi' mi' mi' |
lad lad lad lad lad lad lad lad |
si si dod' dod' red' red' mi' mi' |
red'4 mi' la8 la la la |
si si si si si si si si |
do' do' sol' sol' la' la' la' la' |
si' si' si' si' si si si si |
si8\f si si si si si si si |
la la la la la la la la |
si si si si mi' mi' mi' mi' |
mi'4 la'2 sol'8 fad' |
mi' si do' sol' la' fad' si' si |
mi' mi' mi' mi' fad' fad' fad' fad' |
sol' sol' sol' mi' red' red' red' red' |
mi' mi' mi' mi' mi' mi' fad' fad' |
sol' fad' mi' re'! do' la re' re' |
sol sol' si' sol' fad' la' re' fad' |
sol' si' fad' re' dod' mi' dod' la |
re' re' re' re' dod' dod' dod' dod' |
re' re' re' re' red' red' red' red' |
mi' mi' mi' mi' mi' mi' do'! sol |
la la si si mi' fad' sol' la' |
si' si red' si mi' fad' sol' sold' |
la' si' dod'' la' re'' re' fad' la' |
sol' la' si' re'' do''! sol' mi' do' |
fa' re' sol' fa' mi' mi' fa' sol' |
do'2:16\p do':16 |
re':16 re':16 |
do'8 do' do' do' mi' mi' mi' mi' |
fa' fa' fa' fa' sol' sol' sol' sol' |
do'\f do' do' do' do' do' do' do' |
do'\p do' do' do' do' do' si si |
lad\f lad'\p[ lad' lad'] lad'[ lad' lad' lad'] |
si' si' mi' mi' fad'! fad' fad' fad' |
si\f si si si la! la la la |
sol\p sol sol sol sol sol sol sol |
la la la la la la la la |
fad'8 fad' fad' fad' fad' fad' fad' fad' |
re' re' re' re' re' re' re' re' |
do' do' do' do' re' re' re' re' |
<sol' si'>1 |
<fad' la'> |
<mi' sol'>2 do' |
do' si |
sol8 sol sol sol la la la la |
si si si si si si si si |
si\f si si si si' si' si' si' |
mi' mi' mi' mi' re'! re' la' la' |
sol'8 sol' sol' sol' la' la' la' la' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' sol' sol' |
do' do' do' do' sol' sol' sol' sol' |
re' re' re' re' re' re' re' re' |
sol la16 si do' re' mi' fad' sol' la' si' la' sol' la' si' la' |
sol'4 r r2 |
sol8 la16 si do' re' mi' fad' sol' la' si' la' sol' la' si' la' |
sol'4 r r2 | \allowPageTurn
sol'8 sol' sol' sol' fad' fad' mi' mi' |
la' la' la' la' sol' sol' sol' sol' |
re' re' re' re' mi' mi' mi' mi' |
si si si si si si si si |
mi' mi' mi' mi' mi' dod' mi' la' |
la' fad' la' re' re' sol'16 la' si'8 si' |
mi' la' dod' mi' re' fad' la' la' |
<< la' \\ { fad' re' fad' la' } >> re'' fad' la' fad' |
fad'2:16 fad':16 |
sol':16 sol':16 |
mi':16 mi':16 |
fad':16 fad':16 |
fad'4 r la' la |
la8 la la la la la la la |
re' re' re' re' re' re' re' re' |
dod' dod' dod' dod' dod' dod' dod' dod' |
si si si si mi' mi' mi' mi' |
la la la la la la la la |
re' re' re' re' re' re' re' re' |
dod' dod' dod' dod' re' re' re' re' |
mi' mi' mi' mi' mi' mi' mi' mi' |
la si dod' re' mi' fad' sold' la' |
re' re' re' re' mi' mi' mi' mi' |
fad' fad' fad' fad' fad' fad' re' re' |
mi' mi' mi' mi' mi' mi' mi' mi' |
dod' dod' la la mi' mi' mi' mi' |
la' la dod' mi' la' la' dod'' la' |
fad' fad la dod' fad' fad' la' fad' |
re' re' fad' re' mi' mi' mi' mi' |
la' la si dod' re' mi' fad' sold' |
la'2:16 mi':16 |
mi':16 mi':16 |
dod'2:16 dod':16 |
si:16 re'4:16 mi'4:16 |
re'2:16 re':16 |
mi':16 mi':16 |
dod'':16 si':16 |
fad':16 fad':16 |
fad'8 fad' fad' fad' fad' fad' fad' fad' |
si si si si si si si si |
la'! la' la' la' la' la' la' la' |
sol' sol' sol' sol' sol' sol' sol' sol' |
dod' dod' dod' dod' dod' dod' dod' dod' |
re' re' fad' la' re'' la' fad' la' |
re' re' fad' fad' la' la' do'' do'' |
si' si' si' si' si si si si |
mi' mi' sold' sold' si' si' re'' re'' |
dod'' dod'' dod'' dod'' sol'! sol' fad' fad' |
dod' dod' dod' dod' mi' mi' re' re' |
dod'4 re' r re' |
sol'8 sol' sol' sol' la' la' la' la' |
re'4 r fad'8 fad' sol' sol' |
do'' do'' si' si' fad' fad' sol' sol' |
do''4 si' sol'8 sol' sol' sol' |
sol' sol' fad' sol' la' la' la' la' |
re' re' re' re' fad' fad' fad' fad' |
sol' sol' sol' sol' si' si' si' si' |
la' la' la' la' la' la' la' la' |
la la la la la la la la |
re' re' fad' re' sol' mi' la' sol' |
fad' re' fad' re' sol' mi' la' la |
re' re'' dod'' re'' la' re'' fad' la' |
re'2 r |
