\clef "treble" \transposition sol <>
<<
  \tag #'(corno1 corni) { do''4 s8 mi'' mi''4 }
  \tag #'(corno2 corni) { mi'4 s8 mi' mi'4 }
  { s4 r8 }
>> r4 |
R1*2 |
r2 r8 \tag#'corni <>^"a 2." mi'8 mi'4 |
R1 |
<<
  \tag #'(corno1 corni) {
    do''2 re'' |
    do''4 do'' do'' do'' |
    do''8 do''16 do'' do''8 do'' do'' do''16 do'' do''8 do'' |
    do''4 do''8 do'' do''2 |
    re''2 do''4 s |
    re'' s re'' s |
  }
  \tag #'(corno2 corni) {
    mi'2 sol' |
    mi'4 mi' mi' mi' |
    do'8 do'16 do' do'8 do' do' do'16 do' do'8 do' |
    mi'4 mi'8 mi' do'2 |
    sol'2 mi'4 s |
    do' s do' s |
  }
  { s1*4 | s2. r4 | s4 r s r | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | }
  { sol'2 do'' | }
>>
<<
  \tag #'(corno1 corni) {
    re''2 do''4 s |
    mi'' s mi'' s |
    do''2 re'' |
    do'' do''4 re'' |
    do''8
  }
  \tag #'(corno2 corni) {
    sol'2 mi'4 s |
    mi' s mi' s |
    mi'2 sol' |
    do' do'4 sol |
    do'8
  }
  { s2. r4 | s r s r | }
>> r8 r4 r2 |
R1*5 |
<>\f <<
  \tag #'(corno1 corni) { do''2 do''4 }
  \tag #'(corno2 corni) { mi'2 mi'4 }
>> r4 |
R1*5 |
\tag#'corni <>^"a 2." re''1 |
R1*2 |
<<
  \tag #'(corno1 corni) { sol'1 | do' }
  \tag #'(corno2 corni) { sol | do' }
>>
R1*8 |
<>\f <<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>> \twoVoices #'(corno1 corno2 corni) << re''2 re'' >> |
<<
  \tag #'(corno1 corni) {
    re''4 s mi'' s |
    s1 |
    mi''4 mi'' mi'' s |
    do''2 re'' |
    do''1 |
    do'' |
    do''8 do''16 do'' do''8 do'' do'' do''16 do'' do''8 do'' |
  }
  \tag #'(corno2 corni) {
    sol'4 s mi' s |
    s1 |
    mi'4 mi' mi' s |
    mi'2 sol' |
    mi'1 |
    do' |
    do'8 do'16 do' do'8 do' do' do'16 do' do'8 do' |
  }
  { s4 r s r | R1 | s2. r4 | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | do'' | }
  { do''2 sol' | mi'1 | }
>>
<<
  \tag #'(corno1 corni) {
    re''4 s re'' s |
    re''2 do'' |
  }
  \tag #'(corno2 corni) {
    do'4 s do' s |
    sol'2 do' |
  }
  { s4 r s r | }
>>
\tag#'corni <>^"a 2." sol'1 |
mi' |
mi'4 mi'8 mi' mi'4 mi'8 mi'16 mi' |
<<
  \tag #'(corno1 corni) { mi'4 }
  \tag #'(corno2 corni) { do' }
>> r4 r2 |
R1*2 |
\tag#'corni <>^"a 2." <>\p mi'1~ |
mi' |
R1 |
<>\f <<
  \tag #'(corno1 corni) { mi'1 | }
  \tag #'(corno2 corni) { do'1 | }
>>
R1*13 |
<>\p \twoVoices #'(corno1 corno2 corni) << mi'2 mi' >> r |
R1*3 |
<>\f <<
  \tag #'(corno1 corni) { do''1 | }
  \tag #'(corno2 corni) { mi' | }
>>
r4 \twoVoices #'(corno1 corno2 corni) << re''2 re'' >> <<
  \tag #'(corno1 corni) { fa''4 }
  \tag #'(corno2 corni) { re'' }
>>
\twoVoices #'(corno1 corno2 corni) << mi''4 mi'' >> r r2R1 |
r2 r4 <<
  \tag #'(corno1 corni) {
    mi''4 |
    do''2 re'' |
  }
  \tag #'(corno2 corni) {
    mi'4 |
    mi'2 sol' |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''2 mi'' |
    do''2. re''4 |
    mi''2 re'' | }
  { mi'1~ |
    mi'2 do'4 sol' |
    do''2. sol'4 | }
>>
<<
  \tag #'(corno1 corni) { do''4 s re''2 | mi''4 }
  \tag #'(corno2 corni) { mi'4 s sol'2 | do''4 }
  { s4 r }
>> r4 \twoVoices #'(corno1 corno2 corni) << re''2 re'' >> |
<<
  \tag #'(corno1 corni) { re''2 }
  \tag #'(corno2 corni) { sol' }
>> \twoVoices #'(corno1 corno2 corni) << re''2 re'' >> |
<<
  \tag #'(corno1 corni) { re''2 mi'' | do''4 }
  \tag #'(corno2 corni) { sol'2 mi' | mi'4 }
>> r4 r2 |
r4 \twoVoices #'(corno1 corno2 corni) <<
  { mi'8 mi'16 mi' }
  { mi'8 mi'16 mi' }
>> <<
  \tag #'(corno1 corni) { mi'4 }
  \tag #'(corno2 corni) { do' }
>> r4 |
\twoVoices #'(corno1 corno2 corni) << mi'4 mi' >> r4 r2 |
\twoVoices #'(corno1 corno2 corni) << re''4 re'' >> r4 r2 |
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { do' }
>> r8 \twoVoices #'(corno1 corno2 corni) <<
  { do'16 do' do'4 }
  { do'16 do' do'4 }
>> r4 |
r r8 \twoVoices #'(corno1 corno2 corni) <<
  { do'16 do' do'4 }
  { do'16 do' do'4 }
>> r4 |
R1*8 |
r2 <>\f \twoVoices #'(corno1 corno2 corni) << sol'2 sol' >> |
<<
  \tag #'(corno1 corni) { sol'8 }
  \tag #'(corno2 corni) { do' }
>> r8 r4 r2 |
R1*10 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { do''2 mi'' |
    s4 re'' re''2 |
    mi''2 re'' | }
  { mi'1 |
    s4 re'' sol'2 |
    do'4 do''2 sol'4 | }
  { s1 | r4 }
>>
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { do' }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { sol'1 | }
  \tag #'(corno2 corni) { do' }
>>
R1 |
<<
  \tag #'(corno1 corni) {
    sol'1 |
    do''8 do''16 do'' do''8 do'' do'' do''16 do'' do''8 do'' |
  }
  \tag #'(corno2 corni) {
    do'1 |
    do'8 do'16 do' do'8 do' do' do'16 do' do'8 do' |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 | }
  { do''2 sol' | }
>>
<<
  \tag #'(corno1 corni) {
    do''8 do'' do'' do'' do'' do'' do'' do'' |
    do''4
  }
  \tag #'(corno2 corni) {
    mi'8 do' do' do' do' do' do' do' |
    do'4
  }
>> r4 r2 |
r8 <<
  \tag #'(corno1 corni) {
    do''8 do'' do'' do'' do'' do'' do'' |
    do''4
  }
  \tag #'(corno2 corni) {
    do'8 do' do' do' do' do' do' |
    do'4
  }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { do''2 re''4 }
  \tag #'(corno2 corni) { do'2 sol'4 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { do''4 | re''2 }
  { do''4 | re''2 }
>> <<
  \tag #'(corno1 corni) {
    mi''2 |
    mi''4 s mi'' s |
    mi'' s mi''
  }
  \tag #'(corno2 corni) {
    do''2 |
    mi'4 s mi' s |
    mi' s mi'
  }
  { s2 | s4 r s r | s r }
>> r8 \twoVoices #'(corno1 corno2 corni) <<
  { mi'16 mi' | mi'4 }
  { mi'16 mi' | mi'4 }
>> r4 r2 |
\tag #'(corno1 corni) <>^"Corni in D." R1*2 |
\transposition re |
r8 \tag#'corni <>^"a 2." do'8 mi' sol' do'' mi'' sol'' mi'' |
\twoVoices #'(corno1 corno2 corni) <<
  { do''1 | re'' | re'' | mi'' | }
  { do''2 do'4 do' | sol'1 | sol' | do'' | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 mi''8 do'' sol''4 sol' |
    do''1 |
    mi'' |
    re'' |
  }
  \tag #'(corno2 corni) {
    do'4 mi'8 do' sol'4 sol |
    do'1 |
    do'' |
    sol' |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''2 re''4 re'' | re''1 | }
  { re''2 re''4 re'' | sol'1 | }
>>
<<
  \tag #'(corno1 corni) { mi''2 fad'' | }
  \tag #'(corno2 corni) { do''2 re'' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { sol''2 mi'' | re''1 | }
  { sol'1 | re'' | }
>>
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { mi''2 }
  \tag #'(corno2 corni) { do'' }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''2 | mi'' }
  { re'' | mi'' }
>> <<
  \tag #'(corno1 corni) {
     mi''2 |
     re''1 |
     re''4 re''8 re''
  }
   \tag #'(corno2 corni) {
     do''2 |
     re''1 |
     sol'4 sol'8 sol'
  }
>> \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' |
    re''1 |
    mi'' |
    mi''2 re'' | }
  { re''4 re'' |
    sol'1 |
    mi' |
    do''2 re'' | }
>>
<<
  \tag #'(corno1 corni) { re''4 }
  \tag #'(corno2 corni) { sol' }
>> r4 r2 |
<<
  \tag #'(corno1 corni) { re''1 | mi'' | mi'' | mi'' | }
  \tag #'(corno2 corni) { sol'1 | mi' | mi' | mi' | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1 | fa'' | mi''2 }
  { mi'2 do'' | re''1 | mi''2 }
>> r2 |
<<
  \tag #'(corno1 corni) {
    mi''2 mi'' |
    mi''4 mi''8 mi'' mi''4 mi'' |
    do''1 |
  }
  \tag #'(corno2 corni) {
    mi'2 mi' |
    mi'4 mi'8 mi' mi'4 mi' |
    mi'1 |
  }
>>
\twoVoices #'(corno1 corno2 corni) << mi''2 mi'' >> r |
<<
  \tag #'(corno1 corni) {
    fa''2 s |
    re'' s |
    mi''1 |
  }
  \tag #'(corno2 corni) {
    re''2 s |
    sol' s |
    do''1 |
  }
  { s2 r | s r | }
>>
\twoVoices #'(corno1 corno2 corni) << do''4 do'' >> <<
  \tag #'(corno1 corni) {
    do''8 do'' do''4 do'' |
    do''1 |
  }
  \tag #'(corno2 corni) {
    do'8 do' do'4 do' |
    do'1 |
  }
>>
r4 \twoVoices #'(corno1 corno2 corni) <<
  { re''4 re'' re'' | }
  { re''4 re'' re'' | }
>>
<<
  \tag #'(corno1 corni) {
    re''4 s re'' mi'' |
    fa'' r fa'' mi'' |
    re'' do'' do''2 |
    do'' mi''4 re'' |
    do''1~ |
    do''~ |
    do'' |
    re''4 mi''2 re''4 |
    do''1 |
    do'' |
    mi'' |
    re'' |
    do''4 s s re'' |
    mi'' s s re'' |
    do'' do'' do'' do'' |
    do''2
  }
  \tag #'(corno2 corni) {
    sol'4 s sol' do'' |
    re'' s sol' sol' |
    sol' sol' do'2 |
    do' do''4 sol' |
    do'1~ |
    do'~ |
    do' |
    sol'4 do''2 sol'4 |
    do'1 |
    do' |
    do'' |
    sol' |
    do'4 s s sol' |
    do'' s s sol' |
    mi' mi' mi' mi' |
    mi'2
  }
  { s4 r s2 | s4 r s2 | s1*10 | s4 r r s | s r r s | }
>> r2 |
