\clef "treble" mi'' sol''16( fad'') mi''( red'') mi''8 si' si''4~ |
si''16( la'' sol'' fad'') sol''( fad'' mi'' red'') mi''8 si' si''4~ |
si''16( la'' sol'' fad'') sol''( fad'' mi'' red'') mi''( re'' do'' si') do''( si' la' sold') |
la'( si') do''-. si'-. la'-. sol'!-. fad'-. mi'-. red'8 red'' mi'' si'' |
do'''8 do''16( la') sol'( mi') fad'( red') mi'8 mi'' fad' red'' |
mi'16 mi'' sol'' fad'' mi'' re''! do'' si' la' fad'' la'' sol'' fad'' mi'' re'' do'' |
si' sol' re' sol' si' sol' si' re'' sol'' re'' si' re'' sol'' re'' sol'' si'' |
re''' re''' re''' re''' re''' si'' sol'' re'' si'' si'' si'' si'' si'' sol'' re'' si' |
sol'' sol'' sol'' sol'' sol'' mi'' si' sol' do'' mi'' sol'' do'' si' re'' sol'' si' |
la' sol'' la'' sol'' la' fad'' la'' fad'' sol'' re'' si'' la'' sol'' fad'' mi'' re'' |
dod'' la' mi'' dod'' la'' mi'' dod'' mi'' dod'' la' mi'' dod'' la'' mi'' dod'' mi'' |
re'' la' fad'' re'' la'' fad'' re'' fad'' mi'' la' sol'' mi'' la'' sol'' mi'' sol'' |
fad'' la' fad'' re'' la'' fad'' re'' fad'' sol'' re'' do'' si' do'' la' sol' fad' |
sol' fad' sol' mi' mi'' red'' mi'' sol' fad' mi' fad' si fad'' mi'' fad'' red'' |
mi'' fad'' sol'' fad'' mi'' re''! do'' si' la' sol' fad' mi' re' la' re'' do'' |
si' re'' sol'' fad'' sol'' fa'' mi'' re'' mi'' fad'' sol'' si' la' sol'' la' fad'' |
<<
  \tag #'violino1 {
    sol''8\p re''4 sol''8 la''4( re''8) do''-. |
    si'4 r sol''16( la'' si'' la'') sol''( fad'' mi'' re'') |
    mi''4 r mi''16( do'' re'' mi'') fa''( sol'' la'' si'') |
    do'''4 r mi''16( fad''! sol'' fad'') sol''( mi'' re'' do'') |
    re''4 r sol'16( la' si' do'') re''( mi'' fad'' sol'') |
    sol''4( fad''8) mi''-. re''4. do''8 |
    si'16(\f sol') si'-. re''-. sol''( re'') sol''( re'') sol''8 re''\p re'' re'' |
    re'' re'' re'' re'' re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' dod'' dod'' si' si' |
    la' la' la' la' mi'' mi'' mi'' mi'' |
    fad'' fad'' fad'' fad'' la'' la'' la'' la'' |
    si'' si'' si'' si'' re'' re'' re'' re'' |
    dod''16\f mi' mi' mi' mi'4:16 mi'2:16 |
    fa':16\p fa':16 |
    fa':16 fa':16 |
    re':16 re':16 |
    re':16 re':16 |
    sol':16 sol':16 |
    sol':16 fad'!:16 |
    si'!8 si' si' si' si' si' si' si' |
    dod'' dod'' dod'' dod'' re'' re'' re'' re'' |
    <la' sol''> q q q <la' fad''> q q q |
    si'' si'' si'' si'' <dod'' mi'> q q q |
    re'' re'' si'' si'' la'' la'' sol'' sol'' |
    fad'' fad'' fad'' fad'' mi'' mi'' mi'' mi'' |
  }
  \tag #'violino2 {
    sol''8 si'\p si' si' la' la' la' la' |
    sol'16( re' sol' si') sol'( re' sol' si') sol'4 r |
    sol'16( mi' sol' do'') sol'( mi' sol' do'') sol'4 r |
    sol'16( mi' sol' do'') sol'( mi' sol' do'') sol'4 r |
    sol'16( re' sol' si') sol'( re' sol' si') sol'4 r |
    do''8 do'' do'' do'' <la' fad'>8 q q q |
    sol'16(\f re') sol'-. si'-. re''( si') re''( si') re''8 si'\p si' si' |
    la' la' la' la' la' la' la' la' |
    sol' sol' sol' sol' sol' sol' fad' fad' |
    mi' mi' mi' mi' la' la' la' la' |
    la' la' la' la' re'' re'' re'' re'' |
    re'' re'' re'' re'' si' si' si' si' |
    la'16\f dod' dod' dod' dod'4:16 dod'2:16 |
    dod':16\p dod':16 |
    re':16 re':16 |
    la:16 la:16 |
    sib:16 sib:16 |
    re':16 re':16 |
    dod':16 re':16 |
    re'8 re' re' re' mi' mi' mi' mi' |
    la' la' la' la' la' la' la' la' |
    <la' mi''> q q q re'' re'' re'' re'' |
    re'' re'' re'' re'' sol' sol' sol' sol' |
    fad' re''4 re'' re'' re''8 |
    re'' re'' re'' re'' dod'' dod'' dod'' dod'' |
  }
>>
re''8\f re'' re''8.\trill dod''32 re'' \tuplet 3/2 { dod''8( mi'' sol'') } sol''8.\trill fad''32 sol'' |
fad''8*2/3( la'' do''!) do''8.\trill si'32 do'' si'8*2/3( fad'' la'') la''8.\trill sol''32 la'' |
sol''8*2/3( si'') red''-. mi''( sol'') si'-. do''( si' la') sol'( fad' mi') |
si si' si si si si si <<
  \tag #'violino1 { do'' si' la' sol' fad' | }
  \tag #'violino2 { la' sol' fad' mi' red' | }
>>
mi'-. sol'-. si'-. mi''-. sol''-. si''-. la'' fad'' re'' la' fad' re'! |
sol si re' sol' si' re'' sol'' si'' fad'' sol'' mi'' si' |
re' sol' si' re'' sol'' si'' mi' sol' si' mi'' sol'' si'' |
re' sol' si' re'' sol'' si'' do'' sol'' do''' re'' sol'' si'' |
mi'' sol'' la'' sol'' mi'' la' re'' fad'' la'' re' do'' fad'' |
mi' sol' si' mi'' sol'' la'' si'' sol'' mi'' si' sol' mi' |
do' mi' la' do'' mi'' la'' dod' mi' la' dod'' mi'' la'' |
re' sol' la' re'' sol'' la'' mi' sol' la' dod'' sol'' la'' |
re' fad' la' re'' fad'' la'' do'! fad' la' do'' fad'' la'' |
si red' fad' si' fad'' la'' si mi' sol' si' mi'' sol'' |
si sol' si' mi'' sol'' si'' fad' mi'' fad'' si red'' fad'' |
mi''4 sol''16( fad'' sol'' la'') sol''8 mi'' dod'' si' |
lad'4 dod'''16( si'' lad'' si'') dod'''8 lad'' fad'' mi'' |
re'' si'' si' fad'' mi' sol'' fad' lad' |
<<
  \tag #'violino1 {
    si'8.\p fad'16 si'8. re''16 fad''8. si'16 re''8. fad''16 |
    si''8. si16 red'8. fad'16 si'8. red''16 fad''8. red''16 |
    mi''16 si' si' si' fad'' si' si' si' sol'' si' si' si' la'' si' si' si' |
    sol''\f <sol' si> q q q4:16 q2:16 |
    \tuplet 3/2 { sol'8-.\p si'-. si'-. } \tuplet 3/2 { si'-. re''-. re''-. } re''8*2/3-. sol''-. sol''-. sol''( la'' fa'') |
    \ru#2 \tuplet 3/2 { mi''8-. mi''-. mi''-. } sol''8*2/3 sol'' sol'' sol'' sol'' sol'' |
    la'' la'' la'' la'' la'' la'' re'' re'' re'' re'' re'' re'' |
    do'' do'' do'' do'' do'' do'' sol'' sol'' sol'' sol'' sol'' sol'' |
    la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' la'' |
    mi'' mi'' mi'' mi'' mi'' mi'' re'' re'' re'' re'' re'' re'' |
    do''8\f mi''( red'' mi'') fa''( mi'' red'' mi'') |
    r re''!(\p dod'' re'') mi''( re'' dod'' re'') |
    do''! do'' do'' do'' do'' do'' do'' do'' |
    do''8 fad''!4 la'' do'' la'8 |
    sol' sol''4 sol' sol'' sol'8 |
    mi'8*2/3 mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' mi' |
    red'16 si' si' si' mi' si' si' si' fad' si' si' si' sol' si' si' si' |
    la'4 sol' do''8 do'' do'' do'' |
    si' si' si' si' red' red' red' red' |
    mi' sol' si' si' do'' do'' do'' do'' |
    sol' sol' sol' sol' fad' fad' fad' fad' |
  }
  \tag #'violino2 {
    si'16\p fad' fad' fad' fad'4:16 fad'2:16 |
    fad':16 fad':16 |
    sol'8 r la' r sol' r fad' r |
    <mi' sol>16\f q q q q4:16 q2:16 |
    <re' sol>1\p | \allowPageTurn
    \ru#2 \tuplet 3/2 { do'8-. do'-. do'-. } do''8*2/3 do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' si' si' si' si' si' si' |
    do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' do'' |
    do'' do'' do'' do'' do'' do'' si' si' si' si' si' si' |
    do''8\f mi'( red' mi') fa'( mi' red' mi') |
    r re'!(\p dod' re') mi'( re' dod' re') |
    do'! mi' mi' mi' mi' mi' mi' mi' |
    fad'!8 la'4 do'' la' fad'8 |
    mi' mi''4 mi' mi'' mi'8 |
    dod'8*2/3 dod' dod' dod' dod' dod' dod' dod' dod' dod' dod' dod' |
    si si si si si si si si si si si si |
    fad'4 mi' mi'8 mi' mi' mi' |
    sol' sol' sol' sol' fad' fad' fad' fad' |
    mi' mi' mi' mi' mi' mi' mi' mi' |
    mi' mi' mi' mi' red' red' red' red' |
  }
>>
mi'4 si''2\f \grace la'16 sol'8 fad'16 mi' |
fad'4 do'''2 \grace si'16 la'8 sol'16 fad' |
sold'4 re'''2 \grace do''16 si'8 la'16 sold' |
la' la' si' si' do'' do'' dod'' dod'' re'' re'' red'' red'' mi'' mi'' fad'' fad'' |
sol''8 red'' mi'' sol' la' fad' si' si |
mi'4 <<
  \tag #'violino1 {
    si'16( mi'') sol''-. si''-. la''4 la'16( re''!) fad''-. la''-. |
    si''4 si'16( re'') sol''-. si''-. si''4 si'16( red'') fad''-. si''-. |
    si''4 sol'16( si') mi''-. sol''-. sol''4 la'16( re''!) fad''-. la''-. |
    si''4 si'16( mi'') sol''-. si''-. la''4 la'16( re'') fad''-. la''-. |
    sol''4 sol''8.\trill fad''32 sol'' la''4 do''8.\trill si'32 do'' |
    si'4 re''8.\trill dod''32 re'' mi''4 sol'8.\trill fad'32 sol' |
    fad'4 la'16( re'') fad''-. la''-. sol''4 sol'16( dod'') mi''-. sol''-. |
    fad''4 fad'16( la') re''-. fad''-. si''4 fad'16( si') red''-. fad''-. |
    sol''4 sol'16( si') mi''-. sol''-. sol''4 mi'16( sol') si'-. mi''-. |
    fad''4 fad'16( si') red''-. fad''-.
  }
  \tag #'violino2 {
    sol'16( si') mi''-. sol''-. re''!4 re'16( fad') la'-. re''-. |
    re''4 sol'16( si') mi''-. sol''-. fad''4 fad'16( si') red''-. fad''-. |
    sol''4 mi'16( sol') si'-. mi''-. do''4 re'!16( fad') la'-. re''-. |
    re''4 sol'16( si') mi''-. sol''-. sol''4 fad'16( la') re''-. do''-. |
    si' re'' re' re' re'4:16 re'2:16 |
    re'16 sol' re' re' re' fad' la la la2:16 |
    la4 fad'16( la') re''-. fad''-. mi''4 mi'16( la') dod''-. mi''-. |
    re''4 re'16( fad') la'-. re''-. fad''4 si16( fad') si'-. si-. |
    si4 mi'16( sol') si'-. mi''-. mi''4 sol16( mi') sol'-. si'-. |
    do''4 red'16( fad') si'-. red''-.
  }
>> mi''4 si''16( sol'' fad'' mi'') |
red''4 fad''16( red'' si' la') sol'4 si''16( sol'' mi'' re'') |
dod''4 mi''16( dod'' la' sol') fad'4 la''16( fad'' re'' do'') |
si'4 re''16( si' sol' fa') mi'4 do''16( mi'' sol'' mi'') |
la''4 si'8.\trill la'32 si' do''8 mi' fa' sol' |
do'16 <>\p <<
  \tag #'violino1 {
    sol'16 sol' sol' sol'4:16 sol'2:16 |
    fa':16 fa':16 |
    mi':16 sol':16 |
    la':16 re':16 |
    mi'16\f sol' sol' sol' sol' do'' do'' do'' do'' mi'' mi'' mi'' mi'' sol'' sol'' sol'' |
    sol''8\p sol''4 sol'' sol'' sol''8 |
    dod'8\f fad''!4\p fad'' fad'' fad''8 |
    fad'' fad''([ sol'' mi'']) re'' re'' dod'' dod'' |
    re''\f <re'' re'>4 re''8 fad' <re' do''!>4 do''8 |
    si'8 re''4\p re''8 r re''4 re''8 |
    r fad''4 fad''8 r fad''4 fad''8 |
    r la''4 la''8 r la''4 la''8 |
    r sol''4 sol''8 r sol''4 sol''8 |
    r mi''4 la''8 sol'' sol'' fad'' fad'' |
    sol''( si' re'' sol'') si''( sol'' re'' si') |
    r si'( red'' fad'') si''( fad'' red'' si') |
    r si'( mi'' sol'') r fad'( do'' fad'') |
    r la''( sol'' fad'') mi'' mi'' red'' red'' |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    sol'' sol'' sol'' sol'' fad'' fad'' fad'' fad'' |
    sol''16\f
  }
  \tag #'violino2 {
    mi'16 mi' mi' mi'4:16 mi'2:16 |
    si:16 si:16 |
    do':16 do':16 |
    do':16 si:16 |
    do'16\f mi' mi' mi' mi' sol' sol' sol' sol' do'' do'' do'' do'' mi'' mi'' mi'' |
    mi''8\p mi''4 mi'' mi'' mi''8 |
    fad'8\f dod''4\p dod'' dod'' dod''8 |
    re'' re''([ mi'' dod'']) si' si' lad' lad' |
    si'\f si([ re' fad']) r la( re' fad') |
    sol'8 si'4\p si'8 r si'4 si'8 |
    r do''!4 do''8 r do''4 do''8 |
    r do''4 do''8 r do''4 do''8 |
    r si'4 si'8 r si'4 si'8 |
    r8 sol'4 do''8 si' si' la' la' |
    si'( sol' si' re'') sol''( re'' si' sol') |
    r fad'( si' red'') fad''( red'' si' fad') |
    r sol'( si' mi'') r mi'( fad' la') |
    r do''( si' la') sol' sol' fad' fad' |
    sol' sol' si' si' do'' do'' do'' do'' |
    mi'' mi'' mi'' mi'' red'' red'' red'' red'' |
    mi''16\f
  }
>> fad'' mi'' red'' mi'' fad'' sol'' la'' si'' la'' sol'' fad'' mi'' re'' do'' si' |
do'' la' si' do'' re'' mi'' fad'' sol'' la'' sol'' fad'' mi'' re'' do'' si' do'' |
si' la' si' do'' re'' mi'' fad'' sol'' re'8 sol'' re' fad'' |
sol''16 sol si re' sol' si re' sol' si' re' sol' si' re'' sol' si' re'' |
sol'' sol'' sol'' sol'' sol'' re'' si' re'' sol'' sol'' sol'' sol'' sol'' re'' sol'' la'' |
si'' si re' sol' si' re' sol' si' re'' sol' si' re'' sol'' si' re'' sol'' |
si'' si'' si'' si'' si'' sol'' si'' sol'' re''' re''' re''' re''' re''' si'' sol'' re'' |
<<
  \tag #'violino1 {
    \ru#4 { mi''16 do''' } re'' si'' re'' si'' si' sol'' si' sol'' |
  }
  \tag #'violino2 {
    \ru#4 { do'' mi'' } si' re'' si' re'' sol' si' sol' si' |
  }
>>
re'16 la' sol'' la' la'' la' sol'' la' re' la' fad'' la' la'' la' fad'' la' |
sol'' sol la si do' re' mi' fad' sol' la' si' la' sol' la' si' la' |
sol' sol' la' si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' la'' si'' la'' |
sol'' sol la si do' re' mi' fad' sol' la' si' la' sol' la' si' la' |
sol' sol' la' si' do'' re'' mi'' fad'' sol'' la'' si'' la'' sol'' la'' si'' la'' |
sol''8 <<
  \tag #'violino1 {
    re''8 re'' re'' r re'' mi'' re'' |
    dod'' dod'' dod'' dod'' re'' re'' re'' re'' |
    \ru#2 { si'16( si'') si'-. si'-. si'4:16\dotFour } |
    si'8 mi'' red'' dod'' red''16 fad'' si'' fad'' red'' fad'' si' red'' |
    <mi'' si' mi'>4
  }
  \tag #'violino2 {
    sol'8 sol' sol' la' la' si' si' |
    la' la' la' la' si' si' si' si' |
    la' fad'' fad'' fad'' sol' sol'' sol'' sol'' |
    fad' fad' fad' fad' <fad' la'> q q q | \allowPageTurn
    <sold' si' mi''>4
  }
>> si''16 sold'' mi'' re''! dod'' re'' mi'' dod'' la' dod'' mi'' sol'' |
fad'' sol'' la'' fad'' re'' la' si' do''! si' sol'' si'' la'' sol'' fad'' mi'' re'' |
dod'' la' re'' fad' mi'8 dod'' re''16 fad'' la'' re''' mi'' re''' mi'' dod''' |
re'''8 re'16 re' fad' fad' la' la' re'' re'' fad'' fad'' la'' la'' fad'' fad'' |
<<
  \tag #'violino1 {
    <re'' re'>4 r <re' la' fad''> <re' la' la''> |
    <la' sol''>4 r q <la' dod'''> |
    <la' mi'''> r <la' mi''> <la' sol''> |
    <re' la' fad''> r <re' re''> <re' la' fad''> |
    <re' la' la''>8 la'-. re''-. fad''-. fad''( mi'') re''( dod'') |
    re''16 re''
  }
  \tag #'violino2 {
    re''16 la' la' la' la'4:16 la'2:16 |
    la':16 la':16 |
    dod'':16 dod'':16 |
    re'':16 re'':16 |
    re''8-. fad'-. la'-. re''-. la'( sol') fad'( mi') |
    re'16 re'
  }
>> re' re' fad' fad' la' la' re'' re'' fad' fad' la' la' re'' re'' |
<<
  \tag #'violino1 {
    fad''16 si'' si'' si'' si''4:16 si'16 si'' si'' si'' si''4:16 |
    mi''16 la'' la'' la'' la''4:16 la'16 la'' la'' la'' la''4:16 |
    si''16 si'' sold'' sold'' mi'' mi'' re'' re'' si' si' sold' sold' mi' mi' re' re' |
    dod' dod' mi' mi' la' la' mi' mi' dod'' dod'' la' la' mi'' mi'' dod'' dod'' |
    fad''2:16 sold'':16 |
    la'':16 re'':16 |
    dod'':16 si':16 |
  }
  \tag #'violino2 {
    fad''2:16 fad'16 fad'' fad'' fad'' fad''4:16 |
    la'16 mi'' mi'' mi'' mi''4:16 mi'16 mi'' mi'' mi'' mi''4:16 |
    re'':16 si':16 sold':16 sold:16 |
    la16 la dod' dod' mi' mi' dod' dod' la' la' mi' mi' dod'' dod'' mi'' mi'' |
    la'2:16 si':16 |
    mi'2:16 fad'4:16 si':16 |
    la'2:16 sold':16 |
  }
>>
la'16 la' si' si' dod'' dod'' re'' re'' mi'' mi'' fad'' fad'' sold'' sold'' la'' la'' |
<<
  \tag #'violino1 {
    fad''2:16 sold'':16 |
    la'':16 fad'':16 |
    dod'':16 si':16 |
    la'4:16 la'':16 si':16 sold'':16 |
    la''16 la' la' la' la'4:16 la''2:16 |
    la''16 la' la' la' la'4:16 la''16 dod''' dod''' dod''' dod'''4:16 |
    fad''16 la'' la'' la'' la''4:16 dod''16 mi'' mi'' mi'' sold' si' si' si' |
  }
  \tag #'violino2 {
    la'2:16 re'':16 |
    dod'':16 la':16 |
    la':16 sold':16 |
    la'4:16 dod'':16 re'':16 si':16 |
    dod''16 la' la' la' la'4:16 dod''2:16 |
    dod''16 la' la' la' la'4:16 dod''16 la'' la'' la'' la''4:16 |
    la'16 fad'' fad'' fad'' fad''4:16 la'16 dod'' dod'' dod'' si' sold' sold' sold' |
  }
>>
la'8 la si dod' re' mi' fad' sold' |
<<
  \tag #'violino1 {
    la'4 r <la' dod'' mi''> <mi'' dod'''> |
    q r <fad' dod'' lad''> <mi'' dod'''> |
    <mi'' dod'''>4 r <fad' dod'' mi''> <fad' dod'' lad''> |
    <fad' re'' si''> r fad''4:16 lad':16 |
    si'2:16 fad'':16 |
    sol'':16 sol'':16 |
    lad'':16 si'':16 |
    mi'':16 re'':16 |
    dod'':16 dod'':16 |
    si'4:16 re'':16 fad'':16 si'':16 |
    red''2:16 si'':16 |
    mi'':16 si'':16 |
    la'':16 sol'':16 |
    <la' fad''>:16 q:16 |
    q:16 q:16 |
    <si' sol''>:16 q:16 |
    <si' sold''>:16 q:16 |
    <la'' la'>2:16 dod''4:16 re'':16 |
    mi''2:16 dod''4:16 re'':16 |
    mi'':16 fad'':16 la'':16 do''':16 |
    si'':16 re''2:16 dod''!4:16 |
    <re'' re'>2:16 q:16 |
    q:16 q:16 |
    q:16 sol'':16 |
    dod''4:16 re'':16 re'':16 dod'':16 |
    re''2:16 la''2:16 |
    si'':16 sol'':16 |
    fad'':16 fad'':16 |
    mi'':16 mi'':16 |
  }
  \tag #'violino2 {
    la'2:16 dod'':16 |
    dod'':16 dod'':16 |
    fad'2:16 fad':16 |
    fad':16 fad':16 |
    fad':16 si':16 |
    si':16 si':16 |
    mi'':16 re'':16 |
    dod'':16 si':16 |
    si':16 lad':16 |
    si'4:16 fad':16 re'':16 re':16 |
    si'2:16 red'':16 |
    si':16 mi'':16 |
    mi'':16 mi'':16 |
    <re'' re'>:16 <fad' re''>:16 |
    <re'' re'>:16 q:16 |
    q:16 q:16 |
    <mi' mi''>:16 q:16 |
    q:16 <la'' la'>:16 |
    q:16 q:16 |
    q:16 fad'':16 |
    sol''4:16 si':16 fad':16 mi':16 |
    re'4 r do'':16 si':16 |
    <fad' la'>:16 sol':16 do'':16 si':16 |
    la':16 sol':16 si'2:16 |
    la':16 fad'4:16 mi':16 |
    re'2:16 re'':16 |
    re'':16 re'':16 |
    re'':16 re'':16 |
    re'':16 dod'':16 |
  }
>>
re''4 <re' la' la''> <re' si' si''> <la' dod'''> |
<re' la' re'''> <re' la' fad''> <re' si' sol''> <la' dod'' mi''> |
<re' la' fad''> <re' la' la''> <re' la' fad''> <re' la' la''> |
<re' la' fad''>2 r |
