\clef "bass" mi8 sol si si, sol, sol mi sol |
red do' si la sol mi sol mi |
red do' si la sol sold la si |
do'16( si) la-. si-. do'-. si-. la-. sol!-. fad si, si la sol fad sol mi |
la,8 la si si, do sol la si |
mi mi mi mi fad fad fad fad |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
mi mi mi mi do do sol sol |
re re re re sol sol sol sol |
sol sol sol sol sol sol sol sol |
fad fad fad fad dod dod dod dod |
do! do do do si, si, la, la, |
si, si, si, si, si, si, si, si, |
mi mi mi mi fad fad fad fad |
sol sol si, si, do do re re |
sol8\p sol sol sol fad fad fad fad |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
la la la la re re re re |
sol\f sol sol sol sol sol\p sol sol |
fad fad fad fad fad fad fad fad |
mi mi mi mi mi mi re re |
dod dod dod dod dod dod dod dod |
re re re re fad fad fad fad |
sol sol sol sol sold sold sold sold |
la\f la la la la la la la |
la\p la la la la la la la |
sib sib sib sib sib sib sib sib |
fad! fad fad fad fad fad fad fad |
sol sol sol sol sol sol sol sol |
sib sib sib sib sib sib sib sib |
la la la la si! si si si |
sol sol sol sol sol sol sol sol |
sol sol sol sol fad fad fad fad |
dod dod dod dod re re re re |
sol sol sol sol la la la la |
si si sol sol fad fad sol sol |
la la la la la, la, la, la, |
re4\f fad mi la |
re fad red si, |
mi sol \tuplet 3/2 { la8( si la) } \tuplet 3/2 { sol( fad mi) } |
si,4 si, si, r |
mi8 mi mi mi fad fad fad fad |
sol sol sol sol mi mi mi mi |
si, si, si, si, do do do do |
re re re re mi mi re re |
do do do do re re re re |
mi mi mi mi mi mi mi mi |
do do do do dod dod dod dod |
re re re re mi mi mi mi |
re re re re re re re re |
red red red red mi mi mi mi |
si, si, si, si, si, si, si, si, |
mi4 r r mi |
fad r r fad |
si re mi fad |
si,\p si, si, si, |
la, la la la |
sol fad mi red |
mi8\f mi mi mi mi mi mi mi |
si,\p si, si, si, si, si, si, si, |
do do do do mi mi mi mi |
fa fa fa fa sol sol sol sol |
la la la la mi mi mi mi |
fa fa fa fa fa fa fa fa |
sol sol sol sol sol, sol, sol, sol, |
do4-.\f r r2 |
mi4-.\p r r2 | \allowPageTurn
la8 la la la la la la la |
red red red red red red red red |
mi mi mi mi mi mi mi mi |
lad, lad, lad, lad, lad, lad, lad, lad, |
si, si, dod dod red red mi mi |
red4 mi la,8 la, la, la, |
si, si, si, si, si, si, si, si, |
do do sol sol la la la la |
si si si si si, si, si, si, |
mi\f mi mi mi mi mi mi mi |
red red red red red red red red |
re! re re re re re re re |
do sold la sol fad si do' red |
mi si do' sol la fad si si, |
mi mi mi mi fad fad fad fad |
sol sol sol mi red red red red |
mi mi mi mi mi mi fad fad |
sol fad mi re! do la, re re |
sol, sol si sol fad la re fad |
sol si fad re dod mi dod la, |
re re re re dod dod dod dod |
re re re re red red red red |
mi mi mi mi mi mi do! sol, |
la, la, si, si, mi fad sol la |
si si, red si, mi fad sol sold |
la si dod' la re' re fad la |
sol la si re' do'! sol mi do |
fa re sol fa mi mi fa sol |
do8\p do do do do do do do |
do do do do do do do do |
do do do do mi mi mi mi |
fa fa fa fa sol sol sol sol |
do\f do do do do do do do |
do\p do do do do do si, si, |
lad,\f lad\p[ lad lad] lad[ lad lad lad] |
si si mi mi fad! fad fad fad |
si,\f si, si, si, la,! la, la, la, |
sol,\p sol, sol, sol, sol, sol, sol, sol, |
la, la, la, la, la, la, la, la, |
re re re re re re re re |
sol sol sol sol sol sol sol sol |
do do do do re re re re |
sol sol sol sol sol sol sol sol |
red red red red red red red red |
mi mi mi mi la la la la |
la la la la si si si si |
sol sol sol sol la la la la |
si si si si si, si, si, si, |
mi\f mi mi mi sol sol sol sol |
la la la la fad fad fad fad |
sol sol si si re' re' re re |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
sol sol sol sol sol sol sol sol |
do do do do sol sol sol sol |
re re re re re re re re |
sol, la,16 si, do re mi fad sol la si la sol la si la |
sol4 r r2 |
sol,8 la,16 si, do re mi fad sol la si la sol la si la |
sol4 r r2 | \allowPageTurn
sol8 sol sol sol fad fad mi mi |
la la la la sol sol sol sol |
re re re re mi mi mi mi |
si, si, si, si, si, si, si, si, |
mi mi sold sold la la dod dod |
re re fad fad sol! mi16 fad sol8 sold |
la fad16 re la8 la, re re' la la, |
re re fad la re' fad la fad |
re8 re re re re re re re |
mi mi mi mi mi mi mi mi |
la la la la la la la la |
re re re re re re re re |
re re fad re la la la, la, |
re re re re re re re re |
re re re re re re re re |
dod dod dod dod dod dod dod dod |
si, si, si, si, mi mi mi mi |
la, la, la, la, la, la, la, la, |
re re re re re re re re |
dod dod dod dod re re re re |
mi mi mi mi mi mi mi mi |
la, si, dod re mi fad sold la |
re re re re mi mi mi mi |
fad fad fad fad fad fad re re |
mi mi mi mi mi mi mi mi |
dod dod la, la, mi mi mi mi |
la la, dod mi la la dod' la |
fad fad, la, dod fad fad la fad |
re re fad re mi mi mi mi |
la la, si, dod re mi fad sold |
la la la la sol! sol sol sol |
fad fad fad fad fad fad fad fad |
lad lad lad lad fad fad fad fad |
si si si si si, si, dod dod |
si, si, si, si, re re re re |
mi mi mi mi mi mi mi mi |
fad fad fad fad sol sol sol sol |
lad lad lad lad si si si si |
fad fad fad fad fad fad fad fad |
si, si, si, si, si, si, si, si, |
la! la la la la la la la |
sol sol sol sol sol sol sol sol |
dod dod dod dod dod dod dod dod |
re re fad la re' la fad la |
re re fad fad la la do' do' |
si si si si si, si, si, si, |
mi mi sold sold si si re' re' |
dod' dod' dod' dod' sol! sol fad fad |
dod dod dod dod mi mi re re |
dod4 re r re |
sol8 sol sol sol la la la la |
re4 r fad8 fad sol sol |
do' do' si si fad fad sol sol |
do'4 si sol8 sol sol sol |
sol sol fad sol la la la la |
re re re re fad fad fad fad |
sol sol sol sol si si si si |
la la la la la la la la |
la, la, la, la, la, la, la, la, |
re re fad re sol mi la sol |
fad re fad re sol mi la la, |
re re' dod' re' la re' fad la |
re2 r |
