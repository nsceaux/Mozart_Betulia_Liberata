\clef "treble" r2 <<
  \tag #'(oboe1 oboi) { si''2~ | si''1~ | si''2 }
  \tag #'(oboe2 oboi) { si'2~ | si'1~ | si'2 }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''2~ | mi''4 }
  { si'4 do''8 re'' | mi''4 }
>> r4 r2 |
r8 <<
  \tag #'(oboe1 oboi) { la''8 sol'' fad'' }
  \tag #'(oboe2 oboi) { fad'' mi'' red'' }
>> \mergeDifferentlyDottedOn \twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''4. red''8 | }
  { mi''8 sol' fad'4 | }
>>
<<
  \tag #'(oboe1 oboi) { mi''2 re''! | }
  \tag #'(oboe2 oboi) { mi'2 la' | }
>>
\tag#'oboi <>^"a 2." si'2 si'4 si' |
si'2 si' |
si' do''4 si' |
la'2 sol'4 r |
la'2 la' |
la'1 |
la'2 si'4 <<
  \tag #'(oboe1 oboi) {
    mi''4 |
    sol''2 fad'' |
    mi''4 sol''
  }
  \tag #'(oboe2 oboi) {
    mi'4 |
    sol'2 fad' |
    mi'4 mi''
  }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2 | si''4 sol''2~ sol''8 fad'' | }
  { re''!2~ | re''2 mi''4 la' | }
>>
<<
  \tag #'(oboe1 oboi) { sol''8 }
  \tag #'(oboe2 oboi) { si' }
>> r8 r4 r2 |
R1*5 |
<>\f <<
  \tag #'(oboe1 oboi) { re''2 re''4 }
  \tag #'(oboe2 oboi) { si'2 si'4 }
>> r4 |
R1*5 |
<<
  \tag #'(oboe1 oboi) {
    mi''1 |
    fa''~ |
    fa'' |
    re''~ |
    re'' |
    sol''~ |
    sol''2 fad''! |
  }
  \tag #'(oboe2 oboi) {
    dod''1 |
    dod'' |
    re'' |
    la' |
    sib' |
    re'' |
    dod''2 re'' |
  }
  { s1\f s\p }
>>
R1*6 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 dod'' | }
  { fad'4 la' sol'2 | }
>>
<<
  \tag #'(oboe1 oboi) {
    re''4 do''! si' la'' |
    sol'' mi''
  }
  \tag #'(oboe2 oboi) {
    fad'4 la'~ la' fad'' |
    mi'' si'
  }
>> \tag#'oboi <>^"a 2." do''8*2/3 si' la' sol' fad' mi' |
si'4 si' si' r |
si'2 re'' |
si'1 |
si'~ |
si'2 do''4 si' |
la'1 |
sol'4 r r2 |
la'1~ |
la' |
la' |
si'2 <<
  \tag #'(oboe1 oboi) {
    mi''2 |
    sol'' fad'' |
    mi''4 s s mi''8 re'' |
    dod''4 s s dod''8 mi'' |
  }
  \tag #'(oboe2 oboi) {
    mi'2 |
    sol' fad' |
    mi'4 s s dod''8 si' |
    lad'4 s s lad'8 dod''! |
  }
  { s2 | s1 | s4 r r s | s r r s | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 dod'' | si'4 }
  { si'2. lad'4 | si' }
>> r4 r2 |
<>\p <<
  \tag #'(oboe1 oboi) { fad''1 | }
  \tag #'(oboe2 oboi) { red'' }
>>
R1 |
<>\f <<
  \tag #'(oboe1 oboi) { sol'1 | }
  \tag #'(oboe2 oboi) { mi'1 | }
>>
R1*6 |
<<
  \tag #'(oboe1 oboi) { do''1 | si' | }
  \tag #'(oboe2 oboi) { mi'1 | sold' | }
  { s1\f s\p }
>>
R1*3 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi''1 |
    red''4 mi'' fad'' sol'' |
    la'' sol'' }
  { dod''1 |
    si' |
    fad''4 mi'' }
>> r2 |
R1*3 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1 |
    la'' |
    sold'' |
    mi''2 la'4 sol'8 fad' |
    mi'4 mi''~ mi'' red'' | }
  { mi''1 |
    fad'' |
    si' |
    mi''2 la'4 sol'8 fad' |
    mi'4 sol' fad'2 | }
>>
<<
  \tag #'(oboe1 oboi) { mi''2 re''! | }
  \tag #'(oboe2 oboi) { sol'2 la' | }
>>
\tag#'oboi <>^"a 2." si'1 |
si'2 do'' |
si' la' |
sol'4 r <<
  \tag #'(oboe1 oboi) {
    do''2 |
    si'4 s sol''2 |
    fad''
  }
  \tag #'(oboe2 oboi) {
    la'2 |
    sol'4 s mi''2 |
    re''
  }
  { s2 | s4 r }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'2~ | la' si' | }
  { la'2~ | la' si' | }
>>
<<
  \tag #'(oboe1 oboi) {
    mi''2 sol'' |
    fad'' mi''4
  }
  \tag #'(oboe2 oboi) {
    mi'2 sol' |
    fad' mi'4
  }
>> r4 |
\tag#'oboi <>^"a 2." si'1 |
la' |
sol' |
la'4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { si'4 do''8 sol'' la'' re'' |
    do'' }
  { re'4 mi'8 do''4 si'8 |
    do'' }
>> r8 r4 r2 |
R1 |
r2 <>\p <<
  \tag #'(oboe1 oboi) { sol''2 | la'' re'' | }
  \tag #'(oboe2 oboi) { do''2~ | do'' si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''8 }
  { do'' }
>> r8 r4 r2 |
R1*3 | \allowPageTurn
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''1 | }
  { re''2 do''! | }
>>
<<
  \tag #'(oboe1 oboi) { sol''8 }
  \tag #'(oboe2 oboi) { si' }
>> r8 r4 r2 |
R1*4 |
<>\p <<
  \tag #'(oboe1 oboi) { si''1~ | si'' | }
  \tag #'(oboe2 oboi) { si'1~ | si' | }
>>
R1*4 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2 mi'' | }
  { si'1 | }
>>
<<
  \tag #'(oboe1 oboi) { mi''2 re''! | }
  \tag #'(oboe2 oboi) { do''2 la' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'2 la' | sol'4 }
  { sol'2. fad'4 | sol' }
>> r4 r2 |
<<
  \tag #'(oboe1 oboi) { sol''1 | }
  \tag #'(oboe2 oboi) { si' | }
>>
R1 |
<<
  \tag #'(oboe1 oboi) { sol''1 | mi''2 re'' | }
  \tag #'(oboe2 oboi) { si'1 | do''2 si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''1 | sol'4 }
  { re'2 re' | sol'4 }
>> r4 r2 |
r8 <<
  \tag #'(oboe1 oboi) {
    sol''8 sol'' sol'' sol'' sol'' sol'' sol'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    sol'8 sol' sol' sol' sol' sol' sol' |
    sol'4
  }
>> r4 r2 |
r8 <<
  \tag #'(oboe1 oboi) {
    sol''8 sol'' sol'' sol'' sol'' sol'' sol'' |
    sol''4
  }
  \tag #'(oboe2 oboi) {
    sol'8 sol' sol' sol' sol' sol' sol' |
    sol'4
  }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 mi''8 re'' | }
  { si'4 la' sol' | }
>>
<<
  \tag #'(oboe1 oboi) { dod''2 re'' | }
  \tag #'(oboe2 oboi) { mi'2 sol' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'1 | }
  { la'2 sol' | }
>>
<<
  \tag #'(oboe1 oboi) { si'1~ | si'4 s4 sol''!2 | }
  \tag #'(oboe2 oboi) { fad'1 | sold'4 s mi''2 | }
  { s1 | s4 r }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''4 la'' si''2 |
    mi''8 fad'' mi''4 re''8 fad'' mi''4 |
    re''8 re' fad' la' re'' fad'' la'' fad'' |
    re''2 }
  { re''2 re'' |
    dod''8 re''4 dod''8 re''4. dod''8 |
    re''8 re' fad' la' re'' fad'' la'' fad'' |
    re''2 }
>> <<
  \tag #'(oboe1 oboi) {
    fad''2 |
    sol''1 |
    mi'' |
    fad'' |
    la''2 la''8( sol'') fad''( mi'') |
  }
  \tag #'(oboe2 oboi) {
    re''2 |
    dod''1 |
    dod'' |
    re'' |
    fad''2 fad''8( mi'') re''( dod'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 fad'' | si''1 | la'' | }
  { re''1 | fad'' | mi'' | }
>>
<<
  \tag #'(oboe1 oboi) {
    si''2 re'' |
    dod'' mi'' |
    fad'' sold'' |
    la''2 fad'' |
    dod'' si' |
  }
  \tag #'(oboe2 oboi) {
    re''2 si' |
    la' dod'' |
    la' si' |
    mi'' la' |
    la' sold' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) << la'4 la' >> r4 r2 |
<<
  \tag #'(oboe1 oboi) { fad''2 sold'' | }
  \tag #'(oboe2 oboi) { la'2 re'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1 | }
  { dod''2 fad'' | }
>>
<<
  \tag #'(oboe1 oboi) { la''2 sold'' | }
  \tag #'(oboe2 oboi) { dod'' si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2. sold''4 |
    la''1~ |
    la'' | }
  { dod''2 si' |
    dod''1~ |
    dod'' | }
>>
<<
  \tag #'(oboe1 oboi) { fad''2 dod''4 si' | }
  \tag #'(oboe2 oboi) { re''2 la'4 sold' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 si'8 dod'' re'' mi'' fad'' sold'' |
    la''1 |
    lad'' |
    mi'' | }
  { la'4 si'8 dod'' re'' mi'' fad'' sold'' |
    la''2 dod'' |
    dod''1 |
    dod'' | }
>>
<<
  \tag #'(oboe1 oboi) {
    re''2. fad''4 |
    fad''1 |
    sol'' |
    lad''2 si'' |
    mi'' re'' |
  }
  \tag #'(oboe2 oboi) {
    si'2. lad'4 |
    si'1 |
    si' |
    mi''2 re'' |
    dod'' si' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { dod''1 |
    si'2 re'' |
    red''1 |
    mi'' |
    mi'' |
    fad'' |
    la'' |
    si'' |
    sold'' | }
  { si'2 lad' |
    si'1 |
    si' |
    si' |
    la' |
    la' |
    fad'' |
    sol'' |
    si' | }
>>
<<
  \tag #'(oboe1 oboi) { la''2 la'' | la''1 | }
  \tag #'(oboe2 oboi) { la'2 la' | la'1 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1 | }
  { la'2 fad'' | }
>>
<<
  \tag #'(oboe1 oboi) { si''2 fad''4 mi'' | }
  \tag #'(oboe2 oboi) { sol''2 re''4 dod'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) << re''4 re'' >> r4 <<
  \tag #'(oboe1 oboi) { do''4 si' | la' }
  \tag #'(oboe2 oboi) { la' sol' | fad' }
>> \twoVoices #'(oboe1 oboe2 oboi) << sol' sol' >> <<
  \tag #'(oboe1 oboi) { do''4 si' | la' }
  \tag #'(oboe2 oboi) { la' sol' | fad' }
>> \twoVoices #'(oboe1 oboe2 oboi) << sol' sol' >> <<
  \tag #'(oboe1 oboi) {
    sol''2 |
    mi''4 fad''2 mi''4 |
  }
  \tag #'(oboe2 oboi) {
    si'2 |
    dod''!4 re''2 dod''4 |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 la'' |
    si'' sol'' |
    fad''1 |
    mi'' |
    re''4 }
  { re''1 |
    re'' |
    re''~ |
    re''2 dod'' |
    re''4 }
>> <<
  \tag #'(oboe1 oboi) {
    la''4 si'' dod''' |
    re''' fad'' sol'' mi'' |
    fad'' la'' fad'' la'' |
    fad''2
  }
  \tag #'(oboe2 oboi) {
    fad''4 sol'' mi'' |
    re'' la' si' dod'' |
    re'' fad'' re'' fad'' |
    re''2
  }
>> r2 |
