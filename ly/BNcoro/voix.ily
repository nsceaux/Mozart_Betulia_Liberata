<<
  \tag #'giuditta {
    \clef "alto/treble" R1*16 |
    sol'2 la'4. do'8 |
    si4 re' r sol' |
    sol'8[ mi'] mi'4 r2 |
    do'8[ mi'] mi' sol' sol'4. fad'16[ mi'] |
    re'4 re' sol'2~ |
    sol'4 fad'8 mi' re'4. do'8 |
    si4 sol r r8 re' |
    re'4. la'8 la'[ fad'] mi'[ re'] |
    dod'4 dod' r si |
    la4. si'8 la'4. sol'8 |
    fad'4 r la'2 |
    si'4. sol'8 mi'4. re'8 |
    dod'4 r r2 |
    fa'4 fa'8 fa' fa'4. fa'8 |
    fa'4 re' r2 |
    re'4 re'8 re' re'4. re'8 |
    re'4 sib r2 |
    sol4 sol'8 sol' sol'4 sol' |
    sol'2 fad'!4 fad' |
    si'!4. sol'8 mi'4. re'8 |
    dod'2 re' |
    sol' fad'4 fad' |
    si'2 dod' |
    re'4 si' la'8[ fad'] sol'[ mi'] |
    re'2 mi'\trill |
    fad'4 r r2 |
    R1*17 |
    fad'4 fad'8 fad' fad'4 fad' |
    fad' red' r r8 red' |
    mi'4 fad' sol' la' |
    sol' mi' r2 |
    sol'4 sol'8 sol' sol'4. fa'8 |
    mi'4 mi' r r8 do' |
    la4. la'8 sol'4. fa'8 |
    mi'2 sol' |
    la'2. sol'16[ fa'] mi'[ re'] |
    do'2 re'\trill |
    do'4 r r2 |
    r r4 r8 mi' |
    mi'4. mi'8 mi'4. do''8 |
    do''[ fad'!] fad'4 r r8 fad' |
    sol'4. sol'8 sol'4. sol'8 |
    fad'4 fad' r r8 mi' |
    red'4 mi' fad' sol' |
    la' sol' do'2 |
    si4 mi' fad'4. la'8 |
    sol'4 si do' la' |
    si2 fad'\trill |
    mi'4 r r2 |
    R1*18 |
    r2 r4 r8 sol' |
    fa'4 fa' r8 fa' fa' sol' |
    mi'4 mi' r r8 do' |
    la4. la'8 sol'4 si |
    do' do' r2 |
    mi'4 mi'8 mi' mi'4. sol'8 |
    fad'!4 fad' r r8 fad' |
    fad'[ si'] sol'[ mi'] re'4 dod' |
    si r r2 |
    re'4 re'8 re' re'4. sol'8 |
    fad'4 fad' r r8 la' |
    la'4. fad'8 re'4. do'!8 |
    si4 si r r8 sol' |
    mi'4 do''8[ la'] sol'4 fad' |
    sol' sol' r r8 si' |
    si'4. fad'8 fad'4. la'8 |
    sol'4 sol' fad'2~ |
    fad'8[ sol'16 la'] sol'8 fad' mi'4 red' |
    mi' si do'8[ do''] la'[ fad'] |
    si2 fad'\trill |
    mi'4 r r2 |
    R1*20 |
    R1*55 |
  }
  \tag #'vsoprano {
    \clef "soprano/treble" R1*5 |
    si'2 re''4 re'' |
    si'2 si'4 si' |
    si'2 si' |
    si'4 si'8 si' do''4 si' |
    la'2 sol'4 r |
    la'4 la'8 la' la'4 la' |
    la'1 |
    r4 la' si' mi' |
    sol'2 fad' |
    mi'4 r r2 |
    R1*30 |
    si'2 re''4 re'' |
    si'2 si'4 si' |
    si'2 si' |
    si' do''4 si' |
    la' la' la'2 |
    sol'4 r r2 |
    la'2 la'4 la' |
    la'2 la' |
    la'2. la'4 |
    si'2 mi' |
    sol' fad' |
    mi'4 r r2 |
    R1*28 |
    si'2 re''4 re'' |
    si'2 si'4 si' |
    si' si' do'' do''8 do'' |
    si'4 si' la'2 |
    sol'4 r r2 |
    r la'~ |
    la'4 la'8 la' la'4 la' |
    la' la' si'2 |
    mi' sol' |
    fad' mi'4 r |
    R1*27 |
    si'2 re''4 re'' |
    re''2 si' |
    r4 re''2 re''4 |
    re''2 re'' |
    mi''4 mi''8 mi'' re''4 re'' |
    re''1 |
    si'4 r r2 |
    re''2 re''4 re'' |
    re''2 re'' |
    re'' re'' |
    re''2. mi''8 re'' |
    dod''4 dod'' re'' re'' |
    si'1~ |
    si'2. si'4 |
    sold' r r2 |
    R1*3 |
    la'4 la'8 la' la'4. re''8 |
    dod''4 dod'' r2 |
    la'4 la'8 la' la'4. mi''8 |
    re''4 re'' la'2~ |
    la'4 re''8 fad'' fad''[ mi''] re''[ dod''] |
    re''4 re'' r2 |
    fad''2 re''4 si' |
    mi''2 dod''4 r |
    re''2 si'4 mi' |
    dod''2 la'4 r |
    fad''2 sold'4 sold' |
    la'2 re'' |
    dod''( si') |
    la'4 r r2 |
    fad''2 sold'4 sold' |
    la'2 re''4 re'' |
    dod''2( si') |
    la'4 dod'' si'2 |
    la'4 r r2 |
    R1*3 |
    mi''4 mi''8 mi'' mi''4 mi'' |
    mi'' lad' r fad'8 fad' |
    dod''4. dod''8 dod''4 mi'' |
    re'' si' r2 |
    r4 fad'' re'' si' |
    sol'' r r2 |
    lad'2 si'4 si' |
    mi''2 re''4 re'' |
    dod''1 |
    si'4 r r2 |
    red''8. red''16 red''4 r2 |
    mi''8. mi''16 mi''4 r2 |
    r4 mi'' mi'' sol' |
    fad' la' r2 |
    re''4 re''8 re'' re''4 re'' |
    re''8[ si'] sol'4 r2 |
    mi''4 mi''8 mi'' mi''4 mi'' |
    mi''8[ dod''] la'4 dod'' re'' |
    mi'' r dod'' re'' |
    mi'' fad'' r do'' |
    si' re''8[ mi''] re''4( dod''!) |
    re'' r do'' si' |
    la' sol' do'' si' |
    la' sol' r sol'' |
    dod''! re''8[ mi''] re''4( dod'') |
    re''2 la' |
    si' sol'' |
    fad''1\melisma |
    mi''\melismaEnd |
    re''4 r r2 |
    R1*3 |
  }
  \tag #'valto {
    \clef "alto/treble" R1*5 |
    sol'2 la'4 la' |
    sol'2 sol'4 sol' |
    sol'2 sol' |
    sol'4 sol'8 sol' sol'4 sol' |
    sol'4( fad') sol'4 r |
    mi'4 mi'8 mi' mi'4 mi' |
    re'2 mi' |
    re'2. do'4 |
    si( mi'2) red'4 |
    mi'4 r r2 |
    R1*30 |
    mi'2 re'4 re' |
    re'2 mi'4 mi' |
    sol'2 sol' |
    sol'4 sol'8 sol' sol'4 sol' |
    sol'2( fad') |
    sol'4 r r2 |
    sol'2 sol'4 sol' |
    sol'2 sol' |
    fad'4 fad' fad' fad' |
    fad'\melisma si'4. la'8 sol'4~ |
    sol'8[ fad'] mi'2\melismaEnd red'4 |
    mi' r r2 |
    R1*28 |
    sol'2 la'4 la' |
    sol'2 fad'4 fad' |
    sol'8 la' si'2 la'8 la' |
    la'4 sol' sol'( fad') |
    sol'4 r r2 |
    r mi' |
    re'4 re' mi' mi' |
    re'2 fad' |
    mi'4 si mi'2~ |
    mi'4 red' mi' r |
    R1*28 |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol' |
    sol'2 sol' |
    sol'4 sol'8 sol' sol'4 sol' |
    sol'2( fad') |
    sol'4 r r2 |
    sol'2 sol'4 sol' |
    sol'2 sol' |
    sol' sol' |
    sol' la'4 si' |
    mi'2 sol' |
    fad' mi' |
    mi'4( red'8[ dod'?] red'4.) red'8 |
    mi'4 r r2 |
    R1*3 |
    fad'4 fad'8 fad' fad'4. fad'8 |
    sol'4 sol' r2 |
    sol'4 sol'8 sol' sol'4. sol'8 |
    fad'4 fad' fad'2~ |
    fad'4 fad'8 la' la'[ sol'] fad'[ mi'] |
    fad'4 fad' r2 |
    si2 re'4 fad' |
    la'2 <la' la>4 r |
    mi'2 mi'4 mi' |
    mi'2 mi'4 r |
    la'2 mi'4 mi' |
    mi'2 fad' |
    mi'1 |
    mi'4 r r2 |
    la'2 re'4 re' |
    dod'2 fad'4 la' |
    la'2( sold') |
    la'4 la' la'( sold') |
    la' r r2 |
    R1*3 |
    la'4 la'8 la' la'4 la' |
    lad' fad' r mi'8 mi' |
    mi'4. fad'8 fad'4 fad' |
    fad' fad' r2 |
    r4 si' fad' re' |
    si r r2 |
    mi'2 re'4 re' |
    fad'2 fad'4 si' |
    si'2( lad') |
    si'4 r r2 |
    fad'8. fad'16 fad'4 r2 |
    sol'8. mi'16 mi'4 r2 |
    r4 sol' sol' mi' |
    re' fad' r2 |
    la'4 la'8 la' la'4 la' |
    sol' re' r2 |
    sold'4 si'8 si' sold'4 sold' |
    la'8[ mi'] dod'4 la la' |
    sol'! r sol' fad' |
    mi' re' r fad' |
    re' si' la'2 |
    la'4 r la' sol' |
    fad' sol' la' sol' |
    fad' sol' r si' |
    la' la' la'2 |
    la' la' |
    sol' sol' |
    la'1~ |
    la' |
    la'4 r r2 |
    R1*3 |
  }
  \tag #'vtenore {
    \clef "tenor/G_8" R1*5 |
    mi'2 re'4 re' |
    re'2 re'4 re' |
    re'2 re' |
    mi'4 mi'8 mi' mi'4 re' |
    re'2 si4 r |
    dod'4 dod'8 dod' dod'4 dod' |
    re' la sol sol |
    fad fad sol la |
    sol4.( la8 si4) la |
    sol4 r r2 |
    R1*30 |
    sol2 la4 la |
    sol2 sol4 sol |
    re'2 mi' |
    re' do'4 re' |
    mi' mi' re'( do') |
    si r r2 |
    mi'2 mi'4 mi' |
    re'2 dod' |
    re' do'! |
    si si |
    si2. si4 |
    si r r2 |
    R1*28 |
    mi'2 re'4 re' |
    re'4 si r si |
    si sol sol re'8 re' |
    re'4 mi' mi'( re') |
    re' r r2 |
    r sol |
    fad4 fad sol sol |
    fad2( la) |
    sol4 r r8 mi sol si |
    do'4 si si r |
    R1*28 |
    si2 re'4 re' |
    si2 si4 si |
    si2 si |
    do' si4 si |
    la la la2 |
    sol4 r r2 |
    si2 si4 si |
    si2 si |
    si si |
    si la4 sol |
    la2 si |
    la sol |
    fad2. fad4 |
    mi r r2 |
    R1*3 |
    re'4 re'8 re' re'4. la8 |
    la4 la r2 |
    dod'4 dod'8 dod' mi'4. dod'8 |
    re'4 la r2 |
    re'4 la8 la la4 la |
    la la r2 |
    fad2 si4 re' |
    mi'2 mi'4 r |
    sold2 sold4 sold |
    la2 dod'4 r |
    re'2 si4 si |
    dod'2 la |
    la( sold) |
    la4 r r2 |
    re' si4 si |
    la2 la4 fad' |
    mi'1 |
    mi'4 mi' mi'2 |
    mi'4 r r2 |
    R1*3 |
    dod'4 dod'8 dod' dod'4 dod' |
    dod' dod' r dod'8 dod' |
    dod'4. dod'8 lad4 dod' |
    si re' r2 |
    r4 re' si fad' |
    mi' r r2 |
    dod'2 si4 si |
    dod'2 si4 re' |
    fad'1 |
    re'4 r r2 |
    si8. si16 si4 r2 |
    si8. si16 si4 r2 |
    r4 la la la |
    la re' r2 |
    fad'4 fad'8 fad' fad'4 fad' |
    re' si r2 |
    si4 mi'8 mi' re'4 si |
    la8[ dod'] mi'4 mi' re' |
    la r la la |
    la la r la |
    sol re' fad'( mi') |
    re' r re' re' |
    re' re' re' re' |
    re' re' r re' |
    mi' fad' fad'( mi') |
    fad'2 re' |
    re' re' |
    re'1~ |
    re'2( dod') |
    re'4 r r2 |
    R1*3 |
  }
  \tag #'vbasso {
    \clef "bass" R1*5 |
    mi2 fad4 fad |
    sol2 sol4 sol |
    sol2 sol |
    mi4 mi8 mi do4 sol |
    re2 sol4 r |
    sol4 sol8 sol sol4 sol |
    mi2 dod |
    do! si,4( la,) |
    si,2. si,4 |
    mi4 r r2 |
    R1*30 |
    mi2 fad4 fad |
    sol2 mi4 mi |
    si,2 do |
    re mi4 re |
    do do re2 |
    mi4 r r2 |
    do2 dod4 dod |
    re2 mi |
    re re |
    red mi |
    si,2. si,4 |
    mi r r2 |
    R1*28 |
    mi2 fad4 fad |
    sol4.( mi8) red4 red |
    mi mi r2 |
    sol8[ fad] mi re! do la, re4 |
    sol, r r2 |
    r dod |
    re4 re dod dod |
    re2( red) |
    mi4 mi2 do!8 sol, |
    la,4 si, mi r |
    R1*28 |
    sol2 sol4 sol |
    sol2 sol4 sol |
    sol2 sol |
    do2 sol4 sol |
    re re re2 |
    sol,4 r r2 |
    sol2 sol4 sol |
    sol2 sol |
    sol sol |
    sol fad4 mi |
    la2 sol |
    re mi |
    si,2. si,4 |
    mi r r2 |
    R1*3 |
    re4 re8 re re4 re |
    mi4 mi r2 |
    la4 la8 la la4. la8 |
    re'4 re r2 |
    re4 fad8 re la4 la, |
    re re r2 |
    re2 re4 re |
    dod2 dod4 r |
    si,2 mi4 mi |
    la2 la,4 r |
    re2 re4 re |
    dod2 re |
    mi1 |
    la,4 r r2 |
    re2 mi4 mi |
    fad2 fad4 re |
    mi1 |
    dod4 la mi2 |
    la,4 r r2 |
    R1*3 |
    la4 la8 la sol!4 sol |
    fad fad r fad8 fad |
    lad4. lad8 fad4 fad |
    si si, r2 |
    r4 si, re re |
    mi r r2 |
    fad2 sol4 sol |
    lad2 si4 si, |
    fad1 |
    si,4 r r2 |
    la!8. la16 la4 r2 |
    sol8. sol16 sol4 r2 |
    r4 dod dod dod |
    re re r2 |
    re4 fad8 fad la4 do' |
    si si, r2 |
    mi4 sold8 sold si4 re' |
    dod'4 dod sol! fad |
    dod r mi re |
    dod re r re |
    sol sol la2 |
    re4 r fad sol |
    do' si fad sol |
    do' si r sol |
    sol fad8[ sol] la2 |
    re fad |
    sol si |
    la1~ |
    la |
    re4 r r2 |
    R1*3 |
  }
>>
