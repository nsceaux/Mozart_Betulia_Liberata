\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = "Corni in G."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Giuditta
      shortInstrumentName = \markup\character Giu.
    } \withLyrics <<
      \global \keepWithTag #'giuditta \includeNotes "voix"
    >> \keepWithTag #'giuditta \includeLyrics "paroles"
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro
      shortInstrumentName = \markup\character Co.
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenore \includeNotes "voix"
      >> \keepWithTag #'vtenore \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasso \includeNotes "voix"
      >> \keepWithTag #'vbasso \includeLyrics "paroles"
    >>
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*4\pageBreak
        s1*5\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*7\break s1*7\pageBreak
        s1*6\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*6\pageBreak
        s1*6\break s1*6\pageBreak
        s1*5\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*7\break s1*6\pageBreak
        s1*4\break s1*5\pageBreak
        s1*5\break s1*5\pageBreak
        s1*6\break s1*5\pageBreak
        s1*7\break s1*6\pageBreak
        s1*8\break s1*7\pageBreak
        s1*8\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
