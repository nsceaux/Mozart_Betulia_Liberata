\clef "bass" si,1~ |
si,~ |
si, |
do~ |
do2 dod |
re sold~ |
sold do~ |
do mi |
fa1 |
la,~ |
la,2 sib,~ |
sib, si,~ |
si, do~ |
do do |
si, do |
lad, si,~ |
si, dod~ |
dod re~ |
re sol,~ |
sol, do!4 fa! |
r4 sol do2 |
