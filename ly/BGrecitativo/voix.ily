\clef "bass" <>^\markup\character Achior
r4 r8 re sol sol r si |
sol sol r4 sol8 re16 re r8 re |
fa! fa r la fa fa fa fa16 mi |
do8 do r4 mi mi8 fa |
sol la16 sib sib8 mi r mi16 fa sol8 sol16 fa |
re4 r16 re re fa mi8 mi r mi |
sold sold si do' la la r la |
la la si do' do' sol sib la |
fa fa r4 fa fa8 sol |
mib8 mib r4 la4 la8 do' |
do'4 mib8 fa re4 r |
fa4 fa8 sol lab lab r4 |
fa8 fa16 fa fa8 mib do do r4 |
sol8 sol r sol16 la fad8 fad r4 |
re8 re16 mi fa!8 sol mi! mi r16 mi mi fad |
sol8 sol sol mi16 fad re8 re r fad |
fad fad fad si la la r mi16 fad |
sol4 sol8 fad re re r4 |
la4 si8 do'! si si r re16 mi |
fa!4 fa8 sol mi mi r16 la fa mi |
do8 do r4 r2 |
