Giu -- dit -- ta, O -- zì -- a, po -- po -- li, a -- mi -- ci, io ce -- do,
vin -- to son i -- o. Pren -- de un no -- vel -- lo a -- spet -- to
o -- gni co -- sa per me. Da quel che fu -- i
non so chi mi tra -- sfor -- ma, in me l’an -- ti -- co A -- chi -- or più non tro -- vo. Al -- tri pen -- sie -- ri,
sen -- to al -- tre vo -- glie in me. Tut -- to son pie -- no,
tut -- to del vo -- stro Di -- o. Gran -- de, in -- fi -- ni -- to,
u -- ni -- co lo con -- fes -- so. I fal -- si nu -- mi
o -- dio, de -- te -- sto, e i ver -- go -- gno -- si in -- cen -- si,
che lor cre -- du -- lo of -- fer -- si. Al -- tri non a -- mo,
non co -- no -- sco al -- tro Di -- o, che il Dio d’A -- bra -- mo.
