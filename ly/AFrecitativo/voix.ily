\clef "tenor/G_8" <>^\markup\character Ozia
la8 la16 la la8 si dod' dod' r dod'16 mi' |
dod'4 si8 la re' re' r4 |
r8 la la re' do'! do' r16 sol sol la |
sib4 do'8 sol la la r4 |
do'4 si!8 la sold sold r sold |
si4 r16 si si do' re'8 re' r16 re' re' fa' |
re'8 re' r si re' re' re' do' |
la la r16 la si do' do'8 sol! r sol |
sib sib sib do' la la r16 la la sib |
do'8 do'16 do' mib'8 re' sib4 r8 sib |
sib sib do' re' re' la r la16 la |
la4 la8 sib do' do' r do'16 mib' |
do'8 do' sib sol sol4 r16 sib do' re' |
do'8 do' r do' do' sol sol la |
sib sib16 sib sib8 re' sib sib r sol16 la |
sib4 r8 sib16 do' la4 r16 la la la |
re'8 re' r fa' re' la la sold |
si!8 si r4 r sol! |
do'4 r8 do' mi' mi'16 mi' mi'8 re'16 mi' |
do'8 do' r do'16 do' do'8 sol16 la sib8 la |
fa fa r fa la la la sib |
do' do' do' re' sib sib r4 |
sib4 sib8 re' sib sib r sib16 sol |
dod'4 dod'8 re' la4 r |
\ffclef "soprano/treble" <>^\markup\character Cabri
si'!4 si'8 mi'' mi'' si' r16 si' si' do'' |
re''8 re'' r4 re''8 re''16 re'' re''8 fa'' |
re'' re'' r si'16 do'' re''4 re''8 do'' |
la' la' r do'' si' si' r fad' |
fad' fad' fad' sol' la'4 la'8 do'' |
la' la' la' sol'16 la' fad'8 fad' r red''16 fad'' |
fad''4 la'8 sol' mi'4 r |
re''!8 re'' r re'' re'' la' r la'16 la' |
la'4 la'8 si' do''16 do'' do'' mi'' do''8 do''16 re'' |
si'8 si' r4 si' si'8 do'' |
re''8 re'' r4 fa''8 re''16 re'' r re'' re'' mi'' |
do''8 do'' r mi'' do'' do'' r16 do'' do'' si' |
sol'8 sol' r4
\ffclef "soprano/treble" <>^\markup\character Amital r8 sib' sib' mib'' |
mib'' sib' r4 sib'8 sib'16 sib' sib'8 do'' |
reb''8 reb'' r reb'' sib' sib' r sib'16 sib' |
sol'8 sol' r4 sol' sol'8 lab' |
sib'8 sib'16 sib' do''8 sol' lab'4 r |
do''4 do''8 do'' la'!8 la' r do'' |
do'' fad' r la'16 sib' do''4 do''8 sib' |
sol'8 sol' r4 sib' sib'8 do'' |
re''8 re'' re'' mib''16 fa'' fa''8 sib' sib' do'' |
lab' lab' fa'16 fa' fa' sol' lab'4 lab'8 sib' |
sol'4 r16 sib' do'' reb'' do''8 do''16 sol' sib' sib' sib' lab' |
fa'8 fa' r16 do'' do''8 la'! la' r4 |
la'8 la'16 la' la'8 sib' do'' do'' r16 do'' do'' mib'' |
do''8 do'' r la' do'' do'' do'' reb'' |
sib'4 r16 sib' sib' reb'' do''8 do'' r sol' |
sol' sol' sol' lab' sib' sib' r mi''!16 sol'' |
sol''4 sib'8 lab' fa' fa' r fa' |
lab' lab' r lab' re'' re''16 re'' re''8 fa'' |
fa''8 si'! r4 si'8 si'16 si' si'8 do'' |
re'' re''16 re'' re''8 do'' la'!4 r |
do''8 do''16 do'' do''8 la' fad' fad' r la' |
do'' do'' do'' la' sib'4 r |
sib'4 sib'8 sol' mib' mib' r mib' |
reb'' reb'' reb'' do'' lab'4 r16 do'' sib' sib' |
la'!8 la' r do''16 reb'' mib''4 mib''8 reb'' |
sib'4 r fa''8 sib'16 sib' r8 sib' |
sol'!8 sol' r sol' sib'4 r |
mib''4 r8 sib'16 sib' sol'8 sol' r4 |
sol'8 sol'16 sol' sol'8 lab' sib' sib' sib' dob'' |
reb''4 reb''8 dob'' lab' lab' r dob'' |
sib' sib' r fa' fa'4 fa'8 solb' |
lab'8 lab'16 lab' r8 solb' mib'4 r16 sib' sib' reb'' |
do''8 do'' r8 sol'!16 lab' sib'4 sib'8 lab' |
fa'8 fa' r16 do'' do'' re''! si'!8 si' r si' |
re'' re'' re'' mib'' do'' do'' r sol' |
do'' do'' r do'' mib'' mib'' re'' mib'' |
do'' do'' r si' re'' re'' r4 |
