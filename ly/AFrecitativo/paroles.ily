Già le me -- mo -- rie an -- ti -- che
dun -- que an -- da -- ro in o -- bli -- o? Che in -- gra -- ta è que -- sta
di -- men -- ti -- can -- za, o fi -- gli! Ah ci sov -- ven -- ga
chi siam, qual Dio n’as -- si -- ste, e quan -- ti, e qua -- li
pro -- di -- gi o -- prò per no -- i. Chi a’ pas -- si no -- stri
di -- vi -- se l’E -- ri -- tre -- o, chi l’on -- de a -- ma -- re
ne rad -- dol -- cì, ne -- gli a -- ri -- di ma -- ci -- gni
chi di lim -- pi -- di u -- mo -- ri
am -- pie ve -- ne ci a -- per -- se, e chi per tan -- te 
i -- gno -- te so -- li -- tu -- di -- ne in -- fe -- con -- de
ci gui -- dò, ci nu -- trì, po -- tre -- mo a -- des -- so
te -- mer che n’ab -- ban -- do -- ni? Ah no! Mi -- nac -- cia il su -- per -- bo O -- lo -- fer -- ne
già da lun -- ga sta -- gion Be -- tu -- lia; e pu -- re
non ar -- di -- sce as -- sa -- lir -- la. Ec -- co -- vi un se -- gno
del ce -- le -- ste fa -- vor.

Sì, ma frat -- tan -- to,
più cru -- del -- men -- te il con -- dot -- tier fe -- ro -- ce
ne di -- strug -- ge se -- den -- do. I fon -- ti, ond’ eb -- be
la cit -- tà, già fe -- li -- ce, ac -- que op -- por -- tu -- ne,
il ti -- ran -- no oc -- cu -- pò. L’on -- da, che re -- sta,
a mi -- su -- ra fra no -- i
scar -- sa -- men -- te si par -- te; on -- de la se -- te
ir -- rì -- ta, e non ap -- pa -- ga;
nu -- tri -- sce e non e -- stin -- gue.

A tal ne -- mi -- co,
che per le no -- stre ve -- ne
si pas -- ce, si dif -- fon -- de, ah con qual’ ar -- mi
re -- si -- ste -- rem? Guar -- da -- ci in vol -- to; os -- ser -- va
a qual se -- gno siam giun -- ti. Al -- le que -- re -- le
a -- bi -- li or -- mai non so -- no, i pet -- ti stan -- chi
dal fre -- quen -- te a -- ne -- lar; le sca -- bre lin -- gue,
le fau -- ci in -- a -- ri -- di -- te. Umo -- re al pian -- to
man -- ca su gli oc -- chi no -- stri, e cre -- sce sem -- pre,
di pian -- ger la ca -- gion. Né il mal più gran -- de
per me, che ma -- dre so -- no,
è la pro -- pria mi -- se -- ria; i fi -- gli, i fi -- gli
ve -- der -- mi, oh Di -- o! mi -- se -- ra -- men -- te in -- tor -- no
lan -- guir co -- sì, nè dal mor -- ta -- le ar -- do -- re
po -- ter -- li ri -- sto -- rar; que -- sta è la pe -- na
che pa -- ra -- gon non ha, che non s’in -- ten -- de
da chi ma -- dre non è. Sen -- ti -- mi, O -- zì -- a;
tu sei, tu che ne reg -- gi,
del -- le mi -- se -- rie no -- stre
la prim e -- ra ca -- gio -- ne. Id -- di -- o ne sia
fra noi giu -- di -- ce, e te. Par -- lar di pa -- ce
con l’As -- si -- ro non vu -- oi; pe -- rir ci ve -- di
fra cen -- to af -- fan -- ni e cen -- to;
e dor -- mi? e sie -- di ir -- re -- so -- lu -- to, e len -- to?
