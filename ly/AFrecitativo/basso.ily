\clef "bass" dod1~ |
dod2 fa~ |
fa mi~ |
mi fa~ |
fa re~ |
re sold~ |
sold1 |
do2 mi~ |
mi fa~ |
fa re~ |
re fad~ |
fad1~ |
fad2 sib, |
mi!1~ |
mi~ |
mi2 fa~ |
fa1 |
mi4 re do2~ |
do1~ |
do |
la,~ |
la,2 re~ |
re sol~ |
sol r4 la |
sold1~ |
sold~ |
sold |
do2 red~ |
red1~ |
red~ |
red2 mi |
fad1~ |
fad |
sol~ |
sol |
mi | \allowPageTurn
r4 re mib2~ |
mib1 |
mi!1~ |
mi~ |
mi2 fa~ |
fa mib |
re1 |
sol~ |
sol2 re~ |
re1 |
mib2 mi |
fa mib~ |
mib la,!~ |
la,1 |
reb2 mi!~ |
mi1~ |
mi2 si,! |
sib,1 |
sold,~ |
sold,2 red~ |
red re~ |
re sol~ |
sol1~ |
sol2 do |
mib1 |
reb2 re |
mib1~ |
mib~ |
mib~ |
mib2 dob |
re1~ |
re2 mib |
mi!1 |
fa~ |
fa2 mib~ |
mib1 |
lab2 sol |
