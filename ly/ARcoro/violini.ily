\clef "treble" <sol mib'>4 r8 r16 <<
  \tag #'violino1 {
    sol'16 sol'4 r8 r16 sib' |
    sib'8. mib''16 mib''8. sol''16 sol''8. sib''16 sib''8. sib''16 |
    sib''8 sol''4 mib'' sib' sol'8 |
    <mib' sol>4 r8 r16 sol' sol'4 r8 r16 do'' |
    do''8. mib''16 mib''8. sol''16 sol''8. do'''16 do'''8. do'''16 |
    do'''8 sol''4 mib'' do'' sol'8 |
    lab4 r8 r16 mib' mib'4 r8 r16 do'' |
    do''8. mib''16 mib''8. lab''16 lab''8. do'''16 do'''8. do'''16 |
    do'''8 lab''4 mib'' do'' lab'8 |
    sib4 r8 r16 fa' fa'4 r8 r16 lab' |
    sol'8. sib'16 sib'8. mib''16 mib''8. sol''16 sol''8. sib''16 |
    sib''8 sol''4 mib'' sib' sol'8 |
  }
  \tag #'violino2 {
    mib'16 mib'4 r8 r16 sol' |
    sol'8. sib'16 sib'8. mib''16 mib''8. sol''16 sol''8. sol''16 |
    sol''8 mib''4 sib' sol' mib'8 |
    do'4 r8 r16 mib' mib'4 r8 r16 mib' |
    mib'8. sol'16 sol'8. mib''16 mib''8. sol''16 sol''8. sol''16 |
    sol''8 mib''4 do'' sol' mib'8 |
    do'4 r8 r16 do' do'4 r8 r16 mib' |
    mib'8. do''16 do''8. mib''16 mib''8. lab''16 lab''8. lab''16 |
    lab''8 mib''4 do'' lab' do'8 |
    sib4 r8 r16 re' re'4 r8 r16 fa' |
    mib'8. sol'16 sol'8. sib'16 sib'8. mib''16 mib''8. sol''16 |
    sol''8 mib''4 sib' sol' mib'8 |
  }
>>
mib'16 mib' mib' mib' sol' sol' sib' sib' mib'' mib'' sib' sib' sol' sol' sib' sib' |
<<
  \tag #'violino1 {
    mib'8 sol''4( sib''8) re' fa''4( sib''8) |
    do' mib''4( do'''8) do' mib''4( do'''8) |
    fa'8 mib''4( do'''8) do' fa'4( mib''8) |
    sib8 fa'4( re''8) sib re''4( fa''8) |
    sib8 fa''4( sib''8) re' sib''4( re''8) |
    mib' do''4( sib''8) mi' do''4( sib''8) |
    la''16 fa' fa' fa' fa'4:16 fa''16 do'' do'' do'' la'' fa'' fa'' fa'' |
    sib'' fa' fa' fa' fa'4:16 fa''16 re'' re'' re'' re''' sib'' sib'' sib'' |
    la'' fa' fa' fa' fa'4:16 la'16 fa' fa' fa' do'' la' la'' fa'' |
    sib'' reb'' reb'' reb'' reb''4:16 sib''16 reb'' reb'' reb'' sib'' reb'' reb'' reb'' |
    do''4
  }
  \tag #'violino2 {
    mib'16 sib' sib' sib' sib'( mib'') mib''( sol'') re' sib' sib' sib' sib'( re'') re''( fa'') |
    do' sol' sol' sol' sol'( do'') do''( mib'') do' sol' sol' sol' sol'( do'') do''( mib'') |
    do' fa' fa' fa' fa'( do'') do''( mib'') la do'' do'' do'' do''( la') la'( fa') |
    sib sib' sib' sib' sib'( fa') fa'( re') re' fa' fa' fa' fa'( sib') sib'( re'') |
    re' sib' sib' sib' sib'( re'') re''( fa'') sib fa'' fa'' fa'' fa''( re'') re''( sib') |
    mib' sol' sol' sol' sol'( do'') do''( mib'') sol'' sol' sol' sol' sol'( mi') mi'( do') |
    do'4 r8 do''16 la' fa'8 mib''!16 do'' la'8 fa''16 mib'' |
    re''4 r8 fa'16 re' sib8 re''16 sib' fa'8 fa''16 re'' |
    do''4 r8 do''16 la' fa'8 do''16 la' fa'8 do'' |
    reb''16 sib' sib' sib' sib'4:16 reb''16 sib' sib' sib' reb'' sib' sib' sib' |
    la'4
  }
>> r4 fa'16 sol' fa' mib' re'! mib' re' do' |
sib8 <<
  \tag #'violino1 {
    sib''8 r sib'' r fa''16 re'' sib''8 fa''16 re'' |
    sol''8 sol'' r sol'' r mib''16 do'' sol''8 mib''16 do'' |
    la''8 la'' r do'''16 la'' sib''8 sib' r re''16 sib' |
    mib''8 do''16 fa' re''8 sib'16 fa' do''8 sib'' do'' la'' |
  }
  \tag #'violino2 {
    re'4 fa' sib' sib8~ |
    sib mib'4 sol' sib' sol'8 |
    mib' do'4 mib'8 re' re''4 sib'8 |
    fa' fa''4 fa'8 fa' fa'' mib'' do'' |
  }
>>
<sib re' sib'>4 r8 r16 <<
  \tag #'violino1 {
    re''16 re''4 r8 r16 fa'' |
    fa''8. sib''16 sib''8. fa''16 fa''8. re''16 re''8. sib'16 |
    sib'8. re''16 re''8. sib'16 sib'8.
  }
  \tag #'violino2 {
    fa'16 fa'4 r8 r16 re'' |
    re''8. fa''16 fa''8. re''16 re''8. sib'16 sib'8. fa'16 |
    fa'8. sib'16 sib'8. fa'16 fa'8.
  }
>> sib16 sib8. lab!16 |
sol8. <<
  \tag #'violino1 {
    si'16 si'8. re''16 re''8. sol''16 sol''8. si''16 |
    si''8. re''16 re''8. si''16 si''8. re''16 re''8. re'''16 |
    re'''8. fa''16 fa''8. re''16 re''8. sol'16 sol'8. fa''16 |
    mib''4 r8 r16 <mib' sol>16 q4 r8 r16 sol' |
    sol'8. do''16 do''8. mib''16 mib''8. sol''16 sol''8. do'''16 |
    reb'''8 reb''4 fa'' sib' reb''8 |
    do''8 do'''4 do'' do''' do''8~ |
    do'' lab''4 lab' fa'' lab''8 |
    lab''4 sol'' r2 |
    fa''16 fa' fa' fa' fa'4:16 la'16 fa' fa' fa' do'' la' la'' fa'' |
    sib'' sib' sib' sib' sib'4:16 fa''16 sib' sib' sib' lab''! fa'' fa'' fa'' |
    sol'' mib' mib' mib' mib'4:16 sib'16 sol' sol' sol' mib'' sib' sib' sib' |
    sol'' mib'' mib'' mib'' mib'' do'' do'' do'' sol' mib'' mib'' mib'' mib'' do''' do''' do''' |
    sib''2:16 la'':16 |
    sol''4 r8 r16 sol' sol'4 r8 r16 sib' |
    sib'8. re''16 re''8. sol''16 sol''8. sib''16 sib''8. re'''16 |
    re'''8 sol''4( sib''8) sol sol''4( sib''8) |
    mi' sol''4( sib''8) reb'8 fa''4( sib''8) |
    do' mi''4( sol''8) do' sol''4( sib''8) |
    fa' fa''4( lab''8) fa' do'''4( mib''!8) |
    re''16 sib'' sib'' sib'' sib'' re'' fa'' re'' mib'' sib'' sib'' sib'' sib'' mib'' sol'' mib'' |
    fa'' lab'' lab'' lab'' lab'' fa'' re'' lab' sol' sol'' sol'' sol'' sol'' mib'' sib' sol' |
    mib' solb'' solb'' solb'' solb''4:16 la''2:16 |
    sib''4
  }
  \tag #'violino2 {
    sol'16 sol'8. si'16 si'8. re''16 re''8. sol''16 |
    lab''!8. si'16 si'8. re''16 re''8. lab'!16 lab'8. lab''16 |
    lab''8. re''16 re''8. si'16 si'8. sol'16 re'8. re''16 |
    do''4 r8 r16 do' do'4 r8 r16 mib' |
    mib'8. sol'16 sol'8. do''16 do''8. mib''16 mib''8. sol''16 |
    lab''8 lab'4 fa' sib reb'8 |
    do'8 do''4 do' do'' do'8~ |
    do' do''4 fa' lab' fa''8 |
    fa''4 mi'' r2 |
    r8 do''16 la' fa'8 fa'' r do''16 la' fa'8 fa'' |
    r fa'16 re' sib8 sib' r fa'16 re' sib8 sib' |
    r sib'16 sol' mib'8 mib'' r mib'16 sib sol8 sol' |
    r sol'16 mib' do'8 mib' r mib'16 do' sol8 mib'' |
    sol''2:16 fad'':16 |
    sol''4 r8 r16 sib sib4 r8 r16 re' |
    re'8. sib'16 sib'8. re''16 re''8. sol''16 sol''8. sib''16 |
    sib''16 sib' sib' sib' sib'( re'') re''( sol'') re' sib' sib' sib' sib'( re'') re''( sol'') |
    sol'16 sib' sib' sib' sib'( reb'') reb''( sol'') sib sib' sib' sib' sib'( reb'') reb''( fa'') |
    mi'' do' do' do' do'( mi') mi'( sol') sol do'' do'' do'' sol'( mi') mi'( do') |
    lab lab' lab' lab' lab'( do'') do''( fa'') do'( fa') fa' fa' fa'( la') la'( do'') |
    sib'8 sib16 re' fa'8 sib' r mib'16 sol' sib'8 mib'' |
    r sib16 re' fa'8 sib' r sib16 mib' sol'8 mib' |
    mib'16 mib'' mib'' mib'' mib''4:16 mib''2:16 |
    re''4
  }
>> r8 r16 <sib' re'>16 q4 r8 r16 <fa' re''>16 |
q8 fa''16 fa'' lab''! lab'' fa'' fa'' re'' re'' sib' sib' lab' lab' fa' fa' |
<<
  \tag #'violino1 {
    <sib mib'>8 sib'' r sib'' r sol''!16 mib'' sib'8 sol'16 mib' |
    re'8 sol'' r si'' r8 fa''16 re'' si'8 fa'16 re' |
    do'8 mib'' r sol'' r do'''16 sol'' mib''8 do''16 sol' |
    mib'8 sib'' r reb'' r sib''16 reb'' mib'8 sib'16 reb'' |
    \ru#2 { do''-> do'' do'' do'' do'' do'' do'' do'' } |
    \ru#2 { mib''-> mib'' mib'' mib'' mib'' mib'' mib'' mib'' } |
    re''-> re'' re'' re'' re'' re'' re'' re'' sib''-> sib'' sib'' sib'' sib'' sib'' sib'' sib'' |
    la''2:16 la'':16 |
    sib''2
  }
  \tag #'violino2 {
    <mib' sol>8 sol'4 sib' mib'' mib'8 |
    fa'8 si'4 re'' fa'' si8 |
    do' sol'4 mib'' sol'' mib'8 |
    reb' sib'4 sib' mib' mib''8 |
    \ru#2 { mib'16-> mib' mib' mib' mib' mib' mib' mib' } |
    \ru#2 { la'16-> la' la' la' la' la' la' la' } |
    sib'-> sib' sib' sib' sib' sib' sib' sib' re''-> re'' re'' re'' re'' re'' re'' re'' |
    mib''2:16 mib'':16 |
    re''2
  }
>> r\fermata |
<sol mib'>4 r8 r16 <<
  \tag #'violino1 {
    sol'16 sol'4 r8 r16 sib' |
    sib'8. mib''16 mib''8. sol''16 sol''8. sib''16 sib''8. sib''16 |
    do'''8 lab''4 mib'' do'' lab'8 |
    fa' re'4 fa' re' fa'8 |
  }
  \tag #'violino2 {
    mib'16 mib'4 r8 r16 sol' |
    sol'8. sib'16 sib'8. mib''16 mib''8. sol''16 sol''8. sol''16 |
    lab''8 mib''4 do'' lab' mib'8 |
    re' sib4 re' sib re'8 |
  }
>>
<sol mib'>4 r8 r16 <<
  \tag #'violino1 {
    sol'16 sol'4 r8 r16 sib' |
    sib'8. mib''16 mib''8. sol''16 sol''8. sib''16 sib''8. sib''16 |
    do'''8 lab''4 mib'' do'' lab'8 |
    sol' mib'4 sol' sib'8 lab'([ fa']) |
  }
  \tag #'violino2 {
    mib'16 mib'4 r8 r16 sol' |
    sol'8. sib'16 sib'8. mib''16 mib''8. sol''16 sol''8. sol''16 |
    lab''8 mib''4 do'' lab' mib'8 |
    mib' sib4 mib' sol'8 fa'([ re']) |
  }
>>
mib'16 mib' sib sib sol sol sib sib mib' mib' sol' sol' sib' sib' sol' sol' |
mib' mib' sib sib sol sol sib sib mib' mib' sol' sol' sib' sib' sol' sol' |
mib'4 <sol mib' mib''>4 q r |
