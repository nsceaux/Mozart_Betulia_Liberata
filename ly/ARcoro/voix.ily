<<
  \tag #'vsoprano {
    \clef "soprano/treble" mib''2. mib''4 |
    mib'' mib'' r2 |
    R1 |
    mib''2. mib''4 |
    mib''4 r r2 |
    R1 |
    do''2. do''4 |
    do'' do'' r2 |
    R1 |
    sib'2. sib'4 |
    sol' r r2 |
    R1*2 |
    r4 sib' sib' sib' |
    mib''2 mib''4 mib''8 mib'' |
    mib''2 mib''4 mib'' |
    mib'' re'' r2 |
    sib'2. fa''8[ re''] |
    do''4 do'' do'' sib' |
    la' r r2 |
    re''2. re''4 |
    do'' do'' r2 |
    reb''2. sib'4 |
    la' r r2 |
    r4 sib' sib' sib' |
    sol''2 sol'' |
    la' sib' |
    mib''4 re''8 re'' do''2 |
    sib'4 r r2 |
    R1*2 |
    r4 si' si' re'' |
    re''2 re'' |
    r4 re''8 re'' re''4 sol'8 fa'' |
    mib''4 do'' r2 |
    R1 |
    reb''2 reb''4 reb'' |
    do''2. sib'!4 |
    lab'2 fa''4 lab' |
    lab' sol' r2 |
    r4 la' la' la' |
    sib'2 sib'4 sib' |
    sib'2 sib' |
    mib'' mib''4 mib'' |
    re''2. re''4 |
    sib' r r2 |
    R1 |
    sib'2 sib'4 sib' |
    sib'4 sib' r sib'8 sib' |
    sib'2 sol''4 sib' |
    lab'! r r2 |
    re''2 mib''4 mib'' |
    fa''2 mib''4 mib''8 mib'' |
    solb''2 mib''4. la'8 |
    sib'4 r r2 |
    R1 |
    sib'2 sib'4 sib' |
    si'2 si' |
    r4 do''2 do''4 |
    reb''2 reb'' |
    do''2. do''4 |
    mib'' mib'' mib'' la' |
    sib' sib' r2 |
    r4 mib'' mib'' la' |
    sib' sib' r2\fermata |
    R1 |
    mib''2. mib''4 |
    do'' do'' r2 |
    sib'2. sib'4 |
    sol'4 r r2 |
    sib'2. mib''4 |
    mib'' do'' r2 |
    sib'2. re'4 |
    mib' r r2 |
    R1*2 |
  }
  \tag #'valto {
    \clef "alto/treble" sol'2. sol'4 |
    sol'4 sol' r2 |
    R1 |
    sol'2. sol'4 |
    sol' r r2 |
    R1 |
    lab'2. lab'4 |
    lab' lab' r2 |
    R1 |
    fa'2. re'4 |
    mib' r r2 |
    R1*2 |
    r4 sol' fa' fa' |
    sol'2 sol'4 sol'8 sol' |
    fa'2 fa'4 fa' |
    fa'4 fa' r2 |
    fa'2. fa'4 |
    sol' sol' sol' sol' |
    fa' r r2 |
    fa'2. fa'4 |
    fa' fa' r2 |
    sib'2. sol'4 |
    fa' r r2 |
    r4 fa' fa' fa' |
    mib'2 mib' |
    mib' re' |
    fa'4 fa'8 fa' fa'2 |
    fa'4 r r2 |
    R1*2 |
    r4 sol' sol' sol' |
    lab'!2 lab' |
    r4 sol'8 sol' sol'4 sol'8 sol' |
    sol'4 sol' r2 |
    R1 |
    lab'2 sib'4 sib' |
    lab'2 mi' |
    fa' lab'4 fa' |
    fa' mi' r2 |
    r4 fa' fa' fa' |
    fa'2 fa'4 lab'! |
    sol'2 sol' |
    sol' sol'4 sol' |
    sol'2 fad' |
    sol'4 r r2 |
    R1 |
    sol'2 sol'4 sol' |
    sol'4 sol' r fa'8 fa' |
    mi'2 do'4 sol' |
    fa'4 r r2 |
    fa'2 sol'4 sol' |
    lab'2 sol'4 sol'8 sol' |
    mib'2 solb'4. solb'8 |
    re'4 r r2 |
    R1 |
    sol'!2 sol'4 sol' |
    sol'2 sol' |
    r4 sol'2 sol'4 |
    mib'2 mib' |
    mib'2. mib'4 |
    do' fa' fa' fa' |
    fa' fa' r2 |
    r4 do' fa' fa' |
    fa' fa' r2\fermata |
    R1 |
    sol'2. sol'4 |
    lab' lab' r2 |
    fa'2. fa'4 |
    mib'4 r r2 |
    sol'2. sol'4 |
    lab' lab' r2 |
    mib'2. sib4 |
    sib4 r r2 |
    R1*2 |
  }
  \tag #'vtenore {
    \clef "tenor/G_8" sib2. sib4 |
    sib sib r2 |
    R1 |
    do'2. do'4 |
    do' r r2 |
    R1 |
    mib'2. mib'4 |
    mib' mib' r2 |
    R1 |
    re'2. fa4 |
    sol r r2 |
    R1*2 |
    r4 sol sib sib |
    do'2 do'4 do'8 do' |
    do'2 do'4 do' |
    do'4 sib r2 |
    re'2. sib4 |
    sib sib sol do' |
    do' r r2 |
    sib2. sib4 |
    la la r2 |
    sol2. reb'4 |
    do' r r2 |
    r4 re'! re' sib |
    do'2 do' |
    do' sib |
    do'4 sib8 sib sib4( la) |
    sib4 r r2 |
    R1*2 |
    r4 re' re' si |
    re'2 re' |
    r4 si8 si sol4 re'8 re' |
    do'4 mib' r2 |
    R1 |
    fa'2 fa'4 mib' |
    mib'2 do' |
    do' re'4 re' |
    do' do' r2 |
    r4 do' do' do' |
    sib2 sib4 fa' |
    mib'2 mib' |
    sol mib'4 do' |
    sib2 la |
    sol4 r r2 |
    R1 |
    re'2 re'4 re' |
    reb'4 reb' r fa8 fa |
    sol2 sol4 do' |
    do'4 r r2 |
    sib2 sib4 sib |
    sib2 sib4 sib8 sib |
    la2 la4. mib'8 |
    fa'4 r r2 |
    R1 |
    mib'2 sib4 mib' |
    fa'2 fa' |
    r4 mib'2 mib'4 |
    sib2 sib |
    lab2. lab4 |
    la la do' mib' |
    re' re' r2 |
    r4 la do' mib' |
    re' re' r2\fermata |
    R1 |
    sib2. sib4 |
    do' mib' r2 |
    re'2. re'4 |
    mib'4 r r2 |
    mib'2. sib4 |
    do' mib' r2 |
    sol2. fa4 |
    mib r r2 |
    R1*2 |
  }
  \tag #'vbasso {
    \clef "bass" mib2. mib4 |
    mib mib r2 |
    R1 |
    do2. do4 |
    do r r2 |
    R1 |
    lab2. lab4 |
    lab lab, r2 |
    R1 |
    sib2. sib,4 |
    mib4 r r2 |
    R1*2 |
    r4 mib re re |
    do2 do'4 do'8 sib |
    la2 la4 la |
    sib4 sib, r2 |
    re2. re4 |
    mib mib mi mi |
    fa r r2 |
    fa2. fa4 |
    fa fa r2 |
    mi2. mi4 |
    fa r r2 |
    r4 sib, re re |
    mib2 mib |
    fa sol |
    la4 sib8 sib fa2 |
    sib,4 r r2 |
    R1*2 |
    r4 sol sol sol |
    fa2 fa |
    r4 si,8 si, si,4 si,8 si, |
    do4 do r2 |
    R1 |
    fa2 sol4 sol |
    lab2 sol |
    fa si,4 si, |
    do do r2 |
    r4 mib! mib mib |
    re2 re4 re |
    mib2 mib |
    do do4 do |
    re2 re |
    sol,4 r r2 |
    R1 |
    sol2 sol4 sol |
    mi4 mi r reb8 reb |
    do2 mi4 mi |
    fa r r2 |
    sib2 sol4 mib |
    re2 mib4 mib8 mib |
    dob2 dob4. dob8 |
    sib,4 r r2 |
    R1 |
    mib2 mib4 mib |
    re2 re |
    r4 do2 do'4 |
    sol2 sol |
    lab2. lab,4 |
    do4. do8 do4 do |
    sib, sib, r2 |
    r4 do do do |
    sib, sib, r2\fermata |
    R1 |
    mib2. mib4 |
    lab4 lab, r2 |
    sib,2. sib,4 |
    mib4 r r2 |
    mib2. mib4 |
    lab lab, r2 |
    sib,2. sib,4 |
    mib4 r r2 |
    R1*2 |
  }
>>