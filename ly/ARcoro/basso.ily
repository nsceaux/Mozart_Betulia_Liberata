\clef "bass" \ru#5 { mib8 sol sib sol } mib sol sib mib' |
\ru#4 { do mib sol mib } |
\ru#2 { do mib sol do' } |
\ru#4 { lab,8 do mib do } |
\ru#2 { lab, do mib lab } |
sib, re fa re sib, re fa sib |
\ru#3 { mib sol sib sol } mib sol sib mib' |
mib mib sol sib mib' sib sol sib |
mib sol sib mib re fa sib re |
do mib sol mib do sol do' sib |
la do' mib' do' la la la la |
sib sib, sib, sib, sib, re fa sib |
re fa sib fa re fa sib re |
mib sol do' sol mi sol do' mi |
fa la? do' la! mib'! do' mib' do' |
fa sib re' sib fa sib re' sib |
fa la do' la mib' do' la fa |
mi sol sib sol mi sol sib mi |
fa4 r fa16 sol fa mib! re mib re do |
sib,8 sib, sib, sib, re re re re |
mib mib mib mib mib mib mib mib |
fa fa fa fa sol sol sol sol |
la la sib sib fa fa fa fa |
\ru#5 { sib,8 re fa re } sib,8. sib,16 sib,8. lab,!16 |
sol,8 sol sol sol sol sol sol sol |
fa fa fa fa fa fa fa fa |
si, si, si, si, si, si, si, si, |
\ru#4 { do mib sol mib } |
fa fa fa fa sol sol sol sol |
lab lab lab lab sol sol sol sol |
fa fa fa fa si, si, si, si, |
do4 do r2 |
mib!8 mib mib mib mib mib mib mib |
re re re re re re re re |
mib mib mib mib mib mib mib mib |
do do do do do do do do |
re re re re re re re re |
\ru#6 { sol sib re' sib } |
mi sol sib sol reb fa sib fa |
do mi sol do' mi sol do' mi |
fa lab do' lab fa la do' fa |
sib sib sib sib sol sol mib mib |
re re re re mib mib mib mib |
dob dob dob dob dob dob dob dob |
sib, re fa re sib, re fa re |
sib,4 r8 fa' re' sib lab fa |
mib mib mib mib mib mib mib mib |
re re re re re re re re |
do do do do do do do do |
sol sol sol sol sol sol sol sol |
lab lab lab lab lab, lab, lab, lab, |
do do do do do do do do |
sib, sib, sib, sib, sib, sib, sib, sib, |
do do do do do do do do |
sib,2 r\fermata |
\ru#3 { mib8 sol sib sol } mib sol sib mib |
lab, do mib do lab, do mib lab, |
sib, re fa re sib, re fa sib, |
\ru#3 { mib sol sib sol } mib sol sib mib |
lab, do mib do lab, do mib lab, |
sib, mib sol mib sib, sib, sib, sib, |
mib sib, sol, sib, mib sol sib sol |
mib sib, sol, sib, mib sol sib sol |
mib4 mib mib r |

