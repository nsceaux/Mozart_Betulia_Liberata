\clef "alto" \ru#5 { mib'8 sol' sib' sol' } mib' mib' mib' mib' |
do'4 r8 r16 do' do'4 r8 r16 mib' |
mib'8. sol'16 sol'8. do'16 do'8. mib'16 mib'8. mib'16 |
\ru#2 { do'8 mib' sol' do'' } |
\ru#2 { lab do' mib' do' } |
lab8. lab'16 lab'8. do''16 do''8. mib'16 mib'8. mib'16 |
\ru#2 { lab8 do' mib' lab' } |
sib re' fa' re' sib re' fa' sib' |
\ru#3 { mib' sol' sib' sol' } mib' mib' mib' mib' |
mib' mib sol sib mib' sib sol sib |
mib' sib' sol' mib' re' sib' fa' re' |
do' sol' mib' sol' do' mib' sol' sol' |
fa'2:16 fa':16 |
fa'8 re'4 sib8 sib2:16 |
sib8 fa' re' sib sib re' fa' sib' |
sib'4:16 sib'16( sol') sol'( mib') do'2:16 |
<<
  { fa'2 mib' | re'1 | mib' | sol' | fa'4 } \\
  { la2 do' | sib1 | do' | sib | la4 }
>> r4 fa'16 sol' fa' mib' re' mib' re' do' |
sib8 sib4 re' fa' sib'8 |
sol'8 sib4 mib' sol' sib'8 |
do'' mib'4 do'8 sib sol'4 sol8 |
la do' sib re' fa' fa' fa' fa' |
fa' re' sib re' fa' re' sib re' |
fa'8. re'16 re'8. sib'16 sib'8. fa'16 fa'8. re'16 |
re'8. fa'16 fa'8. re'16 re'8. sib16 sib8. lab!16 |
sol8. re'16 re'8. sol'16 sol'8. si'16 si'8. re'16 |
re'8. lab'!16 lab'8. lab'16 lab'8. si16 si8. si'16 |
fa'8. sol'16 sol'8. sol'16 sol'8. re'16 sol8. sol'16 |
sol'8 mib' do' sol sol mib do do' |
do'8. mib'16 mib'8. sol'16 sol'8. do''16 do''8. mib'16 |
fa'8 fa' fa' fa' sol' sol' sol' sol' |
lab' lab' lab' lab' sol' sol' sol' sol' |
fa' fa' fa' fa' si si si si |
do'4 do' r2 |
r4 r8 la16 do' fa'8 la' r do'16 la |
fa8 fa' r fa'16 re' sib8 sib' r fa'16 re' |
sib8 sol' r sib16 sol mib8 sol' r sol'16 mib' |
do'8 mib' r do'16 sol mib8 sol' r sol' |
re'2:16 re':16 |
re'8 sib sol sib re' sib sol sib |
sol8. sol'16 sol'8. sib'16 sib'8. re'16 re'8. sol'16 |
sol'8 re' sib sol sol re' sib re' |
reb' reb' sib sol fa' reb' sib fa |
sol sol' mi' do' do'2:16 |
fa8 fa' lab' fa' fa fa' mib'! do' |
re'4 r8 sib16 re' sol'8 sib' r sib'16 sol' |
fa'8 re' r lab'16 fa' mib'8 sol' r sol16 sib |
la2:16 solb':16 |
fa'8 re' sib re' fa' re' sib re' |
fa' fa' lab'! fa' re' sib lab fa |
mib sib4 sol'! sol' sib8 |
re'8 fa'4 fa' si sol8 |
sol do'4 do' mib' sol'8 |
sib'8 mib'4 mib' sib' sib'8 |
lab' lab lab lab lab lab lab lab |
do' do' do' do' do' do' do' do' |
sib sib sib sib sib sib sib sib |
do' do' do' do' do' do' do' do' |
sib2 r\fermata |
sib8 sol mib sol sib sol mib sol |
sib8. sol'16 sol'8. sib'16 sib'8. mib'16 mib'8. mib'16 |
mib'8 do'4 lab' mib' do'8 |
<fa' lab>1 |
<sol mib'>8 mib' sol' mib' sib sol mib sol |
sib8. sol'16 sol'8. sib'16 sib'8. mib'16 mib'8. mib'16 |
mib'8 do'4 lab' mib' do'8 |
sib sib sib sib sib sib sib sib |
mib' sib sol sib mib' sol' sib' sol' |
mib' sib sol sib mib' sol' sib' sol' |
mib'4 mib' mib' r |
