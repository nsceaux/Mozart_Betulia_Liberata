\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with {
        instrumentName = "Corni in Es."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new ChoirStaff \with {
      instrumentName = \markup\character Coro
    } <<
      \new Staff \withLyrics <<
        \global \keepWithTag #'vsoprano \includeNotes "voix"
      >> \keepWithTag #'vsoprano \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'valto \includeNotes "voix"
      >> \keepWithTag #'valto \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vtenore \includeNotes "voix"
      >> \keepWithTag #'vtenore \includeLyrics "paroles"
      \new Staff \withLyrics <<
        \global \keepWithTag #'vbasso \includeNotes "voix"
      >> \keepWithTag #'vbasso \includeLyrics "paroles"
    >>
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\pageBreak
        s1*7\break s1*4\pageBreak
        s1*4\break s1*5\pageBreak
        s1*6\break s1*6\pageBreak
        s1*6\break s1*5\pageBreak
        s1*4\break s1*5\pageBreak
        s1*5\break s1*7\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
