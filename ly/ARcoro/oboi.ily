\clef "treble" <>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib'2 sol' }
  { mib'1 }
>>
<<
  \tag #'(oboe1 oboi) {
    sib'4 mib'' sol'' sib'' |
    sib''2 sib' |
    do''1~ |
    do''4 mib''
  }
  \tag #'(oboe2 oboi) {
    sol'4 sib' mib'' sol'' |
    sol''2 sol' |
    mib'1~ |
    mib'4 sol'
  }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''4 do''' | }
  { mib''2 | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''2 sol' |
    do''1~ |
    do''~ |
    do'' |
    re'' |
    mib''2 sol'' |
    sib''4 sol'' mib'' sib' |
  }
  \tag #'(oboe2 oboi) {
    mib''2 mib' |
    lab'1~ |
    lab'~ |
    lab' |
    fa' |
    sol'2 mib'' |
    sol''4 mib'' sib' sol' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'8 mib' sol' sib' mib'' sib' sol' sib' |
    sol''2 fa'' |
    mib''1~ |
    mib''~ |
    mib''4 }
  { mib'4 sol'8 sib' mib'' sib' sol' sib' |
    sib'1 |
    sol' |
    fa'2 do''~ |
    do''4 }
>> <<
  \tag #'(oboe1 oboi) {
    re''4 re''2 |
    fa''1 |
    sol'' |
    do'' |
    re'' |
    do'' |
    reb'' |
    do'' |
    re''! |
    sol''1 |
    mib''2 re'' |
  }
  \tag #'(oboe2 oboi) {
    sib'4 sib'2 |
    \tag #'oboi \once\tieDown sib'1~ |
    sib' |
    la' |
    sib' |
    la' |
    sib' |
    la' |
    sib' |
    sib' |
    do''2 sib' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do''4 re'' do''2 |
    sib' re'' |
    fa''1~ |
    fa'' |
    sol'' |
    lab''! |
    fa''( |
    mib'')~ |
    mib'' |
    reb''2 sib'' |
    mib''2 mi'' |
    fa''2 lab'' | }
  { fa'4 sib'2 la'4 |
    sib'1 |
    re''~ |
    re'' |
    si'~ |
    si' |
    re''( |
    do'')~ |
    do'' |
    reb''2 sib'' |
    mib'' mi'' |
    fa''1 | }
>>
<<
  \tag #'(oboe1 oboi) { lab''4 sol'' }
  \tag #'(oboe2 oboi) { fa''4 mi'' }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    fa''1~ |
    fa'' |
    sol'' |
    mib'' |
    sib'2 la' |
  }
  \tag #'(oboe2 oboi) {
    la'1 |
    sib' |
    sib' |
    do'' |
    sol'2 fad' |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol'2 sib'~ |
    sib'1 |
    sib''~ |
    sib''~ |
    sib'' | }
  { sol'1~ |
    sol' |
    sol''~ |
    sol''2 fa'' |
    mi'' sol'' | }
>>
<<
  \tag #'(oboe1 oboi) { lab''!2 la'' | }
  \tag #'(oboe2 oboi) { fa''2 mib''! | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''1 | }
  { re''2 mib'' | }
>>
<<
  \tag #'(oboe1 oboi) { lab''2 sol'' | }
  \tag #'(oboe2 oboi) { fa''2 mib'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { solb''2 la'' |
    sib''4 sib' }
  { mib''1 |
    re''4 sib' }
>> <<
  \tag #'(oboe1 oboi) { re''2 | fa''1 }
  \tag #'(oboe2 oboi) { sib'2 | re''1 }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''1 |
    sol'' |
    sol''2 mib'' |
    reb''1 |
    do'' |
    mib'' |
    re'' |
    mib'' | }
  { mib''2 sib' |
    si'1 |
    do'' |
    sib' |
    lab' |
    la' |
    sib' |
    la' | }
>>
<<
  \tag #'(oboe1 oboi) { re''2 }
  \tag #'(oboe2 oboi) { sib' }
>> r2\fermata |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mib'2 sol' |
    sib'1 |
    do'' |
    fa'' |
    sol'' |
    sib' |
    do'' | }
  { mib'1 |
    sol' |
    lab' |
    re'' |
    mib'' |
    sol' |
    lab' | }
>>
<<
  \tag #'(oboe1 oboi) {
    sol''2. fa''4 |
    sol''1~ |
    sol''~ |
    sol''4 mib'' mib''
  }
  \tag #'(oboe2 oboi) {
    mib''2. re''4 |
    mib''1~ |
    mib''~ |
    mib''4 sol' sol'
  }
>> r4 |
