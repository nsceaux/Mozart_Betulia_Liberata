\clef "treble" \transposition mib <>
<<
  \tag #'(corno1 corni) {
    do''1~ |
    do''~ |
    do'' |
    do''~ |
    do''~ |
    do'' |
    do''~ |
    do''~ |
    do'' |
    sol' |
  }
  \tag #'(corno2 corni) {
    do'1~ |
    do'~ |
    do' |
    mi'~ |
    mi'~ |
    mi' |
    do'~ |
    do'~ |
    do' |
    sol |
  }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''1~ |
    do''~ |
    do''8 do' mi' sol' do'' sol' mi' sol' | }
  { do'1~ |
    do'~ |
    do'8 do' mi' sol' do'' sol' mi' sol' | }
>>
<<
  \tag #'(corno1 corni) { do''2 re'' | mi''1 | }
  \tag #'(corno2 corni) { do'2 sol' | do'1 | }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 |
    re''~ |
    re'' |
    mi'' | }
  { r4 re'' re''2 |
    sol'1~ |
    sol'~ |
    sol' | }
>>
\tag#'corni <>^"a 2." re''1~ |
re''~ |
re'' |
mi'' |
re''4 r r2 |
\twoVoices #'(corno1 corno2 corni) <<
  { re''1 |
    mi'' |
    re''2 mi'' |
    s4 re'' re'' re''8 re'' | }
  { sol'1 |
    do''1 |
    re''2 mi'' |
    s4 re'' re'' re''8 re'' | }
  { s1*3 | r4 }
>>
<<
  \tag #'(corno1 corni) {
    re''4 s re'' s |
    re''1~ |
    re'' |
    mi'' |
  }
  \tag #'(corno2 corni) {
    sol'4 s sol s |
    sol'1~ |
    sol' |
    mi' |
  }
  { s4 r s r | }
>>
R1 |
\twoVoices #'(corno1 corno2 corni) <<
  { mi''1~ | mi''2 do''~ | do''1 | }
  { mi'1~ | mi'~ | mi' | }
>>
r2 r4 \tag#'corni <>^"a 2." do''4 |
do'' r r2 |
R1*2 |
<<
  \tag #'(corno1 corni) { re''1 | re'' | do'' | do'' | }
  \tag #'(corno2 corni) { re''1 | sol' | do' | mi' | }
>>
R1 |
<<
  \tag #'(corno1 corni) { mi''1 }
  \tag #'(corno2 corni) { mi' }
>>
\tag#'corni <>^"a 2." mi'4 mi' mi' mi' |
mi'1 |
R1*3 |
<<
  \tag #'(corno1 corni) {
    re''2 mi'' |
    fa'' mi'' |
    do''1 |
    re''~ |
    re'' |
    do'' |
    re'' |
    do'' |
    do'' |
    do'' |
    do'' |
    re'' |
    do'' |
    re''2
  }
  \tag #'(corno2 corni) {
    sol'2 do'' |
    re'' do'' |
    do'1 |
    sol'~ |
    sol' |
    mi' |
    mi' |
    mi' |
    do' |
    do' |
    do' |
    sol' |
    do' |
    sol2
  }
>> r2\fermata |
<<
  \tag #'(corno1 corni) {
    do''1 |
    do'' |
    do'' |
    re'' |
    do'' |
    do'' |
    do'' |
    mi''2. re''4 |
    do''1~ |
    do''~ |
    do''4 do'' do''
  }
  \tag #'(corno2 corni) {
    do'1 |
    do' |
    do' |
    sol' |
    mi' |
    do' |
    do' |
    do''2. sol'4 |
    mi'1~ |
    mi'~ |
    mi'4 mi' mi'
  }
>> r4 |
