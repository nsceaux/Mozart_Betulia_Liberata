Oh pro -- di -- gio! Oh stu -- por!
Oh pro -- di -- gio! Oh stu -- por! Pri -- va -- ta as -- su -- me
del -- le pub -- bli -- che cu -- re
Don -- na im -- bel -- le il pen -- sier!
Oh pro -- di -- gio! Oh stu -- por!
Con chi go -- ver -- na
non di -- vi -- de i con -- si -- gli! A’ ri -- schi e -- spo -- sta
im -- pru -- den -- te non sem -- bra! Or -- na con tan -- to
stu -- dio se stes -- sa, e non ri -- sve -- glia un so -- lo
dub -- bio di sua vir -- tù! Nul -- la pro -- met -- te;
e fa tut -- to spe -- rar!
Nul -- la pro -- met -- te;
e fa tut -- to spe -- rar!
Qual fra’ vi -- ven -- ti
può l’au -- to -- re ig -- no -- rar di tai por -- ten -- ti,
di tai por -- ten -- ti?
Oh pro -- di -- gio! oh stu -- por!
oh pro -- di -- gio! oh stu -- por!
