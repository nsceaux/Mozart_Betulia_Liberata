\clef "soprano/treble" <>^\markup\character Cabri
r8 la' la' re'' re'' la' r la' |
la' la' la' si' do'' do''16 do'' do''8 re'' |
si' si' r si' si'8. si'16 si'8 do'' |
la' la' r red''16 fad'' fad''4 la'8 si' |
sol' sol' r16 sol' sol' sol' do''8 do'' do'' mi'' |
do'' do'' r do''16 mi'' mi''8 si' r si'16 lad' |
dod''8 dod''
\ffclef "soprano/treble" <>^\markup\character Amital
r8 dod''16 dod'' la'!4 la'8 la' |
fad' fad' fad' sol' la' la' r4 |
la'8 la'16 la' la'8 si' do''! do'' r do''16 mi'' |
do''4 do''8 si' sol' sol'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sol |
do' do' r4
\ffclef "soprano/treble" <>^\markup\character Cabri
do''4 r8 do'' |
si' si' r4 la'8 la'16 la' la'8 sol' |
mi' mi'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si si si si mi' |
re' re' r la16 la la4 la8 si |
do'8 do'16 do' si8 do' la la r4 |
do'8 do'16 do' re'8 la si si r4 |
\ffclef "soprano/treble" <>^\markup\character Amital
re''4 si'8 re'' re'' sol' r si'16 do'' |
re''4 re''8 mi'' do'' do'' r16 do'' sib' do'' |
la'4 r la'8 la'16 la' la'8 sib' |
do'' do'' r do''16 re'' mib''4 mib''8 re'' |
sib'4 r16 sib' sib' re'' si'8 si' r si'16 do'' |
re''4 re''8 mi''! do'' do'' r16 do'' re'' mi'' |
re''8 re'' r la' la' la' la' si' |
do''16 do'' do'' do'' do''8 si'16 do'' la'8 la' r la' |
do'' do'' r re'' si' si' r4 |
r8 sol' sol' la' si' si'
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 re' |

