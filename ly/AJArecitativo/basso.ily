\clef "bass" fad1~ |
fad |
sol |
red |
mi~ |
mi2 sol |
fad1~ |
fad~ |
fad~ |
fad2 si, |
mi1 |
red |
mi |
fad~ |
fad~ |
fad2 sol~ |
sol1~ |
sol2 mi |
fa1~ |
fa |
re2 fa~ |
fa mi |
fad1~ |
fad~ |
fad2 sol~ |
sol1 |
