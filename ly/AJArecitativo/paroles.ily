Chi è co -- ste -- i, che qual sor -- gen -- te au -- ro -- ra
s’ap -- pres -- sa a no -- i; ter -- ri -- bi -- le d’a -- spet -- to
qual fa -- lan -- ge or -- di -- na -- ta; e a pa -- ra -- go -- ne
del -- la lu -- na, del sol bel -- la ed e -- let -- ta?

Al -- la chio -- ma ne -- glet -- ta, al roz -- zo man -- to, al -- le di -- mes -- se ci -- glia,
di Me -- ra -- ri è la fi -- glia.

Giu -- dit -- ta!

Sì, la fi -- da
ve -- do -- va di Ma -- nas -- se.

Qual mai ca -- gion la tras -- se
dal se -- gre -- to sog -- gior -- no, in cui s’a -- scon -- de,
vol -- ge il quart’ an -- no or -- ma -- i?

So ch’i -- vi o -- ran -- do
pas -- sa de -- sta le not -- ti,
di -- giu -- na i dì, so che don -- nol -- le il cie -- lo
e ric -- chez -- za, e bel -- tà; ma che di -- sprez -- za
la bel -- tà, la ric -- chez -- za; e tal di -- ven -- ne,
che ri -- tro -- var non spe -- ra in le -- i mac -- chia l’in -- vi -- dia o fin -- ta, o ve -- ra.
Ma non sa -- pre -- i…

Che a -
