Te so -- lo a -- do -- ro,
men -- te in -- fi -- ni -- ta,
men -- te in -- fi -- ni -- ta,
fon -- te di vi -- ta,
fon -- te di vi -- ta,
di ve -- ri -- tà, __
di ve -- ri -- tà,
fon -- te di vi -- ta,
di ve -- ri -- tà,
di ve -- ri -- tà.

In cui si muo -- ve,
da cui di -- pen -- de
quan -- to com -- pren -- de
l’e -- ter -- ni -- tà,
quan -- to com -- pren -- de
l’e -- ter -- ni -- tà,
l’e -- ter -- ni -- tà.

Te so -- lo a -- do -- ro,
men -- te in -- fi -- ni -- ta,
men -- te in -- fi -- ni -- ta,
fon -- te di vi -- ta,
fon -- te di vi -- ta,
di ve -- ri -- tà, __
di ve -- ri -- tà,
fon -- te di vi -- ta,
di ve -- ri -- tà, __
di ve -- ri -- tà.
