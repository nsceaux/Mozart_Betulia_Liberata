\clef "treble" fa' la' fa' |
do''2.~ |
do''4 sib'2\trill |
la'4 <<
  \tag #'violino1 {
    la''4 la'' |
    la''( sol''8) sib'( la' sol') |
    fa'4 fa'' fa'' |
    fa''( mib''8) sol'( fa' mib') |
    re'4 re'' re'' |
    do'' mi''! fa'' |
    \grace mi''8 re'' do''16 sib' la'4 sol' |
    fa' <fa' la' fa''> r |
  }
  \tag #'violino2 {
    fa''4 fa'' |
    fa''( mi''8) sol'( fa' mi') |
    re'4 re'' re'' |
    re''( do''8) mib'( re' do') |
    sib4 sib' sib' |
    sib'4. la'16 sol' fa'4~ |
    fa'8 sol' fa'4 mi'! |
    fa'4 <do' fa' la'> r |
  }
>>
fa'4\p la' fa' |
do''2.~ |
do''4 sib'2\trill |
la'4 do'( la) |
<<
  \tag #'violino1 {
    sol4 sol''4. fa''8 |
    mi''4 fa'' sol''~ |
    sol''8.( la''32 sib'') la''8( sol'') fa''( mi'') |
    fa''( mi'') fa''4 r |
    la''( fa'') re''-. |
    dod'' re'' r |
    sol''( mi'') do''! |
    si' do'' sol'' |
    la''8( fa'' re''4.) do''8 |
    si' sol''4 sol'' sol''8 |
    la''4 do'' re''~ |
    re'' mi''2~ |
    mi''4 fa''2 |
    fa''8 re'''4 re'' fa''8 |
    mi''16( sol'' la'' fa'') mi''8 mi'' re'' re'' |
  }
  \tag #'violino2 {
    r4 re' re'' |
    sib' la' re'~ |
    re'8 re'' do''( sib') la'( sol') |
    fa'4 la r |
    R2. |
    la'8 la' la' la' la' la' |
    sol' sol' sol' sol' sol' sol' |
    sol' sol' sol' sol' do'' do'' |
    do'' la'4 la' la'8 |
    re' si'4 si'8 do'' do'' |
    do''4 la'2 |
    si'2. |
    do'' |
    re''8 fa''4 sol' re''8 |
    do''16( mi'' fa'' re'') do''8 do'' si' si' |
  }
>>
do''4 r\fermata r |
<<
  \tag #'violino1 {
    la''4( fad'' sol'') |
    fa''!( red'' mi'') |
    fa''8 la''4 la'' la''8~ |
    la'' do'''4 do''' do'''8 |
    do'''2.:16 |
    si'':16 |
    do'''4\f do'' mi'' |
    mi''( re''8) fa'( mi' re') |
    do'4 do''' do''' |
    do'''( sib''!8) re'( do' sib) |
    la4 la'' do'' |
    si'4 si'' do''' |
    \grace si''16 la''8 sol''16 fa'' mi''4 re''\trill |
  }
  \tag #'violino2 {
    r4 mi'( re') |
    r do'( si) |
    la8 do''4 do'' do''8~ |
    do'' mi''4 mi'' mi''8 |
    mi''2.:16 |
    re'':16 |
    do''4\f sol' do'' |
    do''( si'8) re'( do' si) |
    la4 la'' la'' |
    la''( sol''8) sib!( la sol) |
    la4 re' la'~ |
    la'4 sol'8 fa' mi' sol' |
    do'4 do'' si' |
  }
>>
do''4 <do'' mi' sol> r |
re'4\p fad' la' |
<<
  \tag #'violino1 {
    do''2.:16 |
    <sib'! re'>:16 |
    <mib' la'>4 do''8 la' sol' fad' |
    sol' sol'16 fad' sol'8 sib' re'' sol'' |
    sib''4 sol'' mi'' |
    dod''4 re''8 fa''!4 la''8 |
    la' la' sol'' sol'' fa'' fa'' |
    mi''8 la'4 dod''' la'8 |
    re''' re''4 re'' re''8~ |
    re'' re''4 re'' re''8 |
    re''4 fa'' mi'' |
    re''8 re'' re'' re'' re'' re'' |
    re'' sol'' fa''4 mi'' |
  }
  \tag #'violino2 {
    <fad' la'>2.:16 |
    sol':16 |
    sol'4. do'8 sib la |
    sol4 r r |
    R2. |
    sib'4 la' fa'!8( re') |
    sol' sol' mi' mi' re' re' |
    dod'4 mi'2 |
    la8 la'4 la' la'8 |
    sib' sib'4 sol' mi'8 |
    fa'4 re'' dod'' |
    la''8 la'' la'' la'' la'' la'' |
    sib''4 re'' dod'' |
  }
>>
<re'' re'>4 r\fermata r |
fa'4 la' fa' |
do''!2.~ |
do''2 sib'4\trill |
la' do'( la) |
sol4 <<
  \tag #'violino1 {
    sol''4. fa''8 |
    mi''4 fa'' sol''~ |
    sol''8.( la''32 sib'') la''8( sol'') fa''( mi'') |
    fa''8. do''16 do''( re'' do'' sib') la'4 |
    re'' sib'' sol'' |
    fad'' sol'' r |
    do'' la'' fa''! |
    mi'' fa''4. fa''8 |
    re'' re'' re'' re'' re'' re'' |
    mi''8 do'''4 mi''16( sol'') fa''8 do'' |
    re'' re'' re'' re'' re'' re'' |
    do'' do'' do'' do'' do'' do'' |
    sib' sib' sib' sib' sib' sib' |
    la'16( do'') do''( fa'') fa''( la'') la''( do''') do'''8 do'' |
    \grace mi''16 re''8 do''16 sib' la'8 la' sol' sol' |
    fa'4 r\fermata r |
    sib''( sold'' la'') |
    sol''!( mi'' fa'') |
    re''8 fa''4 fa'' fa''8~ |
    fa'' re'''4 re''' re'''8 |
    la''2.:16 |
    sol'':16 |
    fa''4 la''\f la'' |
    la''( sol''8) sib'( la' sol') |
    fa'4 fa'' fa'' |
    fa''4( mib''8) sol'( fa' mib') |
    re'4 re'' re'' |
    do'' mi''! fa'' |
    \grace mi''16 re''8 do''16 sib' la'4 sol'\trill |
  }
  \tag #'violino2 {
    re'4 re' |
    do'8 sib la4 re' |
    re'' do''8( sib') la'( sol') |
    fa'8. la'16 la'( sib' la' sol') fa'4 |
    R2. |
    re'8 re' re' re' re' re' |
    do' do' do' do' do' do' |
    do' do' do' do' do' do' |
    fa' fa' fa' fa' sol' sol' |
    sol' mi'4 do' fa'8 |
    fa' do'' sib' la' sib'4 |
    r8 sib' la' sol' la'4 |
    r8 la' sol' fa' sol'4 |
    fa'16( la') la'( do'') do''( fa'') fa''( la'') la''8 fa' |
    fa' sol' fa' fa' mi' mi' |
    re'4 r\fermata r |
    r fa'( mi') |
    r re'( do') |
    fa'8 re''4 re'' re''8~ |
    re'' fa''4 fa'' fa''8 |
    fa''2.:16 |
    fa''2:16 mi''4:16 |
    fa''4 fa''\f fa'' |
    fa''( mi''8) sol'( fa' mi') |
    re'4 re'' re'' |
    re''( do''8) mib'( re' do') |
    sib4 sib' sib' |
    sib'4. la'16 sol' fa'4 |
    fa'8 sol' fa'4 mi'\trill |
  }
>>
fa' <fa' la' fa''> r |
