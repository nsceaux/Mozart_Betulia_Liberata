\clef "alto" R2. |
mi'4 sol' mi' |
fa'2 sol'4 |
do' la' si' |
do'' do' dod' |
re' fa' re' |
la' la la' |
sib'! sol' fa' |
mi' do'! re' |
sib' do'' do' |
fa' fa r |
R2. |
mi'4\p sol' mi' |
fa'2 sol'4 |
do' fa2 |
r4 sib sib |
do' re' sib |
sib' do'' do' |
fa' fa r |
R2. |
la'4( fa') re' |
si do' r |
sol'( mi') mi' |
fa'8 fa' fa' fa' fad' fad' |
sol' sol' sol' sol' mi' mi' |
fa'! fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' |
la' la' la' la' la' la' |
si' si' si' si' si' si' |
do'' fa' sol' sol' fa' fa' |
mi'4 r\fermata r |
r do'( si) |
r la( sol) |
fa8 fa' fa' fa' fa' fa' |
fad' fad' fad' fad' fad' fad' |
sol' sol' sol' sol' sol' sol' |
sol sol sol sol sol sol |
do'4\f mi' fad' |
sol'2 sold'4 |
la' do' red' |
mi'2 mi'4 |
fa'! fa' fa' |
sol'!2 la'8 mi' |
fa' re' sol'4 sol |
do' do' r |
R2. |
re'4\p fad' la' |
sol' sib' re'' |
do' la re' |
sol r r |
R2. |
mi'4 fa'! re' |
mi' la la |
la la' sol' |
fa'8 fa' fa' fa' fa' fa' |
sol' sol' sol' sol' sol' sol' |
la'4 la' la' |
fad'8 fad' fad' fad' fad' fad' |
sol' sol' la' la' la la |
re'4 r\fermata r |
R2. |
mi'4 sol' mi' |
fa' sol' mi' |
do' fa2 |
sib2 la4 |
sol fa sib |
sib' do'' do' |
fa' fa fa |
R2. |
re'4 sib' sol' |
mi' fa' r |
do' la' fa' |
sib8 sib sib sib si si |
do' do' sib'! sib' la' la' |
fa'8 fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' |
re' re' re' re' mi' mi' |
do' do' do' do' do' la |
sib re' do' do' sib sib |
la4 r\fermata r |
r4 re'( do') |
r sib( la) |
sib8 sib sib sib sib sib |
si si si si si si |
do' do' do' do' do' do' |
do' do' do' do' do' do' |
fa'4 sol'\f si' |
do'' do' dod' |
re' fa' sold' |
la' la la |
sib sol'! fa' |
mi' do'! re'8 la |
sib4 do' do' |
fa' fa r |
