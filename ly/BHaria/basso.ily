\clef "bass" R2. |
do4 mi do |
re2 mi4 |
fa la si |
do' do dod |
re fa re |
la la, la |
sib! sol fa |
mi do! re |
sib do' do |
fa fa, r |
R2. |
do4\p mi do |
re2 mi4 |
fa la( fa) |
r sib, sib, |
do re sib, |
sib do' do |
fa fa, r |
R2. |
la4( fa) re |
si, do r |
sol( mi) mi |
fa8 fa fa fa fad fad |
sol sol sol sol mi mi |
fa! fa fa fa fa fa |
sol sol sol sol sol sol |
la la la la la la |
si si si si si si |
do' fa sol sol sold sold |
la4 r\fermata r |
r do( si,) |
r la,( sol,) |
fa,8 fa fa fa fa fa |
fad fad fad fad fad fad |
sol sol sol sol sol sol |
sol, sol, sol, sol, sol, sol, |
do4\f mi fad |
sol2 sold4 |
la do red |
mi2 mi4 |
fa! fa fa |
sol!2 la8 mi |
fa re sol4 sol, |
do do r |
R2. |
re4\p fad la |
sol sib re' |
do la, re |
sol, r r |
R2. |
sol4 fa! re |
dod dod re |
la, la sol |
fa8 fa fa fa fa fa |
sol sol sol sol sol sol |
la4 la la |
fad8 fad fad fad fad fad |
sol sol la la la, la, |
re4 r\fermata r |
R2. |
do!4 mi do |
re mi do |
fa la( fa) |
sib, sib, sib, |
do re sib, |
sib do' do |
fa fa, fa, |
R2. |
re4 sib sol |
mi fa r |
do la fa |
sib,8 sib, sib, sib, si, si, |
do do sib! sib la la |
sib sib sib sib sib sib |
fa fa fa fa fa fa |
sol sol sol sol do do |
fa fa fa fa fa fa |
sib, sib, do do dod dod |
re4 r\fermata r |
r re( do!) |
r sib,( la,) |
sib,8 sib, sib, sib, sib, sib, |
si, si, si, si, si, si, |
do do do do do do |
do do do do do do |
fa4 sol\f si |
do' do dod |
re fa sold |
la la, la, |
sib, sol! fa |
mi do! re8 la, |
sib,4 do do |
fa fa, r |
