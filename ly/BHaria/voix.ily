\clef "bass" R2.*11 |
fa4 la fa |
do'2.~ |
do'2\melisma sib4\trill\melismaEnd |
la4 r r |
sol sol4. fa8 |
mi4 fa sol~ |
sol8.[ la32 sib] la8[ sol] fa[ mi] |
fa[ mi] fa4 r |
la fa re |
dod re r |
sol mi do! |
si, do sol |
la8([ fa] re4.) do8 |
si,4\melisma r sol |
la4. fa16[ mi] re8[ do16 re] |
si,8 si4 sol16[ fa] mi8[ re16 mi] |
do8 do'4 la16[ sol] fa8[ mi16 fa] |
re4\melismaEnd r fa |
mi8.([ fa16] sol4) sold |
la r\fermata r |
la fad sol |
fa!( red) mi |
fa2. |
fad2 fad4 |
sol2 mi8([ do]) |
re2\startTrillSpan~ re8.\stopTrillSpan do16 |
do4 r r |
R2.*7 |
re4 fad la |
do'2. |
sib!4 r r |
la do'8[ la] sol[ fad] |
sol4 sol r |
sib sol mi |
dod re r8 re |
la,4 sol( fa!) |
mi4 r r |
la fa re |
sib sol mi |
fa8[ la] re4( dod) |
do'!2 la4 |
sib la( dod) |
re r\fermata r |
fa4 la fa |
do'2.~ |
do'2( \outsideSlur sib4)\trill |
la r r |
sol4 sol4. fa8 |
mi4 fa sol~ |
sol8.[ la32 sib] la8[ sol] fa[ mi] |
fa4 fa r |
re sib sol |
fad sol r |
do la fa! |
mi fa r8. fa16 |
re8( sib4 la8) sol[ fa] |
mi4\melisma r do |
re4. sib16[ la] sol8[ sib] |
do4. la16[ sol] fa8[ la] |
sib,4. sol16[ fa] mi8[ sol] |
la,4\melismaEnd r4 fa |
sib, do( dod) |
re r\fermata r |
sib sold la |
sol! mi fa |
sib,2. |
si, |
do4.( do'8) la[ fa] |
sol2\startTrillSpan~ sol8.\stopTrillSpan fa16 |
fa4 r r |
R2.*7 |
