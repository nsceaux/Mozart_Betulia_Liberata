\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Achiore
      shortInstrumentName = \markup\character Ach.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s2.*12\pageBreak
        s2.*11\break s2.*8\break s2.*11\break s2.*11\pageBreak
        s2.*12\break s2.*9\break s2.*9\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
