\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basso"
      \origLayout { s1*4\break s1*4\break s1*4\break }
    >>
  >>
  \layout {
    indent = \smallindent
    short-indent = 0
  }
  \midi { }
}
