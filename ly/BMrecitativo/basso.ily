\clef "bass" sol1 |
lab~ |
lab2 r4 sib |
sol2 mi!~ |
mi fa~ |
fa r4 sol |
fad1~ |
fad2 sol~ |
sol1 |
dod |
re2 sold~ |
sold la~ |
la red~ |
red1 |
mi~ |
mi2 r4 fad |
si,1 |
