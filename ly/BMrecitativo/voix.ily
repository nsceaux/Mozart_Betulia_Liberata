\ffclef "tenor/G_8" <>^\markup\character Ozia
sib4 sib8 mib' mib' sib r16 sib sib do' |
lab8 lab r lab do' do' do'16 do' sib lab |
re'!8 re' fa' re'16 mib' mib'8 sib r4 |
\ffclef "soprano/treble" <>^\markup\character Amital
sib'8 sib'16 sib' do''8 reb'' do'' do'' r do'' |
do'' sol' sol' sib' lab' lab' r fa''16 re''! |
si'!4 r8 si'16 do'' do''8 sol' r4 |
\ffclef "soprano/treble" <>^\markup\character Cabri
r8 la'! la' re'' re'' la' r16 la' la' sib' |
do''8 do'' do'' sib' sol'4
\ffclef "bass" <>^\markup\character Achior
r8 re |
sol4 r8 sol sol sol la sib |
la la r mi sol sol sol fa |
re re
\ffclef "alto/treble" <>^\markup\character Giuditta
la'8 re' r fa' mi' mi' |
r16 si si do' re'8 do' la4 r |
mi'4 mi'8 la' fad' fad' r fad'16 fad' |
fad'4 fad'8 sol' la'4 r16 la' la' sol' |
mi'8 mi' r mi' sol' sol' fad' re' |
lad' lad' lad' lad'16 si' si'8 fad' r4 |
R1 |
