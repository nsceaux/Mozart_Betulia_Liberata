Se -- guan -- si, o Car -- mi, i fug -- gi -- ti -- vi; e sia
il più di no -- stre pre -- de
pre -- mio a Giu -- dit -- ta.

O ge -- ne -- ro -- sa Don -- na,
te so -- pra ogn’ al -- tra Id -- di -- o
fa -- vo -- rì, be -- ne -- dis -- se.

In o -- gni e -- ta -- de
del tu va -- lor si par -- le -- rà.

Tu sei
la gio -- ia d’Is -- ra -- e -- le,
l’o -- nor del po -- pol tu -- o.

Ba -- sta. Do -- vu -- te
non son tai lo -- di a me. Dio fu la men -- te,
che’l gran col -- po gui -- dò, la ma -- no io fu -- i:
i can -- ti -- ci fe -- sti -- vi of -- fran -- si a lu -- i.
