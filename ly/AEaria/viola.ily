\clef "alto" sib'8 sib' sib' sib' sib' sib' |
la' la' la' la' la' la' |
sol' sol' sol' do'' do'' do'' |
si' si' si' fa' fa' fa' |
mib' mib' mib' la' la' la' |
sol' sol' sol' sol sol sol |
sol' sol' sol' do' re' mib' |
sol' la' sib' la sib do' |
sib mib' re' do' sib la |
sol4\p r8 sol'4 r8 |
sol'4 r8 sol'4 r8 |
mib'4 r8 mib'4 r8 |
re'2.\fp |
sol'4 r8 la'4 r8 |
si'4 r8 si4 r8 |
do'4 r8 mib'4 r8 |
re'4 r8 re'4 r8 |
sol'4 r8 sol'4 r8 |
sol'4 r8 sol4 r8 |
la4 r8 la4-. r8 |
sib4 r8 re'4 r8 |
mib'4 r8 mib'4 r8 |
mib'4 r8 mi'4 r8 |
fa'4 r8 r4 r8 |
R2. |
fa'4 r8 fa'4 r8 |
fa'4 r8 fa'4 r8 |
sib'4 r8 sib'4 r8 |
solb'4 r8 solb'4 r8 |
mib'4 r8 mib'4 r8 |
mib'4 r8 mi'4 r8 |
fa'8\f sol'! fa' mib'! re' do' |
sib2.\p |
fa |
sol |
fa8 fa fa fa' fa' fa' |
fa' fa' fa' mib' mib' mib' |
re'4 r8 fa4 r8 |
sol-\sug\fp sol sol sol sol sol |
fa'-\sug\fp fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' |
mib' mib' mib' sol' sol' sol' |
fa' fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' |
fa'\f fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' |
fa' fa' fa' fa' fa' fa' |
sib4 r8 sib'4 r8 |
fa'8 fa' fa' fa' fa' fa' |
fa'4 r8 r4 r8 |
sib2.\p~ |
sib |
si8( re') re'-. r re' re' |
do'4 r8 re'4 r8 |
mib'4 r8 mib'4 r8 |
fa'4 r8 fa'4 r8 |
sol'4 r8 sol4 r8 |
do'4 r8 r4 r8 |
R2. |
re'4 r8 re'4 r8 |
sol'4 r8 sol'4 r8 |
mi'4 r8 mi'4 r8 |
fad'4 r8 re'4 r8 |
sol'4 r8 sib4 r8 |
do'4 r8 dod'4 r8 |
re' sib'-. sib'-. la'( fad') la'-. |
sol'4. re' |
re'8-. re'-. re'-. do'( la) do'-. |
sib4 r8 sol'( fad' sol') |
re'4 r8 r4\fermata r8 |
sol4 r8 sol4 r8 |
sol'4 r8 sol'4 r8 |
mib'4 r8 mib'4 r8 |
re'2.\fp |
sol'4 r8 la'4 r8 |
si'4 r8 si4 r8 |
do'4 r8 mib'4 r8 |
re'4 r8 re'4 r8 |
sol'4 r8 sol'4 r8 |
sol'4 r8 sol'4 r8 |
fa'4 r8 fa'4 r8 |
mib'4 r8 re'4 r8 |
do'4 r8 do'4 r8 |
do'4 r8 do'4 r8 |
do'4 r8 dod'4 r8 |
re'4 r8 r4 r8 |
R2. |
fad'4 r8 fad'4 r8 |
la'4 r8 do''4 r8 |
sib'4 r8 sib4 r8 |
la4 r8 re'4 r8 |
sol'4 r8 fa'!4 r8 |
mib'4 r8 dod'4 r8 |
re'8\f mib' re' do'! sib la |
sib2.\p |
re' |
mib' |
re'8 re' re' re' re' re' |
mib' mib' mib' do' do' do' |
re' re' re' re' re' re' |
sol' sol' sol' sol' sol' sol' |
sol'-\sug\fp sol' sol' sol' sol' sol' |
do' do' do' do' do' do' |
re' re' re' re' re' re' |
sol'-\sug\fp sol' sol' sol' sol' sol' |
la'-\sug\fp la' la' la' la' la' |
sol'-> sol' sol' sib-> sib sib |
do'-> do' do' do'-> do' do' |
re' re' re' re' re' re' |
re'\cresc re' re' re' re' re' |
re'\f sol' sib' sib' sib' sib' |
re' la' do'' do'' do'' do'' |
sib'4 r8 sol'4 r8 |
sol'4 r8 mib'4 r8 |
la4 la'8 re' sib la |
re' re' re' re' re' re' |
re' re' re' re'4 r8 |
