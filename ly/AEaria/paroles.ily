Ma qual vir -- tù non ce -- de
fra tan -- ti og -- get -- ti e tan -- ti,
fra tan -- ti og -- get -- ti e tan -- ti,
ad __ av -- vi -- lir __ ba -- stan -- ti
il più fe -- ro -- ce cor?
Ma qual vir -- tù __ non ce -- de
fra tan -- ti og -- get -- ti e tan -- ti,
ad av -- vi -- lir __ ba -- stan -- ti
li più fe -- ro -- ce __ cor,
il più, __ il più __ fe -- ro -- ce cor?

Se non __ vo -- len -- do an -- co -- ra
si pian -- ge, si pian -- ge a -- gli al -- trui pian -- ti,
se im -- pal -- li -- dir __ ta -- lo -- ra
ci fa l’al -- trui pal -- lor? __

Ma qual vir -- tù non ce -- de
fra tan -- ti og -- get -- ti e tan -- ti,
fra tan -- ti og -- get -- ti e tan -- ti,
ad __ av -- vi -- lir __ ba -- stan -- ti
il più __ fe -- ro -- ce __ cor?
Ma qual vir -- tù __ non ce -- de
fra tan -- ti og -- get -- ti e tan -- ti,
ad av -- vi -- lir __ ba -- stan -- ti
li più fe -- ro -- ce __ cor,
il più fe -- ro -- ce __ cor,
il più, il più fe -- ro -- ce cor?
