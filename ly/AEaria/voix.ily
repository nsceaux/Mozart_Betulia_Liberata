\clef "soprano/treble" R2.*9 |
sol'4 r8 sib'4 do''8 |
re''4 r8 r4 re''8 |
re''[ si'] do'' r r do'' |
do''[ sib'!] la' mib''[ re''] do'' |
sib'[ re''] re'' r4 r8 |
fa'!2. |
mib'4. sol'' |
fad''8[ mib'' re''] do''[ sib'] la' |
sib'[ la'] sol' r4 r8 |
mib''4.~ mib''8 re'' mib'' |
fa'4.~ fa'4 mib''8 |
mib''[ re''] re'' r4 r8 |
do''4. mib''4 re''8 |
do''4. sib' |
la'4 r8 r4 r8 |
R2. |
fa''4. fa''4 fa''8 |
fa''4.~ fa''4 la'8 |
sib'4 sib'8 r4 r8 |
r4 r8 r4 reb''8 |
do''4. mib'' |
solb''8[ fa'' mib''] reb''[ do''] sib' |
la'4 la'8 r4 r8 |
fa''4. re''!4 do''8 |
sib'4.~ sib'4 sib'8 |
sol''!4( mib''8) do''4 r8 |
r4 r8 mib''4. |
re'' sol'' |
fa''8[ mib'' re''] do''[ sib' la'] |
mi''!2. |
mib''!2. |
re''4.~ re''8[ mib''] fa'' |
sol''[ fa'' mib''] re''[ do'' sib'] |
fa'2. |
do''\startTrillSpan |
sib'4\stopTrillSpan r8 r4 r8 |
R2.*4 |
r4 r8 r4 fa'8 |
re''4.~ re''4 re''8 |
re''4.~ re''4 re''8 |
re''[ si'] si' r r fa'' |
mib''4 do''8 r4 r8 |
mib'2. |
fa'4. lab'' |
sol''8[ fa'' mib''] re''[ do'' si'] |
do''4 do''8 r4 r8 |
R2. |
re''4.~ re''8 mib'' re'' |
re''4.~ re''8[ sol''] sib' |
do''4 do''8 r4 r8 |
r4 r8 do''4. |
sib' sol'' |
mib''8[ re'' do''] sib'[ la' sol'] |
re''2.~ |
re''~ |
re''~ |
re''\melisma |
re''4\trill\melismaEnd r8 r4\fermata r8 |
sol'4 r8 sib'4 do''8 |
re''4 r8 r4 re''8 |
re''[ do''] do'' r r do'' |
do''[ sib'] la' mib''[ re''] do'' |
sib'4 re''8 r4 r8 |
fa'!2. |
mib'4. sol'' |
fad''8[ mib'' re''] do''[ sib'] la' |
sib'[ la'] sol' r4 r8 |
mib''4.~ mib''8 sol'' mib'' |
re''4.~ re''4 re''8 |
do''4. sib' |
lab'2. |
do''4.~ do''4 mib''8 |
mib''[ re'' do''] sib'[ la'! sol'] |
fad'4 r8 r4 r8 |
R2. |
mib''4. mib''4 mib''8 |
mib''4.~ mib''4 fad''8 |
sol''4. re''8 r r |
r4 r8 r4 do''!8 |
sib'4.~ sib'4 sol''8 |
sol''4.~ sol''4 sib'8 |
la'4 la'8 r4 r8 |
re''4. sib'4 la'8 |
sol'4.~ sol'4 sol'8 |
mib''4( do''8) la'4 r8 |
r4 r8 fad''4. |
sol'' mib'' |
re''8[ do'' sib'] la'[ sol' fad'] |
sol'2. |
fa''! |
mib''4. sol''8[ fa'' mib''] |
re''[ do'' sib'] la'[ sol' fad'] |
mi''2. |
fad'' |
sol''4. re'' |
mib''! sol'' |
<< { \voiceOne re''2. \oneVoice } \new Voice { \voiceTwo re' } >> |
la'2.\trill |
sol'4 r8 r4 r8 |
R2.*6 |
