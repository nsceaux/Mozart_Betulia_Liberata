\clef "treble" <re' re''>4.~ re''8 mib''( re'') |
re''4. re'' |
sol''~ sol''8 la''( sol'') |
sol''4. sol'' |
do'''~ do'''8 la''-. fad''-. |
re'''4. re'' |
mib''~ mib''8 fa'' sol'' |
sib' do'' re'' fad' sol' la' |
sol'4 r8 r4 r8 |
<>\p <<
  \tag #'violino1 {
    re''8 sib' sib' r sib'( do'') |
    r re'' re'' r re'' re'' |
    r si'( do'') r do'' do'' |
    <la' fad''>2.\fp |
    r8 sol'' sol'' r fad'' fad'' |
    r fa''! fa'' r fa'' fa'' |
    r mib'' mib'' r sol'' sol'' |
    fad''( mib'') re''-. do''-. sib'-. la'-. |
    sol' sib' sib' r re'' re'' |
    r mib'' mib'' r mib'' mib'' |
    r mib'' mib'' r mib'' mib'' |
    r re'' re'' r re'' re'' |
    r do'' do'' r mib''( re'') |
    r do'' do'' r sib' sib' |
    la' fa'-. sol'-. la'-. la'-. sib'-. |
    do''-. la'-. sib'-. do''-. do''-. re''-. |
    mib''( do'') do''-. r do''-. do''-. |
    mib''( do'') do''-. r do''-. do''-. |
    reb''( sib') sib'-. r sib'-. sib'-. |
    sib''( reb'') reb''-. r reb''-. reb''-. |
    r do'' do'' r mib'' mib'' |
    r solb'' solb'' r sib' sib' |
  }
  \tag #'violino2 {
    sib'8 sol' sol' r sol'( la') |
    r sib' sib' r sib' sib' |
    r sol' sol' r sol' sol' |
    <re' do''>2.\fp |
    r8 sib' sib' r do'' do'' |
    r re'' re'' r re'' re'' |
    r sol' sol' r dod' dod' |
    re'8( do''!) sib'-. la'-. sol'-. fad'-. |
    sol' sol' sol' r sib' sib' |
    r sib' sib' r sib' sib' |
    r do'' do'' r do'' do'' r sib' sib' r sib' sib' |
    r sib' sib' r sol' sol' |
    r <sol' sib'> q r sol' sol' |
    fa'4 r8 r fa'-. sol'-. |
    la'-. fa'-. sol'-. la'-. la'-. sib'-. |
    do''( la') la'-. r la'-. la'-. |
    do''( la') la'-. r la'-. la'-. |
    fa''8( reb'') reb''-. r reb''-. reb''-. |
    reb''( sib') sib'-. r sib'-. sib'-. |
    r sib' sib' r solb' solb' |
    r do' do' r sol'! sol' |
  }
>>
la'8-.\f sol'-. fa'-. mib'-. re'-. do'-. |
sib\p sib sib sib sib sib |
sib sib sib sib sib sib |
<<
  \tag #'violino1 {
    do'8 do' do' do' do' do' |
    do'4. mib''8 mib'' mib'' |
    re'' re'' re'' sol'' sol'' sol'' |
    fa''( mib'') re''-. do''-. sib'-. la'-. |
    mi''\fp mi'' mi'' mi'' mi'' mi'' |
    mib''!\fp mib'' mib'' mib'' mib'' mib'' |
    re'' re'' re'' re'' mib'' fa'' |
    sol''( fa'') mib''-. re''-. do''-. sib'-. |
    re''4.:16 re'':16 |
    do'':16 do'':16 |
  }
  \tag #'violino2 {
    sib8 sib sib sib sib sib |
    la la la do'' do'' do'' |
    sib' sib' sib' sib' sib' sib' |
    sib'( sol') fa'-. mib'-. re'-. do'-. |
    sib8-\sug\fp sib sib sib sib sib |
    do'-\sug\fp do' do' do' do' do' |
    sib sib sib sib sib sib |
    sib sib sib sib sib sib |
    sib'4.:16 sib':16 |
    sib':16 la':16 |
  }
>>
sib'8\f sib'' fa'' fa''4.\trill |
fa''8 do''' la'' mib''4.\trill |
re''8-. sib''( lab'') lab''4.\trill |
sol''16 la''! sib'' la'' sol'' fad'' sol'' la'' sib'' sol'' fa'' mib'' |
\ru#3 { re''16 sib'' } \ru#3 { do'' la'' } |
sib''8 sol'-.\p fa'-. mib'-. re'-. do'-. |
sib8 <<
  \tag #'violino1 {
    re''8 re'' r re'' re'' |
    re''( fa'') fa''-. r fa'' fa'' |
    fa''( fa') fa'-. r fa'' fa'' |
    r mib''-. mib''-. r fa''( re'') |
    r sol''-. sol''-. r mib''( do'') |
    r lab''-. lab''-. r fa''( re'') |
    sol''-. fa''-. mib''-. re''-. do''-. si'-. |
    do'' mib''( re'') do''-. re''-. sib'!-. |
    la'-. do''( sib') la'-. sib'-. sol'-. |
    fad'( la') la'-. r la' la' |
    r sib' sib' r sib' sib' |
    r do'' do'' r do'' do'' |
    r <re' do''> q r q q |
  }
  \tag #'violino2 {
    fa'8 fa' r fa' fa' |
    fa'( re'') re''-. r re'' re'' |
    re''( si') si'-. r si' si' |
    r do''-. do''-. r do' do' |
    r do''-. do''-. r do'( mib') |
    r re'-. fa'-. r lab'( fa') |
    mib'-. re'-. do'-. fa'-. mib'-. re'-. |
    mib'4 r8 r4 r8 |
    r8 mib''( re'') do''-. re''-. sib'-. |
    la'( fad') fad'-. r fad' fad' |
    r sol' sol' r re'( sol') |
    r <sol' sib'> q r q q |
    r <re' la'> q r <fad' la'> q |
  }
>>
sib'8-. sol'-. sib'-. re''( sol'') re''-. |
mib''( re'') do''-. sib'-. la'-. sol'-. |
<<
  \tag #'violino1 {
    re''4 r8 r4 r8 |
    r re'''-. re'''-. do'''( la'') do'''-. |
    sib''-. sib''-. sib''-. la''( fad'') la''-. |
    sol''-. sib'-. do''-. re''( do'' sib') |
    la'4 r8 r4\fermata r8 |
    sol'8 sol' sol' r sib'( do'') |
    r re'' re'' r sib''( re'') |
    r do'' do'' r do'' do'' |
    <la' fad''>2.\fp |
    r8 sol'' sol'' r fad'' fad'' |
    r fa''! fa'' r fa'' fa'' |
    r mib'' mib'' r sol'' sol'' |
    fad''( mib'') re''-. do''-. sib'-. la'-. |
    sol'( sib') sib'-. r re'' re'' |
    r mib''-. mib''-. r sol''( mib'') |
    r re''-. re''-. r fa''( re'') |
    r do'' do'' r sib' sib' |
    r lab' lab' r lab' lab' |
    r do'' do'' r do''( mib'') |
    mib''( re'') do''-. sib'-. la'!-. sol'-. |
    fad'-. fad'( sol') la'-. la'-. sib'-. |
    do''-. la'( sib') do''-. do''-. re''-. |
    mib'' mib'' mib'' r mib'' mib'' |
    r mib'' mib'' r fad'' fad'' |
    r sol''( re'') r re''-. re''-. |
    r dod'' dod'' r do'' do'' |
    r sib' sib' r sol'' sol'' |
    r sib' sib' r sol'' sol'' |
    fad''-.\f
  }
  \tag #'violino2 {
    re'8 re''-. re''-. do''( la') do''-. |
    sib'-. sib'-. sib'-. la'( fad') la'-. |
    sol'-. sib'-. re''-. re'' re'' re'' |
    re'' sol'-. la'-. sib'( la' sol') |
    fad'4 r8 r4\fermata r8 |
    sib8 sib sib r sol'( la') |
    r sib' sib' r re''( sib') |
    r8 sol' sol' r sol' sol' |
    <re' do''>2.\fp |
    r8 sib' sib' r do'' do'' |
    r re'' re'' r re'' re'' |
    r sol' sol' r dod' dod' |
    re'( do''!) sib'-. la'-. sol'-. fad'-. |
    sol' sol' sol' r sib' sib' |
    r sib' sib' r sib' sib' |
    r la' la' r la' la' r sol' sol' r fa' fa' |
    r mib' mib' r mib' mib' |
    r lab' lab' r lab' lab' |
    lab'4. sol'8 fad'-. mi'-. |
    la!4 r8 r4 r8 |
    r8 fad'( sol') la'!-. la'-. sib'-. |
    do'' do'' do'' r do'' do'' |
    r fad' fad' r mib'' mib'' |
    r re''( sib') r sol'-. sol'-. |
    r mi'-. mi'-. r fad' fad' |
    r sol' sol' r sib' sib' |
    r sol' sol' r sib' sib' |
    la'\f
  }
>> mib''8-. re''-. do''-. sib'-. la'-. |
sol'\p sol' sol' sol' sol' sol' |
sol' sol' sol' sol' sol' sol' |
<<
  \tag #'violino1 {
    la'8 la' la' la' la' la' |
    la' la' la' do'' do'' do'' |
    sib' sib' sib' mib'' mib'' mib'' |
    re''( do'') sib'-. la'-. sol'-. fad'-. |
    sol' sol'' sol'' sol'' sol'' sol'' |
    fa''!\fp fa'' fa'' fa'' fa'' fa'' |
    mib'' mib'' mib'' sol''( fa'') mib''-. |
    re''-. do''-. sib'-. la'-. sol'-. fad'-. |
    mi''\fp mi'' mi'' mi'' mi'' mi'' |
    fad''\fp fad'' fad'' fad'' fad'' fad'' |
    sol''-> sol'' sol'' re''-> re'' re'' |
    mib''!-> mib'' mib'' sol''-> sol'' sol'' |
    sib''2.:16 |
    la'':16\cresc |
  }
  \tag #'violino2 {
    sol'8 sol' sol' sol' sol' sol' |
    fad' fad' fad' la' la' la' |
    sol' sol' sol' sol' sol' sol' |
    sib'( la') sol'-. do'-. sib-. la-. |
    sib sib' sib' sib' sib' sib' |
    <re' si'>-\sug\fp q q q q q |
    do'' do'' do'' mib''( re'') do''-. |
    sib'-. la'-. sol'-. do'-. sib-. la-. |
    sib-\sug\fp sib sib sib sib sib |
    mib'!-\sug\fp mib' mib' mib' mib' mib' |
    re'-> re' re' sol'-> sol' sol' |
    sol'-> sol' sol' sol'-> sol' sol' |
    sol''2.:16 |
    sol''4.:16\cresc fad'':16 |
  }
>>
sol''8-.\f sib''-. re''-. re''4.\trill |
la''8-. do'''-. re''-. re''4.\trill |
sol''16 la'' sib'' la'' sol'' fa'' mib'' fa'' sol'' fa'' mib'' re'' |
do'' re'' mib'' re'' do'' sib' la' sib' do'' sib' la' sol' |
fad'8 re' <re' la' fad''> <re' sib' sol''> r r |
\ru#3 { sib'16 sol'' } \ru#3 { la' fad'' } |
sol''8 <sol' sib>8 q q4 r8 |
