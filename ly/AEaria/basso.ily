\clef "bass" sol8 sol sol sol sol sol |
fad fad fad fa fa fa |
mi mi mi mib mib mib |
re re re si si si |
do' do' do' do do do |
sib, sib, sib, sib sib sib |
do' do' do' do' do' do' |
re' re' re' re re re |
sol4 r8 r4 r8 |
sol4\p r8 sol4 r8 |
sol4 r8 sol4 r8 |
mib4 r8 mib4 r8 |
re2.\fp |
sol4 r8 la4 r8 |
si4 r8 si,4 r8 |
do4 r8 mib4 r8 |
re4 r8 re4 r8 |
sol4 r8 sol4 r8 |
sol4 r8 sol,4 r8 |
la,4 r8 la,4 r8 |
sib,4 r8 re4 r8 |
mib4 r8 mib4 r8 |
mib4 r8 mi4 r8 |
fa4 r8 r4 r8 |
R2. | \allowPageTurn
fa4 r8 fa4 r8 |
fa4 r8 fa4 r8 |
sib4 r8 sib4 r8 |
solb4 r8 solb4 r8 |
mib4 r8 mib4 r8 |
mib4 r8 mi4 r8 |
fa8\f sol! fa mib! re do |
sib,2.\p |
re |
mib |
fa4. la8 la la |
sib sib sib mib mib mib |
fa4 r8 fa,4 r8 |
sol,-\sug\fp sol, sol, sol, sol, sol, |
la,-\sug\fp la, la, la, la, la, |
sib, sib, sib, sib, sib, sib, |
mib mib mib mib mib mib |
fa fa fa fa fa fa |
fa, fa, fa, fa, fa, fa, |
sib,\f sib, sib, sib sib sib |
la la la la la la |
sib sib sib re re re |
mib4 r8 mib4 r8 |
fa fa fa fa fa fa |
sib,4 r8 r4 r8 |
sib2.\p |
lab |
sol4 r8 sol,4 r8 |
do4 r8 re4 r8 |
mib4 r8 mib4 r8 |
fa4 r8 fa4 r8 |
sol4 r8 sol,4 r8 |
do4 r8 r4 r8 |
R2. | \allowPageTurn
re4 r8 re4 r8 |
sol4 r8 sol4 r8 |
mi4 r8 mi4 r8 |
fad4 r8 re4 r8 |
sol4 r8 sib,4 r8 |
do4 r8 dod4 r8 |
re4 r8 r4 r8 |
re4 r8 r4 r8 |
re4 r8 r4 r8 |
re re re re re re |
re4 r8 r4\fermata r8 |
sol4 r8 sol4 r8 |
sol4 r8 sol4 r8 |
mib4 r8 mib4 r8 |
re2.\fp |
sol4 r8 la4 r8 |
si4 r8 si,4 r8 |
do4 r8 mib4 r8 |
re4 r8 re4 r8 |
sol4 r8 sol4 r8 |
sol4 r8 sol4 r8 |
fa4 r8 fa4 r8 |
mib4 r8 re4 r8 |
do4 r8 do4 r8 |
do4 r8 do4 r8 |
do4 r8 dod4 r8 |
re4 r8 r4 r8 |
R2. | \allowPageTurn
fad4 r8 fad4 r8 |
la4 r8 do'4 r8 |
sib4 r8 sib,4 r8 |
la,4 r8 re4 r8 |
sol4 r8 fa!4 r8 |
mib4 r8 dod4 r8 |
re8\f mib' re' do'! sib la |
sol2.\p |
sib,2. |
do |
re8 re re re re re |
mib mib mib do do do |
re re re re re re |
sol sol sol sol sol sol |
sol-\sug\fp sol sol sol sol sol |
do do do do do do |
re re re re re re |
dod-\sug\fp dod dod dod dod dod |
do!-\sug\fp do do do do do |
sib,-> sib, sib, sib,-> sib, sib, |
do-> do do do-> do do |
re re re re re re |
re\cresc re re re re re |
sol\f sol sol sol sol sol |
fad fad fad fad fad fad |
sol4 r8 do4 r8 |
mib4 r8 do4 r8 |
re8 re' do' sib la sol |
re' re' re' re re re |
sol sol, sol, sol,4 r8 |
