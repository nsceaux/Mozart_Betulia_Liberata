\clef "bass" si,1~ |
si,~ |
si,~ |
si, |
do~ |
do2 dod~ |
dod re~ |
re la~ |
la sib |
fad1 |
sol~ |
sol2 r4 la |
sold1~ |
sold2 do~ |
do1~ |
do~ |
do |
fa |
re |
fad2 sib, |
dod1 |
fa2 sold~ |
sold1~ |
sold~ |
sold2 la~ |
la1 |
si,1~ |
si,2 do |
r4 re red2~ |
red1~ |
red2 mi~ |
mi1~ |
mi2 fa~ |
fa1~ |
fa2 re~ |
re1 |
mib |
r4 fa mi!2 |
fa r4 r16 fa-. mi!-. re-. |
dod1~ |
dod~ |
dod |
re |
mi~ |
mi~ |
mi |
la2 sol~ |
sol1 |
do |
red~ |
red2 mi~ |
mi1 |
fad |
sol~ |
sol |
mi~ |
mi2 la |
re1 |
do2 fa |
r4 sol fad2~ |
fad1~ |
fad2 sol~ |
sol1~ |
sol |
mi |
re~ |
re2 fad~ |
fad sol |
sol1 |
r4 la sold2~ |
sold1~ |
sold2 dod |
mid1 |
fad |
sol! |
do!2 r4 re |
mib8-. mib( fa! sol) si,!2~ |
si,1 |
r8 si, re fa sold,2~ |
sold,1 |
la,8-. do'( fa la) dod2~ |
dod re |
mi1 |
fa~ |
fa |
dod~ |
dod2 re~ |
re r4 mi |
fa1~ |
fa |
re |
fad~ |
fad |
sol |
dod~ |
dod2 fa! |
sold1~ |
sold2 do!~ |
do1 |
fad2 sol~ |
sol1 |
r4 la fad2~ |
fad1~ |
fad |
si,~ |
si,~ |
si,2 mi |
sold1~ |
sold2 do |
red1 |
mi2 fad~ |
fad sol~ |
sol1~ |
sol2 r4 <la la,>4 |
sib,1~ |
sib,2 mib |
fad1 |
sol2 mi!~ |
mi fa!~ |
fa1~ |
fa2 mi~ |
mi1~ |
mi |
la~ |
la2 r4 si |
mi1 |
