Trop -- po mal cor -- ri -- spon -- de (O -- zi -- a, per -- do -- na)
a’ tuoi dol -- ci co -- stu -- mi
tal di -- sprez -- zo o -- sten -- tar de’ no -- stri Nu -- mi.
Io co -- sì, tu lo sa -- i,
del tuo Di -- o non par -- lai.

Prin -- ci -- pe, è ze -- lo
quel che chia -- mi roz -- zez -- za. In te co -- nob -- bi
chia -- ri se -- mi del ve -- ro; e m’af -- fa -- ti -- co
a far -- li ger -- mo -- gliar.

Ma non ti ba -- sta,
ch’io ve -- ne -- ri il tuo Di -- o?

No, con -- fes -- sar -- lo
u -- ni -- co per es -- sen -- za
deb -- be cias -- cu -- no, ed a -- do -- rar -- lo so -- lo.

E chi so -- lo l’af -- fer -- ma?

Il ve -- ne -- ra -- to
con -- sen -- so d’o -- gni e -- tà; de -- gli a -- vi no -- stri
la fi -- da au -- to -- ri -- tà; l’i -- stes -- so Di -- o
di cui pre -- di -- ca -- sti
i pro -- di -- gi, il po -- ter, chi di sua boc -- ca
lo pa -- le -- sò, che, quan -- do
se me -- de -- si -- mo de -- scris -- se,
dis -- se: ”Io son quel che so -- no”; e tut -- to dis -- se.

L’au -- to -- ri -- tà de’ tuoi pro -- du -- ci in va -- no
con me ne -- mi -- co.

E ben, con te ne -- mi -- co
l’au -- to -- ri -- tà non va -- glia; uom pe -- rò se -- i;
la ra -- gion ti con -- vin -- ca; a me ri -- spon -- di
con a -- ni -- mo tran -- quil -- lo, il ver si cer -- chi,
non la vit -- to -- ria.

Io già t’a -- scol -- to.

Or dim -- mi:
cre -- di, A -- chior, che pos -- sa
co -- sa al -- cu -- na pro -- dur -- si
sen -- za la sua ca -- gion?

No.

D’u -- na in al -- tra
pas -- san -- do col pen -- sier, non ti ri -- du -- ci
qual -- che ca -- gio -- ne a con -- fes -- sar, da cui
tut -- te di -- pen -- dan l’al -- tre?

E ciò di -- mo -- stra,
che v’è Di -- o non ch’è so -- lo.
Es -- ser non pon -- no que -- ste pri -- me ca -- gio -- ni i no -- stri De -- i?

Qua -- li De -- i, ca -- ro pren -- ce? I tron -- chi, i mar -- mi
scul -- ti da voi?

Ma se que’ mar -- mi a’ sag -- gi
fos -- ser sim -- bo -- li sol del -- le im -- mor -- ta -- li
es -- sen -- ze cre -- a -- tri -- ci, an -- cor di -- re -- sti,
che i miei Dei non son De -- i?

Sì, per -- chè mol -- ti.

Io ri -- pu -- gnan -- za al -- cu -- na
nel nu -- me -- ro non veg -- go.

Ec -- co -- la. Un Di -- o
con -- ce -- pir non poss’ i -- o,
se per -- fet -- to non è.

Giu -- sto è il con -- cet -- to.

Quan -- do dis -- si per -- fet -- to,
dis -- si in -- fi -- ni -- to an -- cor.

L’un l’al -- tro in -- clu -- de;
non si dà chi l’i -- gno -- ri.

Ma l’es -- sen -- ze, che a -- do -- ri,
se non più, son di -- stin -- te, e se di -- stin -- te,
han con -- fi -- ni fra lor. Dir dun -- que de -- i
che ha con -- fin l’in -- fi -- ni -- to, o non son De -- i.

Da que -- sti lac -- ci, in cui
m’im -- pli -- cà il tuo par -- lar, ce -- da -- si al ve -- ro,
di -- scio -- glier -- mi non so, ma non per que -- sto
per -- sua -- so son i -- o. D’ar -- te ti ce -- do,
non di ra -- gio -- ne. E ab -- ban -- do -- nar non vo -- glio,
gli dei che a -- do -- ro, e ve -- do,
per un Dio, che non pos -- so
nè pu -- re im -- ma -- gi -- nar.

S’e -- gli ca -- pis -- se
nel no -- stro im -- ma -- gi -- nar, Dio non sa -- reb -- be.
Chi po -- trà fi -- gu -- rar -- lo? E -- gli di par -- ti,
co -- me il cor -- po, non co -- sta; e -- gli in af -- fet -- ti,
co -- me l’a -- ni -- me no -- stre,
non è di -- stin -- to, ei non sog -- gia -- ce a for -- ma,
co -- me tut -- to il cre -- a -- to; e se gli as -- se -- gni,
par -- ti, af -- fet -- ti, fi -- gu -- ra, il cir -- con -- scri -- vi,
per -- fe -- zion gli to -- gli.

E quan -- do il chia -- mi
tu stes -- so e buo -- no e gran -- de,
nol cir -- con -- scri -- vi al -- lor?

No; buo -- no il cre -- do,
ma sen -- za qua -- li -- tà; gran -- de ma sen -- za
quan -- ti -- tà, nè mi -- su -- ra; o -- gnor pre -- sen -- te;
sen -- za si -- to, o con -- fi -- ne; e se in tal gui -- sa,
qual sia, non spie -- go, al -- men di lui non for -- mo
un’ i -- dea che l’ol -- trag -- gi.

E dun -- que va -- no
lo spe -- rar di ve -- der -- lo.

Un dì po -- tre -- sti
me -- glio fis -- sar -- ti in lu -- i; ma puoi frat -- tan -- to
ve -- der -- lo o -- vun -- que vuo -- i.

Ve -- der -- lo! e co -- me?
se im -- ma -- gi -- nar nol so?

Co -- me nel so -- le
a fis -- sar le pu -- pil -- le in va -- no a -- spi -- ri;
e pur sem -- pre, e per tut -- to il sol ri -- mi -- ri.
