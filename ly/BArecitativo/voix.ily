\ffclef "bass" <>^\markup\character Achior
r8 re16 re sol8 sol16 si sol8 sol r sol |
sol re r re si, si, r re16 re |
re4 re8 mi fa fa r fa16 fa |
fa4 fa8 la fa fa fa mi |
do do r4 mi r8 mi |
sol4 r8 sol16 la sib8 sib r mi16 fa |
sol4 sol8 fa re re r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
re'8 la16 la r8 fa' fa' do' do'8. do'16 |
mib'4 mib'8 re' sib sib r16 sib do' re' |
re'8 la r la16 sib do'4 do'8 sib |
sol8 sol r16 sib la sol dod'8 dod' r mi' |
dod' dod' dod' re' la4 r |
\ffclef "bass" <>^\markup\character Achior
mi4 mi8 fad sold sold r sold |
si8. si16 sold8 mi la la r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
do'4 r8 do'16 re' sib8 sib r4 |
sol8 sol16 sol sol8 la sib sib sib sib16 re' |
sib8 sib r4 r16 sol sol la sib8 la |
fa fa
\ffclef "bass" <>^\markup\character Achior
r8 fa16 sol la4 sol8 fa |
sib8 sib r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
sib8 sib16 sib sib8 do'16 re' |
re'8 la do' do'16 sib sol4 r16 sol la sib |
la8 la r la mi'! mi' mi' fa' |
re'4 r16 re' mi' fa' mi'8 mi' r mi' |
mi'4 si!8 si sold sold r sold16 la |
si8 si r si16 do' re'4 r16 re' re' fa' |
re'8 re'16 re' re'8 mi' do'4 r8 la |
do' do' do' do' do'8. do'16 re'8 mi' |
re'8 re' r4 re'8 sol r sol |
si4 re'8 r16 mi' do'8 do' r16 mi' do' si |
sol8 sol r4
\ffclef "bass" <>^\markup\character Achior
fad4 fad8 fad |
si si si si si fad r fad |
la la r16 la la sol mi8 mi
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si |
mi'4 r16 mi' mi' mi' do'8 do' r4 |
sib8 sib16 sib sib8 do' la la r4 |
la4 la8 sib do' do' r do'16 re' |
mib'4 mib'8 re' sib sib r16 sib sib do' |
lab8 lab r fa' fa' lab lab sib |
sol sol r16 mib' mib' do' la!8 la sol' mib'16 re' |
sib8 sib r4
\ffclef "bass" <>^\markup\character Achior
r8 sol sib la |
fa fa
\ffclef "tenor/G_8" <>^\markup\character Ozia
r fa' fa' do' r4 |
la8 la r4 dod'8 dod' r dod' |
dod' dod' dod' re' mi' mi' r mi' |
dod' dod' r4 dod'8 dod'16 mi' dod'8 la |
re'4 r
\ffclef "bass" <>^\markup\character Ach.
la4
\ffclef "tenor/G_8" <>^\markup\character Ozia
la8 la16 do'! |
do'8 sol! r sol sol sol sol la |
sib4 sib8 sib16 re' sib8 sib sol sol16 la |
sib8 sib sib do' la4 r8 la16 si |
do'8 do'16 do' do'8 si re' re'
\ffclef "bass" <>^\markup\character Achior
r16 re re re |
sol8 sol r sol16 la fa8 fa r fa16 mi |
do8 do r4 sol sol8 do' |
si si r fad16 fad fad4 fad8 sol |
la la si fad sol sol
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sol16 sol |
do'8 do' r do'16 do' do'8 sol r mi' |
re' re' r re' re' la do' re'16 la |
si4
\ffclef "bass" <>^\markup\character Achior
r8 sol sol sol sol la |
si si r si16 si si4 la8 si |
sold8 sold la si si mi r mi |
si si si do' la la r16 la si do' |
do'8 fa fa la fa fa fa mi |
sol! sol r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
fa'4 r8 si16 do' |
do'8 sol r4
\ffclef "bass" <>^\markup\character Achior
la4 la8 la |
fad4 fad8 la la re r re |
la8. la16 la8 si sol sol r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
re'8 sol16 sol r4 r r8 si |
re' re' r re'16 re' re'4 dod'8 si |
lad8 lad r lad16 si dod'4 dod'8 re' |
si4
\ffclef "bass" <>^\markup\character Achior
si8 fad16 fad re8 re
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si16 si |
si4 dod'8 re' re' la r4 |
la8 la16 si do'!8 si sol4
\ffclef "bass" <>^\markup\character Achior
r16 re re re |
sol8 sol r sol16 si sol4 sol8 fad |
re re r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
si4 r8 mi' |
mi'4 si8 si sold sold r sold16 la |
si4 si8 dod' la la r16 la si dod' |
dod'8 sold r sold16 la si4 si8 la |
fad4 r la la8 re' |
si si r re'16 do'! si4 la8 sol |
mi' mi' r16 mi' do' si sol8 sol r4 |
r2
\ffclef "bass" <>^\markup\character Achior
r8 re re re |
sol sol r sol16 sol sol8 sol16 sol sol8 la |
fa!4 r r mi8 mi16 fad |
sold8 sold r si si si si do' |
la4 r r8 la la la |
la mi16 fa sol!8 sol16 fa re8 re r4 |
mi4 mi8 do' do' sol r16 sol sib la |
fa8 fa r fa fa fa fa sol |
la la r la la4 r8 sib |
sol sol r sol mi mi r mi16 fa |
sol4 sol8 la fa fa r re |
sold sold sold la mi4 r |
\ffclef "tenor/G_8" <>^\markup\character Ozia
do'4 do'8 fa' fa' do' r do' |
la la la sib do'4 do'8 do'16 re' |
sib8 sib r sib16 sib sib4 do'8 re' |
re'8 la r4 la la8 sib |
do' do' r do'16 mib' do'8 do' r sib |
sol sol r4 sib sib8 sib |
la la r la16 si! dod'4 dod'8 re' |
mi'!8 mi' r16 mi' mi' fa' re'8 re' r16 re' mi' fa' |
mi'8 mi' r si sold sold r si16 do' |
re'4 re'8 do' la la r16 la la si |
do'8 do' r4 mi'8 do' r mi' |
re' re' r re' si si r16 sol' sol' mi' |
dod'8 dod' r4 dod'8 dod' dod' re' |
re' la r4
\ffclef "bass" <>^\markup\character Achior
r8 la la la |
fad fad r fad fad fad fad sol |
la la r16 fad fad sol la4 fad8 re |
sol4 r
\ffclef "tenor/G_8" <>^\markup\character Ozia
sol4 r |
sol4 sol8 sol si si r si |
re' re' re' mi' do'!4 r |
mi'8 si r si sold sold sold la |
si4 re'8 do' la la r16 la si do' |
si8 si r red'16 fad' fad'4 la8 si |
sol sol r16 si si mi' re'!8 re' r la |
do'4 r8 re' si si r sol |
si si la sol dod'8 dod' r sol'16 mi' |
dod'4 dod'8 re' re' la r4 |
\ffclef "bass" <>^\markup\character Achior
r8 fa! fa sib sib fa fa sol |
lab4 lab8 sol mib mib
\ffclef "tenor/G_8" <>^\markup\character Ozia
r16 sib sib mib' |
re'8 re' r4 do'8 do'16 do' do'8 sib |
sol8 sol r16 sib do' re' do'8 do' r sol' |
sol' sib r16 sib sib la fa8 fa
\ffclef "bass" <>^\markup\character Achior
r8 fa |
la la r do' la la r la |
la la la sold si!4 r |
\ffclef "tenor/G_8" <>^\markup\character Ozia
si!4 si8 mi'! mi' si r si16 si |
si4 si8 dod' re' re' re' dod' |
la la r dod'16 la red'8 red' r red'16 fad' |
red'8 red' red' mi' mi' si r4 |
R1 |

