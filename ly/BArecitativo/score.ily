\score {
  \new ChoirStaff <<
    \new Staff \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff <<
      \global \includeNotes "basso"
      \origLayout {
        s1*5\break s1*5\break s1*5\break s1*4\pageBreak
        s1*5\break s1*5\break s1*5\break s1*5\break
        s1*5\break s1*4\break s1*5\break s1*5\break s1*6\pageBreak
        s1*5\break s1*6\break s1*6\break s1*6\break s1*6\break
        s1*5\break s1*6\break s1*6\break s1*5\pageBreak
        s1*5\break
      }
    >>
  >>
  \layout {
    indent = \smallindent
    short-indent = 0
  }
  \midi { }
}
