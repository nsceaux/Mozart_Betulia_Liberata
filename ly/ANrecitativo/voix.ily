\clef "soprano/treble" <>^\markup\character Cabri
r4 r8 la' re''4 do''8 do''16 si' |
sol'8 sol' r4
\ffclef "soprano/treble" <>^\markup\character Amital
sol'8 sol'16 sol' sol'8 sol'16 la' |
si'8 si' si' do'' re'' re'' si' sol' |
do''4 r
\ffclef "tenor/G_8" <>^\markup\character Ozia
do'8 do' r sol |
la la
\ffclef "soprano/treble" <>^\markup\character Carmi
r8 fa' la' la' la' sib' |
do'' do'' do'' re'' sib' sib' r fa' |
sib' sib' sib' re'' re'' la' la' sib' |
do'' do''16 do'' do'' do'' do'' mib'' do''8. la'16 do''8 sib' |
sol' sol' r re'' sib'4 la'8 sol' |
dod''8 dod'' r16 dod'' dod'' mi'' dod''4 dod''8 re'' |
re'' la' r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 fa fa fa |
sib sib r sib16 re' sib4 sib8 la |
do' do' r4
\ffclef "bass" <>^\markup\character Achior
do'4 si8 la |
sold sold r4 sold8 sold16 la si8 do' |
la4 r8 mi la4 la8 do' |
do' sol! r mi sol sol sol la |
fa fa
\ffclef "tenor/G_8" <>^\markup\character Ozia
r16 fa fa sol la8 la do' do'16 re' |
sib8 sib
\ffclef "bass" <>^\markup\character Achior
r16 fa fa fa re4 r |
r r8 mi la4 la8 la |
la mi mi mi dod4 r16 dod dod re |
mi8 mi r mi16 fad sol4 sol8 fad |
re re r16 fad fad sol la8 la16 la do'!8 si |
sol sol r4 sol8 sol16 sol la8 si |
si fad r fad16 fad fad4 mi8 fad |
red4 red8 red16 mi fad8 fad r fad |
fad fad fad sol la la r do' |
la8 la16 la r8 la16 sol mi8 mi r16 mi fad sol |
sol8 re16 re re re re mi fa!8 fa r16 fa fa la |
fa8 fa16 re fa fa fa mi do8 do r16 sol mi sol |
sol8 do r4 sol8 sol16 sol sol8 la16 sib |
la8 la r la la mi r16 mi sol fa |
re8 re r16 la la la fad8 fad r fad16 sol |
la4 la8 si! sol4 r |
sol4 sol8 si si16 fad fad sol la8 sol |
mi mi r16 sol la si la8 la r la |
la mi mi mi dod dod r dod |
mi mi r4 sol8 sol r sol16 si |
sol8 sol r sol16 fad re8 re r16 fad fad fad |
mi8 mi r mi mi mi mi fad |
sold sold sold sold16 la si8 si r4 |
si8 re16 re re8 mi do do r16 la si do' |
do'8 sol! r sol16 la sib4 sib8 la |
fa4 r8 fa la4 r8 la |
la4 la8 sib sol sol sol sol |
mi mi r16 mi mi fa sol8 sol r sol16 la |
sib4 sib8 la fa fa r16 la la si |
sold8 sold r sold si si si do' |
la la r4 la r8 la16 do' |
do'8 sol! r sol sib sib la sol |
la la <<
  { \voiceOne si!8 do' sol sol \oneVoice }
  \new Voice \with { autoBeaming = ##f } { \voiceTwo fa8 mi do do }
>> r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
r4 r8 la re' re' re' fad' |
re'4 r re' re'8 re' |
re' la r la16 si do'!4 do'8 si |
sol sol r4
\ffclef "bass" <>^\markup\character Achior
sol4 la8 si |
si fad r fad la la la sol |
mi mi r16 si sol si si8 mi r sol16 la |
si4 si8 do' la la r la16 la fad4 sol8 la la re r4 |
fad8 fad16 fad fad8 sol la la la si |
sol sol
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sol16 la si4 la8 sol |
do' do' r4 do'8 do'16 do' do'8 sol |
la4 r
\ffclef "soprano/treble" <>^\markup\character Amital
la'4 la'8 do'' |
la'4 r8 la' la' la' la' sold' |
si'4
\ffclef "bass" <>^\markup\character Achior
r16 si si si sol!8 sol r4 |
sol8 sol16 sol mi8 sol sol do r16 sol sib la |
fa4 r8 fa la la r do' |
la la r16 la sol la fa8 fa r fa |
la la sol fa si! si r16 si si do' |
do'8 sol r4 r2 |


