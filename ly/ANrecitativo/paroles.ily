Si -- gnor, Car -- mi a te vie -- ne.

E la com -- mes -- sa
cu -- sto -- dia del -- le mu -- ra ab -- ban -- do -- nò?

Car -- mi, che chie -- di?

Io ven -- go un pri -- gio -- nie -- ro a pre -- sen -- tar -- ti, av -- vin -- to
ad un tron -- co il la -- scia -- ro
vi -- ci -- no al -- la cit -- tà le schie -- re o -- sti -- li;
A -- chior -- re è il suo no -- me;
de -- gli Am -- mo -- ni -- te è il Pren -- ce.

E co -- sì trat -- ta
o -- lo -- fer -- ne gli a -- mi -- ci?

E de’ su -- per -- bi
que -- sto l’u -- sa -- to stil; per lo -- ro è of -- fe -- sa
il ver che non lu -- sin -- ga.

I sen -- si tuo -- i
spie -- ga più chia -- ri.

Ub -- bi -- di -- rò. Sde -- gnan -- do
l’as -- si -- ro con -- dot -- tier che a lui pre -- ten -- da
di re -- si -- ster Be -- tu -- lia; a me ri -- chie -- se
di voi no -- ti -- zia. Io, le me -- mo -- rie an -- ti -- che
ri -- chia -- man -- do al pen -- sier, tut -- te gli e -- spo -- si
del po -- po -- lo d’I -- sra -- ele
le o -- ri -- gi -- ni, i pro -- gres -- si; il cul -- to a -- vi -- to de’ nu -- me -- ro -- si De -- i, che per un so -- lo
cam -- bia -- ro i pa -- dri vo -- stri; i lor pas -- sag -- gi
dal -- le Cal -- de -- e con -- tra -- de
in car -- ra, in -- di in E -- git -- to; i du -- ri im -- pe -- ri,
di quel bar -- ba -- ro Rè. Dis -- si la vo -- stra
pro -- di -- gio -- sa fu -- ga, i lun -- ghi er -- ro -- ri,
le scor -- te por -- ten -- to -- se, i ci -- bi, Tac -- que,
le bat -- ta -- glie, i tri -- on -- fi; e gli mo -- stra -- i
che, quan -- do al vo -- stro Di -- o fo -- ste fe -- de -- li,
sem -- pre pu -- gnò per vo -- i. Con -- clu -- si al fi -- ne
i miei det -- ti co -- sì. “Cer -- chiam, se que -- sti
al lor Dio so -- no in -- fi -- di; e, se lo so -- no,
la vit -- to -- ria è per no -- i. Ma se non han -- no
de -- lit -- to in -- nan -- zi lu -- i, nò, non la spe -- ro,
mo -- ven -- do an -- ch’a lor dan -- no il mon -- do in -- te -- ro.”

O e -- ter -- na ve -- ri -- tà, co -- me tri -- on -- fi
an -- che in boc -- ca a’ ne -- mi -- ci!

Ar -- se O -- lo -- fer -- ne
di rab -- bia a’ det -- ti mie -- i, da se mi scac -- cia,
in Be -- tu -- lia m’in -- vi -- a;
e quì l’em -- pio mi -- nac -- cia
og -- gi al -- la stra -- ge vo -- stre u -- nir la mi -- a.

Co -- stui dun -- que si fi -- da
tan -- to del suo po -- ter?

Dun -- que ha co -- stui
si po -- ca u -- ma -- ni -- tà?

Non ve -- de il so -- le
a -- ni -- ma più su -- per -- ba,
più fie -- ro cor. Son ta -- li
i mo -- ti, i det -- ti suo -- i,
che tre -- ma il più co -- stan -- te in fac -- cia a lu -- i.
