\clef "bass" fad1 |
si,1~ |
si, |
mi |
fa1~ |
fa2 re~ |
re fad~ |
fad1 |
sol~ |
sol |
r4 la re2 |
mi1 |
fa |
re |
do |
mi |
la, |
re |
dod~ |
dod~ |
dod | \allowPageTurn
re |
si, |
red~ |
red~ |
red~ |
red2 mi |
si,1~ |
si,2 do~ |
do1 |
dod |
re2 do!~ |
do si,~ |
si, red |
mi dod~ |
dod1~ |
dod~ |
dod2 re |
sold1~ |
sold~ |
sold2 la |
mi1 |
fa~ |
fa2 dod~ |
dod1~ |
dod2 re~ |
re1 |
do |
mi |
fa2 r4 sol |
fad1~ |
fad~ |
fad |
sol |
red |
mi~ |
mi2 fad~ |
fad1~ |
fad |
sol |
mi |
fa!~ |
fa |
mi~ |
mi |
fa~ |
fa~ |
fa |
r4 sol do2 |
