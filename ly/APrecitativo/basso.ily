\clef "bass" sold1~ |
sold~ |
sold~ |
sold2 dod~ |
dod1 |
re~ |
re |
sol~ |
sol2 mi~ |
mi sold~ |
sold do |
mi1 |
fa2 fad |
sol1 |
mi2 fa!~ |
fa1~ |
fa~ |
fa2 r4 sol |
do1 |
r4 re sol,2 |
dod1~ |
dod~ |
dod2 fad~ |
fad1~ |
fad~ |
fad2 red~ |
red1~ |
red~ |
red~ |
red2 sol~ |
sol fad~ |
fad sol~ |
sol1 |
mi2 re |
mi1~ |
mi |
la2 mi~ |
mi1~ |
mi~ |
mi2 fa~ |
fa1~ |
fa |
sib,~ |
sib, |
fad~ |
fad2 sol~ |
sol1~ |
sol |
r4 la sold2~ |
sold1~ |
sold2 la~ |
la r4 <si si,> |
do1~ |
do |
fad~ |
fad |
sol~ |
sol~ |
sol2 r4 la |
re1 |
