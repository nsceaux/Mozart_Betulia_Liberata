\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 si16 si mi'8 si sold sold r sold |
sold sold sold sold16 la si4 si8 dod' |
re'4 re'8 re'16 fad' re'8 re' r si16 dod' |
re'4 re'8 dod' la4 r16 la la si |
sol!8 sol r dod'16 mi' mi'4 sol8 la |
fad4 r re' re'8 fad' |
re' re'16 re' re'8 la fad4
\ffclef "soprano/treble" <>^\markup\character Cabri
do''!8 do''16 si' |
sol'8 sol'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sol si si si do'! |
re'4 re'8 mi' do'4 r8 sol |
do'8 do' r mi' mi' si r si16 do' |
re'4 re'8 do' la la r16 la si do' |
do'8 sol sol la sib8. sib16 sib8 la |
fa8 fa r16 do' re' mib' re'8 re' r16 la do' sib |
sol4 r re'8 sol r re' |
do' do' r do' la la r la |
do'4 fa'8 re'16 do' si!8 si r si |
re' re' r fa' fa' si r16 re' do' re' |
si4 si8 do' do' sol r4 |
\ffclef "bass" <>^\markup\character Achior
sol8 sol16 sol do'8 la fad fad r16 fad fad sol |
sol8 re r4 r2 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 la la si dod' dod' r4 |
dod'8 dod'16 dod' dod'8 re' mi' mi' r dod'16 re' |
mi'4 dod'8 la re' re'
\ffclef "alto/treble" <>^\markup\character Giud.
r8 la' |
fad' fad'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 la re' re' r re' |
re' re' re' mi' do'! do' r la16 si |
do'4 re'8 la si si r fad |
si si r si red' red' r4 |
fad'8 red' r fad' fad' si si do' |
la la r16 fad fad sol la8 la16 la la8 la |
red'8 red'16 fad' red'8 si mi' mi' r si |
si si16 si si8 mi' re'! re' r4 |
do'8 do'16 do' re'8 la si4 r |
si4 si8 re' re' sol r sol16 si |
sol4 sol8 fad la la r16 re' re' si |
sold8 sold r si sold sold sold sold16 la |
si8 si r si re' re' mi' si |
do'4 r16 do' do' do' do'8 sol! r sol |
sol sol sol sol do'4 do'8 do'16 mi' |
do'8 do' r sol16 sol sol4 sol8 la |
sib8 sib r16 sib do' sol la8 la
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 do' |
fa'8 fa' r16 fa' fa' sol' mib'8 mib' r4 la'4 r8 do'' do''4 mib'8 fa' |
re'8 re' r16 fa' fa' sib' sib'8 fa'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 fa |
sib4 r
\ffclef "alto/treble" <>^\markup\character Giu.
re'4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 sib16 re' |
re'8 la r la fad fad r la |
do'4 re'8 la sib4 
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 re' |
sol'4 r sol'8 sol'16 sol' sol'8 sib' |
sol' sol' r sol'16 sib' sol'4 sol'8 fa'! |
re' re' r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
si!4 si8 mi'! |
mi' si r16 si la si sold8 sold sold la |
si4 si8 do' la la r la |
red' red'16 red' r8 mi' mi' si r4 |
r r8 sol do'4 r8 mi' |
do' do' r4 do'8 sol r16 do' do' do' |
la8 la r16 la la si do'8 do' r4 |
mi'8 do'16 do' r8 do'16 do' la8 la r16 do' re' la |
si8 si r4 re'8 sol r4 |
si4 r8 si16 sol dod'8 dod' r4 |
sol'4 dod'8 re' re' la r4 |
R1 |

