Ti con -- so -- la, A -- chior. Quel Dio di cui
pre -- di -- ca -- sti il po -- ter, l’em -- pie mi -- nac -- ce
tor -- ce -- rà su l’au -- tor. Né a ca -- so il Cie -- lo
ti con -- du -- ce fra noi. Tu de’ ne -- mi -- ci
po -- trai sve -- lar…

Tor -- na Giu -- dit -- ta.

O -- gnu -- no
s’al -- lon -- ta -- ni da me, con -- vie -- ne, o Pren -- ce,
dif -- fe -- rir le ri -- chie -- ste. Al mio sog -- gior -- no
con -- du -- ce -- te -- lo, o ser -- vi: anch’ io fra po -- co
a te ver -- rò. Van -- ne, A -- chior -- re, e cre -- di
che in me lun -- gi da’ tuo -- i,
l’a -- mi -- co, il pa -- dre, il di -- fen -- so -- re a -- vra -- i.

O -- spi -- te sì pie -- to -- so io non spe -- ra -- i.

Sei pur Giu -- dit -- ta, o la dub -- bio -- sa lu -- ce
mi con -- fon -- de gli og -- get -- ti?

Io so -- no.

E co -- me
in sì gio -- con -- de spo -- glie
le fu -- ne -- ste cam -- bia -- sti? Il bis -- so e l’o -- ro,
l’o -- stro, le gem -- me a che ri -- pren -- di, e gli al -- tri
fre -- gi di tua bel -- lez -- za ab -- ban -- do -- na -- ti?
Di bal -- sa -- mi o -- do -- ra -- ti
stil -- la il com -- po -- sto crin! chi le tue go -- te
tan -- to av -- vi -- va e co -- lo -- ra? I mo -- ti tuo -- i
chi a -- dor -- na ol -- tre il co -- stu -- me
di gra -- zia e mae -- stà? Chi que -- sto ac -- cen -- de
in -- so -- li -- to splen -- dor nel -- le tue ci -- glia,
che a ri -- spet -- to co -- strin -- ge e a me -- ra -- vi -- glia?

O -- zi -- a, tra -- mon -- ta il so -- le;
fa, che s’a -- pran le por -- te; us -- cir deg -- g’i -- o.

Us -- cir!

Sì.

Ma fra l’om -- bre, in -- er -- me, e so -- la
co -- sì…

Non più. Fuor che la mia se -- gua -- ce,
al -- tri me -- co non vo -- glio.

(Han -- no i suoi det -- ti
un non so che di ri -- so -- lu -- to e gran -- de,
che m’oc -- cu -- pa, m’op -- pri -- me.) Al -- men… vor -- re -- i…
fi -- glia…  (chi’l cre -- de -- ri -- a! nè pur ar -- di -- sco…
chie -- der -- le do -- ve cor -- ra, in che si fi -- di.)
Fi -- glia… va; Dio t’in -- spi -- ra; e -- gli ti gui -- di.