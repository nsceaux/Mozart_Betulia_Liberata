\clef "treble" \transposition re
\ru#3 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
R1 |
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
\ru#2 {
  <<
    \tag #'(tromba1 trombe) { sol'2 }
    \tag #'(tromba2 trombe) { sol }
  >> r |
}
R1 |
\ru#2 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
<<
  \tag #'(tromba1 trombe) { do''2 }
  \tag #'(tromba2 trombe) { do' }
>> r2 |
\ru#2 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { do' }
>> r4 r2 |
R1*6 |
\ru#4 {
  \twoVoices #'(tromba1 tromba2 trombe) << { re''4 } { re'' } >> r4 r2 |
}
\ru#3 {
  <<
    \tag #'(tromba1 trombe) { sol'2 }
    \tag #'(tromba2 trombe) { sol }
  >> r2 |
}
R1*5 |
\ru#3 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
R1 |
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
\ru#2 {
  <<
    \tag #'(tromba1 trombe) { sol'2 }
    \tag #'(tromba2 trombe) { sol }
  >> r |
}
R1 |
\ru#2 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
\ru#2 {
  <<
    \tag #'(tromba1 trombe) { do''2 }
    \tag #'(tromba2 trombe) { do' }
  >> r2 |
}
\twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
\twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { do' }
>> r4 r2 |
\ru#2 {
  <<
    \tag #'(tromba1 trombe) { sol'4 }
    \tag #'(tromba2 trombe) { sol }
  >> r4 r2 |
}
\ru#2 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
  \twoVoices #'(tromba1 tromba2 trombe) << { sol'2 } { sol' } >> r2 |
}
\ru#4 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2 |
}
\twoVoices #'(tromba1 tromba2 trombe) << { do'2 } { do' } >> r2\fermata |
R2.*39 |
R2*4 |
<>\f \ru#3 {
  \twoVoices #'(tromba1 tromba2 trombe) << { sol'4 } { sol' } >> r |
}
R2 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
R2*5 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
R2*23 |
<>\f \ru#3 {
  \twoVoices #'(tromba1 tromba2 trombe) << { do'4 } { do' } >> r |
}
R2*5 |
<<
  \tag #'(tromba1 trombe) { sol'4 }
  \tag #'(tromba2 trombe) { sol }
>> r4 |
R2*7 |
r4 \tag #'trombe <>^"a 2." sol'4\f |
do' r |
do' r |
do' r |
sol' r |
\ru#2 {
  <<
    \tag #'(tromba1 trombe) { do''4 }
    \tag #'(tromba2 trombe) { do' }
  >> r4 |
}
R2*2 |
\twoVoices #'(tromba1 tromba2 trombe) << { sol'4 } { sol' } >> r |
R2
\twoVoices #'(tromba1 tromba2 trombe) << { sol'4 } { sol' } >> r |
<<
  \tag #'(tromba1 trombe) { do''4 }
  \tag #'(tromba2 trombe) { do' }
>> r4 |
R2 |
<>\f \ru#2 {
  <<
    \tag #'(tromba1 trombe) { do''4 }
    \tag #'(tromba2 trombe) { do' }
  >> r4 |
}
<<
  \tag #'(tromba1 trombe) { do''4 do'' | do'' }
  \tag #'(tromba2 trombe) { do' do' | do' }
>> r4 |
