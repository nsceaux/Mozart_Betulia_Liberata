\piecePartSpecs
#`((violino1)
   (violino2)
   (viola)
   (basso)
   (oboi #:score-template "score-oboi")
   (fagotti)
   (corni #:score "score-corni")
   (trombe #:instrument "Trombe in D."))
