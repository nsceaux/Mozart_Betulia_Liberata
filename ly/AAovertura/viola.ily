\clef "alto" \ru#16 <fa la>8 |
q re' re' re' re' re' re' re' |
re' re' re' re' re' re' re' re' |
\ru#16 <dod' mi'> |
q la la la la la la la |
la la la la la la la la |
re' mi' fa' mi' re' mi' fa' re' |
sol' la' sib' la' sol' la' sib' sol' |
la' si' dod'' si' la' si' dod'' la' |
re'' la' fa' la' re' la si dod' |
re' mi' fa' mi' re' mi' fa' re' |
sol' la' sib' la' sol' la' sib' sol' |
la' si' dod'' si' la' si' dod'' la' |
re'' la' fa' la' re' do'! re' mi' |
fa' la' sol' fa' mi' do' re' mi' |
fa' la' sol' fa' mi' do' re' mi' |
fa' fa' la' la' sib' sib' sib' sib' |
sib' sib' la' la' sib' sib' do'' do'' |
fa'4 r r2 |
<< la'1 \\ { do'4( dod' re' red') } >> |
\ru#24 mi'8
mi' fad' sold' la' si' sold' fad' mi' |
\ru#24 la' |
sol'! sol' sol' sol' sol' sol' sol' sol' |
\ru#16 fa' |
\ru#8 mi' |
sol'4 mi' la' la |
\ru#16 <fa la>8 |
\ru#16 re' |
\ru#16 <dod' mi'> |
q la la la la la la la |
\ru#8 la |
re'8 mi' fa' mi' re' mi' fa' re' |
sol' la' sib' la' sol' la' sib' sol' |
la' si' dod'' si' la' si' dod'' la' |
re'' la' fa' la' re' la si dod' |
re' mi' fa' mi' re' mi' fa' re' |
sol' la' sib' la' sol' la' sib' sol' |
la' si' dod'' si' la' si' dod'' la' |
re'' la' fa' la' re' la si dod' |
re' fa' mi' re' dod' la si dod' |
re' fa' mi' re' dod' la si dod' |
re' re' fa' fa' sol' sol' sol' sol' |
\ru#8 la' |
re' re' fa' fa' sol' sol' sol' sol' |
\ru#8 la' |
\ru#4 { re' re' re' re' re'4 r | }
sib'4 r r2\fermata | \allowPageTurn
R2.*2 |
la2\p re'8 fa' |
mi'4-.\f la-. r |
re'4\p do'2~ |
do'4( sib la) |
sol( fa) r8 re' |
do'2 r4 |
r do'4-.( do'-.) |
fa'2 sol'4 |
lab' sol'2~ |
sol'2.~ |
sol'2 do'4 |
r mi' la |
re' do'2 |
do'2. |
sib2\cresc do'4 |
re'!\f r r |
sol'\p r r |
do' r r |
la r r |
re' r r |
sib2. |
la4 mi' r |
R2.*2 |
la2\p re'8 fa' |
mi'4-.\f la-. r |
re'4\p do'2~ |
do'4 sib2~ sib4 la2~ |
la4 fad'( la') |
sol' dod'( re') |
sol'( fa' mi') |
re' sol' la' |
sib' do'! re' |
mi'! sol la |
re'2 dod'4 |
re'8. mi'16 re'4 dod' |
re' r |
R2*3 |
la'4\f r8 dod'' |
la'4 r8 dod'' |
la'4 re'' |
do''! si' |
do'' sib'! |
la' sol' |
la' sol' |
fa' mi' |
fa' re'' |
do''! sib'! |
la' r |
R2*2 |
r8 la\p la' fa' |
sib'4 la' |
sol' fa' |
mi' re' |
do' sol |
fa mi |
re8\f re' re' r |
re re' re' re' |
do'4:16 sol':16 |
fa':16 do'':16 |
sib' r |
r8 sol' la' sib' |
sol'4 do'' |
re''8 fa'4 mi'8 |
sol'4 fa'~ |
fa'8 re'' fa' mi' |
fa' la' sib' do'' |
fa'4 r |
R2*3 |
re'4\f r8 fad' |
re'4 r8 fad' |
re'4 la' |
sol' fad' |
sol' la' |
sib' la' |
sib' la' |
sol' sold' |
la' r |
R2*2 |
r8 la'\p re' fa' |
sol'4 fa' |
mi' re' |
dod' sib |
la8\f mi' mi r |
mi mi' mi la |
la la mi' mi' |
fa' fa' fad' fad' |
sol' sol' sold' sold' |
la' la' dod' dod' |
re' re' fad' fad' |
sol'4 r |
r8 mi' sol' mi' |
sol'4 fa'~ |
fa'8 re' dod' la' |
sol' sib' la' sib' |
la' la la la |
la4 r |
R2*5 |
