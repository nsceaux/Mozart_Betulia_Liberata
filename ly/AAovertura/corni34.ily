\clef "treble" \transposition fa <>
\ru#3 {
  <<
    \tag #'(corno3 corni) { mi''2 }
    \tag #'(corno4 corni) { do'' }
  >> r2 |
}
R1 |
\ru#3 {
  <<
    \tag #'(corno3 corni) { mi''2 }
    \tag #'(corno4 corni) { mi' }
  >> r2 |
}
R1 |
<<
  \tag #'(corno3 corni) { mi''1 | fa'' | mi'' | mi''2 }
  \tag #'(corno4 corni) { do''1 | re'' | mi'' | do''2 }
>> r2 |
<<
  \tag #'(corno3 corni) { mi''1 | fa'' | mi'' | mi''2 }
  \tag #'(corno4 corni) { do''1 | re'' | mi'' | do''2 }
>> r2 |
r8 \twoVoices #'(corno3 corno4 corni) <<
  { do''8 re'' mi'' fa''2 |
    mi''8 do'' re'' mi'' fa''2 |
    mi''4 do'' do'' do'' |
    re'' mi'' re''2 | }
  { do''8 sol' do'' re''2 |
    do''4 sol'8 do'' re''2 |
    do''2 do''4 do'' |
    sol'4 do''2 sol'4 | }
>>
<<
  \tag #'(corno3 corni) {
    do''4 s s re'' |
    do''
  }
  \tag #'(corno4 corni) {
    do'4 s s sol' |
    do'
  }
  { s4 r r s }
>> r4 r2 |
R1*4 |
<<
  \tag #'(corno3 corni) {
    mi''2 s |
    mi'' s |
    mi'' mi''4 mi'' |
    fa''2 re''4 re'' |
    do''2
  }
  \tag #'(corno4 corni) {
    sol'2 s |
    sol' s |
    sol'2 sol'4 sol' |
    sol'2 sol'4 sol' |
    mi'2
  }
  { s2 r | s r | }
>> r2 |
<<
  \tag #'(corno3 corni) {
    do''4 do''8 do'' do''4 mi'' |
    fa''1 |
  }
  \tag #'(corno4 corni) {
    mi'4 mi'8 mi' mi'4 do'' |
    re''1 |
  }
>>
r4 \twoVoices #'(corno3 corno4 corni) <<
  { fa''4 mi'' mi'' | }
  { re''4 mi'' mi'' | }
>>
\ru#3 {
  <<
    \tag #'(corno3 corni) { mi''2 }
    \tag #'(corno4 corni) { do'' }
  >> r2 |
}
R1 |
\ru#3 {
  <<
    \tag #'(corno3 corni) { mi''2 }
    \tag #'(corno4 corni) { mi' }
  >> r2 |
}
R1 |
<<
  \tag #'(corno3 corni) { mi''1 | fa'' | mi'' | mi''2 }
  \tag #'(corno4 corni) { do''1 | re'' | mi'' | do''2 }
>> r2 |
<<
  \tag #'(corno3 corni) { mi''1 | fa'' | mi'' | }
  \tag #'(corno4 corni) { do''1 | re'' | mi'' | }
>>
\twoVoices #'(corno3 corno4 corni) << { mi''4 } { mi'' } >> r4 r2 |
r2 r4 <<
  \tag #'(corno3 corni) {
    mi''4 |
    mi'' s s mi'' |
    mi''2 fa'' |
    mi''1 |
    mi''2 fa'' |
    mi''1 |
    mi''2 s |
    sol''1 |
    fa'' |
    mi''1 |
  }
  \tag #'(corno4 corni) {
    mi'4 |
    mi' s s mi' |
    do''2 re'' |
    mi''1 |
    do''2 re'' |
    mi''1 |
    do''2 s |
    mi''1 |
    re'' |
    do''1 |
  }
  { s4 | s r r s | s1*4 | s2 r | }
>>
\twoVoices #'(corno3 corno4 corni) <<
  { do''2 }
  { do'' }
>> r2\fermata |
R2.*39 |
R2*4 |
r4 r8 <>\f <<
  \tag #'(corno3 corni) {
    mi''8 |
    mi''4 s8 mi'' |
    mi''4
  }
  \tag #'(corno4 corni) {
    mi'8 |
    mi'4 s8 mi' |
    mi'4
  }
  { s8 | s4 r8 s | }
>> r4 |
R2*3 |
<<
  \tag #'(corno3 corni) { do''4 }
  \tag #'(corno4 corni) { do' }
>> r4 |
R2*3 |
<<
  \tag #'(corno3 corni) { mi''4 }
  \tag #'(corno4 corni) { mi' }
>> r4 |
R2*8 |
\tag #'corni <>^"a 2." re''4\f r |
re'' r |
<<
  \tag #'(corno3 corni) {
    re''2 |
    do'' |
    do''4 s |
    fa''2 |
    re''4 mi'' |
    s8 fa'' mi'' re'' |
    re''4 mi'' |
    s8 fa'' mi'' re'' |
    do''4
  }
  \tag #'(corno4 corni) {
    sol'2 |
    do' |
    do'4 s |
    re''2 |
    sol'4 do'' |
    s8 re'' do'' sol' |
    sol'4 do'' |
    s8 re'' do'' sol' |
    do'4
  }
  { s2*2 | s4 r | s2*2 | r8 s4. | s2 | r8 }
>> r4 |
R2*4 |
r8 <>\f <<
  \tag #'(corno3 corni) {
    mi''8 mi''4 |
    s8 mi'' mi''4 |
  }
  \tag #'(corno4 corni) {
    mi'8 mi'4 |
    s8 mi' mi'4 |
  }
  { s4. | r8 s4. | }
>>
R2*2 |
\tag #'corni <>^"a 2." re''4\f r |
R2 |
re''4 r |
R2 |
<<
  \tag #'(corno3 corni) { mi''4 }
  \tag #'(corno4 corni) { mi' }
>> r4 |
R2*6 |
<>\f <<
  \tag #'(corno3 corni) {
    mi''4 s |
    mi'' s |
    s mi'' |
    mi''
  }
  \tag #'(corno4 corni) {
    mi'4 s |
    mi' s |
    s mi' |
    mi'
  }
  { s4 r | s r | r }
>> r4 |
R2 |
<<
  \tag #'(corno3 corni) { mi''4 }
  \tag #'(corno4 corni) { mi' }
>> r4 |
\twoVoices #'(corno3 corno4 corni) <<
  { mi''4 }
  { mi'' }
>> <<
  \tag #'(corno3 corni) { sol''4 | fa'' }
  \tag #'(corno4 corni) { mi''4 | re'' }
>> r4 |
<<
  \tag #'(corno3 corni) { fa''2 }
  \tag #'(corno4 corni) { re'' }
>>
R2 |
r4 \twoVoices #'(corno3 corno4 corni) <<
  { mi''4 }
  { mi'' }
>>
<<
  \tag #'(corno3 corni) { fa'' }
  \tag #'(corno4 corni) { re'' }
>> \twoVoices #'(corno3 corno4 corni) <<
  { mi''4 }
  { mi'' }
>>
r8 <<
  \tag #'(corno3 corni) { mi''8 mi'' mi'' | mi''4 }
  \tag #'(corno4 corni) { mi'8 mi' mi' | mi'4 }
>> r4 |
R2 |
<>\f <<
  \tag #'(corno3 corni) {
    mi''2~ |
    mi'' |
    mi''4 do'' |
    do''
  }
  \tag #'(corno4 corni) {
    do''2~ |
    do'' |
    do''4 mi' |
    mi'
  }
>> r4 |
