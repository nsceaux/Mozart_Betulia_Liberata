\score {
  <<
    \new StaffGroup <<
      \new Staff \with { \oboiInstr } <<
        \global \keepWithTag #'oboi \includeNotes "oboi"
      >>
      \new Staff \with { \fagottiInstr } <<
        \global \keepWithTag #'fagotti \includeNotes "fagotti"
      >>
      \new GrandStaff \with {
        instrumentName = "Corni"
        shortInstrumentName = "Cor."
      } <<
        \new Staff \with {
          \override InstrumentName.self-alignment-X = #RIGHT
          instrumentName = \markup\small "I. II in D."
          shortInstrumentName = \markup\small D
        } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni12" >>
        \new Staff \with {
          \override InstrumentName.self-alignment-X = #RIGHT
          instrumentName = \markup\small "III. IV in F."
          shortInstrumentName = \markup\small F
        } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni34" >>
      >>
      \new Staff \with {
        instrumentName = \markup\center-column { Trombe \small in D. }
        shortInstrumentName = \markup { Tr \small D. }
      } << \keepWithTag #'() \global \keepWithTag #'trombe \includeNotes "trombe" >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
      \new Staff \with { \bassoInstr } <<
        \global \includeNotes "basso"
        \origLayout {
          s1*7\break s1*8\pageBreak
          \grace s16 s1*8\break s1*7\pageBreak
          s1*8\break s1*7\pageBreak
          \grace s16 s1*6\break s1*8\pageBreak
          s1*2 s2.*10\break s2.*12\pageBreak
          \grace s8 s2.*12\break s2.*5 s2*6\pageBreak
          s2*15\break s2*11\pageBreak
          s2*15\break s2*12\pageBreak
        }
      >>
    >>
  >>
  \layout { }
  \midi { }
}
