\clef "treble" \transposition re <>
\ru#3 {
  <<
    \tag #'(corno1 corni) { do''2 }
    \tag #'(corno2 corni) { do' }
  >> r2 |
}
R1 |
<<
  \tag #'(corno1 corni) {
    re''2 s | re'' s | re'' s |
  }
  \tag #'(corno2 corni) {
    sol'2 s | sol s | sol s |
  }
  { s r | s r | s r | }
>>
R1 |
<<
  \tag #'(corno1 corni) {
    do''1~ | do'' | re'' | do''2
  }
  \tag #'(corno2 corni) {
    do'1~ | do' | sol' | do'2
  }
>> r2 |
<<
  \tag #'(corno1 corni) {
    do''1~ | do'' | re'' | do''2
  }
  \tag #'(corno2 corni) {
    do'1~ | do' | sol' | do'2
  }
>> r2 |
R1*6 |
\tag #'corni <>^"a 2." re''2 r |
re''2 r |
re''4 r re'' r |
re''1 |
<<
  \tag #'(corno1 corni) {
    re''2 s |
    re'' s |
    re''2 re''4 re'' |
  }
  \tag #'(corno2 corni) {
    sol'2 s |
    sol' s |
    sol' sol'4 sol' |
  }
  { s2 r | s r | }
>>
\tag #'corni <>^"a 2." re''2 r |
R1*2 |
re''1 |
fa''4 re'' sol'' sol' |
\ru#3 {
  <<
    \tag #'(corno1 corni) { do''2 }
    \tag #'(corno2 corni) { do' }
  >> r2 |
}
R1 |
<<
  \tag #'(corno1 corni) {
    re''2 s | re'' s | re'' s |
  }
  \tag #'(corno2 corni) {
    sol'2 s | sol s | sol s |
  }
  { s r | s r | s r | }
>>
R1 |
<<
  \tag #'(corno1 corni) {
    do''1~ | do'' | re'' | do''2
  }
  \tag #'(corno2 corni) {
    do'1~ | do' | sol' | do'2
  }
>> r2 |
<<
  \tag #'(corno1 corni) {
    do''1 | do'' | re'' | do''4
  }
  \tag #'(corno2 corni) {
    do'1 | do' | sol' | do'4
  }
>> r4 r2 |
<<
  \tag #'(corno1 corni) {
    s8 sol'' sol'' sol'' sol''4 s |
    s8 sol'' sol'' sol'' sol''4 s |
    do''1 |
  }
  \tag #'(corno2 corni) {
    s8 sol' sol' sol' sol'4 s |
    s8 sol' sol' sol' sol'4 s |
    do'1 |
  }
  { r8 s4. s4 r | r8 s4. s4 r | }
>>
r2 \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 | do''1 }
  { sol'2 | do'1 }
>>
r2 \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 | }
  { sol'2 | }
>>
<<
  \tag #'(corno1 corni) {
    do''4 do'' do'' s |
    s do'' do'' s |
    s do'' do'' s |
    s do'' do'' s |
    do''2
  }
  \tag #'(corno2 corni) {
    do'4 do' do' s |
    s do' do' s |
    s do' do' s |
    s do' do' s |
    do'2
  }
  { s2. r4 |
    r s2 r4 |
    r s2 r4 |
    r s2 r4 | }
>> r2\fermata | \allowPageTurn
R2.*39 |
R2*4 |
<<
  \tag #'(corno1 corni) {
    s8 re'' re''4 |
    s8 re'' re''4 |
    re''4
  }
  \tag #'(corno2 corni) {
    s8 sol' sol'4 |
    s8 sol' sol'4 |
    sol'4
  }
  { r8 s4.\f | r8 s4. | }
>> r4 |
R2*5 |
<<
  \tag #'(corno1 corni) { do''4 }
  \tag #'(corno2 corni) { do' }
>> r4 |
R2 |
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { sol }
>> r4 |
R2*8 |
\tag #'corni <>^"a 2." do''4\f r |
do'' r |
R2*13
<<
  \tag #'(corno1 corni) {
    s8 mi'' mi''4 |
    s8 mi'' mi''4 |
    do''2~ |
    do''~ |
    do''~ |
    do'' |
    do''4
  }
  \tag #'(corno2 corni) {
    s8 do'' do''4 |
    s8 do'' do''4 |
    do'2~ |
    do'~ |
    do'~ |
    do' |
    do'4
  }
  { r8 s4.\f | r8 }
>> r4 |
R2 |
<<
  \tag #'(corno1 corni) { sol'4 }
  \tag #'(corno2 corni) { sol }
>> r4 |
R2*6 |
\tag #'corni <>^"a 2." re''4\f r |
re'' r |
do'' <<
  \tag #'(corno1 corni) { re''4 | do''2 }
  \tag #'(corno2 corni) { sol'4 | do'2 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do''4 re'' | }
  { do'' re'' | }
>>
<<
  \tag #'(corno1 corni) { re''2 | do'' | do''4 }
  \tag #'(corno2 corni) { sol'2 | do' | do'4 }
>> r4 |
<<
  \tag #'(corno1 corni) { fa''2 | re''4 }
  \tag #'(corno2 corni) { re''2 | sol'4 }
>> \tag #'corni <>^"a 2." do''4 |
r8 sol' sol' sol' |
do'' re'' do''4 |
r8 do'' <<
  \tag #'(corno1 corni) { re''4 | do'' }
  \tag #'(corno2 corni) { sol'4 | do' }
>> r4 |
R2 |
<>\f <<
  \tag #'(corno1 corni) {
    do''2~ |
    do'' |
    do''4 do'' |
    do''
  }
  \tag #'(corno2 corni) {
    do'2~ |
    do' |
    do'4 do' |
    do'
  }
>> r4 |
