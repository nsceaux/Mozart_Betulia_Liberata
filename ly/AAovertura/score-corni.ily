\score {
  \new GrandStaff <<
    \new Staff \with {
      instrumentName = \markup "I. II in D."
    } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni12" >>
    \new Staff \with {
      instrumentName = \markup "III. IV in F."
    } << \keepWithTag #'() \global \keepWithTag #'corni \includeNotes "corni34" >>
  >>
  \layout { indent = \largeindent }
}
