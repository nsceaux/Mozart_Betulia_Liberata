\clef "bass" <>
<<
  \tag #'(fagotto1 fagotti) { la1~ | la | re'~ | re' | dod'~ | dod' | la~ | la | }
  \tag #'(fagotto2 fagotti) { fa1~ | fa | re~ | re | la~ | la | la,~ | la, | }
>>
\tag #'fagotti <>^"a 2." re8 mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la, si, dod |
re mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re do! re mi |
fa la sol fa mi do re mi |
fa la sol fa mi do re mi |
fa fa la la sib sib sib sib |
sib sib la la sib sib do' do' |
fa fa re re sib, sib, do do |
fa1 |
\ru#24 mi8
mi fad sold la si sold fad mi |
\ru#24 la |
sol! sol sol sol sol sol sol sol |
<<
  \tag #'(fagotto1 fagotti) { do'1~ | do' | dod' | }
  \tag #'(fagotto2 fagotti) { la1~ | la | sol }
>>
\tag #'fagotti <>^"a 2." sol4 mi la2 |
<<
  \tag #'(fagotto1 fagotti) {
    la1~ | la | re'~ | re' | dod' | dod' | la~ | la |
  }
  \tag #'(fagotto2 fagotti) {
    fa1~ | fa | re~ | re | la | la | la,~ | la, |
  }
>>
\tag #'fagotti <>^"a 2." re8 mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la, si, dod |
re mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la si dod' |
re' fa' mi' re' dod' la si dod' |
re' fa' mi' re' dod' la si dod' |
re' re' fa fa sol sol sol sol |
\ru#8 la |
re' re' fa fa sol sol sol sol |
\ru#8 la |
\ru#4 { re re re re re4 r | }
sib4 r r2\fermata | \allowPageTurn
R2.
\tag #'fagotti <>^"a 2." la2.\p~ |
la |
dod4-.\f re-. r |
sib4\p do' sib |
la sib la |
sol fa sib, |
do-. do'( sib) |
lab2 mi4 |
fa2.~ |
fa4( sol fa)~ |
fa sol( fa) |
mi2 fa4 |
do4( dod re) |
sib do' do |
fa( sol la) |
sib-.\cresc sib,( lab,) |
sol,\f r r |
<>\p <<
  \tag #'(fagotto1 fagotti) { si2. | do' | dod' | re' | re' | dod'4 }
  \tag #'(fagotto2 fagotti) { sol2. | mib | sol | fa | sib | la4 }
>>
\twoVoices #'(fagotto1 fagotto2 fagotti) <<
  { la,4 }
  { la, }
>> r |
R2. |
\tag #'fagotti <>^"a 2." la2.\p~ |
la |
sol4-.\f fa-. r |
sib4\p do' sib |
la sib la |
sol la sol |
fad2. |
sol4 mib re |
dod2. |
re4 mi! fad |
sol la sib |
dod mi fa! |
sol la la, |
sib,8 sol la4 la, |
re r |
R2*3 |
la4\f r8 dod' |
la4 r8 dod' |
la4 re' |
do'! si |
do' sib! |
la sol |
la sol |
fa mi |
fa re' |
do'! sib! |
la r |
R2*8 |
sol4.\f lab8 |
sol4. si8 |
do' do' mi mi |
fa fa la! la |
sib!4 r |
r8 sib la sol |
do'4 la |
sib do'8 do |
do'4 re' |
sib do' |
fa8 la sib do' |
fa4 r |
R2*3 |
re4\f r8 fad |
re4 r8 fad |
re4 la |
sol fad |
sol la |
sib la |
sib la |
sol sold |
la r |
R2*6 |
la4.\f sib8 |
la4. dod'8 |
re' re mi mi |
fa fa fad fad |
sol sol sold sold |
la la dod dod |
re re fad fad |
sol4 r |
r8 mi sol mi |
la4 sib |
la la, |
sib,8 sol fa sol |
la4 la, |
re r |
R2 |
re4 r8 fa\f |
re4 r8 fa |
re4 re |
re r |
