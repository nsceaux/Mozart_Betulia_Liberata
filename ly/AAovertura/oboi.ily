\clef "treble" \tag #'oboi <>^"a 2." re'2 r |
re' r |
re'2. <<
  \tag #'(oboe1 oboi) {
    la''4 |
    sold''( la'' sib'' la'') |
    mi''2. sol''4 |
    mi''2. sol''4 |
    mi''2. sib''4 |
    la''( sol'' fa'' mi'') |
  }
  \tag #'(oboe2 oboi) {
    fa''4 |
    mi''( fa'' sol''! fa'') |
    dod''2. mi''4 |
    dod''2. mi''4 |
    dod''2. sol''4 |
    fa''( mi'' re'' dod'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 fa'' }
  { re''2 }
>> <<
  \tag #'(oboe1 oboi) { la''2 | sib''1 | mi'' | fa''2 }
  \tag #'(oboe2 oboi) { fa''2 | re''1 | dod'' | re''2 }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    fa''2 la'' |
    sib''1 |
    dod''-\tag #'oboi _"I" |
  }
  \tag #'(oboe2 oboi) {
    re''2 fa'' |
    re''1 |
    sol''-\tag #'oboi ^"II" |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 }
  { fa''2 }
>> r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { do'''1~ | do''' | la''4 }
  { do''1~ | do''~ | do''4 }
>> \tag #'oboi <>^"a 2." fa''4 re'' re''' |
mi'' fa''2 mi''4 |
fa'' r r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''1 | }
  { do''4( dod'' re'' red'') | }
>>
<<
  \tag #'(oboe1 oboi) { sold''2 }
  \tag #'(oboe2 oboi) { mi'' }
>> r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { mi'2 }
  { mi' }
>> r |
<<
  \tag #'(oboe1 oboi) { sold''1~ | sold'' | la''2 }
  \tag #'(oboe2 oboi) { si'1 | re'' | do''2 }
>> r2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'2 }
  { la' }
>> r |
<<
  \tag #'(oboe1 oboi) {
    la''1 |
    mi''-\tag #'oboi _"I" |
    fa''-\tag #'oboi _"I" |
    la''-\tag #'oboi ^"I" |
    sib'' |
  }
  \tag #'(oboe2 oboi) {
    do''1 |
    sib''!-\tag #'oboi ^"II" |
    la''-\tag #'oboi ^"II" |
    fa''-\tag #'oboi _"II" |
    dod'' |
  }
>>
\tag #'oboi <>^"a 2." sol''4 mi'' la'' la' |
re'2 r |
re' r |
re'2. <<
  \tag #'(oboe1 oboi) {
    la''4 |
    sold''( la'' sib'' la'') |
    mi''2. sol''!4 |
    mi''2. sol''4 |
    mi''2. sib''4 |
    la''( sol'' fa'' mi'') |
  }
  \tag #'(oboe2 oboi) {
    fa''4 |
    mi''( fa'' sol''! fa'') |
    dod''2. mi''4 |
    dod''2. mi''4 |
    dod''2. sol''4 |
    fa''( mi'' re'' dod'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 fa'' }
  { re''2 }
>> <<
  \tag #'(oboe1 oboi) { la''2 | sib'' sib'' | mi''1 | fa''2 }
  \tag #'(oboe2 oboi) { fa''2 | sol'' re'' | dod''1 | re''2 }
>> r2 |
<<
  \tag #'(oboe1 oboi) {
    la''1 |
    re''-\tag #'oboi _"I" |
    sol''-\tag #'oboi ^"I" |
    fa''4
  }
  \tag #'(oboe2 oboi) {
    fa''1 |
    sib''-\tag #'oboi ^"II" |
    dod''-\tag #'oboi _"II" |
    re''4
  }
>> r4 r2 |
r8 \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''8 dod'' re'' sol''2 |
    fa''8 re'' mi'' fa'' mi''2 |
    re''4 la'' sib''2 | }
  { re''8 mi'' fa'' mi''2 |
    fa''8 re'' dod'' re'' sol''2 |
    fa''4 re''2 re''4 | }
>>
<<
  \tag #'(oboe1 oboi) { fa''2 mi'' | }
  \tag #'(oboe2 oboi) { re''2 dod'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''4 la'' sib''2 | }
  { fa''4 re''2 re''4 | }
>>
<<
  \tag #'(oboe1 oboi) { fa''2 mi'' | }
  \tag #'(oboe2 oboi) { re''2 dod'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 }
  { re'' }
>> <<
  \tag #'(oboe1 oboi) {
    re'''2 |
    la''1 |
    sol''-\tag #'oboi _"I" |
    fa''!-\tag #'oboi _"I" |
    re''4-\tag #'oboi _"I"
  }
  \tag #'(oboe2 oboi) {
    sib''2 |
    fad''1 |
    sib''-\tag #'oboi ^"II" |
    la''-\tag #'oboi ^"II" |
    sold''4-\tag #'oboi ^"II"
  }
>> r4 r2\fermata | \allowPageTurn
R2.*15 | \allowPageTurn
\mergeDifferentlyDottedOn
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2.~ | fa''~ | fa''8 }
  { fa''2 mib''4 | reb''2 do''4 | si'8 }
  { s2.\p | s\cresc | s8\f }
>> r8 r4 r |
R2.*13 | \allowPageTurn
<>\p <<
  \tag #'(oboe1 oboi) { la''2. }
  \tag #'(oboe2 oboi) { re'' }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2.~ | sib''4( la'' sol'') | }
  { re''4 sol''2~ | sol''4( fa'' mi'') | }
>>
<<
  \tag #'(oboe1 oboi) { fa''8 }
  \tag #'(oboe2 oboi) { re'' }
>> r r4 r |
R2.*4 | \allowPageTurn
R2*4 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { la''2~ | la''~ | la''4 }
  { la'4 si'8\rest mi'' | dod''4 si'8\rest mi'' | dod''4 }
>> \tag #'oboi <>^"a 2." sib''4 |
la'' sold'' |
la'' sol''! |
fa'' mi'' |
fa'' mib'' |
re'' dod'' |
re'' sib' |
la' sold' |
la' r |
R2*8 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { fa''2~ |
    fa'' |
    mi''4 sib''! |
    la'' mib'' | }
  { si'4. do''8 |
    si'4. re''8 |
    do''2~ |
    do''4 fa'' | }
>>
<<
  \tag #'(oboe1 oboi) { re''4 }
  \tag #'(oboe2 oboi) { fa' }
>> r4 |
<<
  \tag #'(oboe1 oboi) {
    sib''2~ |
    sib''4 la'' |
    sol''8 sib'' la'' sol'' |
    mi''4 fa''~ |
    fa''8 sib'' la'' sol'' |
  }
  \tag #'(oboe2 oboi) {
    re''2 |
    mi''4 fa'' |
    re''8 sol'' fa'' mi'' |
    sib'4 la' |
    sol'8 sol'' fa'' mi'' |
  }
>>
\tag #'oboi <>^"a 2." fa''8 la' sib' do'' |
fa'4 r |
R2*3 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { re''2 | re''' | }
  { fad'4 si'8\rest la'' | fad''4 si'8\rest la'' | }
>>
\tag #'oboi <>^"a 2." re'''4 do''' |
sib'' la'' |
sib'' la'' |
sol'' fad'' |
sol'' fa''! |
mi'' re'' |
la'' r |
R2*6 |
<>\f \twoVoices #'(oboe1 oboe2 oboi) <<
  { sol''2~ | sol'' | }
  { dod''4. re''8 | dod''4. mi''8 | }
>>
<<
  \tag #'(oboe1 oboi) { fa''4 }
  \tag #'(oboe2 oboi) { re'' }
>> \tag #'oboi <>^"a 2." dod''' |
re''' do''! |
sib' re' |
dod' sol'' |
fad'' do''! |
sib' r |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sib''2 |
    dod''4 re''~ |
    re'' dod'' |
    re''8 mi'' fa''4~ |
    fa'' mi'' |
    re'' }
  { sol''2~ |
    sol''4 fa''~ |
    fa'' mi'' |
    re''8 dod'' re''4~ |
    re'' dod'' |
    re''
  }
>> r4 |
R2 |
<>\f <<
  \tag #'(oboe1 oboi) {
    fa''2~ |
    fa'' |
    fa''4 la'' |
    fa''
  }
  \tag #'(oboe2 oboi) {
    re''2~ |
    re'' |
    re''4 fa'' |
    re''
  }
>> r |
