\clef "treble" << re'2. \\ re' >> fa'4 |
<< re'2. \\ re' >> fa'4 |
<< re'2. \\ re' >> <<
  \tag #'violino1 {
    la'4 |
    sold'( la' sib' la') |
  }
  \tag #'violino2 {
    fa'4 |
    mi'( fa' sol' fa') |
  }
>>
<mi' la>2. sol'4 |
<mi' la>2. sol'4 |
<mi' la>2. <<
  \tag #'violino1 {
    sib'4 |
    la'( sol' fa' mi') |
  }
  \tag #'violino2 {
    sol'4 |
    fa'( mi' re' dod') |
  }
>>
re'4 <re' la' fa''> <re' la' la''> <re' re'' re'''> |
sib'' sol'' r8 sib'' sol'' mi'' |
dod''4 mi'' la' la'' |
\grace sol''16 fa''8 mi''16 fa'' re''4 re'8 la si dod' |
re'4 <re' la' fa''> <re' la' la''> <re' re'' re'''> |
sib'' sol'' r8 sib'' sol'' mi'' |
dod''4 mi'' la' la'' |
\grace sol''16 fa''8 mi''16 fa'' re''4 r8 <<
  \tag #'violino1 {
    mi''8 fa'' sol'' |
    la'' do''' sib'' la'' sol'' mi'' fa'' sol'' |
    la'' do''' sib'' la'' sol'' mi'' fa'' sol'' |
    la''16 la'' la'' la''
  }
  \tag #'violino2 {
    mi'8 fa' sol' |
    la' do'' sib' la' sol' mi' fa' sol' |
    la' do'' sib' la' sol' mi' fa' sol' |
    la'16 la' la' la'
  }
>> fa''4:16 re'':16 re''':16 |
mi''4:16 fa'':16 sol':16 sol'':16 |
la'':16 fa'':16 sol':16 mi'':16 |
fa'4 <<
  \tag #'violino1 { la'2 la'4 | }
  \tag #'violino2 { dod'4( re' red') | }
>>
mi'2. sold'4 |
mi'2. si'4 |
mi' <mi' re'' si''> mi' <sold' mi'' re'''> |
mi'8 fad' sold' la' si' sold' fad' mi' |
<< la'2. \\ la' >> do''4 |
<< la'2. \\ la' >> mi''4 |
la' <mi' do'' la''> la' <la' mi'' do'''> |
sol'! <mi' do'' sib''!> r8 sib'! sol' mi' |
<fa' la>2. la'4 |
fa' la' do'' fa'' |
dod'2. sol'4 |
sol' mi' la' la |
<< re'2. \\ re' >> fa'4 |
<< re'2. \\ re' >> fa'4 |
<< re'2. \\ re' >> <<
  \tag #'violino1 {
    la'4 |
    sold'( la' sib' la') |
  }
  \tag #'violino2 {
    fa'4 |
    mi'( fa' sol' fa') |
  }
>>
<mi' la>2. sol'4 |
<mi' la>2. sol'4 |
<mi' la>2. <<
  \tag #'violino1 {
    sib'4 |
    la'( sol' fa' mi') |
  }
  \tag #'violino2 {
    sol'4 |
    fa'( mi' re' dod') |
  }
>>
re'4 <re' la' fa''> <re' la' la''> <re' re'' re'''> |
sib'' sol'' r8 sib'' sol'' mi'' |
dod''4 mi'' la' la'' |
\grace sol''16 fa''8 mi''16 fa'' re''4 re'8 la si dod' |
re'4 <re' la' fa''> <re' la' la''> <re' re'' re'''> |
sib'' sol'' r8 sib'' sol'' mi'' |
dod''4 mi'' la' la'' |
\grace sol''16 fa''8 mi''16 fa'' re''4 r8 <<
  \tag #'violino1 {
    dod''8 re'' mi'' |
    fa'' la'' sol'' fa'' mi'' dod'' re'' mi'' |
  }
  \tag #'violino2 {
    dod'8 re' mi' |
    fa' la' sol' fa' mi' dod' re' mi' |
  }
>>
fa''8 la'' sol'' fa'' mi'' dod'' re'' mi'' |
<<
  \tag #'violino1 {
    fa''4:16 la'':16 sib'':16 re''':16 |
    fa'':16 la'':16 dod'':16 mi'':16 |
    fa''4:16 la'':16 sib'':16 re''':16 |
    fa'':16 la'':16 dod'':16 mi'':16 |
  }
  \tag #'violino2 {
    fa'4:16 re''2:16 sib''4:16 |
    re''2:16 mi''4:16 dod'':16 |
    fa'4:16 re''2:16 sib''4:16 |
    re''2:16 mi''4:16 dod'':16 |
  }
>>
re''8 re' re' re' re'4 <<
  \tag #'violino1 { re'''4 | do'''8 }
  \tag #'violino2 { sib''4 | la''8 }
>> re'8 re' re' re'4 <<
  \tag #'violino1 { do'''4 | sib''8 }
  \tag #'violino2 { la''4 | sol''8 }
>> re'8 re' re' re'4 <<
  \tag #'violino1 { sib''4 | la''8 }
  \tag #'violino2 { sol''4 | fa''8 }
>> re'8 re' re' re'4 <<
  \tag #'violino1 { la''4 | sold'' }
  \tag #'violino2 { fa''4 | <fa'' re''> }
>> r4 r2\fermata | \allowPageTurn
<<
  \tag #'violino1 {
    la''4\p( fa'' re'') |
    dod''( re'' fa'') |
    mi''( fa'' la'') |
    sib''4-.\f la''-. r |
    sol''8.(\p la''32 sib'') la''4( sol'') |
    fa''8.( sol''32 la'') sol''4( fa'') |
    mi''( fa'' sib'') |
    la''( sol'') r |
    do'4-. do''( sib') |
    lab'-. fa''( mib'') |
    reb''( do'' si') |
    sib'!2.\fp |
    sib'4( re'' do'') |
    sol'( sib' la'!)~ |
    la'8 sib' la'4 sol'\trill |
    fa'2.~ |
    fa'\cresc~ |
    fa'4\f fa''4\p fa'' |
    \grace sol''8 fa''4 mib''8 re'' do'' si' |
    do''4 sol'' sol'' |
    \grace la''8 sol''4 fa''8 mi''! re'' dod'' |
    re''4 la'' la'' |
    \grace sib''8 la''4 sol''8 fa'' mi'' re'' |
    la''4 la' r |
    la''(\p fa'' re'') |
    dod''( re'' fa'') |
    mi''( fa'' la'') |
    sib''4-.\f la''-. r |
    sol''8.(\p la''32 sib'') la''4( sol'') |
    fa''8.( sol''32 la'') sol''4( fa'') |
    mi''8.( fa''32 sol'') fa''4( mib'') |
    re''( la' do'') |
    sib'2.~ |
    sib'4( la' sol') |
    fa'8
  }
  \tag #'violino2 {
    R2. |
    la'4(\p fa' re') |
    dod'( re' fa') |
    sol'-.\f fa'-. r |
    fa'2(\p mi'4)~ |
    mi'( re' do') |
    sib( la) re''8( sol'') |
    fa''4( mi'') r |
    R2. |
    do'4-. lab'( sol') |
    fa'( mi' re')~ |
    re'2 mi'8( fa') |
    sol'4( sib' la'!) |
    mi'( sol' fa')~ |
    fa'8 sol' fa'4 mi'\trill |
    fa'2 mib'4 |
    reb'2\cresc do'4 |
    si\f r r |
    si(\p re'! fa') |
    mib'( do') r |
    dod'( mi'! sol') |
    fa'( re') r |
    re'4 re'' sold' |
    la' dod'' r |
    R2.*2 |
    la'4(\p fa' re') |
    dod'4-.\f re'-. r |
    fa'2\p mi'4~ |
    mi' re'2~ |
    re' do'4 |
    la' r re' |
    re' sol'( fa') |
    mi'2. |
    la8
  }
>> la''4 dod'' re''8~ |
re'' mib''4 fad' sol'8~ |
sol' sib'4 dod' re'8 |
sib' sol''16 mi'' mi''2\trill |
re''16 sib' la' sol' fa'4 mi'8.\trill re'32 mi' |
re'4 r8 fa'\p |
re'4 r8 fa' |
re'4 sib' |
r8 sib'( la' sold') |
<<
  \tag #'violino1 {
    la'16\f la'' la'' la'' la''4:16 |
    la''2:16 |
    la''4:16 sib'':16 |
    la'':16 sold'':16 |
    la'':16 sol''!:16 |
    fa'':16 mi'':16 |
    fa'':16 mib'':16 |
    re'':16 dod'':16 |
    re'':16 sib':16 |
    la':16 sold':16 |
    la' r |
    do''!4\p r8 reb''( |
    do''4) r8 reb''( |
    do''4) r8 fa''~ |
    fa'' mi''4 re''8~ |
    re'' do''4 sib'8~ |
    sib' la'4 sol'8~ |
    sol'8 fa'4 mi'8~ |
    mi' re'4 do'8 |
    si16\f fa'' fa'' fa'' fa''4:16 |
    fa''2:16 |
    mi''4:16
  }
  \tag #'violino2 {
    la'4\f r8 mi'' |
    dod''4 r8 mi'' |
    la'4:16 sib':16 |
    la':16 sold':16 |
    la':16 sol'!:16 |
    fa':16 mi':16 |
    fa':16 mib':16 |
    re':16 dod':16 |
    re':16 sib:16 |
    la:16 sold:16 |
    la4 r |
    r8 mi'(\p fa'4) |
    r8 mi'( fa'4) |
    r8 fa' do'' la' |
    sol'4 fa' |
    mi' re' |
    do' sib |
    la sib |
    la sol |
    si4.\f do'8 |
    si4 r8 sol' |
    sol'4:16
  }
>> sib''!:16 |
la'':16 mib'':16 |
re''8 mi''!16 fa'' sol'' la'' sib'' do''' |
re'''4 r |
r8 mi'' fa'' la'' |
sol''8. la''32 sib'' la''8 sol'' |
r mi'' fa'' la'' |
sol''8. la''32 sib'' la''8 sol'' |
fa'' la' sib' do'' |
fa'4 r8 la'\p |
fa'4 r8 la' |
fa'4 mib'' |
r8 mib'' do'' la' |
<<
  \tag #'violino1 {
    sold'4\f re'''16 re''' re''' re''' |
    re'''2:16 |
    re'''4:16 do''':16 |
    sib'':16 la'':16 |
    sib'':16 la'':16 |
    sol'':16 fad'':16 |
    sol'':16 fa''!:16 |
    mi'':16 re'':16 |
    la''4 r |
    la'4\p r8 sib'( |
    la'4) r8 sib'( |
    la'4) r8 fa''~ |
    fa'' mi''4 re''8~ |
    re'' dod''4 sib'8~ |
    sib' la'4 sold'8 |
    sol'!16\f sol'' sol'' sol'' sol''4:16 |
    sol''2:16 |
    fa''4:16
  }
  \tag #'violino2 {
    fad'4\f r8 la'' |
    fad''4 r8 la'' |
    re''4:16 do'':16 |
    sib':16 la':16 |
    sib':16 la':16 |
    sol':16 fad':16 |
    sol':16 fa'!:16 |
    mi':16 re':16 |
    la'4 r |
    r8 re'\p( dod'4) |
    r8 re'( dod'4) |
    r8 re' fa' la' |
    sib'4 la' |
    sol' fa' |
    mi' re' |
    dod'4.\f re'8 |
    dod'4. mi'8 |
    re'4:16
  }
>> dod'''4:16 |
re''':16 do''!:16 |
sib':16 re':16 |
dod':16 sol'':16 |
fad'':16 do''!:16 |
sib'8 do''16 re'' mi'' fa''! sol'' la'' |
sib''4 r |
r8 dod''' re''' re'' |
la'4 mi''8.\trill re''32 mi'' |
re''8 dod''' re''' re'' |
la'4 mi''8.\trill re''32 mi'' |
re''4 r8 fa'\p |
re'4 r8 fa' |
re'4 <re' re'' re'''>\f |
<re' la' la''> <re' re'' re'''> |
<re' la' la''> <re' la' fa''> |
q4 r |
