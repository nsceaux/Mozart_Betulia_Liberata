\clef "bass" \ru#16 re8 |
re4 r r2 |
R1 |
\ru#16 la8 |
la,4 r r2 |
R1 re8 mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la, si, dod |
re mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re do! re mi |
fa la sol fa mi do re mi |
fa la sol fa mi do re mi |
fa fa la la sib sib sib sib |
sib sib la la sib sib do' do' |
fa fa re re sib, sib, do do |
fa,4 r r2 |
\ru#24 mi8
mi fad sold la si sold fad mi |
\ru#24 la |
sol! sol sol sol sol sol sol sol |
\ru#16 fa |
\ru#8 mi |
sol4 mi la la, |
\ru#16 re8 |
re4 r r2 |
R1 |
\ru#16 la8 |
la,4 r r2 |
R1 |
re8 mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la, si, dod |
re mi fa mi re mi fa re |
sol la sib la sol la sib sol |
la si dod' si la si dod' la |
re' la fa la re la si dod' |
re' fa' mi' re' dod' la si dod' |
re' fa' mi' re' dod' la si dod' |
re' re' fa fa sol sol sol sol |
\ru#8 la |
re' re' fa fa sol sol sol sol |
\ru#8 la |
\ru#4 { re re re re re4 r | }
sib4 r r2\fermata | \allowPageTurn
R2.*2 |
la4(\p fa re) |
dod-.\f re-. r |
sib4\p do' sib |
la sib la |
sol fa sib, |
do-. do'( sib) |
lab2 mi4 |
fa2.~ |
fa4( sol fa)~ |
fa sol( fa) |
mi2 fa4 |
do4( dod re) |
sib do' do |
fa( sol la) |
sib-.\cresc sib,( lab,) |
sol,\f r r |
sol\p r r |
do r r |
la, r r |
re r r |
sib2. |
la4 la8\f sol fa mi |
re4 r r |
la(\p fa re) |
dod( re fa) |
sol4-.\f fa-. r |
sib4\p do' sib |
la sib la |
sol la sol |
fad2. |
sol4 mib re |
dod2. |
re4 mi! fad |
sol la sib |
dod mi fa! |
sol la la, |
sib,8 sol la4 la, |
re r |
R2*3 |
la4\f r8 dod' |
la4 r8 dod' |
la4 re' |
do'! si |
do' sib! |
la sol |
la sol |
fa mi |
fa re' |
do'! sib! |
la r |
R2*8 |
sol4.\f lab8 |
sol4. si8 |
do' do' mi mi |
fa fa la! la |
sib!4 r |
r8 sib la sol |
do'4 la |
sib do'8 do |
do'4 re' |
sib do' |
fa8 la sib do' |
fa4 r |
R2*3 |
re4\f r8 fad |
re4 r8 fad |
re4 la |
sol fad |
sol la |
sib la |
sib la |
sol sold |
la r |
R2*6 |
la4.\f sib8 |
la4. dod'8 |
re' re mi mi |
fa fa fad fad |
sol sol sold sold |
la la dod dod |
re re fad fad |
sol4 r |
r8 mi sol mi |
la4 sib |
la la, |
sib,8 sol fa sol |
la4 la, |
re r |
R2 |
re4 r8 fa\f |
re4 r8 fa |
re4 re |
re r |
