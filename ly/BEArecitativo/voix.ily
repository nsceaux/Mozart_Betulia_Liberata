\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 la16 la la8 la16 si dod'8 dod'16 re' mi'8 mi'16 fad' |
re'4 r16 re' re' re' re'8 la r la |
do'! do' r16 do' do' si sol8 sol r16 sol sol la |
si8 si r16 si si do' re'4 fa'!8 fa'16 mi' |
do'8 do'
\ffclef "soprano/treble" <>^\markup\character Amital
r8 do'' do'' sol' r16 sol' sol' la' |
sib'4 sib'8 do'' la' la' r4 |
do''4 re''8 mib'' re''8 re'' r re''16 sib' |
mi''!4 mi''8 fa'' fa'' do'' r4 |
\ffclef "soprano/treble" <>^\markup\character-text Coro in lontano
r4 r8 do'' do''4 la'8 fa'' |
fa''4 do''8
\ffclef "tenor/G_8" <>^\markup\character Ozia
do' do' fa
\ffclef "soprano/treble" <>^\markup\character Cabri
r8 fa' |
la' la' r do'' si'! si' r4 |
re''4 re''8 fa'' re'' re'' r re''16 re'' |
re''4 do''8 re'' si' si' si' si'16 do'' |
re''4 re''8 si'16 sol' do''8 do''
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 mi' |
do' do' do' do'16 si re'4
\ffclef "soprano/treble" <>^\markup\character Cabri
r8 sol' |
si'4
\ffclef "soprano/treble" <>^\markup\character Amital
re''8 si'16 re'' re''8 sold' r sold'16 la' |
si'4 si'8 do'' la' la' r4 |
\ffclef "tenor/G_8" <>^\markup\character Ozia
red'8 red'16 red' red'8 mi' si4 r |
\ffclef "alto/treble" <>^\markup\character Giuditta
r4 r8 si mi' mi' r sold' |
mi' mi'
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 mi' mi' si
\ffclef "soprano/treble" <>^\markup\character Amital
r16 si' re'' do'' |
la'8 la' r4
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 mi' mi' la' |
sol'! sol' r sol' sib' sib' sib' la' |
fa' fa' r4 fa' sol'8 la' |
la'8 mi'16 mi' sol'8 fa' re' re' r4 |
fa'8 fa'16 fa' fa'8 sol' mib' mib' r16 do' do' re' |
mib'8 mib' mib' mib'16 re' sib4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 fa |
sib8 sib16 sib sib8 do'16 re' re'8 la
\ffclef "alto/treble" <>^\markup\character Giuditta
r16 re' re' mi' |
fad'8 fad' r la'16 sib' sol'8 sol' r sol'16 sol' |
sol'8 sol' sol' sib' sol'4 sol'8 fa'! |
re' re' r4
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 fa'! fa' fa' |
re'4 do'8 re' sib sib
\ffclef "alto/treble" <>^\markup\character Giuditta
r8 fa'16 sol' |
lab'4 lab'8 re'16 mib' fa'4 fa'8 mib' |
do'4
\ffclef "soprano/treble" <>^\markup\character Amital
r16 sol' do'' sol' la'!8 la'
\ffclef "bass" <>^\markup\character Achior
r mi!16 fa |
sol4 la8 mi fa fa r4 |
\ffclef "alto/treble" <>^\markup\character Giuditta
sold'4 sold'8 la' la' mi' r4 |
r
\ffclef "tenor/G_8" <>^\markup\character Ozia
r8 fad' fad' si
\ffclef "bass" <>^\markup\character Achior
r8 si |
si fad
\ffclef "soprano/treble" <>^\markup\character Amital
r si' sol' sol'
\ffclef "alto/treble" <>^\markup\character Giuditta
r si' |
si' fad' r4

