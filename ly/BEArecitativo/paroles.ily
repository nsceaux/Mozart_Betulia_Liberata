Lun -- ga -- men -- te non du -- ra ec -- ces -- si -- vo do -- lor. Cia -- scu -- no a’ ma -- li
o ce -- de o s’ac -- co -- stu -- ma. Il no -- stro sta -- to
non è pe -- rò sen -- za spe -- ran -- za.

In -- ten -- do:
tu in Giu -- dit -- ta con -- fi -- di. Oh que -- sta par -- mi,
trop -- po fol -- le lu -- sin -- ga.

All’ ar -- mi, all’ ar -- mi!

Quai gri -- da!

Ac -- cor -- ri, O -- zì -- a. Sen -- ti il tu -- mul -- to,
che fra’ no -- stri guer -- rie -- ri
là si de -- stò pres -- so al -- le por -- te?

E qua -- le
n’è la ca -- gion?

Chi sà?

Mi -- se -- ri no -- i!
sa -- ran giun -- ti i ne -- mi -- ci.

Cor -- ra -- si ad os -- ser -- var.

Fer -- ma -- te, a -- mi -- ci.

Giu -- dit -- ta!

E -- ter -- no Di -- o!

Lo -- diam, com -- pa -- gni,
lo -- dia -- mo il Si -- gnor no -- stro. Ec -- co a -- dem -- pi -- te
le sue pro -- mes -- se: ei per mia man tri -- on -- fa;
la no -- stra fe -- de e -- gli pre -- mi -- ò!

Ma que -- sto im -- prov -- vi -- so tu -- mul -- to…

Io lo de -- sta -- i,
non vi tur -- bi. A mo -- men -- ti
ne u -- di -- re -- te gli ef -- fet -- ti.

E se frat -- tan -- to O -- lo -- fer -- ne…

O -- lo -- fer -- ne
già sve -- na -- to mo -- rì.

Che di -- ci ma -- i!

Chi ha sve -- na -- to O -- lo -- fer -- ne?

Io lo sve -- na -- i.

Tu stes -- sa!

E quan -- do?

E co -- me?

U -- dite.
