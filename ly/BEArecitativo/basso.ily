\clef "bass" dod1 |
fad~ |
fad2 si,~ |
si,1 |
do~ |
do2 fa~ |
fa sib,~ |
sib, r4 do |
fa1~ |
fa~ |
fa2 sol~ |
sol1~ |
sol~ |
sol2 mi |
la sol~ |
sol mi~ |
mi la~ |
la r4 si |
sold1~ |
sold |
la |
mi |
fa |
dod2 re~ |
re la,~ |
la, sib,~ |
sib, fad~ |
fad sib,~ |
sib,1 |
r4 la, sib,2~ |
sib,1 |
si,! |
do2 dod~ |
dod re~ |
re r4 mi |
red1~ |
red2 mi |
r4 fad
