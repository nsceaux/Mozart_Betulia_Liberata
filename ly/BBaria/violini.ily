\clef "treble"
<<
  \tag #'(violino1 violini) {
    la'8 |
    mi''4~ mi''16 fad''( re'' si') |
    dod''4~ dod''16 mi''( dod'' la') |
    la'( sold') sold'8 r4 |
    re''4 dod''16( si') fad''( mi'') |
    mi''4~ mi''16( dod'' si' re'') |
    dod''[-. mi'']-.
  }
  \tag #'(violino2 violini) {
    r8 |
    r16 dod'( mi' la') dod''( re'' si' sold') |
    r la( dod' mi') la'( dod'' mi' dod') |
    r re'-. re'-. re'-. re'( dod') dod'( si) |
    si4 la16( sold) re'( dod') |
    dod'4~ dod'16( la sold si) |
    la[-. dod']-.
  }
>> r16 la''-. la''( sold'') r sol'' |
fad''32[ mid'' fad'' sold''!] la''[ sold'' fad'' mid''] fad''[ mi'' re'' dod''] si'[ la' sold' la'] |
sold'[( si') mi''-. sold''-.] si''( sold'') si''( sold'') mi''8 r |
<<
  \tag #'(violino1 violini) {
    mi''8.(\p dod''16 mi''8) r |
    re''8.( si'16 re''8) r |
    dod''16\f ( mi'') la'( dod'') dod''( si') la'( sold') |
    la'8.( si'32 dod'' si'8) r |
    mi''8.(\p dod''16 mi''8) r |
    re''8.( si'16 re''8) r |
    dod''32(\f re'' mi'' dod'') la'4( lad'8) |
    si'16( re'' fad''4) fad''8 |
    mi''16( si'') <re''' mi''>4 q8 |
  }
  \tag #'(violino2 violini) {
    r16 dod'(\p re' mi') r mi'( re' dod') |
    r si( dod' re') r si'( la' sold') |
    la'8(\f mi') mi'16( re') dod'( si) |
    dod' mi' dod' la sold re' si sold |
    la16(\p dod' mi' la') r mi'( re' dod') |
    r si( dod' re') r re'( dod' si) |
    dod'\f mi' mi' mi' mi'4:16 |
    fad'8 si16 dod' re'8 red' |
    mi'8 si''4 si''8 |
  }
>>
dod'''32(\p la''16.) sold''32( fad''16.) mi''32( re''16.) dod''32( si'16.) |
mid''16([\f fad'']) <<
  \tag #'(violino1 violini) {
    mi''32( re'' dod'' si') la'8 sold' |
    la'16-. mi'-. mi'( re') dod'8 la'\p |
    mi'' mi''~ mi''16 fad''( re'' si') |
    dod''8 dod''~ dod''16 mi''( dod'' la') |
    la'( sold') sold'8-. r4 |
    re''4 dod''16( si') fad''( mi'') |
    mi''8 mi''~ mi''16 dod''( si' re'') |
    dod''-. mi''-. mi''( dod'') la'4~ |
    la'16 sold''( la'' mid'') fad''([ dod'']) \grace mi''32 re''16( dod''32 si') |
    la'16( dod'') dod''4 \grace mi''32 re''16( dod''32 si') |
    la'16-. dod'( re' mi') fad'( mi' fad' sold') |
    la'8 mi'' mi'' mi'' |
    mi'' mi'' mi'' mi'' |
    mi'' red'' red'' fad'' |
    si'8. dod''16 re''!4 |
    dod''8. red''16 mi''8 sold' |
    fad' <si la'>4->\f sold'16\trill fad'32 sold' |
    fad'8 si'\p si' si' |
    dod'' dod'' dod'' dod'' |
    dod''2:16 |
    si'8 si' si' si' |
    si'2:16 |
    red''8 fad'' la'' fad'' |
    red'' fad'' la'' fad'' |
    red'' mi'' dod'' la' |
    sold'16 sold'' sold'' sold'' fad''4:16 |
    mi''8.(\f sold''16) si''8 r |
    la''8.( fad''16 la''8) r |
    sold''16(\p si'') mi''( sold'') \grace sold''16 fad''8( mi''16 red'') |
    mi''8.( fad''32 sold'') fad''8 r |
    si''8.(\f sold''16 si''8)-. r |
    la''8.( fad''16 la''8)-. r8 |
    sold''16-.\p si''-. mi''-. sold''-. sold''( fad'') mi''( red'') |
    mi''16( fad'') re''!4-> si'8 |
    mi' dod'' dod'' dod'' |
    red''-. mi''32(\f sold''16.) si''32( sold''16.) sold''32( mi''16.) |
    mi''32( dod''16.) mi''32(\p dod''16.) dod''32( la'16.) la''32( fad''16.) |
    mi''4:16 red'':16 mi''8 si' \tuplet 3/2 { dod''16[ red'' mi''] } \tuplet 3/2 { fad''[ sold'' la''] } |
    sold''2:16 |
    fad'':16 |
  }
  \tag #'(violino2 violini) {
    fad'16( re') dod'8 si |
    la16-. dod'-. dod'( si) la8 r |
    r16 dod'(\p mi' la') dod''( re'' si' sold') |
    r la( dod' mi') la'( dod'') mi'( dod') |
    r re'-. re'-. re'-. re'( dod') dod'( si) |
    si4 la16( sold) re'( dod') |
    dod'8 dod'~ dod'16 la( sold si) |
    la[-. dod']-. dod''[( la')] r mi'( fad' sol') |
    fad'4 r |
    dod'16( mi' la' mi') la'-.[ la'-.] \grace dod''32 si'16( la'32 sold'!) |
    la'16-. la( si dod') re'( dod' re' si) |
    dod'8 dod'' dod'' dod'' |
    si' si' si' si' |
    la' la' la' la'~ |
    la' sold' la' si' |
    mi'8. fad'16 sold'8 mi' |
    red'8 <si fad'>4->\f mi'16\trill red'32 mi' |
    red'8-. red''(\p mi'') mi'-. |
    mi'8 mi' mi' mi' |
    mi'2:16 |
    mi'8 mi' mi' mi' |
    sold'2:16 |
    la'8 fad' red' fad' |
    la' fad' red' fad' |
    la' sold' mi' fad' |
    mi'16 mi'' mi'' mi'' mi'' mi'' red'' red'' |
    mi''-. si'(\f la' sold') r sold'( la' si') |
    r fad'( sold' la') r la'( sold' fad') |
    mi'8\p si' dod''16( la' sold' fad') |
    sold'8.( fad'32 mi') red'16 la'( sold' fad') |
    sold'-.\f sold'( la' si') r si'( la' sold') |
    r fad'( sold' la') r la'( sold' fad') |
    sold'8\p si' dod''16( la') sold'( fad') |
    sold'8 <mi' si'>4-> re''!8 |
    dod'' la' mi' la' |
    la'-. sold'-. r4 |
    dod'4\f mi'8\p dod''32( la'16.) |
    sold'4:16 fad':16 |
    mi'2:16 |
    mi'':16 |
    mi''4:16 red'':16 |
  }
>>
mi''4\f~ mi''16 sold''( si'' sold'') |
dod'''( la'') sold''( fad'') si''( sold'') fad''( mi'') |
dod'''( la'') sold''( fad'') si''( sold'') fad''( mi'') |
<<
  \tag #'(violino1 violini) {
    si'4 fad''8.\trill mi''32( fad'') |
    mi''16-. si'-. si'( la') sold'8-. r |
    r8 si(\p dod' re'!) |
    r dod'( re' mi') |
    si r dod' r |
    r16 si''-. sold''-. mi''-. red''-. dod''-. si'-. la'-. |
    sold'8 si( dod' re'!) |
    r dod'( re' mi') |
    si r dod' r |
    si sold'4 sold'8 |
    sold' red'' red'' red'' |
    mi'' mi'' re''! re'' |
    re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' |
    si' re'' fad'' la' |
    sold'16-. sold'( la' fad') mi'8 mi' |
    mi'' mi''~ mi''16 fad''( re'' si') |
    dod''8 dod''~ dod''16( mi'' dod'' la') |
    la'8 sold' r4 |
    re''4 dod''16( si') fad''( mi'') |
    mi''8 mi''~ mi''16 dod''( si' re'') |
    dod''8 mi''16( dod'') la'4~ |
    la'16 sold''( la'' mid'') fad''([ dod'']) \grace mi''32 re''16( dod''32 si') |
    la'8 dod''4 \grace mi''32 re''16( dod''32 si') |
    la'16-. dod'( re' mi') fad'( mi' fad' sold') |
    la'8 la' la' la' |
    la' re'' fad'' la'' |
    sol'' sol'' sol'' sol'' |
    fad'' fad'' fad'' fad'' |
    fad'' fad'' fad'' fad'' |
    mi'' si( dod' re') |
    r mi'( re' dod') |
    re' re'' re'' re'' |
    <re'' mi'> q q q |
    dod'' dod'' dod'' dod'' |
    <dod'' mi'> q q q |
    re'' re'' re'' re'' |
    <re'' mi'> q q q |
    dod''16( mi'') sol''( mi'') fad''( dod'') re''( si') |
    la'4:16 sold'!:16 |
    la'8.(\f dod''16 mi''8-.) r |
    re''8.( si'16 re''8-.) r |
    dod''16(\p mi'') la'( dod'') dod''( si') la'( sold') |
    la'8.( si'32 dod'') si'8 r |
    mi''8.(\f dod''16 mi''8-.) r |
    re''8.( si'16 re''8-.) r |
    dod''16(\p mi'') la'( dod'') dod''( si' la' sold') |
    la'16 sol'' <la' sol''>4. |
    \grace sol''16 fad''8 mi''16 re'' re''8 re'' |
    sold''!8 la''32(\f fad''16.) fad''32( re''16.) re''32( si'16.) |
    si'32( fad'16.) fad''32(\p re''16.) mi''32( dod''16.) re''32( si'16.) |
    la'4:16 sold':16 |
    mi''8 mi'' fad''4:16 |
    dod''2:16 |
    si':16 |
  }
  \tag #'(violino2 violini) {
    si'8 mi''4 red''8 |
    mi''16-. sold'-. sold'( fad') mi'8-. r |
    r8 sold(\p la si) |
    r la( si dod') |
    sold r la r |
    r16 sold''-. si'-. sold'-. fad'-. la'-. sold'-. fad'-. |
    mi'8 sold( la si) |
    r la( si dod') |
    sold r la r |
    sold sold'( fad' mi') |
    red' sold sold' sold' |
    sold' sold' la' la' |
    si' si' si' si' |
    mi' mi' la' la' |
    <fad' la'> q q fad' |
    mi'16-. si( dod' la) sold8 r |
    r16 dod'( mi' la') dod''( re'' si' sold') |
    r la( dod' mi') la'( dod'') mi'( dod') |
    r re'-. re'-. re'-. re'( dod') dod'( si) |
    si4 la16( sold) re'( dod') |
    dod'8 dod'~ dod'16 la( sold si) |
    la8 dod' r16 mi'( fad' sol') |
    fad'4 r |
    dod'16[ mi'8 la' la'16] \grace dod''32 si'16( la'32 sold') |
    la'16-. la( si dod') re'( dod' re' si) |
    dod'8 dod' dod' dod'' |
    re'' la' la' re' |
    re' re' dod' dod' |
    re' la' la' lad' |
    si' si' si' la'! |
    sold' sold( la si) |
    r dod'( si la) |
    si si' <si' sold'> q |
    q q q q |
    la' la' la' la' |
    <la' dod'> q q q |
    si' si' si' si' |
    <si' sold'> q q q |
    la'8 la'4 fad'16( re') |
    dod'4:16 si:16 |
    dod'16 dod''(\f si' la') r la'( si' dod'') |
    r sold'( la' si') r si'( la' sold') |
    la'4\p~ la'16 fad'( mi' re') |
    dod'8.( si32 la) sold16 re'(\f dod' si) |
    r16 dod'( mi' la') r dod''( si' la') |
    r si'( sold' re') r sold'( la' si') |
    la'4\p~ la'16 fad'( mi' re') |
    dod'8 <la' mi''>4. |
    \grace si'16 la'8 sol'16 fad' fad'8 fad' |
    re'' dod'' r4 |
    r8 re'32( si16.) dod'32( la16.) fad'32( re'16.) |
    dod'4:16 si:16 |
    la16 la' la' la' la'4:16 |
    la'2:16 |
    la'4:16 sold':16 |
  }
>>
la'16[\f la''8 mi''16( dod'' la')] r mi''32( sol'') |
fad''16[ la''8 fad''16( re'' la')] r dod''32( mi'') |
<<
  \tag #'(violino1 violini) {
    re''8 re''32( mi'' fad'' re'') dod''([ re'' mi'' dod'']) si'([ dod'' re'' si']) |
    <mi' dod'' la''>4
  }
  \tag #'(violino2 violini) {
    re''16 la''8 la' la'' la'16 |
    <mi' dod''>4
  }
>> r4\fermata |
<la mi' la'>4~ la'16 dod''( mi'' dod'') |
fad''-. re''( dod'' si') mi''-. dod''( si' la') |
re'''-. si''( la'' sold'') la''-. mi''( fad'') la''-. |
<<
  \tag #'(violino1 violini) {
    mi'4 si''8.\trill la''32( si'') |
    la''16-. mi'-. mi'( re') dod'8-. r |
  }
  \tag #'(violino2 violini) {
    mi'8 la''4 sold''8 |
    la''16-. dod'-. dod'( si) la8-. r |
  }
>>
%%
<<
  \tag #'(violino1 violini) {
    r8 la'4\p la' la'8 |
    r la'4 la' la'8 |
    la'4 mi'( fad') |
    sol'8 sol' sol' sol' sol' sol' |
    mi'' mi'' mi'' mi'' mi'' mi'' |
    re''-. dod''-. re''-. mi''-. fad''-. sol''-. |
    la''2.~ |
    la''4 sol''8 fad'' mi'' re'' |
    mi''8 mi'' re'' re'' dod'' dod'' |
    re''\f fad'4 fad' fad'8 |
    fad'16 fad''\p fad'' fad'' fad''2:16 |
    r16 fad'' fad'' fad'' fad''(\f re'') si''( lad'') si''( fad'' re'' si') |
    r sold''!\p sold'' sold'' sold''2:16 |
    la''!:16 fad''4:16 |
    fad''8 re'' dod'' si' la' sold'! |
    fad'16 fad'' fad'' fad'' fad'' la'' la'' la'' la'' dod''' dod''' dod''' |
    re'''8 re''' dod''' si'' la'' sold''! |
    fad''4 r r |
    dod''8 dod''~ dod''16( la') re''( si') |
    mi''8 mi''4 fad''16( sol'') |
    sol''( fad'') mi''( re'') dod''( si') fad'( la') |
    sold'!-. sold'( la' fad') mi'8 r |
  }
  \tag #'(violino2 violini) {
    r8 fad'4\p fad' fad'8 |
    r8 fad'4 fad' fad'8 |
    sol'4 dod'( re') |
    <mi' dod'>8 q q q q q |
    dod'' dod'' dod'' dod'' dod'' dod'' |
    re''-. mi'-. fad'-. dod'-. re'-. mi'-. |
    fad'4( re' la) |
    dod'2( si4) |
    si'8 si' la' la' sol' sol' |
    fad'\f fad'( mi' fad' mi' re') |
    dod'16 dod''\p dod'' dod'' dod''2:16 |
    r16 re'' re'' re'' re''(\f fad'') si''( lad'') si''( fad'' re'' si') |
    r16 dod''\p dod'' dod'' dod''2:16 |
    dod''2.:16 |
    re''8 si' la' sold' fad' mid' |
    fad'16 fad'' fad'' fad'' fad''4:16 fad'':16 |
    fad''8 si'' la'' sold'' fad'' mid'' |
    fad''4 r r |
    la'16( mi') dod'( mi') la'( mi') si'( sold') |
    dod''( la') dod''( la') dod''( la') re''( mi'') |
    mi''( re'') dod''( si') lad'( si') si-. si-. |
    si-. si( dod' la) sold8 r |
  }
>>
