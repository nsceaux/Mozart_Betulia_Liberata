\clef "bass" r8 |
la la la la |
la la la la |
si si si si |
mi mi mi mi |
mi mi mi mi |
la la la la |
re re re red |
mi mi mi r |
la\p r la, r |
si, r mi r |
la\f dod re mi |
la la, mi r |
la,\p r la r |
si r mi r |
la\f la dod dod |
re re'16 dod' si8 la |
sold mi16-. fad-. sold-. si-. sold-. mi-. |
la8-.\p si-. dod'-. re'-. |
r re'\f mi' mi |
la4 la,8 r |
la8\p la la la |
la la la la |
si si si si |
mi mi mi mi |
mi mi mi mi |
la la dod dod |
re4 r |
mi8 mi mi mi |
la, la la, la |
la la la la |
sold sold sold sold |
fad fad si, red |
mi mi fad sold |
la la sold lad |
si\f si si si |
si, la!(\p sold) sold-. |
la la la la |
la la la la |
sold sold sold sold |
mi mi mi mi |
si,4 r |
si, r |
si8 dod' la fad |
si si si, si, |
mi\f r mi' r |
si r si, r |
mi\p sold la si |
mi mi si, r |
mi\f r mi' r |
si r si, r |
mi\p sold la si |
mi-. mi( fad sold) |
la4 r8 fad |
si dod' r4 |
la\f dod\p |
si,8 si, si, si, |
sold4 la |
si8 si si si |
si, si, si, si, |
mi8\f mi16 fad sold8 mi |
la r sold r |
la r sold r |
r sold la si |
mi mi mi r |
mi4\p r |
mi r |
mi8 r mi r |
mi r r4 |
mi4 r |
mi r |
mi8 r mi r |
mi mi red dod |
sid, sid, sid, sid, |
dod dod fad fad |
sold sold sold sold |
la la fad fad |
re re re red |
mi mi mi16 re! dod si, |
la,8 la, la, la, |
la, la, la, la, |
si, si, si, si, |
mi mi mi mi |
mi mi mi mi |
la, la, dod dod |
re4 r |
mi8 mi mi mi |
la, la la, la |
sol! sol sol sol |
fad fad fad fad |
mi mi la, la, |
re re re re |
re re re red |
mi4 r |
mi r |
\ru#24 mi8 |
la8 dod re4 |
mi8 mi mi mi |
la,\f r8 la r |
mi r mi' r |
la8\p fad re mi |
la la, mi r |
la,\f r la r |
mi r mi' r |
la8\p fad re mi |
la la( si dod') |
re'4 r8 re |
mi fad r4 |
r r8 re |
mi mi re re |
dod dod re re |
mi mi mi mi |
mi mi mi mi |
la\f la dod dod |
re re re mi |
fad fad mi red |
mi2\fermata |
la8 la la la |
re r dod r |
mi mid fad16 dod re fad |
mi!8 mi mi mi |
la, la la, r |
re8\p re re re re re |
re re re re re re |
mi mi mi mi mi mi |
la, la, la, la, la, la, |
la, la, la, la, la, la, |
re4 r r |
R2. |
la2 si4 |
sol4 la la, |
re8 re'\f( dod' re' dod' si) |
lad\p lad lad lad lad lad |
si si si\f si si si |
mid mid mid mid mid mid |
fad fad fad fad la la |
si si dod' dod' dod dod |
re re re re la la |
si si dod' dod' dod dod |
fad4 r r |
la8 la la la |
la la la la |
re re re red |
mi mi mi16 sold si sold |
