\clef "tenor/G_8" r8 |
R2*19 |
r4 r8 la |
mi'4~ mi'16[ fad'] re'[ si] |
dod'4~ dod'16[ mi'] dod'[ la] |
la[ sold] sold8 r4 |
re'4 dod'16[ si] fad'[ mi'] |
mi'4~ mi'16[ dod'] si[ re'] |
dod'8 mi' la4~ |
la16[ sold' la' mid'] fad'[ dod'] \grace mi'32 re'16 dod'32[ si] |
la8 dod'4 \grace mi'32 re'16[ dod'32 si] |
la8 la r4 |
mi'8 re'16 dod' dod'8. mi'16 |
mi'16[ si] si8 mi'4~ |
mi'8 red'16 dod' \grace dod' si8. la16 |
la8 sold r re'! |
dod'8. red'16 mi'8. sold16 |
fad4 r |
r8 si si si |
dod'8~ dod'32[\melisma mi' re' mi'] dod'[ la' sold' la'] mi'8 |
la'4 dod' |
si8[~ si32 mi' red' mi'] si[ sold' fad' sold' mi'8] |
si mi'4 sold'16[ mi'] |
red'32[ mi' red' mi'] fad'[ sold' fad' sold'] la'[ sold' la' sold'] fad'[ mi' fad' mi'] |
red'[ mi' red' mi'] fad'[ sold' fad' sold'] la'[ sold' la' sold'] fad'[ mi' fad' mi'] |
red'8\melismaEnd mi' dod' la |
sold16[ si] mi'[ fad'] fad'4\trill |
mi'4 r |
r r8 si |
mi'8. sold'16 \grace sold' fad'8 mi'16[ red'] |
mi'8.[ fad'32 sold'] fad'8 r |
R2*2 |
si8 mi'16 sold' sold'[ fad'] mi'[ red'] |
mi'[ fad'] re'!4. |
\grace re'16 dod'8 si16 la la8 la |
red' mi' r4 |
r8 mi'32[ dod'16.] dod'32[ la16.] la'32[ fad'16.] |
mi'4 red' |
mi'8 si \tuplet 3/2 { dod'16[ red' mi'] } \tuplet 3/2 { fad'[ sold' la'] } |
si2 |
fad'\trill |
mi'4 r |
R2*3 |
r4 r8 mi |
mi'2~ |
mi'~ |
mi'16[ re'!] dod'[ si] re'[ dod'] si[ la] |
sold8 si r4 |
mi'2~ |
mi'~ |
mi'16[ re'] dod' si re'[ dod'] si la |
mi'8 mi r4 |
red'8 sold16 sold red'8 fad' |
fad'16[ mi'] mi'8 re'!4~ |
re'8 dod'16 si fad'8 mi'16[ re'] |
re'[ dod'] dod'4 mi'16[ dod'] |
si8 re' \tuplet 3/2 { fad'16[ mi' re'] } \tuplet 3/2 { dod'[ si la] } |
sold4 r8 mi |
mi'4~ mi'16[ fad'] re'[ si] |
dod'4~ dod'16[ mi'] dod'[ la] |
la8 sold r4 |
re'4 dod'16[ si] fad'[ mi'] |
mi'4~ mi'16[ dod'] si[ re'] |
dod'8 mi' la4~ |
la16[ sold'( la' mid')] fad'[ dod'] \grace mi'32 re'16 dod'32[ si] |
la8 dod'4 \grace mi'32 re'16[ dod'32 si] |
la8 la r4 |
dod'8 dod'16 dod' dod'8. mi'16 |
mi'[ re'] re'8 la'4~ |
la'16[ sol'] fad' mi' \grace re' dod'8. sol16 |
fad8 fad'4 \grace mi'16 re'8 |
\grace dod'8 si4. \tuplet 3/2 { dod'16[ si la] } |
mi'2~ |
mi'\melisma |
re'8[~ re'32 mi' re' dod'] re'[ si dod' re'] mi'[ fad' sold' la'] |
si'8 r re'[ \grace mi'32 re'16 dod'32 si] |
dod'8[~ dod'32 re' dod' si] dod'[ la si dod'] re'[ mi' fad' sold'] |
la'8 r mi'[ \grace fad'32 mi'16 re'32 dod'] |
re'8[~ re'32 mi' re' dod'] re'[ si dod' re'] mi'[ fad' sold' la'] |
si'8 r re'8[ \grace mi'32 re'16 dod'32 si] |
dod'16[ mi']\melismaEnd sol'[ mi'] fad'[ dod'] re'[ si] |
la4 sold! |
la r |
r r8 mi |
dod'16[ mi'] la[ dod'] dod'[ si] la[ sold] |
la8.[ si32 dod'] si8 r |
R2*2 |
dod'16[ mi'] la dod' dod'[ si] la[ sold] |
la[ sol'] sol'4. |
fad'8 mi'16 re' re'8 re' |
re' dod' r4 |
r8 fad'32[ re'16.] mi'32[ dod'16.] re'32[ si16.] |
la4 sold |
mi'8 mi' \tuplet 3/2 { fad'16[ mi' re'] } \tuplet 3/2 { dod'[ si la] } |
mi8\melisma mi'4 re'32[ dod' si la]\melismaEnd |
si2\trill |
la4 r |
R2 |
r8 la la la |
la4( \once\override Script.avoid-slur = #'outside si8.)\trill\fermata la16 |
la4 r |
R2*3 |
r4 r8 la |
re'2 la4 |
fad'2 \grace mi'8 re'4 |
re'8[ dod'] dod'4 r |
sol' mi' dod' |
la2 sol4 |
fad r r |
la'2.~ |
la'4 sol'8[ fad'] mi'[ re'] |
mi'8.[ fad'32 mi'] re'4 dod' |
re' r r8 fad' |
fad'4. dod'8 dod' mi' |
re'4 r r8 re' |
dod'4. re'8 dod' si |
la4 dod' fad'~ |
fad'8[ re'] dod' si la sold! |
la4 fad'2~ |
fad'8[ re'] dod' si la sold! |
fad4 r r |
R2*3 |
r4 r8 mi |
