\clef "treble" <> r8 |
R2*2 |
r16 <<
  \tag #'(oboe1 oboi) {
    re''16-. re''-. re''-. re''( dod'') dod''( si') |
  }
  \tag #'(oboe2 oboi) {
    si'-. si'-. si'-. si'( la') la'( sold') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) << si'4 si' >> r4 |
R2 |
r8 <<
  \tag #'(oboe1 oboi) { dod''8 re'' mi'' | }
  \tag #'(oboe2 oboi) { la' si' dod'' | }
>>
\tag #'oboi <>^"a 2." fad''32[ mid'' fad'' sold''!] la''[ sold'' fad'' mid''] fad''[ mi'' re'' dod''] si'[ la' sold' la'] |
sold'4 r16 <<
  \tag #'(oboe1 oboi) { si''16( la'' sold'') | la''8 }
  \tag #'(oboe2 oboi) { re''16( dod'' si') | dod''8 }
>> r8 r4 |
R2 |
<>\f <<
  \tag #'(oboe1 oboi) { dod''8.( mi''16) mi''( re'' dod'' si') | }
  \tag #'(oboe2 oboi) { la'8.( dod''16) dod''( si' la' sold') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'8[ \tag#'oboi \once\slurDown dod''( si')] }
  { la'[ \tag#'oboi \once\slurUp la''( sold'')] }
>> r8 |
<>\p <<
  \tag #'(oboe1 oboi) { mi''2 | sold'' | la''8 }
  \tag #'(oboe2 oboi) { dod''2 | si' | dod''8 }
>> \twoVoices #'(oboe1 oboe2 oboi) <<
  { la'4 lad'8 |
    si'16( re'' fad''4) fad''8 |
    si'8 si''4 re'''8 | }
  { la'4 sol'8 |
    fad' si'16 dod'' re''8 red'' |
    mi''4. si''8 | }
>>
<<
  \tag #'(oboe1 oboi) { dod'''8 }
  \tag #'(oboe2 oboi) { la'' }
>> r8 r4 |
r8 <<
  \tag #'(oboe1 oboi) { fad''16( re'') dod''8( si') | }
  \tag #'(oboe2 oboi) { re''16( si') la'8( sold') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la'16-. mi''-. mi''( re'') dod''8-. }
  { la'16-. dod''-. dod''( si') la'8-. }
>> r8 |
R2*2 |
r16 <>16\p <<
  \tag #'(oboe1 oboi) { re''16-. re''-. re''-. re''( dod'') dod''( si') | si'4 }
  \tag #'(oboe2 oboi) { si'16-. si'-. si'-. si'( la') la'( sold') | sold'4 }
>> r4 |
R2*4 |
r16 <<
  \tag #'(oboe1 oboi) {
    dod''16( re'' mi'') fad''( mi'' fad'' sold'') |
    la''4
  }
  \tag #'(oboe2 oboi) {
    la'16( si' dod'') re''( dod'' re'' si') |
    dod''4
  }
>> r4 |
R2*4 |
r8 <>\fp \once\outsideSlur <<
  \tag #'(oboe1 oboi) { la''4->( sold''8) | fad'' }
  \tag #'(oboe2 oboi) { fad''4->( mi''8) | red'' }
>> r8 r4 |
R2 |
<>\p <<
  \tag #'(oboe1 oboi) { mi''2~ | mi''4 }
  \tag #'(oboe2 oboi) { dod''2 | si'4 }
>> r4 |
<<
  \tag #'(oboe1 oboi) { sold''2 | }
  \tag #'(oboe2 oboi) { si' | }
>>
R2*8 |
<>\f <<
  \tag #'(oboe1 oboi) { sold''2 | }
  \tag #'(oboe2 oboi) { si' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { red''2 | mi''8 }
  { la''2 | sold''8 }
>> r8 r4 |
R2*4 | \allowPageTurn
<>\p <<
  \tag #'(oboe1 oboi) { sold''4( fad'') | }
  \tag #'(oboe2 oboi) { mi''( red'') | }
>>
\twoVoices #'(oboe1 oboe2 oboi) << mi''8 mi'' >> r r4 |
r4 \twoVoices #'(oboe1 oboe2 oboi) <<
  { sold''4 |
    fad''2 |
    mi''~ |
    mi''~ |
    mi''~ |
    mi''8 sold'' fad''4 |
    mi''16-. si''-. si''( la'') sold''8-. }
  { mi''4~ |
    mi'' red'' |
    mi'' si' |
    dod''8 r si' r |
    dod'' r si' r |
    r mi''4 red''8 |
    mi''16-. sold''-. sold''( fad'') mi''8-. }
  { s4 | s2 | s\f | }
>> r8 |
<<
  \tag #'(oboe1 oboi) {
    s8 si'8( dod'' re''!) |
    s dod''( re'' mi'') |
    si' s dod'' s |
    si' s s4 |
    s8 si'( dod'' re'') |
    s dod''( re'' mi'') |
    si' s dod'' s |
  }
  \tag #'(oboe2 oboi) {
    s8 sold'( la' si') |
    s la'( si' dod'') |
    sold' s la' s |
    sold' s s4 |
    s8 sold'( la' si') |
    s la'( si' dod'') |
    sold' s la' s |
  }
  { r8 s4.\p | r8 s4. | s8 r s r | s r r4 |
    r8 s4. | r8 s4. | s8 r s r | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { si'8 sold'' fad'' mi'' | red'' }
  { si' sold''4 sold''8 | sold'' }
>> r8 r4 |
R2*4 |
r16 <>\p <<
  \tag #'(oboe1 oboi) { sold''16( la'' fad'') mi''8-. }
  \tag #'(oboe2 oboi) { si'16( dod'' la') sold'8-. }
>> r8 |
R2*2 |
r16 <<
  \tag #'(oboe1 oboi) {
    re''16-. re''-. re''-. re''( dod'') dod''( si') |
    si'4
  }
  \tag #'(oboe2 oboi) {
    si'16-. si'-. si'-. si'( la') la'( sold') |
    sold'4
  }
>> r4 |
R2*4 |
r16 <<
  \tag #'(oboe1 oboi) {
    dod''16( re'' mi'') fad''( mi'' fad'' sold'') |
    la''4
  }
  \tag #'(oboe2 oboi) {
    la'16( si' dod'') re''( dod'' re'' si') |
    dod''4
  }
>> r4 |
R2*4 |
<<
  \tag #'(oboe1 oboi) {
    s8 re''( dod'' si') |
    s dod''( re'' mi'') |
    si'
  }
  \tag #'(oboe2 oboi) {
    s8 si'( la' sold') |
    s la'( si' dod'') |
    sold'
  }
  { r8 s4.\p | r8 s4. | }
>> r8 r4 |
R2*7 |
<>\f <<
  \tag #'(oboe1 oboi) { la''2 | sold'' | }
  \tag #'(oboe2 oboi) { dod'' | si' | }
>>
R2*2 |
<>\f <<
  \tag #'(oboe1 oboi) { mi''2 | }
  \tag #'(oboe2 oboi) { dod'' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { sold'2 | la'8 }
  { re''2 | dod''8 }
>> r8 r4 |
R2*4 |
<>\p <<
  \tag #'(oboe1 oboi) { la''4( sold'') | la''8 }
  \tag #'(oboe2 oboi) { dod''4( si') | la'8 }
>> r8 r4 |
r4\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4~ | la'' sold'' | la''2~ | la''~ | la'' | }
  { dod''4 | si'2 | la'4 sol'' |
    fad''4. dod''8 | re''4 dod''8 si' | }
>>
<<
  \tag #'(oboe1 oboi) { la''4 }
  \tag #'(oboe2 oboi) { dod'' }
>> r4\fermata |
<<
  \tag #'(oboe1 oboi) { dod''4. mi''8 | }
  \tag #'(oboe2 oboi) { la'4. dod''8 | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''8 r mi'' r | }
  { la'2 | }
>>
<<
  \tag #'(oboe1 oboi) { sold''4 la'' | }
  \tag #'(oboe2 oboi) { si'4 la' | }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { la''4. sold''8 | }
  { dod''4 si' | }
>>
<<
  \tag #'(oboe1 oboi) {
    la''16-. mi''-. mi''( re'') dod''8-.
  }
  \tag #'(oboe2 oboi) {
    la'16-. dod''-. dod''( si') la'8-.
  }
>> r8 |
R2.*2 |
r4 <>\p <<
  \tag #'(oboe1 oboi) { mi''4( fad'') | sol'' }
  \tag #'(oboe2 oboi) { dod''( re'') | mi'' }
>> r4 r |
R2.*2 |
<>\p <<
  \tag #'(oboe1 oboi) { la'4( re'' fad'') | }
  \tag #'(oboe2 oboi) { fad'4( la' re'') | }
>>
R2.*2 |
\twoVoices #'(oboe1 oboe2 oboi) <<
  { r4 fad''2~ | fad''4 }
  { r8 fad''( mi'' fad'' mi'' re'') | dod''4 }
  { s8 s\f }
>> r4 r |
r <<
  \tag #'(oboe1 oboi) { re''2 }
  \tag #'(oboe2 oboi) { fad' }
>>
R2.*6 |
<<
  \tag #'(oboe1 oboi) {
    dod''4.( re''8) |
    dod''8 mi''4 fad''16( sol'') |
  }
  \tag #'(oboe2 oboi) {
    la'4.( si'8) |
    la'8 dod''4 re''16( mi'') |
  }
>>
\twoVoices #'(oboe1 oboe2 oboi) <<
  { fad''2 | }
  { la'4 si' | }
>>
<<
  \tag #'(oboe1 oboi) {
    sold''!16-. sold''( la'' fad'') mi''8-.
  }
  \tag #'(oboe2 oboi) {
    si'16-. si'( dod'' la') sold'8-.
  }
>> r8 |
