\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \oboiInstr } <<
        \new Staff << \global \keepWithTag #'oboe1 \includeNotes "oboi" >>
        \new Staff << \global \keepWithTag #'oboe2 \includeNotes "oboi" >>
      >>
      \new Staff \with {
        instrumentName = "Corni in A."
        shortInstrumentName = "Cor."
      } <<
        \keepWithTag #'() \global
        \keepWithTag #'corni \includeNotes "corni"
      >>
    >>
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Ozia
      shortInstrumentName = \markup\character Oz.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s2*6\break s2*6\pageBreak
        s2*8\break s2*8\break s2*8\pageBreak
        s2*7\break s2*8\break s2*9\pageBreak
        s2*9\break s2*8\break s2*8\pageBreak
        s2*9\break s2*7\break s2*8\pageBreak
        s2*8\break s2*8\break s2 s2.*8\pageBreak
        s2.*7\break
      }
    >>
  >>
  \layout { }
  \midi { }
}
