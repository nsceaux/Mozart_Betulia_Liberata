\clef "alto" r8 |
la la la la |
la la la la |
\ru#4 si16-. si( la) la( sold) |
mi'8 mi' mi' mi' |
mi' mi' mi' mi' |
la' la' la' la' |
re' re' re' red' |
mi' mi' mi' r |
r16 la(\p si dod') r dod'( si la) |
r sold( la si) r re'( dod' si) |
la4\f fad8 mi |
mi4 mi'8 r |
r16 la(\p dod' mi') r dod'( si la) |
r sold( la si) r si( la sold) |
la\f dod' dod' dod' dod'4:16 |
re'8 re'16 dod' si8 si |
si sold'16-. la'-. si'-. sold'-. mi'-. sold'-. |
la'2\p |
r8 re'16(\f fad') mi'8 mi |
mi4 la8 r |
la8\p la la la |
la la la la |
si4:16\dotFour si16( la) la( sold) |
mi'8 mi' mi' mi' |
mi' mi' mi' mi' |
la' la' dod' dod' |
re'4 r |
mi'8 mi' mi' mi' |
la la' la la' |
la' la' la' la' |
sold' sold' sold' sold' |
fad' fad' si red' |
mi' mi' fad' sold' |
la' la' sold' lad' |
si'\f si' si' si' |
si la'!(\p sold') sold'-. |
la'8 la' la' la' |
la' la' la' la' |
sold' sold' sold' sold' |
mi' mi' mi' mi' |
si4 r |
si r |
si'8 dod'' la' fad' |
si' si' si si |
mi'16-. sold'(\f fad' mi') r mi'( fad' sold') |
r red'( mi' fad') r fad'( mi' red') |
mi'8\p sold' la' si' |
mi' mi si16( fad' mi' red') |
mi'16-.\f mi'( fad' sold') r sold'( fad' mi') |
r red'( mi' fad') r fad'( mi' red') |
mi'8\p sold la si |
si sold'( fad' mi') mi'4 r8 la |
fad'-. mi'-. r4 |
la'4\f dod'\p |
si8 si si si |
sold'4 la' |
si'8 si' si' si' |
si si si si |
mi'8\f sold'16 la' si'8 sold' |
mi'2:16 |
mi':16 |
mi'8 si' dod'' si' |
si' mi' mi r | \allowPageTurn
mi'4\p r |
mi' r |
mi'8 r mi' r |
mi' r r4 |
mi'4 r |
mi' r |
mi'8 r mi' r |
mi'8 mi' red' dod' |
sid sid sid sid |
dod' dod' fad' fad' |
sold' sold' sold' sold' |
la' la' fad' fad' |
re' re' re' red' |
mi'8 mi' mi'16 re'! dod' si |
la8 la la la |
la la la la |
si4:16\dotFour si16( la) la( sold) |
mi'8 mi' mi' mi' |
mi' mi' mi' mi' |
la la dod' dod' |
re'4 r |
mi'8 mi' mi' mi' |
la la' la la' |
sol' sol' sol' sol' |
fad' fad' fad' fad' |
mi' mi' la la |
re' re' re' re' |
re' re' re' red' |
mi'4 r |
mi' r |
\ru#24 mi'8 |
la'8 dod' re'4 |
mi'8 mi' mi' mi' |
la16 mi'(\f re' dod') r dod'( re' mi') |
r si( dod' re') r re'( dod' si) |
la8\p fad'4 si8 |
la mi' mi16 si(\f la sold) |
r la( dod' mi') r mi'( re' dod') |
r re'( si sold) r si( dod' re') |
dod'8\p fad'~ fad'16 re'( dod' si) |
la8 dod'( re' mi') |
re'4 r8 re' |
si' la' r4 |
r r8 re' |
mi' mi mi mi |
dod' dod' re' re' |
mi' mi' mi' mi' |
mi' mi' mi' mi' |
la'\f la' dod' dod' |
re' re' re' mi' |
fad' fad' mi' red' |
mi'2\fermata |
la'8 la' la' la' |
re' r dod' r |
mi' mid' fad'16 dod' re' fad' |
mi'!8 mi' mi' mi' |
la la' la r |
re'4\p r r |
re' r r |
mi' r r |
la8 la la la la la |
sol' sol' sol' sol' sol' sol' |
fad'4 r r |
re'( la fad) |
mi mi'( re') |
sol'8 sol' fad' fad' mi' mi' |
re' si'( lad' si' lad' si') |
dod''4 lad2\p |
si8 si si si si si |
mid' mid' mid' mid' mid' mid' |
fad' fad' fad' fad' fad' fad' |
fad' fad' fad' dod' dod' si |
la8 la la la la la |
si si dod' dod' dod' dod' |
dod'4 r r |
la8 la la la |
la la la la |
la4 r8 fad' |
mi' mi mi r |
