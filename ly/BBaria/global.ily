\tag #'all \key la \major
\tempo "Andante" \midiTempo#60
\time 2/4 \partial 8 s8 s2*64 \bar "|." \segnoMark
s2*62 \bar "|." \fermataMark
\time 3/4 \midiTempo#120 s2.*18 \bar "||"
\time 2/4 \midiTempo#60 s2*4 \bar "|." \dalSegnoMark
