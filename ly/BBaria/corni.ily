\clef "treble" \transposition la
<> r8 |
<<
  \tag #'(corno1 corni) { do''2 | do'' | s | sol'~ | sol' | }
  \tag #'(corno2 corni) { do'2 | do' | s | sol~ | sol | }
  { s2*2 | R2 }
>>
\twoVoices #'(corno1 corno2 corni) <<
  { do'2 | do'4 s | sol'8 re'' re'' }
  { do'2 | do'4 s | sol'8 sol' sol' }
  { s2 | s4 r | }
>> r8 |
R2*2 |
r4 r8 \twoVoices #'(corno1 corno2 corni) <<
  { sol'8 | do''4 re''8 }
  { sol'8 | do' mi'16 do' sol'8 }
>> r8 |
R2*2 |
<<
  \tag #'(corno1 corni) {
    do''4 s |
    s2 |
    s8 re''4 re''8 |
    do''
  }
  \tag #'(corno2 corni) {
    do'4 s |
    s2 |
    s8 sol'4 sol'8 |
    do'
  }
  { s4 r | R2 | r8 }
>> r8 r4 |
r \twoVoices #'(corno1 corno2 corni) <<
  { sol'4 | do''8 do' do' }
  { sol'4 | do'8 do' do' }
>> r8 |
R2*3 |
<>\p <<
  \tag #'(corno1 corni) { sol'4 s | sol'2 | }
  \tag #'(corno2 corni) { sol4 s | sol2 | }
  { s4 r | }
>>
\twoVoices #'(corno1 corno2 corni) << do'4 do' >> r |
R2 |
<<
  \tag #'(corno1 corni) { sol'2 }
  \tag #'(corno2 corni) { sol }
>>
\twoVoices #'(corno1 corno2 corni) << do'4 do' >> r |
R2*30 |
<>\f \twoVoices #'(corno1 corno2 corni) <<
  { sol'2 |
    sol'4 s |
    sol' s |
    s s8 re'' |
    re'' sol' sol' }
  { sol'2 |
    sol'4 s |
    sol' s |
    s s8 re'' |
    sol' sol' sol' }
  { s2 | s4 r | s r | r r8 s | }
>> r8 |
<>\p <<
  \tag #'(corno1 corni) { sol'2~ | sol' | sol'8 }
  \tag #'(corno2 corni) { sol2~ | sol | sol8 }
>> r8 r4 |
R2 |
<<
  \tag #'(corno1 corni) { sol'2~ | sol' | sol'8 }
  \tag #'(corno2 corni) { sol2~ | sol | sol8 }
>> r8 r4 |
R2*6 |
r8 <>\p \twoVoices #'(corno1 corno2 corni) <<
  { sol'8 sol' }
  { sol' sol' }
>> r |
R2*3 |
<<
  \tag #'(corno1 corni) { sol'4 s | sol'2 | }
  \tag #'(corno2 corni) { sol4 s | sol2 | }
  { s4 r | }
>>
\twoVoices #'(corno1 corno2 corni) << do'4 do' >> r |
R2 |
<<
  \tag #'(corno1 corni) { sol'2 }
  \tag #'(corno2 corni) { sol }
>>
\twoVoices #'(corno1 corno2 corni) << do'4 do' >> r |
R2*5 |
<>\p <<
  \tag #'(corno1 corni) { sol'2~ | sol'~ | sol'8 }
  \tag #'(corno2 corni) { sol2~ | sol~ | sol8 }
>> r8 r4 |
R2*7 |
<>\f <<
  \tag #'(corno1 corni) { do''2 | sol' | }
  \tag #'(corno2 corni) { do'2 | sol | }
>>
R2*2 |
<>\f <<
  \tag #'(corno1 corni) { do''2 | sol' | }
  \tag #'(corno2 corni) { do'2 | sol | }
>>
\twoVoices #'(corno1 corno2 corni) << do'8 do' >> r r4 |
R2*7 |
<<
  \tag #'(corno1 corni) {
    sol'2 |
    do''~ |
    do'' |
    do''8 do'' do'' do'' |
  }
  \tag #'(corno2 corni) {
    sol2 |
    do'~ |
    do' |
    do'8 do' do' do' |
  }
  { s2\p | s2\f | }
>>
\twoVoices #'(corno1 corno2 corni) << sol'4 sol' >> r\fermata |
<<
  \tag #'(corno1 corni) { do''2 | do'' | re''4 }
  \tag #'(corno2 corni) { do'2 | do' | sol'4 }
>> \twoVoices #'(corno1 corno2 corni) <<
  { do''4 |
    sol'2 |
    do''8 do' do' }
  { do''8 do' |
    sol'4 sol |
    do'8 do' do' }
>> r8 |
R2.*3 |
\tag#'corni <>^"a 2." do'2.\p |
do' |
do'4 r r |
R2.*12 |
<<
  \tag #'(corno1 corni) { do''2 | do'' | }
  \tag #'(corno2 corni) { do' | do' }
>>
\twoVoices #'(corno1 corno2 corni) << do'4 do' >> r |
r8 \twoVoices #'(corno1 corno2 corni) <<
  { sol'8[ sol'] }
  { sol'[ sol'] }
>> r8 |

