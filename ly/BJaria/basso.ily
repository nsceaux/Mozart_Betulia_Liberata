\clef "bass" r8 |
mi4 mi mi mi |
mi mi mi mi |
mi mi fad si, |
mi mi sold mi |
la, la, si, si, |
mi mi fad fad |
sold sold la lad, |
si, si si, r4 |
r2 r8 si,-. red-. si,-. |
r2 r8 la-. dod'-. la-. |
sold4 la si si, |
r2 r8 si,-. red-. si,-. |
r2 r8 la-. dod'-. la-. |
sold4 la si si, |
mi r mi r |
mi mi mi r |
mi\p mi mi mi |
mi mi mi mi |
mi mi fad si, |
mi r sold r |
la r la, r |
sold, la, si, si, |
mi r mi r |
fad r fad r |
sol r fad r |
mid r sol r |
fad r re r |
mi r mi mid |
fad r r2\fermata |
r2 r8 fad-. lad-. fad |
r2 r8 mi-. sold-. mi-. |
fad4 fad fad fad |
r2 r8 fad-. lad-. fad-. |
r2 r8 mi-. sold-. mi-. |
fad4 fad fad fad |
si, r r2 |
re4 re re re |
mi mi mi mi |
fad fad fad fad |
sold! sold lad mi |
red! mi fad fad |
si, red mi mi |
fad8 fad fad fad fad fad fad fad |
si4\f si, si, si |
dod' lad si red |
mi mi red mi |
fad fad fad, fad, |
si, r fad\p r |
si, si, si, fad |
red red red red |
dod si, lad, sold, |
fadd,\f fadd,\p sold, sold, |
dod fad sold sold, |
dod r dod r |
dod r dod r |
si, r mi r |
la, la, lad, lad, |
si, si, si, si, |
do do do do |
si, r r2\fermata |
r2 r8 si,-. red-. si,-. |
r2 r8 la-. dod'-. la-. |
si4 si si si |
r2 r8 si,-. red-. si,-. |
r2 r8 la-. dod'-. la-. |
si4 si si si |
mi r r2 |
sol,4 sol, sol, sol, |
la, la, la, la, |
si, si, si, si, |
dod! dod la, la |
sold la si si, |
sold,8 sold sold sold sold sold sold sold |
la4 la la la |
sold sold la la, |
si, si, si, si, |
si,8 si, si, si, si, si, si, si, |
mi1~ |
mi~ |
mi~ |
mi~ |
mi4 la, si,2 |
mi4 la, si,2 |
mi4 mi mi mi |
mi mi mi r |
