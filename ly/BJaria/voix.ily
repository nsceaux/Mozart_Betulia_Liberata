\clef "soprano/treble" r8 |
R1*15 |
r2 r4 r8 si' |
si'[ mi''] sold'[ la'] si'4 si' |
si'2. sid'4 |
dod'' si'!8[ dod''] si'4 la' |
sold'2 re' |
dod'4 mi'' r dod'' |
si'( \grace red''16 dod''8) si'16[ la'] sold'4 fad' |
mi'4 r r r8 si' |
lad'4. lad'8 lad'4. dod''8 |
lad'4 r r r8 dod'' |
re''2. dod''8[ si'] |
lad'4 lad' r fad'' |
sol' sol''~ sol''8[ mi''] dod''[ si'] |
lad'4.\melisma sold'8\melismaEnd fad'4\fermata r4 |
R1 |
r2 r4 r8 sold' |
fad'4 r8 red'' mi''4 r8 dod'' |
si'4 r r2 |
r r4 r8 sold' |
fad'4 r8 red'' mi''4 r8 lad' |
si'4 r r2 |
si'4 si'8 si' si'4. re'8 |
dod'4 r r r8 mi'' |
mi''4. fad'8 fad'4. mi'8 |
red'!4 r sold'2 |
fad'8[ fad'']~ fad''16[ sold''] mi''[ dod''] dod''2\trill |
si'4 fad' sold'16[ dod'' mi'' red''] \grace fad''16 mi''8 red''16[ dod''] |
si'2\melisma dod''\trill\melismaEnd |
si'4 r r2 |
R1*3 |
r2 r4 r8 fad' |
fad'[ si'] si'[ red''] red''4. mi''16[ dod''] |
si'4 r r r8 si' |
lad'4. si'16[ sold'] fad'4. mi'8 |
red'4 r fad''2 |
mi''4. fad''16[ red''] dod''4 sid' |
dod''4 r r r8 mi'' |
mi''[ dod''] si'![ la'] la'4 la' |
la'( sold') r re'' |
dod''4~ dod''16[\melisma red''! mi'' red''] mi''[ fad'' sold'' fad''] mi''8\melismaEnd sold' |
sold'4 fad' r red'' |
mi'' sol''2 lad'4 |
si'4.\melisma lad'8\melismaEnd si'4\fermata r |
R1 |
r2 r4 r8 dod'' |
si'4 r8 mi'' fad'4 r8 la' |
sold'4 r r2 |
r2 r4 r8 mi'' |
si'4 r8 dod'' mi'4 r8 red' |
mi'4 r r2 |
mi''4 mi''8 mi'' mi''4. sol'8 |
fad'4 r r r8 fad'' |
fad''4. red''8 si'4. la'8 |
sold'!4 r dod''2 |
si'4~ si'16[ dod''] la'[ fad'] mi'4( red') |
mi'2 re'' |
dod''4~ dod''16[\melisma mi'' red''! dod''] red''4~ red''16[ fad'' mi'' red''] |
mi''[ fad'' sold'' fad''] mi''[ red'' dod'' si'] dod''[ red'' mi'' red''] mi''[ dod'']\melismaEnd si'[ la'] |
sold'8\melisma si'4 mi'' sold''8~ sold''16[ mi'' si']\melismaEnd mi' |
fad'1 |
mi'4 r r2 |
r r4 r8 red'' |
mi''4 r r r8 re'' |
dod''4 r8 dod'' si'4 r8 red' |
mi'4 r8 dod'' si'4 r8 red' |
mi'4 r8 dod'' si'4 r8 red' |
mi'4 r r2 |
R1 |
