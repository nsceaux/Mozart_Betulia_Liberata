Con trop -- pa rea vil -- tà
quest’ al -- ma t’ol -- trag -- giò,
quest’ al -- ma,
quest’ al -- ma t’ol -- trag -- giò,
al -- lor che di -- spe -- rò
del tuo soc -- cor -- so,
del tu -- o __ soc -- cor -- so.

Pie -- tà, Si -- gnor, pie -- tà,
Pie -- tà, Si -- gnor, pie -- tà,
giac -- che il pen -- ti -- to cor
mi -- su -- ra il pro -- prio er -- ror
col suo __ ri -- mor -- so,
col su -- o ri -- mor -- so.

Con trop -- pa rea vil -- tà
quest’ al -- ma t’ol -- trag -- giò,
quest’ al -- ma t’ol -- trag -- giò,
al -- lor che di -- spe -- rò __
del tuo __ soc -- cor -- so,
del tu -- o soc -- cor -- so.

Pie -- tà, Si -- gnor, pie -- tà,
Pie -- tà, Si -- gnor, pie -- tà.
Giac -- che il pen -- ti -- to cor
mi -- su -- ra il pro -- prio er -- ror
col suo __ ri -- mor -- so,
col suo, __
col suo __ ri -- mor -- so.
Pie -- tà, pie -- tà, Si -- gnor, pie -- tà, pie -- tà,
Si -- gnor, Si -- gnor, pie -- tà.
