\score {
  <<
    \new StaffGroup <<
      \new GrandStaff \with { \violiniInstr } <<
        \new Staff << \global \keepWithTag #'violino1 \includeNotes "violini" >>
        \new Staff << \global \keepWithTag #'violino2 \includeNotes "violini" >>
      >>
      \new Staff \with { \violaInstr } <<
        \global \includeNotes "viola"
      >>
    >>
    \new Staff \with {
      instrumentName = \markup\character Amital
      shortInstrumentName = \markup\character Am.
    } \withLyrics <<
      \global \includeNotes "voix"
    >> \includeLyrics "paroles"
    \new Staff \with { \bassiInstr } <<
      \global \includeNotes "basso"
      \origLayout {
        s8 s1*6\break s1*8\break s1*8\pageBreak
        s1*6\break s1*8\break s1*7\break s1*7\pageBreak
        s1*6\break s1*8\break s1*7\break s1*6\pageBreak
      }
    >>
  >>
  \layout { }
  \midi { }
}
