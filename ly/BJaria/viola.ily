\clef "alto" r8 |
mi'4 mi' mi' mi' |
mi' mi' mi' mi' |
mi' mi' fad' si |
mi' mi' sold' mi' |
la la si si |
mi' mi' fad' fad' |
sold' sold' la' lad |
si si' si r4 |
r2 r8 red'-. fad'-. red'-. |
r2 r8 dod'-. mi'-. dod'-. |
si4( la sold fad) |
r2 r8 red'-. fad'-. red'-. |
r2 r8 dod'-. mi'-. dod'-. |
si4( la sold fad) |
sold4 r si r |
si si si r |
mi'\p mi' mi' mi' |
mi' mi' mi' mi' |
mi' mi' fad' si |
mi'8 mi mi mi r mi mi mi |
la4 r la r |
sold la si si |
mi' r mi' r |
fad' r fad' r |
sol' r fad' r |
mid' r sol' r |
fad' r re' r |
mi' r mi' mid' |
R1^\fermataMarkup |
r2 r8 lad-. dod'-. lad-. |
r2 r8 sold'-. si'-. sold'-. |
fad'4 fad' fad' fad' |
r2 r8 lad-. dod'-. lad-. |
r2 r8 sold'-. si'-. sold'-. |
fad'4 fad' fad' fad' |
si r r2 |
re'4 re' re' re' |
mi' mi' mi' mi' |
fad' fad' fad' fad' |
sold'! sold' lad' mi' |
red'! mi' fad' fad' |
si red' mi' mi' |
fad'8 fad' fad' fad' fad' fad' fad' fad' |
fad'4\f red'2 fad'4 |
mi' dod' fad fad' |
si dod' fad'8 si'4 sold'16( mi') |
red'4 si'2 lad'4 |
si' r fad\p r |
si si si fad' |
red' red' red' red' |
dod' si lad sold |
lad\f lad\p red red' |
dod' fad' sold' sold |
dod' r dod' r |
dod' r dod' r |
si r mi' r |
la la lad lad |
si si si si |
do' do' do' do' |
si r r2\fermata |
r2 r8 red'-. fad'-. red'-. |
r2 r8 dod'-. mi'-. dod'-. |
si1 |
r2 r8 red'-. fad'-. red'-. |
r2 r8 dod'-. mi'-. dod'-. |
si1 |
mi'4 r r2 |
sol4 sol sol sol |
la la la la |
si si si si |
dod'! dod' la la' |
sold( la) sold( fad) |
sold8 sold' sold' sold' sold' sold' sold' sold' |
la'4 la' la' la' |
sold' sold' la' la |
si si si si |
si8 si si si si si si si |
si1 |
dod'2( red'!) |
si1 |
dod'2( red') |
mi'4.( dod'8) si2 |
sold4( la) sold8( si la fad) |
mi1 |
mi4 si si r |
