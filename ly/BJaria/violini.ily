\clef "treble" \omit TupletBracket
<<
  \tag #'violino1 {
    si'8 |
    si'( mi'') sold'( la') si'4 si' |
    si'2. sid'4 |
    \tuplet 3/2 { dod''8([ red'' mi'']) } \tuplet 3/2 { si'!([ mi' dod'']) } si'4( la') |
    la'( sold') r mi''8( sold'') |
    sold''4( fad''8) mi''-. red'' dod'' si' la' |
    \tuplet 3/2 { sold'[ si' mi''] } mi''8*2/3[ sold'' mi''] la'[ dod'' mi''] mi''[ la'' mi''] |
    si'[ mi'' sold''] si''[ si' re''] dod''[ si' la'] sold'[ fad' mi'] |
    red'!4 si'16( dod'' red'' dod'') si'4 r |
    mi'4.. ( fad'32 sold' la'4) r |
    sold'4..( la'32 si' dod''4) r |
    si'8( mi'') mi'( dod'') mi'4( red') |
    mi'4..( fad'32 sold' la'4) r |
    sold'4..( la'32 si' dod''4) r |
    si'8( mi'') mi'( dod'') mi'4( red') |
    mi'4 sold'16( fad' sold' la') sold'4 mi''16( red'' mi'' fad'') |
    mi''4 mi' mi' r8 si'\p |
    si'( mi'') sold'( la') si'4-. si'-. |
    si'2. sid'4 |
    \tuplet 3/2 { dod''8([ red'' mi'']) } si'!8*2/3([ mi' dod'']) si'4( la') |
    r8 sold' sold' sold' r re' re' re' |
    dod' mi'' mi'' mi'' dod'' dod'' dod'' dod'' |
    si' si' dod'' la' sold'4 fad' |
    \tuplet 3/2 { r8 sold'[ la'] } \tuplet 3/2 { si'[ si' si'] } r8*2/3 sold'[ la'] si'[ si' si'] |
    r lad'[ lad'] lad'[ lad' lad'] r lad'[ lad'] lad'[ lad' lad'] |
    r lad'[ lad'] lad'[ lad' lad'] r dod''[ dod''] dod''[ dod'' dod''] |
    r re''[ re''] re''[ re'' re''] r mid''[ mid''] mid''[ mid'' mid''] |
    r fad''[ fad''] fad''[ fad'' fad''] r si'[ si'] si'[ si' si'] |
    r si'[ si'] si'[ si' si'] r dod''[ dod''] dod''[ dod'' dod''] |
    dod''4 r r2\fermata |
    si'4..( dod''32 red'' mi''4) r |
    red''4..( mi''32 fad'' sold''4) r |
    r8 fad'' r red'' r dod'' r dod'' |
    si'4..( dod''32 red'' mi''4) r |
    red''4..( mi''32 fad'' sold''4) r |
    r8 fad'' r red'' r mi'' r dod'' |
    si'-. red'( mi' fad') sold'( fad' sold' lad') |
    si'8 si' si' si' si' si' si' si' |
    si' si' si' si' si' si' si' si' |
    lad' lad' lad' lad' lad' lad' lad' lad' |
    si' red'! red' red' dod' sold'' sold'' sold'' |
    fad'' fad''~ fad''16( sold'' mi'' dod'') dod''8 dod'' dod'' dod'' |
    si' si' si' si' si' si' si' si' |
    red'' red'' red'' red'' dod''2:16 |
  }
  \tag #'violino2 {
    r8 |
    sold'( si') mi'( fad') sold'4 sold' |
    sold'2 sold |
    la4 sold8( mi') mi'4( red') |
    red'( mi') r si'8( mi'') |
    mi''4( red''8) dod''-. si' la' sold' fad' |
    \tuplet 3/2 { sold'8[ si' mi''] } mi''8*2/3[ sold'' mi''] la'[ dod'' mi''] mi''[ la'' mi''] |
    si'[ mi'' sold''] si''[ si' re''] dod''[ si' la'] sold'[ fad' mi'] |
    red'!4 red'16( mi' fad' mi') red'4 r |
    R1 |
    mi'4..( fad'32 sold' la'4) r |
    mi'2 si |
    R1 |
    mi'4..( fad'32 sold' la'4) r |
    mi'2 si |
    si4 mi'16( red' mi' fad') mi'4 sold'16( fad' sold' la') |
    sold'4 sold sold r8 sold'\p |
    sold'( si') mi'( fad') sold'4-. sold'-. |
    sold'2 sold |
    la4 sold8( mi') mi'4( red') |
    r8 mi' si si r si si si |
    la dod'' dod'' dod'' mi' mi' mi' mi' |
    mi' mi' mi' mi' mi'4 red' |
    \tuplet 3/2 { r8 mi'[ fad'] } \tuplet 3/2 { sold'[ sold' sold'] } r8*2/3 mi'[ fad'] sold'[ sold' sold'] |
    r mi'[ mi'] mi'[ mi' mi'] r mi'[ mi'] mi'[ mi' mi'] |
    r mi'[ mi'] mi'[ mi' mi'] r lad'[ lad'] lad'[ lad' lad'] |
    r si'[ si'] si'[ si' si'] r si'[ si'] si'[ si' si'] |
    r lad'[ lad'] lad'[ lad' lad'] r fad'[ fad'] fad'[ fad' fad'] |
    r sol'[ sol'] sol'[ sol' sol'] r sol'[ sol'] si'[ si' si'] |
    lad'4 r r2\fermata |
    R1 |
    si'4..( dod''32 red'' mi''4) r |
    r8 red'' r si' r lad' r lad' |
    R1 |
    si'4..( dod''32 red'' mi''4) r4 |
    r8 red'' r si' r dod'' r lad' |
    si'8-. si( dod' red') mi'( red' mi' dod') |
    re' fad' fad' fad' fad' fad' fad' fad' |
    sol' sol' sol' sol' sol' sol' sol' sol' |
    dod' dod' dod' dod' dod' dod' dod' dod' |
    si si si si lad lad lad lad |
    si4. sold'16( si') si'8 si' lad' lad' |
    si' si' fad' fad' sold' sold' sold' sold' |
    si' si' si' si' lad'2:16 |
  }
>>
\tuplet 3/2 { si'8\f red'' fad'' } fad''2 \grace mi''16 red''8 dod''16 si' |
\tuplet 3/2 { lad'8[ dod'' mi'']~ } mi''8*2/3[ fad'' mi''] red''[ fad'' la''!]~ la''[ si'' la''] |
sold''8 lad''4 si'' fad'8 sold' sold'' |
fad''4 si''16( fad'') red''( si') dod''2\trill |
si'4 <<
  \tag #'violino1 {
    fad'8.( sold'16 mi'8) r mi'8.(\p fad'16) |
    red'4 si' red''4. mi''16( dod'') |
    si'8 fad'' fad'' fad'' fad'' fad'' fad'' fad'' |
    fad'8 fad' fad' fad' fad' fad' fad' mi' |
    red'\fp red' red' red' fad'' fad'' fad'' fad'' |
    mi''( sold'' la'' fad'') mi'' mi'' red'' red'' |
    \tuplet 3/2 { dod''8[ dod'' red''] } mi''8*2/3[ mi'' mi''] r mi''[ mi''] mi''[ red'' dod''] |
    r la'[ la'] la'[ la' la'] r la'[ la'] la'[ la' la'] |
    r mi'[ mi'] mi'[ mi' mi'] r sold'[ sold'] sold'[ sold' sold'] |
    mi'8 r dod'' r sold' r sold'' r |
    sold''4 fad'' \tuplet 3/2 { r8 fad''-.[ fad''-.] } \tuplet 3/2 { si''8[ fad'' red''] } |
    sol''1 |
    red''4 r r2\fermata |
    mi'4..( fad'32 sold' la'4) r |
    sold'4..( la'32 si' dod''4) r |
    r8 si' r sold' r la' r fad' |
    mi'4..( fad'32 sold' la'4) r |
    sold'4..( la'32 si' dod''4) r |
    r8 si' r dod'' r sold' r fad' |
    r8 mi'( fad' sold') la'( si' dod'' red'') |
    mi'' mi'' mi'' mi'' mi'' mi'' mi'' mi'' |
    mi' mi' mi' mi' mi' mi' mi' mi' |
    red' red' red' red' fad' fad' fad' fad' |
    mi' mi' mi' mi' dod'' dod'' dod'' dod'' |
    si'8 si'~ si'16( dod'' la' fad') mi'4( red') |
    mi'8 mi' mi' mi' re'' re'' re'' re'' |
    dod'' dod'' dod'' dod'' red''! red'' red'' red'' |
    mi'' mi'' mi'' mi'' dod'' dod'' dod'' dod'' |
    sold' sold' sold' sold' sold' sold' sold' sold' |
    fad' fad' fad' fad' fad'2:16 |
    mi'8-. sold''([ si'' la''] sold''[ fad'' mi'' re'']) |
    dod''( mi'' fad' sold') la'( fad' si' la') |
    sold'8-. sold''([ si'' la''] sold'' fad'' mi'' re'') |
    dod''8( mi'' fad' sold') la'( fad' si' la') |
    sold'( si' dod'' mi'') sold'( si' la' fad') |
    mi'( sold' dod'' mi'') si'( sold' fad' red') |
    mi'8 sold'4( la'16 fad') mi'8 sold'4( la'16 fad') |
    mi'4 mi' mi' r |
  }
  \tag #'violino2 {
    red'8.( mi'16 dod'8) r dod'8.(\p red'16) |
    si4 red' fad' lad' |
    si'8 si' si' si' si' si' si' si' |
    mi'8 mi' mi' red' dod' dod' dod' dod' |
    dod'\fp dod' dod' dod' sid sid sid sid |
    dod' dod''4 dod'' dod''8 sid'[ sid'] |
    \tuplet 3/2 { dod''8[ mi' sold'] } dod''8*2/3[ dod'' dod''] r sold'[ sold'] sold'[ fad' mi'] |
    r mi'[ mi'] mi'[ mi' mi'] r mi'[ mi'] mi'[ mi' mi'] |
    r re'[ re'] re'[ re' re'] r si[ si] si[ si si] |
    la8 r mi' r mi' r mi'' r |
    mi''4 red'' \tuplet 3/2 { r8 red''-.[ red''-.] } \tuplet 3/2 { fad''[ red'' si'] } |
    mi''1 |
    fad''4 r r2\fermata |
    R1 |
    mi'4..( fad'32 sold' la'4) r |
    r8 sold' r mi' r fad' r red' |
    R1 |
    mi'4..( fad'32 sold' la'4) r |
    r8 sold' r la' r mi' r red' |
    r8 mi'( red' mi') fad'( sold' la' fad') |
    sol' si' si si si si si si |
    do' do' do' do' do' do' do' do' |
    fad' fad' fad' fad' red' red' red' red' |
    sold'! sold' sold' sold' mi' mi' mi' mi' |
    mi'2 si |
    mi'8 mi' mi' mi' <mi' si'> q q q |
    la' mi' mi' mi' fad' fad' fad' fad' |
    si si si si mi' mi' mi' mi' |
    mi' mi' mi' mi' mi' mi' mi' mi' |
    mi' mi' mi' mi' red'2:16 |
    mi'2( sold') |
    la'( fad') |
    mi'( sold') |
    la'( fad') |
    mi'2.( red'4) |
    mi'2. red'8( la) |
    sold8 si4( dod'16 la) sold8 si4 dod'16( la) |
    sold4 sold sold r |
  }
>>
