\key mi \major
\time 2/2 \partial 8 \tempo "Andante" \midiTempo#160 s8 s1*8 \bar "||"
\time 4/4 \tempo "Adagio" \midiTempo#80 s1*6 \bar "||"
\time 2/2 \tempo "Andante" \midiTempo#160 s1*15 \bar "||"
\time 4/4 \tempo "Adagio" \midiTempo#80 s1*19 \bar "||"
\time 2/2 s1*12 \bar "||"
\time 4/4 \tempo "Adagio" \midiTempo#80 s1*25 \bar "|."