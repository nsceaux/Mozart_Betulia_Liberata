\livretAct PARTE PRIMA
\livretDescAtt\wordwrap-center {
  Ozìa, Amital, Cabri e Coro
}
\livretRef#'ABrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Popoli di Betulia, ah qual v'ingombra
%# Vergognosa viltà! Pallidi, affliti,
%# Tutti mi siete intorno! E ver, ne stringe
%# D'assedio pertinace il campo assiro:
%# Ma non siam vinti ancor. Dunque sì presto
%# Cedete alle sventure? Io, più di loro,
%# Temo il vostro timor. De' nostri mali
%# Questo, questo è il peggior: questo ci rende
%# Inabili a' ripari. Ogni tempesta
%# Al nocchier che dispera
%# E tempesta fatal, benché leggiera.

\livretRef#'ACaria
\livretPiece ARIA
\livretPers Ozia
%# D'ogni colpa la colpa maggiore
%# E l'eccesso d'un empio timore,
%# Oltraggioso all'eterna pietà.
%# Chi dispera, non ama, non crede:
%# Ché la fede, l'amore, la speme
%# Son tre faci che splendono insieme,
%# Né una ha luce se l'altra non l'ha.

\livretRef#'ADrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
%# E in che sperar?
\livretPers Amital
%# Nella difesa forse
%# Di nostre schiere indebolite e sceme
%# Dall'assidua fatica? estenuate
%# Dallo scarso alimento? intimorite
%# Dal pianto universal? Fidar possiamo
%# Ne' vicini già vinti?
%# Negli amici impotenti? in Dio sdegnato?
\livretPers Cabri
%# Scorri per ogni lato
%# La misera città; non troverai
%# Che oggetti di terror. Gli ordini usati
%# Son negletti o confusi. Altri s'adira
%# Contro il Ciel, contro te; piangendo accusa
%# Altri le proprie colpe antiche e nuove;
%# Chi corre, e non sa dove;
%# Chi geme, e non favella; e lo spavento
%# Come in arida selva appresa fiamma
%# Si comunica e cresce. Ognun si crede
%# Presso a morir. Già ne' congedi estremi
%# Si abbracciano a vicenda
%# I congiunti, gli amici; ed è deriso
%# Chi ostenta ancor qualche fermezza in viso.

\livretRef#'AEaria
\livretPiece ARIA
\livretPers Cabri
%# Ma qual virtù non cede
%# Fra tanti oggetti e tanti
%# Ad avvilir bastanti
%# li più feroce cor?
%# Se non volendo ancora
%# Si piange agli altrui pianti,
%# Se impallidir talora
%# Ci fa l'altrui pallor?

\livretRef#'AFrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Già le memorie antiche
%# Dunque andaro in oblio? Che ingrata è questa
%# Dimenticanza, o figli! Ah ci sovvenga
%# Chi siam, qual Dio n'assiste, e quanti e quali
%# Prodigi oprò per noi. Chi a' passi nostri
%# Divise l'Eritreo, chi l'onde amare
%# Ne raddolcì, negli aridi macigni
%# Chi di limpidi umori
%# Ampie vene ci aperse, e chi per tante
%# Ignote solitudine infeconde
%# Ci guidò, ci nutrì, potremo adesso
%# Temer che ne abbandoni? Ah no. Minaccia
%# Il superbo Oloferne
%# Già da lunga stagion Betulia; e pure
%# Non ardisce assalirla. Eccovi un segno
%# Del celeste favor.
\livretPers Cabri
%# Sì; ma frattanto
%# Più crudelmente il condottier feroce
%# Ne distrugge sedendo. I fonti, ond'ebbe
%# La città, già felice, acque opportune,
%# Il tiranno occupò. L'onda che resta,
%# A misura fra noi
%# Scarsamente si parte; onde la sete
%# Irrìta e non appaga,
%# Nutrisce e non estingue.
\livretPers Amital
%# A tal nemico,
%# Che per le nostre vene
%# Si pasce, si diffonde, ah con qual'armi
%# Resisterem? Guardaci in volto; osserva
%# A qual segno siam giunti. Alle querele
%# Abili ormai non sono i petti stanchi
%# Dal frequente anelar, le scabre lingue,
%# Le fauci inaridite. Umore al pianto
%# Manca su gli occhi nostri, e cresce sempre
%# Di pianger la cagion. Né il mal più grande
%# Per me, che madre sono,
%# E la propria miseria; i figli, i figli
%# Vedermi, oh Dio! miseramente intorno
%# Languir così, né dal mortale ardore
%# Poterli ristorar; questa è la pena
%# Che paragon non ha, che non s'intende
%# Da chi madre non è. Sentimi, Ozìa;
%# Tu sei, tu che ne reggi,
%# Delle miserie nostre
%# La primiera cagione. Iddio ne sia
%# Fra noi giudice e te. Parlar di pace
%# Con l'Assiro non vuoi; perir ci vedi
%# Fra cento affanni e cento;
%# E dormi? e siedi irresoluto e lento?

\livretRef#'AGaria
\livretPiece ARIA
\livretPers Amital
%# Non hai cor, se in mezzo a questi
%# Miserabili lamenti
%# Non ti scuoti, non ti desti,
%# Non ti senti intenerir.
%# Quanto, oh Dio, siamo infelici
%# Se sapessero i nemici,
%# Anche a lor di pianto il ciglio
%# Si vedrebbe inumidir.

\livretRef#'AHrecitativo
\livretPiece RECITATIVO
\livretPers Ozìa
%# E qual pace sperate
%# Da gente senza legge e senza fede,
%# Nemica a nostro Dio?
\livretPers Amital
%# Sempre fia meglio
%# Benedirlo viventi,
%# Che in obbrobrio alle genti
%# Morir, vedendo ed i consorti e i figli
%# Spirar su gli occhi nostri.
\livretPers Ozìa
%# E se né pure
%# Questa misera vita a voi lasciasse
%# La perfidia nemica?
\livretPers Amital
%# Il ferro almeno
%# Sollecito ne uccida, e non la sete
%# Con sì lungo morir. Deh Ozìa, per quanto
%# Han di sacro e di grande e terra e cielo,
%# Per lui, ch'or ne punisce,
%# Gran Dio de' padri nostri, all'armi assire
%# Rendasi la città.
\livretPers Ozia
%# Figli, che dite!
\livretPers Amital
%# Sì, sì, Betulia intera
%# Parla per bocca mia. S'apran le porte,
%# Alla forza si ceda: uniti insieme
%# Volontari corriamo
%# Al campo d'Oloferne. Unico scampo
%# E questo; ognun lo chiede.
\livretPers Coro
%# Al campo, al campo!
\livretPers Ozia
%# Fermatevi, sentite. (Eterno Dio,
%# Assistenza, consiglio!) Io non m'oppongo,
%# Figli, al vostro pensier: chiedo che solo
%# Differirlo vi piaccia, e più non chiedo
%# Che cinque dì. Prendete ardir. Frattanto
%# Forse Dio placherassi, e del suo nome
%# La gloria sosterrà. Se giunge poi
%# Senza speme per noi la quinta aurora,
%# S'apra allor la città, rendasi allora.
\livretPers Amital
%# A questa legge attenderemo.
\livretPers Ozia
%# Or voi
%# Co' vostri accompagnate
%# Questi che al Ciel fervidi prieghi invio,
%# Nunzi fedeli in fra' mortali e Dio.

\livretRef#'AIcoro
\livretPiece ARIA CON CORO
\livretPers Ozia
%# Pietà, se irato sei,
%# Pietà, Signor, di noi:
%# Abbian castigo i rei,
%# Ma l'abbiano da te.
\livretPers Coro
%# Abbian castigo o rei,
%# Ma l'abbiano da te.
\livretPers Ozia
%# Se oppresso chi t'adora
%# Soffri da chi t'ignora,
%# Gli empii diranno poi:
%# „Questo lor Dio dov'è?“
\livretPers Coro
%# Gli empii diranno poi:
%# „Questo lor Dio dov'è?“

\livretRef#'AJArecitativo
\livretPiece RECITATIVO
\livretPers CABRI
%# Chi è costei, che qual sorgente aurora
%# S'appressa a noi; terribile d'aspetto
%# Qual falange ordinata; e a paragone
%# Della luna, del sol bella ed eletta?
\livretPers Amital
%# Alla chioma negletta,
%# Al rozzo manto, alle dimesse ciglia,
%# Di Merari è la figlia.
\livretPers Ozia
%# Giuditta!
\livretPers Cabri
%# Sì, la fida
%# Vedova di Manasse.
\livretPers Ozia
%# Qual mai cagion la trasse
%# Dal segreto soggiorno in cui s'asconde,
%# Volge il quart'anno ormai?
\livretPers Amital
%# So ch'ivi orando
%# Passa desta le notti.
%# Digiuna i dì: so che donolle il Cielo
%# E ricchezza e beltà, ma che disprezza
%# La beltà, la ricchezza; e tal divenne,
%# Che ritrovar non spera
%# In lei macchia l'invidia o finta o vera.
%# Ma non saprei…
\livretDidasPPage (Entra Giuditta.)
\livretPers Giuditta
%# Che ascolto, Ozìa!
\livretRef#'AJBrecitativo
%# Betulia, aimè, che ascolto! All'armi assire
%# Dunque aprirem le porte, ove non giunga
%# Soccorso in cinque dì! Miseri! E questa
%# E la via d'impetrarlo? Ah tutti siete
%# Colpevoli egualmente. Ad un estremo
%# li popolo trascorse: e chi lo regge
%# Nell'altro ruinò. Quello dispera
%# Della pietà divina: ardisce questo
%# Limitarle i confini. Il primo è vile,
%# Temerario il secondo. A chi la speme,
%# A chi manca il timor:
%# Né in questo o in quella
%# Misura si serbò. Vizio ed eccesso
%# Non è diverso. Alla virtù prescritti
%# Sono i certi confini; e cade ognuno,
%# Che per qualunque via da lor si scosta,
%# In colpa equal, benche talvolta opposta.

\livretPers Giuditta
\livretRef#'AKaria
%# Del pari infeconda
%# D'un fiume è la sponda
%# Se torbido eccede,
%# Se manca d'umor.
%# Si acquista baldanza
%# Per troppa speranza,
%# Si perde la fede
%# Per troppo timor.

\livretRef#'ALrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Oh saggia, oh santa, oh eccelsa donna! Iddio
%# Anima i labbri tuoi.
\livretPers Cabri
%# Da tali accuse
%# Chi si può discolpar?
\livretPers Ozia
%# Deh tu, che sei
%# Cara al Signor, per noi perdono implora;
%# Ne guida, ne consiglia.
\livretPers Giuditta
%# In Dio sperate
%# Soffrendo i vostri mali. Egli in tal guisa
%# Corregge, e non opprime; ei de' più cari
%# Così prova la fede: e Abramo e Isacco
%# E Giacobbe e Mosè diletti a lui
%# Divennero così. Ma quei che osaro
%# Oltraggiar mormorando
%# La sua giustizia, o delle serpi il morso
%# O il fuoco esterminò. Se in giusta lance
%# Pesiamo il falli nostri, assai di loro
%# E minore il castigo: onde dobbiamo
%# Grazie a Dio, non querele. Ei ne consoli
%# Secondo il voler suo. Gran prove io spero
%# Della pietà di lui. Voi che diceste
%# Che muove i labbri miei, credete ancora
%# Ch'ei desti i miei pensieri. Un gran disegno
%# Mi bolle in mente, e mi trasporta. Amici,
%# Non curate saperlo. Al sol cadente
%# Della città m'attendi,
%# Ozìa, presso alle porte. Alla grand'opra
%# A prepararmi io vado. Or, fin ch'io torni,
%# Voi con prieghi sinceri
%# Secondate divoti i miei pensieri.

\livretRef#'AMcoro
\livretPiece ARIA CON CORO
\livretPers Ozia e Coro
%# Pietà, se irato sei, ecc.
%# Se oppresso chi t'adora, ecc.
\livretDidasPPage (Entrano Carmi ed Achior.)

\livretRef#'ANrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
%# Signor, Carmi a te viene.
\livretPers Amita
%# E la commessa
%# Custodia delle mura
%# Abbandonò?
\livretPers Ozia
%# Carmi, che chiedi?
\livretPers Carmi
%# Io vengo
%# Un prigioniero a presentarti. Avvinto
%# Ad un tronco il lasciaro
%# Vicino alla città le schiere ostili:
%# Achiorre è il suo nome;
%# Degli Ammoniti è il prence.
\livretPers Ozia
%# E così tratta
%# Oloferne gli amici?
\livretPers Achior
%# E de' superbi
%# Questo l'usato stil. Per loro è offesa
%# Il ver che non lusinga.
\livretPers Ozia
%# I sensi tuoi
%# Spiega più chiari.
\livretPers Achior
%# Ubbidirò. Sdegnando
%# L'assiro condottier che a lui pretenda
%# Di resister Betulia; a me richiese
%# Di voi notizia. Io, le memorie antiche
%# Richiamando al pensier, tutte gli esposi
%# Del popolo d'Israele
%# Le origini, i progressi; il culto avito
%# De' numerosi dèi, che per un solo
%# Cambiaro i padri vostri; i lor passaggi
%# Dalle caldee contrade
%# In Carra, indi in Egitto; i duri imperi
%# Di quel barbaro re. Dissi la vostra
%# Prodigiosa fuga, i lunghi errori,
%# Le scorte portentose, i cibi, Tacque,
%# Le battaglie, i trionfi; e gli mostrai
%# Che, quando al vostro Dio foste fedeli,
%# Sempre pugnò per voi. Conclusi al fine
%# I miei detti così: “Cerchiam se questi
%# Al lor Dio son infidi; e se lo sono,
%# La vittoria è per noi. Ma se non hanno
%# Delitto innanzi a lui, no, non la spero,
%# Movendo anche a lor danno il mondo intero.”
\livretPers Ozia
%# Oh eterna verità, come trionfi
%# Anche in bocca a' nemici!
\livretPers Achior
%# Arse Oloferne
%# Di rabbia a' detti miei. Da sé mi scaccia,
%# In Betulia m'invia;
%# E qui l'empio minaccia
%# Oggi alle strage vostre unir la mia.
\livretPers Ozia
%# Costui dunque si fida
%# Tanto del suo poter?
\livretPers Amital
%# Dunque ha costui
%# Si poca umanità?
\livretPers Achior
%# Non vede il sole
%# Anima più superba,
%# Più fiero cor. Son tali
%# I moti, i detti suoi,
%# Che trema il più costante in faccia a lui.

\livretRef#'AOaria
\livretPiece ARIA
\livretPers Achior
%# Terribile d'aspetto,
%# Barbaro di costumi,
%# O conta sé fra’ numi
%# O nume alcun non ha.
%# Fasto, furor, dispetto
%# Sempre dagli occhi spira;
%# E quanto è pronto all'ira,
%# È tardo alla pietà.

\livretRef#'APrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Ti consola, Achior. Quel Dio, di cui
%# Predicasti il poter, l'empie minacce
%# Torcerà su l'autor. Né a caso il Cielo
%# Ti conduce fra noi. Tu de' nemici
%# Potrai svelar…
\livretPers Cabri
%# Torna Giuditta.
\livretPers Ozia
%# Ognuno
%# S'allontani da me. Conviene, o prence,
%# Differir le richieste. Al mio soggiorno
%# Conducetelo, o servi: anch'io fra poco
%# A te verrò. Vanne, Achiorre, e credi
%# Che in me, lungi da' tuoi,
%# L'amico, il padre, il difensore avrai.
\livretPers Achior
%# Ospite sì pietoso io non sperai.
\livretDidasPPage (Entra Giuditta.)
\livretPers Ozia
%# Sei pur Giuditta, o la dubbiosa luce
%# Mi confonde gli oggetti?
\livretPers Giuditta
%# Io sono.
\livretPers Ozia
%# E come
%# In sì gioconde spoglie
%# Le funeste cambiasti? Il bisso e l'oro,
%# L'ostro, le gemme a che riprendi, e gli altri
%# Fregi di tua bellezza abbandonati?
%# Di balsami odorati
%# Stilla il composto crin! Chi le tue gote
%# Tanto avviva e colora? I moti tuoi
%# Chi adorna oltre il costume
%# Di grazia e maestà? Chi questo accende
%# Insolito splendor nelle tue ciglia,
%# Che a rispetto costringe e a meraviglia?
\livretPers Giuditta
%# Ozìa, tramonte il sole;
%# Fa che s'apran le porte: uscir degg'io.
\livretPers Ozia
%# Uscir!
\livretPers Giuditta
%# Sì.
\livretPers Ozia
%# Ma fra l'ombre, inerme e sola
%# Così…
\livretPers Giuditta
%# Non più. Fuor che la mia seguace,
%# Altri meco non voglio.
\livretPers Ozia
%#  (Hanno i suoi detti
%# Un non so che di risoluto e grande
%# Che m'occupa, m'opprime.) Almen… Vorrei…
%# Figlia… (Chi 'l crederia! né pur ardisco…
%# Chiederle dove corra, in che si fidi.)
%# Figlia… va: Dio t'inspira; egli ti guidi.

\livretRef#'AQaria
\livretPiece ARIA
\livretPers Giuditta
%# Parto inerme, e non pavento;
%# Sola parto, e son sicura;
%# Vo per l'ombre, e orror non ho.
%# Chi m'accese al gran cimento
%# M'accompagna e m'assicura;
%# L'ho nell'alma, ed io lo sento
%# Replicar che vincerò.

\livretRef#'ARcoro
\livretPiece CORO
\livretPersDidas Coro (in lontano)
%# Oh prodigio! Oh stupor! Privata assume
%# Delle pubbliche cure
%# Donna imbelle il pensier! Con chi governa
%# Non divide i consigli! A' rischi esposta
%# Imprudente non sembra! Orna con tanto
%# Studio se stessa; e non risveglia un solo
%# Dubbio di sua virtù! Nulla promette,
%# E fa tutto sperar! Qual fra' viventi
%# Può l'Autore ignorar di tai portenti?
\column-break
