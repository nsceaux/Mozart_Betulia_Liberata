\notesSection "Libretto"
\markuplist\page-columns-title \act\line { LIBRETTO } {
\livretAct PARTE PRIMA
\livretDescAtt\wordwrap-center {
  Ozìa, Amital, Cabri e Coro
}
\livretRef#'ABrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Popoli di Betulia, ah qual v'ingombra }
\livretVerse#12 { Vergognosa viltà! Pallidi, affliti, }
\livretVerse#12 { Tutti mi siete intorno! E ver, ne stringe }
\livretVerse#12 { D'assedio pertinace il campo assiro: }
\livretVerse#12 { Ma non siam vinti ancor. Dunque sì presto }
\livretVerse#12 { Cedete alle sventure? Io, più di loro, }
\livretVerse#12 { Temo il vostro timor. De' nostri mali }
\livretVerse#12 { Questo, questo è il peggior: questo ci rende }
\livretVerse#12 { Inabili a' ripari. Ogni tempesta }
\livretVerse#12 { Al nocchier che dispera }
\livretVerse#12 { E tempesta fatal, benché leggiera. }

\livretRef#'ACaria
\livretPiece ARIA
\livretPers Ozia
\livretVerse#12 { D'ogni colpa la colpa maggiore }
\livretVerse#12 { E l'eccesso d'un empio timore, }
\livretVerse#12 { Oltraggioso all'eterna pietà. }
\livretVerse#12 { Chi dispera, non ama, non crede: }
\livretVerse#12 { Ché la fede, l'amore, la speme }
\livretVerse#12 { Son tre faci che splendono insieme, }
\livretVerse#12 { Né una ha luce se l'altra non l'ha. }

\livretRef#'ADrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
\livretVerse#12 { E in che sperar? }
\livretPers Amital
\livretVerse#12 { Nella difesa forse }
\livretVerse#12 { Di nostre schiere indebolite e sceme }
\livretVerse#12 { Dall'assidua fatica? estenuate }
\livretVerse#12 { Dallo scarso alimento? intimorite }
\livretVerse#12 { Dal pianto universal? Fidar possiamo }
\livretVerse#12 { Ne' vicini già vinti? }
\livretVerse#12 { Negli amici impotenti? in Dio sdegnato? }
\livretPers Cabri
\livretVerse#12 { Scorri per ogni lato }
\livretVerse#12 { La misera città; non troverai }
\livretVerse#12 { Che oggetti di terror. Gli ordini usati }
\livretVerse#12 { Son negletti o confusi. Altri s'adira }
\livretVerse#12 { Contro il Ciel, contro te; piangendo accusa }
\livretVerse#12 { Altri le proprie colpe antiche e nuove; }
\livretVerse#12 { Chi corre, e non sa dove; }
\livretVerse#12 { Chi geme, e non favella; e lo spavento }
\livretVerse#12 { Come in arida selva appresa fiamma }
\livretVerse#12 { Si comunica e cresce. Ognun si crede }
\livretVerse#12 { Presso a morir. Già ne' congedi estremi }
\livretVerse#12 { Si abbracciano a vicenda }
\livretVerse#12 { I congiunti, gli amici; ed è deriso }
\livretVerse#12 { Chi ostenta ancor qualche fermezza in viso. }

\livretRef#'AEaria
\livretPiece ARIA
\livretPers Cabri
\livretVerse#12 { Ma qual virtù non cede }
\livretVerse#12 { Fra tanti oggetti e tanti }
\livretVerse#12 { Ad avvilir bastanti }
\livretVerse#12 { li più feroce cor? }
\livretVerse#12 { Se non volendo ancora }
\livretVerse#12 { Si piange agli altrui pianti, }
\livretVerse#12 { Se impallidir talora }
\livretVerse#12 { Ci fa l'altrui pallor? }

\livretRef#'AFrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Già le memorie antiche }
\livretVerse#12 { Dunque andaro in oblio? Che ingrata è questa }
\livretVerse#12 { Dimenticanza, o figli! Ah ci sovvenga }
\livretVerse#12 { Chi siam, qual Dio n'assiste, e quanti e quali }
\livretVerse#12 { Prodigi oprò per noi. Chi a' passi nostri }
\livretVerse#12 { Divise l'Eritreo, chi l'onde amare }
\livretVerse#12 { Ne raddolcì, negli aridi macigni }
\livretVerse#12 { Chi di limpidi umori }
\livretVerse#12 { Ampie vene ci aperse, e chi per tante }
\livretVerse#12 { Ignote solitudine infeconde }
\livretVerse#12 { Ci guidò, ci nutrì, potremo adesso }
\livretVerse#12 { Temer che ne abbandoni? Ah no. Minaccia }
\livretVerse#12 { Il superbo Oloferne }
\livretVerse#12 { Già da lunga stagion Betulia; e pure }
\livretVerse#12 { Non ardisce assalirla. Eccovi un segno }
\livretVerse#12 { Del celeste favor. }
\livretPers Cabri
\livretVerse#12 { Sì; ma frattanto }
\livretVerse#12 { Più crudelmente il condottier feroce }
\livretVerse#12 { Ne distrugge sedendo. I fonti, ond'ebbe }
\livretVerse#12 { La città, già felice, acque opportune, }
\livretVerse#12 { Il tiranno occupò. L'onda che resta, }
\livretVerse#12 { A misura fra noi }
\livretVerse#12 { Scarsamente si parte; onde la sete }
\livretVerse#12 { Irrìta e non appaga, }
\livretVerse#12 { Nutrisce e non estingue. }
\livretPers Amital
\livretVerse#12 { A tal nemico, }
\livretVerse#12 { Che per le nostre vene }
\livretVerse#12 { Si pasce, si diffonde, ah con qual'armi }
\livretVerse#12 { Resisterem? Guardaci in volto; osserva }
\livretVerse#12 { A qual segno siam giunti. Alle querele }
\livretVerse#12 { Abili ormai non sono i petti stanchi }
\livretVerse#12 { Dal frequente anelar, le scabre lingue, }
\livretVerse#12 { Le fauci inaridite. Umore al pianto }
\livretVerse#12 { Manca su gli occhi nostri, e cresce sempre }
\livretVerse#12 { Di pianger la cagion. Né il mal più grande }
\livretVerse#12 { Per me, che madre sono, }
\livretVerse#12 { E la propria miseria; i figli, i figli }
\livretVerse#12 { Vedermi, oh Dio! miseramente intorno }
\livretVerse#12 { Languir così, né dal mortale ardore }
\livretVerse#12 { Poterli ristorar; questa è la pena }
\livretVerse#12 { Che paragon non ha, che non s'intende }
\livretVerse#12 { Da chi madre non è. Sentimi, Ozìa; }
\livretVerse#12 { Tu sei, tu che ne reggi, }
\livretVerse#12 { Delle miserie nostre }
\livretVerse#12 { La primiera cagione. Iddio ne sia }
\livretVerse#12 { Fra noi giudice e te. Parlar di pace }
\livretVerse#12 { Con l'Assiro non vuoi; perir ci vedi }
\livretVerse#12 { Fra cento affanni e cento; }
\livretVerse#12 { E dormi? e siedi irresoluto e lento? }

\livretRef#'AGaria
\livretPiece ARIA
\livretPers Amital
\livretVerse#12 { Non hai cor, se in mezzo a questi }
\livretVerse#12 { Miserabili lamenti }
\livretVerse#12 { Non ti scuoti, non ti desti, }
\livretVerse#12 { Non ti senti intenerir. }
\livretVerse#12 { Quanto, oh Dio, siamo infelici }
\livretVerse#12 { Se sapessero i nemici, }
\livretVerse#12 { Anche a lor di pianto il ciglio }
\livretVerse#12 { Si vedrebbe inumidir. }

\livretRef#'AHrecitativo
\livretPiece RECITATIVO
\livretPers Ozìa
\livretVerse#12 { E qual pace sperate }
\livretVerse#12 { Da gente senza legge e senza fede, }
\livretVerse#12 { Nemica a nostro Dio? }
\livretPers Amital
\livretVerse#12 { Sempre fia meglio }
\livretVerse#12 { Benedirlo viventi, }
\livretVerse#12 { Che in obbrobrio alle genti }
\livretVerse#12 { Morir, vedendo ed i consorti e i figli }
\livretVerse#12 { Spirar su gli occhi nostri. }
\livretPers Ozìa
\livretVerse#12 { E se né pure }
\livretVerse#12 { Questa misera vita a voi lasciasse }
\livretVerse#12 { La perfidia nemica? }
\livretPers Amital
\livretVerse#12 { Il ferro almeno }
\livretVerse#12 { Sollecito ne uccida, e non la sete }
\livretVerse#12 { Con sì lungo morir. Deh Ozìa, per quanto }
\livretVerse#12 { Han di sacro e di grande e terra e cielo, }
\livretVerse#12 { Per lui, ch'or ne punisce, }
\livretVerse#12 { Gran Dio de' padri nostri, all'armi assire }
\livretVerse#12 { Rendasi la città. }
\livretPers Ozia
\livretVerse#12 { Figli, che dite! }
\livretPers Amital
\livretVerse#12 { Sì, sì, Betulia intera }
\livretVerse#12 { Parla per bocca mia. S'apran le porte, }
\livretVerse#12 { Alla forza si ceda: uniti insieme }
\livretVerse#12 { Volontari corriamo }
\livretVerse#12 { Al campo d'Oloferne. Unico scampo }
\livretVerse#12 { E questo; ognun lo chiede. }
\livretPers Coro
\livretVerse#12 { Al campo, al campo! }
\livretPers Ozia
\livretVerse#12 { Fermatevi, sentite. (Eterno Dio, }
\livretVerse#12 { Assistenza, consiglio!) Io non m'oppongo, }
\livretVerse#12 { Figli, al vostro pensier: chiedo che solo }
\livretVerse#12 { Differirlo vi piaccia, e più non chiedo }
\livretVerse#12 { Che cinque dì. Prendete ardir. Frattanto }
\livretVerse#12 { Forse Dio placherassi, e del suo nome }
\livretVerse#12 { La gloria sosterrà. Se giunge poi }
\livretVerse#12 { Senza speme per noi la quinta aurora, }
\livretVerse#12 { S'apra allor la città, rendasi allora. }
\livretPers Amital
\livretVerse#12 { A questa legge attenderemo. }
\livretPers Ozia
\livretVerse#12 { Or voi }
\livretVerse#12 { Co' vostri accompagnate }
\livretVerse#12 { Questi che al Ciel fervidi prieghi invio, }
\livretVerse#12 { Nunzi fedeli in fra' mortali e Dio. }

\livretRef#'AIcoro
\livretPiece ARIA CON CORO
\livretPers Ozia
\livretVerse#12 { Pietà, se irato sei, }
\livretVerse#12 { Pietà, Signor, di noi: }
\livretVerse#12 { Abbian castigo i rei, }
\livretVerse#12 { Ma l'abbiano da te. }
\livretPers Coro
\livretVerse#12 { Abbian castigo o rei, }
\livretVerse#12 { Ma l'abbiano da te. }
\livretPers Ozia
\livretVerse#12 { Se oppresso chi t'adora }
\livretVerse#12 { Soffri da chi t'ignora, }
\livretVerse#12 { Gli empii diranno poi: }
\livretVerse#12 { „Questo lor Dio dov'è?“ }
\livretPers Coro
\livretVerse#12 { Gli empii diranno poi: }
\livretVerse#12 { „Questo lor Dio dov'è?“ }

\livretRef#'AJArecitativo
\livretPiece RECITATIVO
\livretPers CABRI
\livretVerse#12 { Chi è costei, che qual sorgente aurora }
\livretVerse#12 { S'appressa a noi; terribile d'aspetto }
\livretVerse#12 { Qual falange ordinata; e a paragone }
\livretVerse#12 { Della luna, del sol bella ed eletta? }
\livretPers Amital
\livretVerse#12 { Alla chioma negletta, }
\livretVerse#12 { Al rozzo manto, alle dimesse ciglia, }
\livretVerse#12 { Di Merari è la figlia. }
\livretPers Ozia
\livretVerse#12 { Giuditta! }
\livretPers Cabri
\livretVerse#12 { Sì, la fida }
\livretVerse#12 { Vedova di Manasse. }
\livretPers Ozia
\livretVerse#12 { Qual mai cagion la trasse }
\livretVerse#12 { Dal segreto soggiorno in cui s'asconde, }
\livretVerse#12 { Volge il quart'anno ormai? }
\livretPers Amital
\livretVerse#12 { So ch'ivi orando }
\livretVerse#12 { Passa desta le notti. }
\livretVerse#12 { Digiuna i dì: so che donolle il Cielo }
\livretVerse#12 { E ricchezza e beltà, ma che disprezza }
\livretVerse#12 { La beltà, la ricchezza; e tal divenne, }
\livretVerse#12 { Che ritrovar non spera }
\livretVerse#12 { In lei macchia l'invidia o finta o vera. }
\livretVerse#12 { Ma non saprei… }
\livretDidasPPage (Entra Giuditta.)
\livretPers Giuditta
\livretVerse#12 { Che ascolto, Ozìa! }
\livretRef#'AJBrecitativo
\livretVerse#12 { Betulia, aimè, che ascolto! All'armi assire }
\livretVerse#12 { Dunque aprirem le porte, ove non giunga }
\livretVerse#12 { Soccorso in cinque dì! Miseri! E questa }
\livretVerse#12 { E la via d'impetrarlo? Ah tutti siete }
\livretVerse#12 { Colpevoli egualmente. Ad un estremo }
\livretVerse#12 { li popolo trascorse: e chi lo regge }
\livretVerse#12 { Nell'altro ruinò. Quello dispera }
\livretVerse#12 { Della pietà divina: ardisce questo }
\livretVerse#12 { Limitarle i confini. Il primo è vile, }
\livretVerse#12 { Temerario il secondo. A chi la speme, }
\livretVerse#12 { A chi manca il timor: }
\livretVerse#12 { Né in questo o in quella }
\livretVerse#12 { Misura si serbò. Vizio ed eccesso }
\livretVerse#12 { Non è diverso. Alla virtù prescritti }
\livretVerse#12 { Sono i certi confini; e cade ognuno, }
\livretVerse#12 { Che per qualunque via da lor si scosta, }
\livretVerse#12 { In colpa equal, benche talvolta opposta. }

\livretPers Giuditta
\livretRef#'AKaria
\livretVerse#12 { Del pari infeconda }
\livretVerse#12 { D'un fiume è la sponda }
\livretVerse#12 { Se torbido eccede, }
\livretVerse#12 { Se manca d'umor. }
\livretVerse#12 { Si acquista baldanza }
\livretVerse#12 { Per troppa speranza, }
\livretVerse#12 { Si perde la fede }
\livretVerse#12 { Per troppo timor. }

\livretRef#'ALrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Oh saggia, oh santa, oh eccelsa donna! Iddio }
\livretVerse#12 { Anima i labbri tuoi. }
\livretPers Cabri
\livretVerse#12 { Da tali accuse }
\livretVerse#12 { Chi si può discolpar? }
\livretPers Ozia
\livretVerse#12 { Deh tu, che sei }
\livretVerse#12 { Cara al Signor, per noi perdono implora; }
\livretVerse#12 { Ne guida, ne consiglia. }
\livretPers Giuditta
\livretVerse#12 { In Dio sperate }
\livretVerse#12 { Soffrendo i vostri mali. Egli in tal guisa }
\livretVerse#12 { Corregge, e non opprime; ei de' più cari }
\livretVerse#12 { Così prova la fede: e Abramo e Isacco }
\livretVerse#12 { E Giacobbe e Mosè diletti a lui }
\livretVerse#12 { Divennero così. Ma quei che osaro }
\livretVerse#12 { Oltraggiar mormorando }
\livretVerse#12 { La sua giustizia, o delle serpi il morso }
\livretVerse#12 { O il fuoco esterminò. Se in giusta lance }
\livretVerse#12 { Pesiamo il falli nostri, assai di loro }
\livretVerse#12 { E minore il castigo: onde dobbiamo }
\livretVerse#12 { Grazie a Dio, non querele. Ei ne consoli }
\livretVerse#12 { Secondo il voler suo. Gran prove io spero }
\livretVerse#12 { Della pietà di lui. Voi che diceste }
\livretVerse#12 { Che muove i labbri miei, credete ancora }
\livretVerse#12 { Ch'ei desti i miei pensieri. Un gran disegno }
\livretVerse#12 { Mi bolle in mente, e mi trasporta. Amici, }
\livretVerse#12 { Non curate saperlo. Al sol cadente }
\livretVerse#12 { Della città m'attendi, }
\livretVerse#12 { Ozìa, presso alle porte. Alla grand'opra }
\livretVerse#12 { A prepararmi io vado. Or, fin ch'io torni, }
\livretVerse#12 { Voi con prieghi sinceri }
\livretVerse#12 { Secondate divoti i miei pensieri. }

\livretRef#'AMcoro
\livretPiece ARIA CON CORO
\livretPers Ozia e Coro
\livretVerse#12 { Pietà, se irato sei, ecc. }
\livretVerse#12 { Se oppresso chi t'adora, ecc. }
\livretDidasPPage (Entrano Carmi ed Achior.)

\livretRef#'ANrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
\livretVerse#12 { Signor, Carmi a te viene. }
\livretPers Amita
\livretVerse#12 { E la commessa }
\livretVerse#12 { Custodia delle mura }
\livretVerse#12 { Abbandonò? }
\livretPers Ozia
\livretVerse#12 { Carmi, che chiedi? }
\livretPers Carmi
\livretVerse#12 { Io vengo }
\livretVerse#12 { Un prigioniero a presentarti. Avvinto }
\livretVerse#12 { Ad un tronco il lasciaro }
\livretVerse#12 { Vicino alla città le schiere ostili: }
\livretVerse#12 { Achiorre è il suo nome; }
\livretVerse#12 { Degli Ammoniti è il prence. }
\livretPers Ozia
\livretVerse#12 { E così tratta }
\livretVerse#12 { Oloferne gli amici? }
\livretPers Achior
\livretVerse#12 { E de' superbi }
\livretVerse#12 { Questo l'usato stil. Per loro è offesa }
\livretVerse#12 { Il ver che non lusinga. }
\livretPers Ozia
\livretVerse#12 { I sensi tuoi }
\livretVerse#12 { Spiega più chiari. }
\livretPers Achior
\livretVerse#12 { Ubbidirò. Sdegnando }
\livretVerse#12 { L'assiro condottier che a lui pretenda }
\livretVerse#12 { Di resister Betulia; a me richiese }
\livretVerse#12 { Di voi notizia. Io, le memorie antiche }
\livretVerse#12 { Richiamando al pensier, tutte gli esposi }
\livretVerse#12 { Del popolo d'Israele }
\livretVerse#12 { Le origini, i progressi; il culto avito }
\livretVerse#12 { De' numerosi dèi, che per un solo }
\livretVerse#12 { Cambiaro i padri vostri; i lor passaggi }
\livretVerse#12 { Dalle caldee contrade }
\livretVerse#12 { In Carra, indi in Egitto; i duri imperi }
\livretVerse#12 { Di quel barbaro re. Dissi la vostra }
\livretVerse#12 { Prodigiosa fuga, i lunghi errori, }
\livretVerse#12 { Le scorte portentose, i cibi, Tacque, }
\livretVerse#12 { Le battaglie, i trionfi; e gli mostrai }
\livretVerse#12 { Che, quando al vostro Dio foste fedeli, }
\livretVerse#12 { Sempre pugnò per voi. Conclusi al fine }
\livretVerse#12 { I miei detti così: “Cerchiam se questi }
\livretVerse#12 { Al lor Dio son infidi; e se lo sono, }
\livretVerse#12 { La vittoria è per noi. Ma se non hanno }
\livretVerse#12 { Delitto innanzi a lui, no, non la spero, }
\livretVerse#12 { Movendo anche a lor danno il mondo intero.” }
\livretPers Ozia
\livretVerse#12 { Oh eterna verità, come trionfi }
\livretVerse#12 { Anche in bocca a' nemici! }
\livretPers Achior
\livretVerse#12 { Arse Oloferne }
\livretVerse#12 { Di rabbia a' detti miei. Da sé mi scaccia, }
\livretVerse#12 { In Betulia m'invia; }
\livretVerse#12 { E qui l'empio minaccia }
\livretVerse#12 { Oggi alle strage vostre unir la mia. }
\livretPers Ozia
\livretVerse#12 { Costui dunque si fida }
\livretVerse#12 { Tanto del suo poter? }
\livretPers Amital
\livretVerse#12 { Dunque ha costui }
\livretVerse#12 { Si poca umanità? }
\livretPers Achior
\livretVerse#12 { Non vede il sole }
\livretVerse#12 { Anima più superba, }
\livretVerse#12 { Più fiero cor. Son tali }
\livretVerse#12 { I moti, i detti suoi, }
\livretVerse#12 { Che trema il più costante in faccia a lui. }

\livretRef#'AOaria
\livretPiece ARIA
\livretPers Achior
\livretVerse#12 { Terribile d'aspetto, }
\livretVerse#12 { Barbaro di costumi, }
\livretVerse#12 { O conta sé fra’ numi }
\livretVerse#12 { O nume alcun non ha. }
\livretVerse#12 { Fasto, furor, dispetto }
\livretVerse#12 { Sempre dagli occhi spira; }
\livretVerse#12 { E quanto è pronto all'ira, }
\livretVerse#12 { È tardo alla pietà. }

\livretRef#'APrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Ti consola, Achior. Quel Dio, di cui }
\livretVerse#12 { Predicasti il poter, l'empie minacce }
\livretVerse#12 { Torcerà su l'autor. Né a caso il Cielo }
\livretVerse#12 { Ti conduce fra noi. Tu de' nemici }
\livretVerse#12 { Potrai svelar… }
\livretPers Cabri
\livretVerse#12 { Torna Giuditta. }
\livretPers Ozia
\livretVerse#12 { Ognuno }
\livretVerse#12 { S'allontani da me. Conviene, o prence, }
\livretVerse#12 { Differir le richieste. Al mio soggiorno }
\livretVerse#12 { Conducetelo, o servi: anch'io fra poco }
\livretVerse#12 { A te verrò. Vanne, Achiorre, e credi }
\livretVerse#12 { Che in me, lungi da' tuoi, }
\livretVerse#12 { L'amico, il padre, il difensore avrai. }
\livretPers Achior
\livretVerse#12 { Ospite sì pietoso io non sperai. }
\livretDidasPPage (Entra Giuditta.)
\livretPers Ozia
\livretVerse#12 { Sei pur Giuditta, o la dubbiosa luce }
\livretVerse#12 { Mi confonde gli oggetti? }
\livretPers Giuditta
\livretVerse#12 { Io sono. }
\livretPers Ozia
\livretVerse#12 { E come }
\livretVerse#12 { In sì gioconde spoglie }
\livretVerse#12 { Le funeste cambiasti? Il bisso e l'oro, }
\livretVerse#12 { L'ostro, le gemme a che riprendi, e gli altri }
\livretVerse#12 { Fregi di tua bellezza abbandonati? }
\livretVerse#12 { Di balsami odorati }
\livretVerse#12 { Stilla il composto crin! Chi le tue gote }
\livretVerse#12 { Tanto avviva e colora? I moti tuoi }
\livretVerse#12 { Chi adorna oltre il costume }
\livretVerse#12 { Di grazia e maestà? Chi questo accende }
\livretVerse#12 { Insolito splendor nelle tue ciglia, }
\livretVerse#12 { Che a rispetto costringe e a meraviglia? }
\livretPers Giuditta
\livretVerse#12 { Ozìa, tramonte il sole; }
\livretVerse#12 { Fa che s'apran le porte: uscir degg'io. }
\livretPers Ozia
\livretVerse#12 { Uscir! }
\livretPers Giuditta
\livretVerse#12 { Sì. }
\livretPers Ozia
\livretVerse#12 { Ma fra l'ombre, inerme e sola }
\livretVerse#12 { Così… }
\livretPers Giuditta
\livretVerse#12 { Non più. Fuor che la mia seguace, }
\livretVerse#12 { Altri meco non voglio. }
\livretPers Ozia
\livretVerse#12 { (Hanno i suoi detti }
\livretVerse#12 { Un non so che di risoluto e grande }
\livretVerse#12 { Che m'occupa, m'opprime.) Almen… Vorrei… }
\livretVerse#12 { Figlia… (Chi 'l crederia! né pur ardisco… }
\livretVerse#12 { Chiederle dove corra, in che si fidi.) }
\livretVerse#12 { Figlia… va: Dio t'inspira; egli ti guidi. }

\livretRef#'AQaria
\livretPiece ARIA
\livretPers Giuditta
\livretVerse#12 { Parto inerme, e non pavento; }
\livretVerse#12 { Sola parto, e son sicura; }
\livretVerse#12 { Vo per l'ombre, e orror non ho. }
\livretVerse#12 { Chi m'accese al gran cimento }
\livretVerse#12 { M'accompagna e m'assicura; }
\livretVerse#12 { L'ho nell'alma, ed io lo sento }
\livretVerse#12 { Replicar che vincerò. }

\livretRef#'ARcoro
\livretPiece CORO
\livretPersDidas Coro (in lontano)
\livretVerse#12 { Oh prodigio! Oh stupor! Privata assume }
\livretVerse#12 { Delle pubbliche cure }
\livretVerse#12 { Donna imbelle il pensier! Con chi governa }
\livretVerse#12 { Non divide i consigli! A' rischi esposta }
\livretVerse#12 { Imprudente non sembra! Orna con tanto }
\livretVerse#12 { Studio se stessa; e non risveglia un solo }
\livretVerse#12 { Dubbio di sua virtù! Nulla promette, }
\livretVerse#12 { E fa tutto sperar! Qual fra' viventi }
\livretVerse#12 { Può l'Autore ignorar di tai portenti? }
\column-break
\livretAct PARTE SECONDA
\livretDescAtt\wordwrap-center {
  Ozìa ed Achior.
}
\livretRef#'BArecitativo
\livretPiece RECITATIVO
\livretPers Achior
\livretVerse#12 { Troppo mal corrisponde (Ozia, perdona) }
\livretVerse#12 { A' tuoi dolci costumi }
\livretVerse#12 { Tal disprezzo ostentar de' nostri numi. }
\livretVerse#12 { Io così, tu lo sai, }
\livretVerse#12 { Del tuo Dio non parlai. }
\livretPers Ozia
\livretVerse#12 { Principe, è zelo }
\livretVerse#12 { Quel che chiami rozzezza. In te conobbi }
\livretVerse#12 { Chiari semi del vero; e m'affatico }
\livretVerse#12 { A farli germogliar. }
\livretPers Achior
\livretVerse#12 { Ma non ti basta }
\livretVerse#12 { Ch'io veneri il tuo Dio? }
\livretPers Ozia
\livretVerse#12 { No: confessarlo }
\livretVerse#12 { Unico per essenza }
\livretVerse#12 { Debbe ciascuno, ed adorarlo solo. }
\livretPers Achior
\livretVerse#12 { Ma chi solo l'afferma? }
\livretPers Ozia
\livretVerse#12 { Il venerato }
\livretVerse#12 { Consenso d'ogni età; degli avi nostri }
\livretVerse#12 { La fida autorità; l'istesso Dio }
\livretVerse#12 { Di cui predicasti }
\livretVerse#12 { I prodigi, il poter; chi di sua bocca }
\livretVerse#12 { Lo palesò; che, quando }
\livretVerse#12 { Se medesimo descrisse, }
\livretVerse#12 { Disse; ”Io son quel che sono”; e tutto disse. }
\livretPers Achior
\livretVerse#12 { L'autorità de' tuoi produci in vano }
\livretVerse#12 { Con me nemico. }
\livretPers Ozia
\livretVerse#12 { E ben, con te nemico }
\livretVerse#12 { L'autorità non vaglia. Uom però sei; }
\livretVerse#12 { La ragion ti convinca. A me rispondi }
\livretVerse#12 { Con animo tranquillo, il ver si cerchi, }
\livretVerse#12 { Non la vittoria. }
\livretPers Achior
\livretVerse#12 { Io già t'ascolto. }
\livretPers Ozia
\livretVerse#12 { Or dimmi: }
\livretVerse#12 { Credi, Achior, che possa }
\livretVerse#12 { Cosa alcuna prodursi }
\livretVerse#12 { Senza la sua cagion? }
\livretPers Achior
\livretVerse#12 { No. }
\livretPers Ozia
\livretVerse#12 { D'una in altra }
\livretVerse#12 { Passando col pensier, non ti riduci }
\livretVerse#12 { Qualche cagione a confessar, da cui }
\livretVerse#12 { Tutte dipendan l'altre? }
\livretPers Achior
\livretVerse#12 { E ciò dimostra }
\livretVerse#12 { Che v'è Dio, non che è solo. }
\livretVerse#12 { Esser non ponno Queste prime cagioni i nostri dèi? }
\livretPers Ozia
\livretVerse#12 { Quali dèi, caro prence? I tronchi, i marmi }
\livretVerse#12 { Sculti da voi? }
\livretPers Achior
\livretVerse#12 { Ma se que' marmi a' saggi }
\livretVerse#12 { Fosser simboli sol delle immortali }
\livretVerse#12 { Essenze creatrici, ancor diresti }
\livretVerse#12 { Che i miei dèi non son dèi? }
\livretPers Ozìa
\livretVerse#12 { Sì, perché molti. }
\livretPers Achior
\livretVerse#12 { Io ripugnanza alcuna }
\livretVerse#12 { Nel numero non veggo. }
\livretPers Ozìa
\livretVerse#12 { Eccola. Un Dio }
\livretVerse#12 { Concepir non poss'io, }
\livretVerse#12 { Se perfetto non è. }
\livretPers Achior
\livretVerse#12 { Giusto è il concetto }
\livretPers Ozìa
\livretVerse#12 { Quando dissi perfetto, }
\livretVerse#12 { Dissi infinito ancor. }
\livretPers Achior
\livretVerse#12 { L'un l'altro include: }
\livretVerse#12 { Non si dà chi l'ignori. }
\livretPers Ozìa
\livretVerse#12 { Ma l'essenze che adori, }
\livretVerse#12 { Se non più, son distinte; e se distinte, }
\livretVerse#12 { Han confini fra lor. Dir dunque déi }
\livretVerse#12 { Che ha confino l'infinito, o non son dèi. }
\livretPers Achior
\livretVerse#12 { Da questi lacci, in cui }
\livretVerse#12 { M'implicà il tuo parlar, cedasi al vero. }
\livretVerse#12 { Disciogliermi non so: ma non per questo }
\livretVerse#12 { Persuaso son io. D'arte ti cedo, }
\livretVerse#12 { Non di ragione. E abbandonar non voglio }
\livretVerse#12 { Gli dèi che adoro e vedo, }
\livretVerse#12 { Per un dio che non posso }
\livretVerse#12 { Né pure immaginar. }
\livretPers Ozia
\livretVerse#12 { S'egli capisse }
\livretVerse#12 { Nel nostro immaginar, Dio non sarebbe. }
\livretVerse#12 { Chi potrà figurarlo? Egli di parti, }
\livretVerse#12 { Come il corpo, non consta; egli in affetti, }
\livretVerse#12 { Come l'anime nostre, }
\livretVerse#12 { Non è distinto; ei non soggiace a forma, }
\livretVerse#12 { Come tutto il creato; e se gli assegni }
\livretVerse#12 { Parti, affetti, figura, il circonscrivi, }
\livretVerse#12 { Perfezion gli togli. }
\livretPers Achior
\livretVerse#12 { E quando il chiami }
\livretVerse#12 { Tu stesso e buono e grande, }
\livretVerse#12 { Nol circonscrivi allor? }
\livretPers Ozìa
\livretVerse#12 { No; buono il credo, }
\livretVerse#12 { Ma senza qualità; grande, ma senza }
\livretVerse#12 { Quantità, né misura; ognor presente, }
\livretVerse#12 { Senza sito o confine; e se in tal guisa }
\livretVerse#12 { Qual sia non spiego, almen di lui non formo }
\livretVerse#12 { Un'idea che l'oltraggi. }
\livretPers Achior
\livretVerse#12 { E dunque vano }
\livretVerse#12 { Lo sperar di vederlo. }
\livretPers Ozìa
\livretVerse#12 { Un dì potresti }
\livretVerse#12 { Meglio fissarti in lui: ma puoi frattanto }
\livretVerse#12 { Vederlo ovunque vuoi. }
\livretPers Achior
\livretVerse#12 { Vederlo! E come, }
\livretVerse#12 { Se immaginar nol so? }
\livretPers Ozia
\livretVerse#12 { Come nel sole }
\livretVerse#12 { A fissar le pupille in vano aspiri, }
\livretVerse#12 { E pur sempre e per tutto il sol rimiri. }

\livretRef#'BBaria
\livretPiece ARIA
\livretPers OZIA
\livretVerse#12 { Se Dio veder tu vuoi, }
\livretVerse#12 { Guardalo in ogni oggetto; }
\livretVerse#12 { Cercalo nel tuo petto, }
\livretVerse#12 { Lo troverai con te }
\livretVerse#12 { E se dov'ei dimora }
\livretVerse#12 { Non intendesti ancor, }
\livretVerse#12 { Confondimi, se puoi; }
\livretVerse#12 { Dimmi, dov'ei non è. }

\livretRef#'BCrecitativo
\livretPiece RECITATIVO
\livretPers Achior
\livretVerse#12 { Confuso io son; sento sedurmi, e pure }
\livretVerse#12 { Ritorno a dubitar. }
\livretPers Ozia
\livretVerse#12 { Quando il costume }
\livretVerse#12 { Alla ragion contrasta, }
\livretVerse#12 { Avvien così. Tal di negletta cetra }
\livretVerse#12 { Musica man le abbandonate corde }
\livretVerse#12 { Stenta a temprar, perché vibrate appena }
\livretVerse#12 { Si rallentan di nuovo. }
\livretDidasPPage (Entra Amital.)
\livretPers Amital
\livretVerse#12 { Ah dimmi, Ozia, }
\livretVerse#12 { Che si fa, che si pensa? Io non intendo }
\livretVerse#12 { Che voglia dir questo silenzio estremo }
\livretVerse#12 { A cui passò Betulia }
\livretVerse#12 { Dall'estremo tumulto. Il nostro stato }
\livretVerse#12 { Punto non migliorò. Crescono i mali, }
\livretVerse#12 { E sceman le querele. Ognun chiedea }
\livretVerse#12 { Ieri aita e pietà: stupido ognuno }
\livretVerse#12 { Oggi passa, e non parla. Ah parmi questo }
\livretVerse#12 { Un presagio per noi troppo funesto! }

\livretRef#'BDaria
\livretPiece ARIA
\livretPers Amital
\livretVerse#12 { Quel nocchier che in gran procella }
\livretVerse#12 { Non s'affanna e non favella, }
\livretVerse#12 { E vicino a naufragar }
\livretVerse#12 { E vicino all'ore estreme }
\livretVerse#12 { Quell'infermo che non geme }
\livretVerse#12 { E ha cagion di sospirar. }

\livretRef#'BEArecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Lungamente non dura }
\livretVerse#12 { Eccessivo dolor. Ciascuno a' mali }
\livretVerse#12 { O cede o s'accostuma. Il nostro stato }
\livretVerse#12 { Non è però senza speranza. }
\livretPers Amital
\livretVerse#12 { Intendo: }
\livretVerse#12 { Tu in Giuditta confidi. Ah questa parmi }
\livretVerse#12 { Troppo folle lusinga! }
\livretPersDidas Coro (in lontano)
\livretVerse#12 { All'armi, all'armi! }
\livretPers Ozia
\livretVerse#12 { Quai grida! }
\livretPersDidas Cabri (entrando)
\livretVerse#12 { Accorri, Ozìa. Senti il tumulto }
\livretVerse#12 { Che fra' nostri guerrieri }
\livretVerse#12 { Là si destò presso alle porte? }
\livretPers Ozia
\livretVerse#12 { E quale }
\livretVerse#12 { N'è la cagion? }
\livretPers Cabri
\livretVerse#12 { Chi sà? }
\livretPers Amital
\livretVerse#12 { Miseri noi! }
\livretVerse#12 { Saran giunti i nemici. }
\livretPers Ozia
\livretVerse#12 { Corrasi ad osservar. }
\livretDidasPPage (Entra Giuditta.)
\livretPers Giuditta
\livretVerse#12 { Fermate, amici. }
\livretPers Ozia
\livretVerse#12 { Giuditta! }
\livretPers Amital
\livretVerse#12 { Eterno Dio! }
\livretPers Giuditta
\livretVerse#12 { Lodiam, compagni, }
\livretVerse#12 { Lodiamo il Signor nostro. Ecco adempite }
\livretVerse#12 { Le sue promesse: ei per mia man trionfa; }
\livretVerse#12 { La nostra fede egli premiò. }
\livretPers Ozia
\livretVerse#12 { Ma questo }
\livretVerse#12 { Improvviso tumulto… }
\livretPers Giuditta
\livretVerse#12 { Io lo destai; }
\livretVerse#12 { Non vi turbi. A momenti }
\livretVerse#12 { Ne udirete gli effetti. }
\livretPers Amital
\livretVerse#12 { E se frattanto }
\livretVerse#12 { Oloferne… }
\livretPers Giuditta
\livretVerse#12 { Oloferne }
\livretVerse#12 { Già svenato morì. }
\livretPers Amital
\livretVerse#12 { Che dici mai! }
\livretPers Achior
\livretVerse#12 { Chi ha svenato Oloferne? }
\livretPers Giuditta
\livretVerse#12 { Io lo svenai }
\livretPers Ozia
\livretVerse#12 { Tu stessa! }
\livretPers Achior
\livretVerse#12 { E quando? }
\livretPers Amital
\livretVerse#12 { E come? }
\livretPers Giuditta
\livretRef#'BEBrecitativo
\livretVerse#12 { Udite. Appena }
\livretVerse#12 { Da Betulia partii, che m'arrestaro }
\livretVerse#12 { Le guardie ostili. Ad Oloferne innanzi }
\livretVerse#12 { Son guidata da loro. Egli mi chiede }
\livretVerse#12 { A che vengo e chi son. Parte io gli scopro, }
\livretVerse#12 { Taccio parte del vero. Ei non intende, }
\livretVerse#12 { E approva i detti miei. Pietoso, umano }
\livretVerse#12 { (Ma straniera in quel volto }
\livretVerse#12 { Mi parve la pietà) m'ode, m'accoglie, }
\livretVerse#12 { M'applaude, mi consola. A lieta cena }
\livretVerse#12 { Seco mi vuol. Già su le mense elette }
\livretVerse#12 { Fumano i vasi d'or; già vuota il folle }
\livretVerse#12 { Fra' cibi ad or ad or tazze frequenti }
\livretVerse#12 { Di licor generoso: e a poco a poco }
\livretVerse#12 { Comincia a vacillar. Molti ministri }
\livretVerse#12 { Eran d'intorno a noi; ma ad uno ad uno }
\livretVerse#12 { Tutti si dileguar. L'ultimo d'essi }
\livretVerse#12 { Rimaneva, e il peggior. L'uscio costui }
\livretVerse#12 { Chiuse partendo, e mi lasciò con lui. }
\livretPers Amital
\livretVerse#12 { Fiero cimento! }
\livretPers Giuditta
\livretVerse#12 { Ogni cimento è lieve }
\livretVerse#12 { Ad inspirato cor. Scorza gran parte }
\livretVerse#12 { Era ormai della notte. Il campo intorno }
\livretVerse#12 { Nel sonno universal taceva oppresso. }
\livretVerse#12 { Vinto Oloferne istesso }
\livretVerse#12 { Dal vino, in cui s'immerse oltre il costume, }
\livretVerse#12 { Steso dormia su le funeste piume. }
\livretVerse#12 { Sorgo; e tacita allor colà m'appresso }
\livretVerse#12 { Dove prono ei giaceva. Rivolta al Cielo }
\livretVerse#12 { Più col cor che col labbro: “Ecco l'istante,” }
\livretVerse#12 { Dissi, “o Dio d'Israel, che un colpo solo }
\livretVerse#12 { Liberi il popol tuo. Tu 'l promettesti; }
\livretVerse#12 { In te fidato io l'intrapresi; e spero }
\livretVerse#12 { Assistenza da te.” Sciolgo, ciò detto, }
\livretVerse#12 { Da' sostegni del letto }
\livretVerse#12 { L'appeso acciar; lo snudo; il crin gli stringo }
\livretVerse#12 { Con la sinistra man; l'altra sollevo }
\livretVerse#12 { Quanto il braccio si stende; i voti a Dio }
\livretVerse#12 { Rinnovo in sì gran passo, }
\livretVerse#12 { E su l'empia cervice il colpo abbasso. }
\livretPers Ozìa
\livretVerse#12 { Oh coraggio! }
\livretPers Amital
\livretVerse#12 { Oh periglio! }
\livretPers Giuditta
\livretVerse#12 { Apre il barbaro il ciglio; e incerto ancora }
\livretVerse#12 { Fra 'l sonno e fra la morte, il ferro immerso }
\livretVerse#12 { Sentesi nella gola. Alle difese }
\livretVerse#12 { Sollevarsi procura; e gli el contende }
\livretVerse#12 { L'imprigionato crin. Ricorre a' gridi; }
\livretVerse#12 { Ma interrotte la voce }
\livretVerse#12 { Trova le vie del labbro, e si disperde. }
\livretVerse#12 { Replico il colpo: ecco l'orribil capo }
\livretVerse#12 { Dagli omeri diviso. }
\livretVerse#12 { Guizza il tronco reciso }
\livretVerse#12 { Sul sanguigno terren; balzar mi sento }
\livretVerse#12 { Il teschio semivivo }
\livretVerse#12 { Sotte la man che il sostenea. Quel volto }
\livretVerse#12 { A un tratto scolorir; mute parole }
\livretVerse#12 { Quel labbro articolar; quei occhi intorno }
\livretVerse#12 { Cercar del sole i rai, }
\livretVerse#12 { Morire e minacciar vidi, e tremai. }
\livretPers Amital
\livretVerse#12 { Tremo in udirlo anch'io. }
\livretPers Giuditta
\livretVerse#12 { Respiro al fine; e del trionfo illustre }
\livretVerse#12 { Rendo grazie all'Autore. Svelta dal letto }
\livretVerse#12 { La superba cortina, il capo esangue }
\livretVerse#12 { Sollecita ne involgo; alla mia fida }
\livretVerse#12 { Ancella lo consegno, }
\livretVerse#12 { Che non lungi attendea; del duce estinto }
\livretVerse#12 { M'involo al padiglion; passo fra' suoi }
\livretVerse#12 { Non vista o rispettata, e torno a voi. }
\livretPers Ozìa
\livretVerse#12 { Oh prodigio! }
\livretPers Cabri
\livretVerse#12 { Oh portento! }
\livretPers Achior
\livretVerse#12 { Inerme e sola }
\livretVerse#12 { Tanto pensar, tanto eseguir potesti! }
\livretVerse#12 { E crederti degg'io? }
\livretPers Giuditta
\livretVerse#12 { Credilo a questo }
\livretVerse#12 { Ch'io scopro agli occhi tuoi, teschio reciso. }
\livretPers Achior
\livretVerse#12 { Oh spavento! È Oloferne:io lo ravviso. }
\livretPers Ozìa
\livretVerse#12 { Sostenetelo, o servi: il cor gli agghiaccia }
\livretVerse#12 { L'improviso terror. }
\livretPers Amital
\livretVerse#12 { Fugge quell'alma }
\livretVerse#12 { Per non cedere al ver. }
\livretPers Giuditta
\livretVerse#12 { Meglio di lui }
\livretVerse#12 { Giudichiamo, Amital. Forse quel velo }
\livretVerse#12 { Che gli oscurò la mente }
\livretVerse#12 { A un tratto or si squarciò. Non fugge il vero, }
\livretVerse#12 { Ma gli manca il costume }
\livretVerse#12 { L'impeto a sostener di tanto lume. }

\livretRef#'BFaria
\livretPiece ARIA
\livretPers Giuditta
\livretVerse#12 { Prigionier che fa ritorno }
\livretVerse#12 { Dagli orrori al dì sereno, }
\livretVerse#12 { Chiude i lumi a' rai del giorno, }
\livretVerse#12 { E pur tanto il sospirò. }
\livretVerse#12 { Ma così fra poco arriva }
\livretVerse#12 { A soffrir la chiara luce: }
\livretVerse#12 { Ché l'avviva e lo conduce }
\livretVerse#12 { Lo splendor che l'abbagliò. }

\livretRef#'BGrecitativo
\livretPiece RECITATIVO
\livretPers Achior
\livretVerse#12 { Giuditta, Ozìa, popoli, amici: io cedo, }
\livretVerse#12 { Vinto son io. Prende un novello aspetto }
\livretVerse#12 { Ogni cosa per me. Da quel che fui }
\livretVerse#12 { Non so chi mi trasforma: in me l'antico }
\livretVerse#12 { Achior più non trovo. Altri pensieri, }
\livretVerse#12 { Sento altre voglie in me. Tutto son pieno, }
\livretVerse#12 { Tutto, del vostro Dio. Grande, infinito, }
\livretVerse#12 { Unico lo confesso. I falsi numi }
\livretVerse#12 { Odio, detesto, e i vergognosi incensi }
\livretVerse#12 { Che lor credulo offersi. Altri non amo, }
\livretVerse#12 { Non conosco altro Dio che il Dio d'Abramo. }

\livretRef#'BHaria
\livretPiece ARIA
\livretPers Achior
\livretVerse#12 { Te solo adoro, }
\livretVerse#12 { Mente infinita, }
\livretVerse#12 { Fonte di vita, }
\livretVerse#12 { Di verità }
\livretVerse#12 { In cui si muove, }
\livretVerse#12 { Da cui dipende }
\livretVerse#12 { Quanto comprende }
\livretVerse#12 { L'eternità. }

\livretRef#'BIrecitativo
\livretPiece RECITATIVO
\livretPers Ozìa
\livretVerse#12 { Di tua vittoria un glorioso effetto }
\livretVerse#12 { Vedi, o Giuditta. }
\livretPers Amital
\livretVerse#12 { E non il solo. Anch'io }
\livretVerse#12 { Peccai; mi pento. Il mio timore offese }
\livretVerse#12 { La divina pietà. Fra' mali miei, }
\livretVerse#12 { Mio Dio, non rammentai che puoi, chi sei. }

\livretRef#'BJaria
\livretPiece ARIA
\livretPers Amital
\livretVerse#12 { Con troppa rea viltà }
\livretVerse#12 { Quest'alma ti oltraggiò, }
\livretVerse#12 { Allor che disperò }
\livretVerse#12 { Del tuo soccorso. }
\livretVerse#12 { Pietà, Signor, pietà; }
\livretVerse#12 { Giacché il pentito cor }
\livretVerse#12 { Misura il proprio error }
\livretVerse#12 { Col suo rimorso. }

\livretRef#'BKrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
\livretVerse#12 { Quanta cura hai di noi, Bontà Divina! }
\livretDidasPPage (Entra Carmi.)
\livretPers Carmi
\livretVerse#12 { Furo, o santa eroina, }
\livretVerse#12 { Veri i presagi tuoi: gli Assiri oppresse }
\livretVerse#12 { Eccidio universal. }
\livretPers Ozia
\livretVerse#12 { Forse è lusinga }
\livretVerse#12 { Del tuo desio. }
\livretPers Carmi
\livretVerse#12 { No; del felice evento }
\livretVerse#12 { Parte vid'io; da' trattenuti il resto }
\livretVerse#12 { Fuggitivi raccolsi. In su le mura, }
\livretVerse#12 { Come impose Giuditta al suo ritorno, }
\livretVerse#12 { Destai di grida e d'armi }
\livretVerse#12 { Strepitoso tumulto. }
\livretPers Amital
\livretVerse#12 { E qui s'intese }
\livretPers Carmi
\livretVerse#12 { Temon le guardie ostili }
\livretVerse#12 { D'un assalto notturno, ed Oloferne }
\livretVerse#12 { Corrono ad avvertirne. Il tronco informe }
\livretVerse#12 { Trovan colà nel proprio sangue involto: }
\livretVerse#12 { Tornan gridando indietro. II caso atroce }
\livretVerse#12 { Spargesi fra le schiere, intimorite }
\livretVerse#12 { Già da' nostri tumulti; ecco ciascuno }
\livretVerse#12 { Precipita alla fuga, e nella fuga }
\livretVerse#12 { L'un l'altro urta, impedisce. Inciampa e cade }
\livretVerse#12 { Sopra il caduto il fuggitivo: immerge }
\livretVerse#12 { Stolido in sen l'involontario acciaro }
\livretVerse#12 { Al compagno il compagno; opprime oppresso, }
\livretVerse#12 { Nel sollevar l'amico, il fide amico. }
\livretVerse#12 { Orribilmente il campo }
\livretVerse#12 { Tutto rimbomba intorno. Escon dal chiuso }
\livretVerse#12 { Spaventati i destrieri, e vanno anch'essi }
\livretVerse#12 { Calpestando per l'ombre }
\livretVerse#12 { Gli estinti, i semivivi. A' lor nitriti }
\livretVerse#12 { Miste degli empii e le bestemmie e i voti }
\livretVerse#12 { Dissipa il vento. Apre alla morte il caso }
\livretVerse#12 { Cento insolite vie. Del pari ognuno }
\livretVerse#12 { Teme, fugge, perisce; e ognun del pari }
\livretVerse#12 { Ignora in quell'orrore }
\livretVerse#12 { Di che teme, ove fugge, e perché muore. }
\livretPers Ozia
\livretVerse#12 { Oh Dio! Sogno o son desto? }
\livretPers Carmi
\livretVerse#12 { Odi, o signor, quel mormorio funesto? }

\livretRef#'BLaria
\livretPiece ARIA
\livretPers Carmi
\livretVerse#12 { Quei moti che senti }
\livretVerse#12 { Per l'orrida notte, }
\livretVerse#12 { Son queruli accenti, }
\livretVerse#12 { Son grida interrotte }
\livretVerse#12 { Che desta lontano }
\livretVerse#12 { L'insano terror. }
\livretVerse#12 { Per vincere, a noi }
\livretVerse#12 { Non restan nemici; }
\livretVerse#12 { Del ferro gli uffici }
\livretVerse#12 { Compisce il timor. }

\livretRef#'BMrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
\livretVerse#12 { Seguansi, o Carmi, i fuggitivi; e sia }
\livretVerse#12 { Il più di nostre prede }
\livretVerse#12 { Premio a Giuditta. }
\livretPers Amital
\livretVerse#12 { O generosa donna, }
\livretVerse#12 { Te sopra ogni altra Iddio }
\livretVerse#12 { Favorì, benedisse. }
\livretPers Cabri
\livretVerse#12 { In ogni etade }
\livretVerse#12 { Del tu valor si parlerà. }
\livretPers Achior
\livretVerse#12 { Tu sei }
\livretVerse#12 { La gioia d'Israele, }
\livretVerse#12 { L'onor del popol tuo… }
\livretPers Giuditta
\livretVerse#12 { Basta. Dovute }
\livretVerse#12 { Non son tai lodi a me. Dio fu la mente }
\livretVerse#12 { Che il gran colpo guidò; la mano io fui: }
\livretVerse#12 { I cantici festivi offransi a lui. }

\livretRef#'BNcoro
\livretPiece CORO
\livretPers Coro
\livretVerse#12 { Lodi al gran Dio che oppresse }
\livretVerse#12 { Gli empi nemici suoi, }
\livretVerse#12 { Che combatté per noi, }
\livretVerse#12 { Che trionfò così. }
\livretPers Giuditta
\livretVerse#12 { Venne l'Assiro, e intorno }
\livretVerse#12 { Con le falangi Perse }
\livretVerse#12 { Le valli ricoperse, }
\livretVerse#12 { I fiumi inaridì. }
\livretVerse#12 { Parve oscurato il giorno; }
\livretVerse#12 { Parve con quel crudele }
\livretVerse#12 { Al timido Israele }
\livretVerse#12 { Giunto l'estremo dì. }
\livretPers Coro
\livretVerse#12 { Lodi al gran Dio che oppresse, ecc. }
\livretPers Giuditta
\livretVerse#12 { Fiamme, catene e morte }
\livretVerse#12 { Ne minacciò feroce: }
\livretVerse#12 { Alla terribil voce }
\livretVerse#12 { Betulia impallidì. }
\livretVerse#12 { Ma inaspettata sorte }
\livretVerse#12 { L'estinse in un momento, }
\livretVerse#12 { E come nebbia al vento }
\livretVerse#12 { Tanto furor sparì. }
\livretPers Coro
\livretVerse#12 { Lodi al gran Dio che oppresse, ecc. }
\livretPers Giuditta
\livretVerse#12 { Dispersi, abbandonati }
\livretVerse#12 { I barbari fuggiro: }
\livretVerse#12 { Si spaventò l'Assiro, }
\livretVerse#12 { Il Medo inorridì. }
\livretVerse#12 { Ne fur giganti usati }
\livretVerse#12 { Ad assalir le stelle: }
\livretVerse#12 { Fu donna sola e imbelle }
\livretVerse#12 { Quella che gli atterrì. }
\livretPers Coro
\livretVerse#12 { Lodi al gran Dio che oppresse, ecc. }
\livretPers Tutti
\livretVerse#12 { Solo di tante squadre }
\livretVerse#12 { Veggasi il duce estinto, }
\livretVerse#12 { Sciolta è Betulia, ogni nemico è vinto. }
\livretVerse#12 { Alma, i nemici rei }
\livretVerse#12 { Che t'insidian la luce }
\livretVerse#12 { I vizi son: ma la superbia è il duce. }
\livretVerse#12 { Spegnila; e spento in lei }
\livretVerse#12 { Tutto il seguace stuolo, }
\livretVerse#12 { Mieterai mille palme a un colpo solo. }
}
