\livretAct PARTE SECONDA
\livretDescAtt\wordwrap-center {
  Ozìa ed Achior.
}
\livretRef#'BArecitativo
\livretPiece RECITATIVO
\livretPers Achior
%# Troppo mal corrisponde (Ozia, perdona)
%# A' tuoi dolci costumi
%# Tal disprezzo ostentar de' nostri numi.
%# Io così, tu lo sai,
%# Del tuo Dio non parlai.
\livretPers Ozia
%# Principe, è zelo
%# Quel che chiami rozzezza. In te conobbi
%# Chiari semi del vero; e m'affatico
%# A farli germogliar.
\livretPers Achior
%# Ma non ti basta
%# Ch'io veneri il tuo Dio?
\livretPers Ozia
%# No: confessarlo
%# Unico per essenza
%# Debbe ciascuno, ed adorarlo solo.
\livretPers Achior
%# Ma chi solo l'afferma?
\livretPers Ozia
%# Il venerato
%# Consenso d'ogni età; degli avi nostri
%# La fida autorità; l'istesso Dio
%# Di cui predicasti
%# I prodigi, il poter; chi di sua bocca
%# Lo palesò; che, quando
%# Se medesimo descrisse,
%# Disse; ”Io son quel che sono”; e tutto disse.
\livretPers Achior
%# L'autorità de' tuoi produci in vano
%# Con me nemico.
\livretPers Ozia
%# E ben, con te nemico
%# L'autorità non vaglia. Uom però sei;
%# La ragion ti convinca. A me rispondi
%# Con animo tranquillo, il ver si cerchi,
%# Non la vittoria.
\livretPers Achior
%# Io già t'ascolto.
\livretPers Ozia
%# Or dimmi:
%# Credi, Achior, che possa
%# Cosa alcuna prodursi
%# Senza la sua cagion?
\livretPers Achior
%# No.
\livretPers Ozia
%# D'una in altra
%# Passando col pensier, non ti riduci
%# Qualche cagione a confessar, da cui
%# Tutte dipendan l'altre?
\livretPers Achior
%# E ciò dimostra
%# Che v'è Dio, non che è solo.
%# Esser non ponno Queste prime cagioni i nostri dèi?
\livretPers Ozia
%# Quali dèi, caro prence? I tronchi, i marmi
%# Sculti da voi?
\livretPers Achior
%# Ma se que' marmi a' saggi
%# Fosser simboli sol delle immortali
%# Essenze creatrici, ancor diresti
%# Che i miei dèi non son dèi?
\livretPers Ozìa
%# Sì, perché molti.
\livretPers Achior
%# Io ripugnanza alcuna
%# Nel numero non veggo.
\livretPers Ozìa
%# Eccola. Un Dio
%# Concepir non poss'io,
%# Se perfetto non è.
\livretPers Achior
%# Giusto è il concetto
\livretPers Ozìa
%# Quando dissi perfetto,
%# Dissi infinito ancor.
\livretPers Achior
%# L'un l'altro include:
%# Non si dà chi l'ignori.
\livretPers Ozìa
%# Ma l'essenze che adori,
%# Se non più, son distinte; e se distinte,
%# Han confini fra lor. Dir dunque déi
%# Che ha confino l'infinito, o non son dèi.
\livretPers Achior
%# Da questi lacci, in cui
%# M'implicà il tuo parlar, cedasi al vero.
%# Disciogliermi non so: ma non per questo
%# Persuaso son io. D'arte ti cedo,
%# Non di ragione. E abbandonar non voglio
%# Gli dèi che adoro e vedo,
%# Per un dio che non posso
%# Né pure immaginar.
\livretPers Ozia
%# S'egli capisse
%# Nel nostro immaginar, Dio non sarebbe.
%# Chi potrà figurarlo? Egli di parti,
%# Come il corpo, non consta; egli in affetti,
%# Come l'anime nostre,
%# Non è distinto; ei non soggiace a forma,
%# Come tutto il creato; e se gli assegni
%# Parti, affetti, figura, il circonscrivi,
%# Perfezion gli togli.
\livretPers Achior
%# E quando il chiami
%# Tu stesso e buono e grande,
%# Nol circonscrivi allor?
\livretPers Ozìa
%# No; buono il credo,
%# Ma senza qualità; grande, ma senza
%# Quantità, né misura; ognor presente,
%# Senza sito o confine; e se in tal guisa
%# Qual sia non spiego, almen di lui non formo
%# Un'idea che l'oltraggi.
\livretPers Achior
%# E dunque vano
%# Lo sperar di vederlo.
\livretPers Ozìa
%# Un dì potresti
%# Meglio fissarti in lui: ma puoi frattanto
%# Vederlo ovunque vuoi.
\livretPers Achior
%# Vederlo! E come,
%# Se immaginar nol so?
\livretPers Ozia
%# Come nel sole
%# A fissar le pupille in vano aspiri,
%# E pur sempre e per tutto il sol rimiri.

\livretRef#'BBaria
\livretPiece ARIA
\livretPers OZIA
%# Se Dio veder tu vuoi,
%# Guardalo in ogni oggetto;
%# Cercalo nel tuo petto,
%# Lo troverai con te
%# E se dov'ei dimora
%# Non intendesti ancor,
%# Confondimi, se puoi;
%# Dimmi, dov'ei non è.

\livretRef#'BCrecitativo
\livretPiece RECITATIVO
\livretPers Achior
%# Confuso io son; sento sedurmi, e pure
%# Ritorno a dubitar.
\livretPers Ozia
%# Quando il costume
%# Alla ragion contrasta,
%# Avvien così. Tal di negletta cetra
%# Musica man le abbandonate corde
%# Stenta a temprar, perché vibrate appena
%# Si rallentan di nuovo.
\livretDidasPPage (Entra Amital.)
\livretPers Amital
%# Ah dimmi, Ozia,
%# Che si fa, che si pensa? Io non intendo
%# Che voglia dir questo silenzio estremo
%# A cui passò Betulia
%# Dall'estremo tumulto. Il nostro stato
%# Punto non migliorò. Crescono i mali,
%# E sceman le querele. Ognun chiedea
%# Ieri aita e pietà: stupido ognuno
%# Oggi passa, e non parla. Ah parmi questo
%# Un presagio per noi troppo funesto!

\livretRef#'BDaria
\livretPiece ARIA
\livretPers Amital
%# Quel nocchier che in gran procella
%# Non s'affanna e non favella,
%# E vicino a naufragar
%# E vicino all'ore estreme
%# Quell'infermo che non geme
%# E ha cagion di sospirar.

\livretRef#'BEArecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Lungamente non dura
%# Eccessivo dolor. Ciascuno a' mali
%# O cede o s'accostuma. Il nostro stato
%# Non è però senza speranza.
\livretPers Amital
%# Intendo:
%# Tu in Giuditta confidi. Ah questa parmi
%# Troppo folle lusinga!
\livretPersDidas Coro (in lontano)
%# All'armi, all'armi!
\livretPers Ozia
%# Quai grida!
\livretPersDidas Cabri (entrando)
%# Accorri, Ozìa. Senti il tumulto
%# Che fra' nostri guerrieri
%# Là si destò presso alle porte?
\livretPers Ozia
%# E quale
%# N'è la cagion?
\livretPers Cabri
%# Chi sà?
\livretPers Amital
%# Miseri noi!
%# Saran giunti i nemici.
\livretPers Ozia
%# Corrasi ad osservar.
\livretDidasPPage (Entra Giuditta.)
\livretPers Giuditta
%# Fermate, amici.
\livretPers Ozia
%# Giuditta!
\livretPers Amital
%# Eterno Dio!
\livretPers Giuditta
%# Lodiam, compagni,
%# Lodiamo il Signor nostro. Ecco adempite
%# Le sue promesse: ei per mia man trionfa;
%# La nostra fede egli premiò.
\livretPers Ozia
%# Ma questo
%# Improvviso tumulto…
\livretPers Giuditta
%# Io lo destai;
%# Non vi turbi. A momenti
%# Ne udirete gli effetti.
\livretPers Amital
%# E se frattanto
%# Oloferne…
\livretPers Giuditta
%# Oloferne
%# Già svenato morì.
\livretPers Amital
%# Che dici mai!
\livretPers Achior
%# Chi ha svenato Oloferne?
\livretPers Giuditta
%# Io lo svenai
\livretPers Ozia
%# Tu stessa!
\livretPers Achior
%# E quando?
\livretPers Amital
%# E come?
\livretPers Giuditta
\livretRef#'BEBrecitativo
%# Udite. Appena
%# Da Betulia partii, che m'arrestaro
%# Le guardie ostili. Ad Oloferne innanzi
%# Son guidata da loro. Egli mi chiede
%# A che vengo e chi son. Parte io gli scopro,
%# Taccio parte del vero. Ei non intende,
%# E approva i detti miei. Pietoso, umano
%# (Ma straniera in quel volto
%# Mi parve la pietà) m'ode, m'accoglie,
%# M'applaude, mi consola. A lieta cena
%# Seco mi vuol. Già su le mense elette
%# Fumano i vasi d'or; già vuota il folle
%# Fra' cibi ad or ad or tazze frequenti
%# Di licor generoso: e a poco a poco
%# Comincia a vacillar. Molti ministri
%# Eran d'intorno a noi; ma ad uno ad uno
%# Tutti si dileguar. L'ultimo d'essi
%# Rimaneva, e il peggior. L'uscio costui
%# Chiuse partendo, e mi lasciò con lui.
\livretPers Amital
%# Fiero cimento!
\livretPers Giuditta
%# Ogni cimento è lieve
%# Ad inspirato cor. Scorza gran parte
%# Era ormai della notte. Il campo intorno
%# Nel sonno universal taceva oppresso.
%# Vinto Oloferne istesso
%# Dal vino, in cui s'immerse oltre il costume,
%# Steso dormia su le funeste piume.
%# Sorgo; e tacita allor colà m'appresso
%# Dove prono ei giaceva. Rivolta al Cielo
%# Più col cor che col labbro: “Ecco l'istante,”
%# Dissi, “o Dio d'Israel, che un colpo solo
%# Liberi il popol tuo. Tu 'l promettesti;
%# In te fidato io l'intrapresi; e spero
%# Assistenza da te.” Sciolgo, ciò detto,
%# Da' sostegni del letto
%# L'appeso acciar; lo snudo; il crin gli stringo
%# Con la sinistra man; l'altra sollevo
%# Quanto il braccio si stende; i voti a Dio
%# Rinnovo in sì gran passo,
%# E su l'empia cervice il colpo abbasso.
\livretPers Ozìa
%# Oh coraggio!
\livretPers Amital
%# Oh periglio!
\livretPers Giuditta
%# Apre il barbaro il ciglio; e incerto ancora
%# Fra 'l sonno e fra la morte, il ferro immerso
%# Sentesi nella gola. Alle difese
%# Sollevarsi procura; e gli el contende
%# L'imprigionato crin. Ricorre a' gridi;
%# Ma interrotte la voce
%# Trova le vie del labbro, e si disperde.
%# Replico il colpo: ecco l'orribil capo
%# Dagli omeri diviso.
%# Guizza il tronco reciso
%# Sul sanguigno terren; balzar mi sento
%# Il teschio semivivo
%# Sotte la man che il sostenea. Quel volto
%# A un tratto scolorir; mute parole
%# Quel labbro articolar; quei occhi intorno
%# Cercar del sole i rai,
%# Morire e minacciar vidi, e tremai.
\livretPers Amital
%# Tremo in udirlo anch'io.
\livretPers Giuditta
%# Respiro al fine; e del trionfo illustre
%# Rendo grazie all'Autore. Svelta dal letto
%# La superba cortina, il capo esangue
%# Sollecita ne involgo; alla mia fida
%# Ancella lo consegno,
%# Che non lungi attendea; del duce estinto
%# M'involo al padiglion; passo fra' suoi
%# Non vista o rispettata, e torno a voi.
\livretPers Ozìa
%# Oh prodigio!
\livretPers Cabri
%# Oh portento!
\livretPers Achior
%# Inerme e sola
%# Tanto pensar, tanto eseguir potesti!
%# E crederti degg'io?
\livretPers Giuditta
%# Credilo a questo
%# Ch'io scopro agli occhi tuoi, teschio reciso.
\livretPers Achior
%# Oh spavento! È Oloferne:io lo ravviso.
\livretPers Ozìa
%# Sostenetelo, o servi: il cor gli agghiaccia
%# L'improviso terror.
\livretPers Amital
%# Fugge quell'alma
%# Per non cedere al ver.
\livretPers Giuditta
%# Meglio di lui
%# Giudichiamo, Amital. Forse quel velo
%# Che gli oscurò la mente
%# A un tratto or si squarciò. Non fugge il vero,
%# Ma gli manca il costume
%# L'impeto a sostener di tanto lume.

\livretRef#'BFaria
\livretPiece ARIA
\livretPers Giuditta
%# Prigionier che fa ritorno
%# Dagli orrori al dì sereno,
%# Chiude i lumi a' rai del giorno,
%# E pur tanto il sospirò.
%# Ma così fra poco arriva
%# A soffrir la chiara luce:
%# Ché l'avviva e lo conduce
%# Lo splendor che l'abbagliò.

\livretRef#'BGrecitativo
\livretPiece RECITATIVO
\livretPers Achior
%# Giuditta, Ozìa, popoli, amici: io cedo,
%# Vinto son io. Prende un novello aspetto
%# Ogni cosa per me. Da quel che fui
%# Non so chi mi trasforma: in me l'antico
%# Achior più non trovo. Altri pensieri,
%# Sento altre voglie in me. Tutto son pieno,
%# Tutto, del vostro Dio. Grande, infinito,
%# Unico lo confesso. I falsi numi
%# Odio, detesto, e i vergognosi incensi
%# Che lor credulo offersi. Altri non amo,
%# Non conosco altro Dio che il Dio d'Abramo.

\livretRef#'BHaria
\livretPiece ARIA
\livretPers Achior
%# Te solo adoro,
%# Mente infinita,
%# Fonte di vita,
%# Di verità
%# In cui si muove,
%# Da cui dipende
%# Quanto comprende
%# L'eternità.

\livretRef#'BIrecitativo
\livretPiece RECITATIVO
\livretPers Ozìa
%# Di tua vittoria un glorioso effetto
%# Vedi, o Giuditta.
\livretPers Amital
%# E non il solo. Anch'io
%# Peccai; mi pento. Il mio timore offese
%# La divina pietà. Fra' mali miei,
%# Mio Dio, non rammentai che puoi, chi sei.

\livretRef#'BJaria
\livretPiece ARIA
\livretPers Amital
%# Con troppa rea viltà
%# Quest'alma ti oltraggiò,
%# Allor che disperò
%# Del tuo soccorso.
%# Pietà, Signor, pietà;
%# Giacché il pentito cor
%# Misura il proprio error
%# Col suo rimorso.

\livretRef#'BKrecitativo
\livretPiece RECITATIVO
\livretPers Cabri
%# Quanta cura hai di noi, Bontà Divina!
\livretDidasPPage (Entra Carmi.)
\livretPers Carmi
%# Furo, o santa eroina,
%# Veri i presagi tuoi: gli Assiri oppresse
%# Eccidio universal.
\livretPers Ozia
%# Forse è lusinga
%# Del tuo desio.
\livretPers Carmi
%# No; del felice evento
%# Parte vid'io; da' trattenuti il resto
%# Fuggitivi raccolsi. In su le mura,
%# Come impose Giuditta al suo ritorno,
%# Destai di grida e d'armi
%# Strepitoso tumulto.
\livretPers Amital
%# E qui s'intese
\livretPers Carmi
%# Temon le guardie ostili
%# D'un assalto notturno, ed Oloferne
%# Corrono ad avvertirne. Il tronco informe
%# Trovan colà nel proprio sangue involto:
%# Tornan gridando indietro. II caso atroce
%# Spargesi fra le schiere, intimorite
%# Già da' nostri tumulti; ecco ciascuno
%# Precipita alla fuga, e nella fuga
%# L'un l'altro urta, impedisce. Inciampa e cade
%# Sopra il caduto il fuggitivo: immerge
%# Stolido in sen l'involontario acciaro
%# Al compagno il compagno; opprime oppresso,
%# Nel sollevar l'amico, il fide amico.
%# Orribilmente il campo
%# Tutto rimbomba intorno. Escon dal chiuso
%# Spaventati i destrieri, e vanno anch'essi
%# Calpestando per l'ombre
%# Gli estinti, i semivivi. A' lor nitriti
%# Miste degli empii e le bestemmie e i voti
%# Dissipa il vento. Apre alla morte il caso
%# Cento insolite vie. Del pari ognuno
%# Teme, fugge, perisce; e ognun del pari
%# Ignora in quell'orrore
%# Di che teme, ove fugge, e perché muore.
\livretPers Ozia
%# Oh Dio! Sogno o son desto?
\livretPers Carmi
%# Odi, o signor, quel mormorio funesto?

\livretRef#'BLaria
\livretPiece ARIA
\livretPers Carmi
%# Quei moti che senti
%# Per l'orrida notte,
%# Son queruli accenti,
%# Son grida interrotte
%# Che desta lontano
%# L'insano terror.
%# Per vincere, a noi
%# Non restan nemici;
%# Del ferro gli uffici
%# Compisce il timor.

\livretRef#'BMrecitativo
\livretPiece RECITATIVO
\livretPers Ozia
%# Seguansi, o Carmi, i fuggitivi; e sia
%# Il più di nostre prede
%# Premio a Giuditta.
\livretPers Amital
%# O generosa donna,
%# Te sopra ogni altra Iddio
%# Favorì, benedisse.
\livretPers Cabri
%# In ogni etade
%# Del tu valor si parlerà.
\livretPers Achior
%# Tu sei
%# La gioia d'Israele,
%# L'onor del popol tuo…
\livretPers Giuditta
%# Basta. Dovute
%# Non son tai lodi a me. Dio fu la mente
%# Che il gran colpo guidò; la mano io fui:
%# I cantici festivi offransi a lui.

\livretRef#'BNcoro
\livretPiece CORO
\livretPers Coro
%# Lodi al gran Dio che oppresse
%# Gli empi nemici suoi,
%# Che combatté per noi,
%# Che trionfò così.
\livretPers Giuditta
%# Venne l'Assiro, e intorno
%# Con le falangi Perse
%# Le valli ricoperse,
%# I fiumi inaridì.
%# Parve oscurato il giorno;
%# Parve con quel crudele
%# Al timido Israele
%# Giunto l'estremo dì.
\livretPers Coro
%# Lodi al gran Dio che oppresse, ecc.
\livretPers Giuditta
%# Fiamme, catene e morte
%# Ne minacciò feroce:
%# Alla terribil voce
%# Betulia impallidì.
%# Ma inaspettata sorte
%# L'estinse in un momento,
%# E come nebbia al vento
%# Tanto furor sparì.
\livretPers Coro
%# Lodi al gran Dio che oppresse, ecc.
\livretPers Giuditta
%# Dispersi, abbandonati
%# I barbari fuggiro:
%# Si spaventò l'Assiro,
%# Il Medo inorridì.
%# Ne fur giganti usati
%# Ad assalir le stelle:
%# Fu donna sola e imbelle
%# Quella che gli atterrì.
\livretPers Coro
%# Lodi al gran Dio che oppresse, ecc.
\livretPers Tutti
%# Solo di tante squadre
%# Veggasi il duce estinto,
%# Sciolta è Betulia, ogni nemico è vinto.
%# Alma, i nemici rei
%# Che t'insidian la luce
%# I vizi son: ma la superbia è il duce.
%# Spegnila; e spento in lei
%# Tutto il seguace stuolo,
%# Mieterai mille palme a un colpo solo.
