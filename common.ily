\header {
  copyrightYear = "2018"
  composer = "Wolfgang Amadeus Mozart"
  opus = "K. 118/74c"
  poet = "Pietro Metastasio"
  date = "1771"
}

%% LilyPond options:
%%  urtext  if true, then print urtext score
%%  part    if a symbol, then print the separate part score
#(ly:set-option 'original-layout (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'apply-vertical-tweaks
                (and (not (eqv? #t (ly:get-option 'urtext)))
                     (not (symbol? (ly:get-option 'part)))))
#(ly:set-option 'use-ancient-clef (eqv? #t (ly:get-option 'urtext)))
#(ly:set-option 'print-footnotes (not (symbol? (ly:get-option 'part))))

%% Use rehearsal numbers
#(ly:set-option 'use-rehearsal-numbers #t)

%% Staff size
#(set-global-staff-size
  (cond ((eqv? #t (ly:get-option 'urtext)) 14)
        ((not (symbol? (ly:get-option 'part))) 16)
        (else 18)))

%% Line/page breaking algorithm
%%  optimal   for lead sheets
%%  page-turn for instruments and vocal parts
\paper {
  #(define page-breaking
     (cond ((eqv? (ly:get-option 'part) 'reduction) ly:optimal-breaking)
           ((symbol? (ly:get-option 'part)) ly:page-turn-breaking)
           (else ly:optimal-breaking)))
}

\layout {
  reference-incipit-width = #(* 1/2 mm)
}

\language "italiano"
\include "nenuvar-lib.ily"
\setPath "ly"
\opusPartSpecs
#`((trombe "Trombe" () (#:notes "trombe"))
   (corni "Corni" () (#:notes "corni"))
   
   (flauti "Flauti" () (#:notes "flauti"))
   (oboi "Oboi" () (#:notes "oboi"))
   (fagotti "Fagotti" ((basso "Basso")) (#:clef "bass" #:notes "fagotti"))

   (violino1 "Violino I" ()  (#:notes "violini" #:tag-notes violino1))
   (violino2 "Violino II" () (#:notes "violini" #:tag-notes violino2))
   (viola "Viola" () (#:notes "viola" #:clef "alto"))
   (basso "Basso" () (#:notes "basso" #:clef "bass")))

\opusTitle "Betulia Liberata"

\paper {
  % de la place en bas de page pour les annotations
  last-bottom-spacing.padding = #3
}

\layout {
  indent = #(if (symbol? (ly:get-option 'part))
                smallindent
                largeindent)
  short-indent = #(if (symbol? (ly:get-option 'part))
                      0
                      (* 8 mm))
  ragged-last = ##f
}

\layout {
  \context {
    \Voice
    \override Script.avoid-slur = #'inside
    \override DynamicTextSpanner.style = #'none
  }
  \context {
    \DrumVoice
    \override DynamicTextSpanner.style = #'none
  }
}

\header {
  maintainer = \markup {
    Nicolas Sceaux,
    \with-url #"http://www.lestalenslyriques.com" \line {
      Les Talens Lyriques – Christophe Rousset.
    }
  }
  license = "Creative Commons Attribution-ShareAlike 4.0 License"
  tagline = \markup\sans\abs-fontsize #8
  \override #'(baseline-skip . 0) \vcenter {
    \right-column\bold {
      \with-url #"http://nicolas.sceaux.free.fr" {
        \concat { Éditions \tagline-vspacer }
        \concat { Nicolas \tagline-vspacer }
        \concat { Sceaux \tagline-vspacer }
      }
    }
    \abs-fontsize #9 \with-color #(x11-color 'grey40) \raise #-0.7 \musicglyph #"clefs.petrucci.f"
    \column {
      \line { \tagline-vspacer \copyright }
      \smaller\line {
        \tagline-vspacer
        Sheet music from
        \with-url #"http://nicolas.sceaux.free.fr"
        http://nicolas.sceaux.free.fr
        typeset using \with-url #"http://lilypond.org" LilyPond
        on \concat { \today . }
      }
      \smaller\line {
        \tagline-vspacer \license
        — free to download, distribute, modify and perform.
      }
    }
  }

}

corniInstr = \with {
  instrumentName = "Corni"
  shortInstrumentName = "Cor."
}
trombeInstr = \with {
  instrumentName = "Trombe"
  shortInstrumentName = "Tr."
}
flautiInstr = \with {
  instrumentName = "Flauti"
  shortInstrumentName = "Fl."
}
oboiInstr = \with {
  instrumentName = "Oboi"
  shortInstrumentName = "Ob."
}
fagottiInstr = \with {
  instrumentName = "Fagotti"
  shortInstrumentName = "Fg."
}

violiniInstr = \with {
  instrumentName = "Violini"
  shortInstrumentName = "Vn."
}
violaInstr = \with {
  instrumentName = "Viola"
  shortInstrumentName = "Vla"
}
bassoInstr = \with {
  instrumentName = "Basso"
  shortInstrumentName = "B."
}
bassiInstr = \with {
  instrumentName = "Bassi"
  shortInstrumentName = "B."
}

trill = #(make-articulation "trill")

trillNatural = \markup { \musicglyph #"scripts.trill" \smaller \raise #0.75 \natural }

#(define-markup-command (tacet layout props) ()
   (interpret-markup layout props #{ \markup {
  \hspace#2 \pad-to-box #'(0 . 0) #'(-2 . 1) \small TACET }
                                             #}))

%% Cues
cueTreble =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuringWithClef $cue-name #CENTER "treble" $music #})

cue =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuring $cue-name #CENTER $music #})

cueTransp =
#(define-music-function (parser location cue-name pitch music)
     (string? ly:pitch? ly:music?)
   #{ \transposedCueDuring $cue-name #CENTER $pitch $music #})


quoteViolinoI =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino1 { \includeNotes "violini" }
#}))

quoteViolinoII =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'violino2 { \includeNotes "violini" }
#}))

quoteBasso =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'basso { \includeNotes "basso" }
#}))

%% Livret
#(define-markup-command (livretPiece layout props text) (markup?)
   #:properties ((line-width)
                 (gap 9)
                 (word-space 0))
   (let ((overrides `(line-width . ,(- line-width gap word-space 2))))
     (interpret-markup
      layout props
      #{\markup { \hspace#gap \override#overrides \fontsize#0 \bold\pad-above#2 $text } #})))

%% Cues
cue =
#(define-music-function (parser location cue-name music) (string? ly:music?)
   #{ \cueDuring $cue-name #CENTER $music #})

quoteBasso =
#(define-void-function (cue-name) (string?)
   (add-quotable cue-name #{
  \unfoldRepeats \keepWithTag #'basso { \includeNotes "basso" }
#}))
